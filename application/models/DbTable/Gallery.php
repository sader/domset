<?php

class Model_DbTable_Gallery extends Zend_Db_Table_Abstract {
    const TYPE_VISIBLE = 1;
    const TYPE_HIDDEN = 0;

    protected $_name = 'gallery';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Gallery 
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Gallery
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function saveImages($aImages = array()) {
        foreach ($aImages as $aImage) {
            $this->insert($aImage);
        }
    }

    public function cleanGalleryForOffer($iId) {
        $this->delete(array('id_offer = ?' => $iId));
    }

    public function getGalleryForOffer($iId) {

        return $this->select()
                        ->where('id_offer = ?', $iId)
                        ->order('type ASC')
                        ->order('ord ASC')
                        ->query()
                        ->fetchAll(Zend_db::FETCH_ASSOC);
    }

    public function getVisibleGalleryForOffer($iId, $iLimit = 20) {

        return $this->select()
                        ->where('id_offer = ?', $iId)
                        ->where('type = ?', self::TYPE_VISIBLE)
                        ->order('ord ASC')
                        ->limit($iLimit)
                        ->query()
                        ->fetchAll(Zend_db::FETCH_ASSOC);
    }

}