<?php

class Model_DbTable_AgreementFiles extends Zend_Db_Table_Abstract {
    

    protected $_name = 'agreement_files';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_AgreementFiles
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_AgreementFiles
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

   public function addFilesToAgrement($aTranslatedData)
   {
       foreach($aTranslatedData as $aData)
       {
           $this->insert($aData); 
       }
   }
    
   public function removeFilesFromAgreement($iOfferId)
   {
       $this->delete(array('id_offer = ?' => $iOfferId)); 
       
   }
   
    

}

