<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_DbTable_Wanted_Land extends App_Db_Table {

    protected $_name = 'wanted_land';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Wanted_Land
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Wanted_Land
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function add($aData) {
        $aInsertData = array();
        $aInsertData['id_offer'] = $aData['id_offer'];
        $aInsertData['land_type'] = implode(',', $aData['land_type']);
        $aInsertData['shape'] = implode(',', $aData['shape']);
        $aInsertData['width_from'] = $aData['width_from'];
        $aInsertData['width_to'] = $aData['width_to'];
        $aInsertData['height_from'] = $aData['height_from'];
        $aInsertData['height_to'] = $aData['height_to'];
        $aInsertData['services'] = implode(',', $aData['services']);
        $aInsertData['fence'] = implode(',', $aData['fence']);
        $aInsertData['access'] = implode(',', $aData['access']);
        $aInsertData['additional'] = implode(',', $aData['additional']);

        $this->insert($aInsertData);
    }

    public function edit($aData, $iId) {
        $aUpdateData = array();
        $aInsertData['land_type'] = implode(',', $aData['land_type']);
        $aInsertData['shape'] = implode(',', $aData['shape']);
        $aInsertData['width_from'] = $aData['width_from'];
        $aInsertData['width_to'] = $aData['width_to'];
        $aInsertData['height_from'] = $aData['height_from'];
        $aInsertData['height_to'] = $aData['height_to'];
        $aInsertData['services'] = implode(',', $aData['services']);
        $aInsertData['fence'] = implode(',', $aData['fence']);
        $aInsertData['access'] = implode(',', $aData['access']);
        $aInsertData['additional'] = implode(',', $aData['additional']);

        $this->update($aUpdateData, array('id_offer = ?' => $iId));
    }

    public function remove($iId) {

        $this->delete(array('id_offer = ?' => $iId));
    }

    public function get($iOfferId) {
        return $this->select()
                        ->where('id_offer = ?', $iOfferId)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }

    public function getQuery($aExtraFilters) {


        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=p.id_offer')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), array('o.id',
                    'created' => 'DATE(o.created)',
                    'o.district',
                    'o.city',
                    'o.price',
                    'o.price_surface',
                    'p.land_type',
                    'o.surface'))
                ->columns('(' . $oSubSelect . ') as cover')
                ->join(array('o' => 'offer'), 'p.id_offer=o.id', null)
                ->where('o.status = ?' , Model_DbTable_Offer::STATUS_OPEN);


        if (isset($aExtraFilters['province']) && !empty($aExtraFilters['province'])) {
            $oSelect->where("province IN (?)", $aExtraFilters['province']);
        }


        if (isset($aExtraFilters['access']) && !empty($aExtraFilters['access'])) {
            $oSelect->where("access IN (?)", $aExtraFilters['access']);
        }

        if (isset($aExtraFilters['services']) && !empty($aExtraFilters['services'])) {

            foreach ($aExtraFilters['services'] as $iServices) {
                $oSelect->where('FIND_IN_SET(? , p.services) > 0', $iServices);
            }
        }


        if (isset($aExtraFilters['categories']) && !empty($aExtraFilters['categories'])) {

            $oSubQuery = $this->select()
                    ->setIntegrityCheck(false)
                    ->from('category_offer', array('id_offer'))
                    ->where('id_category IN(?)', $aExtraFilters['categories']);
            $oSelect->join(array('categories' => $oSubQuery), 'categories.id_offer=o.id', null);
        }

        return $oSelect;
    }

    public function getWantedByOfferData($aData) {


      
        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), null)
                ->join(array('w' => 'wanted'), 'w.id=p.id_offer', array('id_wanted' => 'id',
                            'id_type',
                            'price_from',
                            'price_to',
                            'surface_from',
                            'surface_to',
                            'info'))
                        ->join(array('c' => 'client'), 'c.id=w.id_client',
                                array('client_id' => 'c.id', 'client' => 'CONCAT_WS(" " , phone,  "\n" ,surname, name)'))
                ->order('w.created DESC');
                


        if (isset($aData['city']) && !empty($aData['city'])) {
            $oSelect->where('(w.city = "" OR ISNULL(w.city) OR "' . $aData['city'] . '" LIKE CONCAT("%" , w.city , "%") )');
        }

        if (isset($aData['district']) && !empty($aData['district'])) {
            $oSelect->where('(w.district = "" OR ISNULL(w.district) OR "' . $aData['district'] . '" LIKE CONCAT("%" , w.district , "%") )');
        }


       if (isset($aData['section']) && !empty($aData['section'])) {
            $oSelect->where('(w.section = "" OR ISNULL(w.section) OR w.section LIKE ("%'.$aData['section'].'%") )');
        }

        if (isset($aData['street']) && !empty($aData['street'])) {
            $oSelect->where('(w.street = "" OR ISNULL(w.street) OR "' . $aData['street'] . '" LIKE CONCAT("%" , w.street , "%") )');
        }

        if (isset($aData['price']) && !empty($aData['price'])) {
            $oSelect->where('w.price_from = "0.00" OR ISNULL(w.price_from) OR price_from <= "' . $aData['price'] . '"');
            $oSelect->where('w.price_to = "0.00" OR ISNULL(w.price_to) OR price_to >= "' . $aData['price'] . '"');
        }

        if (isset($aData['surface']) && !empty($aData['surface'])) {
            $oSelect->where('w.surface_from = "0.00" OR ISNULL(w.surface_from) OR surface_from <= "' . $aData['surface'] . '"');
            $oSelect->where('w.surface_to = "0.00" OR ISNULL(w.surface_to) OR surface_to >= "' . $aData['surface'] . '"');
        }


        if (isset($aData['width']) && !empty($aData['width'])) {
            $oSelect->where('p.width_from = "0" OR ISNULL(p.width_from) OR width_from <= "' . $aData['width'] . '"');
            $oSelect->where('p.width_to = "0" OR ISNULL(p.width_to) OR width_to >= "' . $aData['width'] . '"');
        }

        if (isset($aData['height']) && !empty($aData['height'])) {
            $oSelect->where('p.height_from = "0" OR ISNULL(p.height_from) OR height_from <= "' . $aData['height'] . '"');
            $oSelect->where('p.height_to = "0" OR ISNULL(p.height_to) OR height_to >= "' . $aData['height'] . '"');
        }

        if (isset($aData['land_type']) && !empty($aData['land_type'])) {
            $oSelect->where('ISNULL(land_type) OR FIND_IN_SET(? , p.land_type) > 0', $aData['land_type']);
        }
        
        if (isset($aData['shape']) && !empty($aData['shape'])) {
            $oSelect->where('ISNULL(shape) OR FIND_IN_SET(? , p.shape) > 0', $aData['shape']);
        }
        
        if (isset($aData['fence']) && !empty($aData['fence'])) {
            $oSelect->where('ISNULL(fence) OR FIND_IN_SET(? , p.fence) > 0', $aData['fence']);
        }
        
        if (isset($aData['access']) && !empty($aData['access'])) {
            $oSelect->where('ISNULL(access) OR FIND_IN_SET(? , p.access) > 0', $aData['access']);
        }






        return $oSelect;
    }

}