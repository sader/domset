<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_DbTable_Wanted_House extends App_Db_Table {

    protected $_name = 'wanted_house';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Wanted_House
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Wanted_House
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function add($aData) {
        $aInsertData = array(
            'id_offer' => $aData['id_offer'],
            'house_surface_from' => $aData['house_surface_from'],
            'house_surface_to' => $aData['house_surface_to'],
            'terrain_surface_from' => $aData['terrain_surface_from'],
            'terrain_surface_to' => $aData['terrain_surface_to'],
            'building_type' => implode(',', $aData['building_type']),
            'rooms_from' => $aData['rooms_from'],
            'rooms_to' => $aData['rooms_to'],
            'floors_from' => $aData['floors_from'],
            'floors_to' => $aData['floors_to'],
            'attic' => implode(',', $aData['attic']),
            'basement' => implode(',', $aData['basement']),
            'year_from' => $aData['year_from'],
            'year_to' => $aData['year_to'],
            'building_material' => implode(',', $aData['building_material']),
            'quality' => implode(',', $aData['quality']),
            'windows' => implode(',', $aData['windows']),
            'roof' => implode(',', $aData['roof']),
            'roof_type' => implode(',', $aData['roof_type']),
            'heat' => implode(',', $aData['heat']),
            'sewerage' => implode(',', $aData['sewerage']),
            'fence' => implode(',', $aData['fence']),
            'garage' => implode(',', $aData['garage']),
            'access' => implode(',', $aData['access']),
            'additional' => implode(',', $aData['additional'])
        );

        $this->insert($aInsertData);
    }

    public function edit($aData, $iId) {

        $aUpdateData = array(
            'house_surface_from' => $aData['house_surface_from'],
            'house_surface_to' => $aData['house_surface_to'],
            'terrain_surface_from' => $aData['terrain_surface_from'],
            'terrain_surface_to' => $aData['terrain_surface_to'],
            'building_type' => implode(',', $aData['building_type']),
            'rooms_from' => $aData['rooms_from'],
            'rooms_to' => $aData['rooms_to'],
            'floors_from' => $aData['floors_from'],
            'floors_to' => $aData['floors_to'],
            'attic' => implode(',', $aData['attic']),
            'basement' => implode(',', $aData['basement']),
            'year_from' => $aData['year_from'],
            'year_to' => $aData['year_to'],
            'building_material' => implode(',', $aData['building_material']),
            'quality' => implode(',', $aData['quality']),
            'windows' => implode(',', $aData['windows']),
            'roof' => implode(',', $aData['roof']),
            'roof_type' => implode(',', $aData['roof_type']),
            'heat' => implode(',', $aData['heat']),
            'sewerage' => implode(',', $aData['sewerage']),
            'fence' => implode(',', $aData['fence']),
            'garage' => implode(',', $aData['garage']),
            'access' => implode(',', $aData['access']),
            'additional' => implode(',', $aData['additional']));

        $this->update($aUpdateData, array('id_offer = ?' => $iId));
    }

    public function remove($iId) {
        $this->delete(array('id_offer = ?' => $iId));
    }

    public function get($iOfferId) {
        return $this->select()
                        ->where('id_offer = ?', $iOfferId)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }

    public function getOffersQuery($aExtraFilters) {


        $oSubSelect = $this->select()->to(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=p.id_offer')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->to(array('p' => $this->_name), array('o.id',
                    'created' => 'DATE(o.created)',
                    'location' => 'CONCAT(o.section, " " , o.street)',
                    'o.price',
                    'o.price_surface',
                    'p.building_type',
                    'o.surface',
                    'p.terrain_surface',
                    'p.quality'))
                ->columns('(' . $oSubSelect . ') as cover')
                ->join(array('o' => 'offer'), 'p.id_offer=o.id', null)
                ->where('o.status = ?' , Model_DbTable_Offer::STATUS_OPEN);


        if (isset($aExtraFilters['province']) && !empty($aExtraFilters['province'])) {
            $oSelect->where("province IN (?)", $aExtraFilters['province']);
        }

        if (isset($aExtraFilters['district']) && !empty($aExtraFilters['district'])) {
            $oSelect->where("district LIKE ?", '%' . $aExtraFilters['district'] . '%');
        }

        if (isset($aExtraFilters['section']) && !empty($aExtraFilters['section'])) {
            $oSelect->where("section LIKE ?", '%' . $aExtraFilters['section'] . '%');
        }

        if (isset($aExtraFilters['quality']) && !empty($aExtraFilters['quality'])) {
            $oSelect->where("quality IN (?)", $aExtraFilters['quality']);
        }

        if (isset($aExtraFilters['rooms_to']) && !empty($aExtraFilters['rooms_to'])) {
            $oSelect->where("rooms >= (?)", $aExtraFilters['rooms_to']);
        }


        if (isset($aExtraFilters['rooms_to']) && !empty($aExtraFilters['rooms_to'])) {
            $oSelect->where("rooms <= (?)", $aExtraFilters['rooms_to']);
        }


        if (isset($aExtraFilters['year_to']) && !empty($aExtraFilters['year_to'])) {
            $oSelect->where("year >= (?)", $aExtraFilters['year_to']);
        }


        if (isset($aExtraFilters['year_to']) && !empty($aExtraFilters['year_to'])) {
            $oSelect->where("year <= (?)", $aExtraFilters['year_to']);
        }

        if (isset($aExtraFilters['bulding_type']) && !empty($aExtraFilters['bulding_type'])) {
            $oSelect->where("bulding_type IN (?)", $aExtraFilters['bulding_type']);
        }

        if (isset($aExtraFilters['bulding_material']) && !empty($aExtraFilters['bulding_material'])) {
            $oSelect->where("bulding_material IN (?)", $aExtraFilters['bulding_material']);
        }

        if (isset($aExtraFilters['sewerage']) && !empty($aExtraFilters['sewerage'])) {
            $oSelect->where("sewerage IN (?)", $aExtraFilters['sewerage']);
        }

        if (isset($aExtraFilters['heat']) && !empty($aExtraFilters['heat'])) {
            $oSelect->where("heat IN (?)", $aExtraFilters['heat']);
        }

        if (isset($aExtraFilters['basement']) && !empty($aExtraFilters['basement'])) {
            $oSelect->where("basement IN (?)", $aExtraFilters['basement']);
        }

        if (isset($aExtraFilters['categories']) && !empty($aExtraFilters['categories'])) {

            $oSubQuery = $this->select()
                    ->setIntegrityCheck(false)
                    ->to('category_offer', array('id_offer'))
                    ->where('id_category IN(?)', $aExtraFilters['categories']);
            $oSelect->join(array('categories' => $oSubQuery), 'categories.id_offer=o.id', null);
        }

        return $oSelect;
    }

    public function getWantedByOfferData($aData) {


        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), null)
                ->join(array('w' => 'wanted'), 'w.id=p.id_offer', array('id_wanted' => 'id',
                            'id_type',
                            'price_from',
                            'price_to',
                            'surface_from',
                            'surface_to',
                            'info'))
                        ->join(array('c' => 'client'), 'c.id=w.id_client',
                                array('client_id' => 'c.id', 'client' => 'CONCAT_WS(" " , phone,  "\n" ,surname, name)'))
                ->order('w.created DESC');
                


        if (isset($aData['city']) && !empty($aData['city'])) {
            $oSelect->where('(w.city = "" OR ISNULL(w.city) OR "' . $aData['city'] . '" LIKE CONCAT("%" , w.city , "%") )');
        }

        if (isset($aData['district']) && !empty($aData['district'])) {
            $oSelect->where('(w.district = "" OR ISNULL(w.district) OR "' . $aData['district'] . '" LIKE CONCAT("%" , w.district , "%") )');
        }


        if (isset($aData['section']) && !empty($aData['section'])) {
            $oSelect->where('(w.section = "" OR ISNULL(w.section) OR w.section LIKE ("%'.$aData['section'].'%") )');
        }

        if (isset($aData['street']) && !empty($aData['street'])) {
            $oSelect->where('(w.street = "" OR ISNULL(w.street) OR "' . $aData['street'] . '" LIKE CONCAT("%" , w.street , "%") )');
        }

        if (isset($aData['price']) && !empty($aData['price'])) {
            $oSelect->where('w.price_from = "0.00" OR ISNULL(w.price_from) OR price_from <= "' . $aData['price'] . '"');
            $oSelect->where('w.price_to = "0.00" OR ISNULL(w.price_to) OR price_to >= "' . $aData['price'] . '"');
        }

        if (isset($aData['surface']) && !empty($aData['surface'])) {
            $oSelect->where('w.surface_from = "0.00" OR ISNULL(w.surface_from) OR surface_from <= "' . $aData['surface'] . '"');
            $oSelect->where('w.surface_to = "0.00" OR ISNULL(w.surface_to) OR surface_to >= "' . $aData['surface'] . '"');
        }


        if (isset($aData['house_surface']) && !empty($aData['house_surface'])) {
            $oSelect->where('house_surface_from = "0.00" OR ISNULL(house_surface_from) OR house_surface_from <= "' . $aData['house_surface'] . '"');
            $oSelect->where('house_surface_to = "0.00" OR ISNULL(house_surface_to) OR house_surface_to >= "' . $aData['house_surface'] . '"');
        }

        if (isset($aData['terrain_surface']) && !empty($aData['terrain_surface'])) {
            $oSelect->where('terrain_surface_from = "0.00" OR ISNULL(terrain_surface_from) OR terrain_surface_from <= "' . $aData['terrain_surface'] . '"');
            $oSelect->where('terrain_surface_to = "0.00" OR ISNULL(terrain_surface_to) OR terrain_surface_to >= "' . $aData['terrain_surface'] . '"');
        }

        if (isset($aData['attic']) && !empty($aData['attic'])) {
            $oSelect->where('ISNULL(attic) OR FIND_IN_SET(? , p.attic) > 0', $aData['attic']);
        }

        if (isset($aData['basement']) && !empty($aData['basement'])) {
            $oSelect->where('ISNULL(basement) OR FIND_IN_SET(? , p.basement) > 0', $aData['basement']);
        }

        if (isset($aData['rooms']) && !empty($aData['rooms'])) {
            $oSelect->where('p.rooms_from = "0" OR ISNULL(p.rooms_from) OR rooms_from <= "' . $aData['rooms'] . '"');
            $oSelect->where('p.rooms_to = "0" OR ISNULL(p.rooms_to) OR rooms_to >= "' . $aData['rooms'] . '"');
        }

        if (isset($aData['floor']) && !empty($aData['floor'])  && $aData['floor'] != '0') {
            $oSelect->where('p.floor_from = "0" OR ISNULL(p.floor_from) OR floor_from <= "' . $aData['floor'] . '"');
            $oSelect->where('p.floor_to = "0" OR ISNULL(p.floor_to) OR floor_to >= "' . $aData['floor'] . '"');
        }

        if (isset($aData['year']) && !empty($aData['year'])) {
            $oSelect->where('p.year_from = "0000" OR ISNULL(p.year_from) OR year_from <= "' . $aData['year'] . '"');
            $oSelect->where('p.year_to = "0000" OR ISNULL(p.year_to) OR year_to >= "' . $aData['year'] . '"');
        }
        if (isset($aData['building_type']) && !empty($aData['building_type'])) {
            $oSelect->where('ISNULL(building_type) OR FIND_IN_SET(? , p.building_type) > 0', $aData['building_type']);
        }

        if (isset($aData['quality']) && !empty($aData['quality'])) {
            $oSelect->where('ISNULL(quality) OR FIND_IN_SET(? , p.quality) > 0', $aData['quality']);
        }

        if (isset($aData['windows']) && !empty($aData['windows'])) {
            $oSelect->where('ISNULL(windows) OR FIND_IN_SET(? , p.windows) > 0', $aData['windows']);
        }

        if (isset($aData['roof']) && !empty($aData['roof'])) {
            $oSelect->where('ISNULL(roof) OR FIND_IN_SET(? , p.roof) > 0', $aData['roof']);
        }

        if (isset($aData['roof_type']) && !empty($aData['roof_type'])) {
            $oSelect->where('ISNULL(roof_type) OR FIND_IN_SET(? , p.roof_type) > 0', $aData['roof_type']);
        }

        if (isset($aData['heat']) && !empty($aData['heat'])) {
            $oSelect->where('ISNULL(heat) OR FIND_IN_SET(? , p.heat) > 0', $aData['heat']);
        }


        if (isset($aData['sewerage']) && !empty($aData['sewerage'])) {
            $oSelect->where('ISNULL(sewerage) OR FIND_IN_SET(? , p.sewerage) > 0', $aData['sewerage']);
        }


        if (isset($aData['fence']) && !empty($aData['fence'])) {
            $oSelect->where('ISNULL(fence) OR FIND_IN_SET(? , p.fence) > 0', $aData['fence']);
        }


        if (isset($aData['garage']) && !empty($aData['garage'])) {
            $oSelect->where('ISNULL(garage) OR FIND_IN_SET(? , p.garage) > 0', $aData['garage']);
        }


        if (isset($aData['access']) && !empty($aData['access'])) {
            $oSelect->where('ISNULL(access) OR FIND_IN_SET(? , p.access) > 0', $aData['access']);
        }

        return $oSelect;
    }

}