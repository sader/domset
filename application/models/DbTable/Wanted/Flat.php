<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_DbTable_Wanted_Flat extends App_Db_Table {

    protected $_name = 'wanted_flat';

    /**
     * Instancja klasy.
     * 
     * @var MModel_DbTable_Wanted_Flat
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Wanted_Flat
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function add($aData) {
        $aInsertData = array();
        $aInsertData['id_offer'] = $aData['id_offer'];
        $aInsertData['rooms_from'] = $aData['rooms_from'];
        $aInsertData['rooms_to'] = $aData['rooms_to'];
        $aInsertData['floor_from'] = $aData['floor_from'];
        $aInsertData['floor_to'] = $aData['floor_to'];
        $aInsertData['levels_from'] = $aData['levels_from'];
        $aInsertData['levels_to'] = $aData['levels_to'];
        $aInsertData['floors_in_building_from'] = $aData['floors_in_building_from'];
        $aInsertData['floors_in_building_to'] = $aData['floors_in_building_to'];
        $aInsertData['building_type'] = is_array($aData['building_type']) ? implode(',', $aData['building_type']) : null;
        $aInsertData['year_from'] = $aData['year_from'];
        $aInsertData['year_to'] = $aData['year_to'];

        if (isset($aInsertData['building_material']))
            $aInsertData['building_material'] = is_array($aData['building_material']) ? implode(',', $aData['building_material']) : null;
        $aInsertData['quality'] = is_array($aData['quality']) ? implode(',', $aData['quality']) : null;
        $aInsertData['installation_quality'] = is_array($aData['installation_quality']) ? implode(',', $aData['installation_quality']) : null;
        $aInsertData['windows'] = is_array($aData['windows']) ? implode(',', $aData['windows']) : null;
        $aInsertData['kitchen'] = is_array($aData['kitchen']) ? implode(',', $aData['kitchen']) : null;
        $aInsertData['noise'] = is_array($aData['noise']) ? implode(',', $aData['noise']) : null;
        $aInsertData['heat'] = is_array($aData['heat']) ? implode(',', $aData['heat']) : null;
        $aInsertData['additional'] = is_array($aData['additional']) ? implode(',', $aData['additional']) : null;

        $this->insert($aInsertData);
    }

    public function edit($aData, $iOfferId) {

        $aUpdateData = array();

        $aUpdateData['rooms_from'] = $aData['rooms_from'];
        $aUpdateData['rooms_to'] = $aData['rooms_to'];
        $aUpdateData['floor_from'] = $aData['floor_from'];
        $aUpdateData['floor_to'] = $aData['floor_to'];
        $aUpdateData['levels_from'] = $aData['levels_from'];
        $aUpdateData['levels_to'] = $aData['levels_to'];
        $aUpdateData['floors_in_building_from'] = $aData['floors_in_building_from'];
        $aUpdateData['floors_in_building_to'] = $aData['floors_in_building_to'];
        $aUpdateData['building_type'] = is_array($aData['building_type']) ? implode(',', $aData['building_type']) : null;
        $aUpdateData['year_from'] = $aData['year_from'];
        $aUpdateData['year_to'] = $aData['year_to'];

        if (isset($aUpdateData['building_material']))
            $aUpdateData['building_material'] = is_array($aData['building_material']) ? implode(',', $aData['building_material']) : null;
        $aUpdateData['quality'] = is_array($aData['quality']) ? implode(',', $aData['quality']) : null;
        $aUpdateData['installation_quality'] = is_array($aData['installation_quality']) ? implode(',', $aData['installation_quality']) : null;
        $aUpdateData['windows'] = is_array($aData['windows']) ? implode(',', $aData['windows']) : null;
        $aUpdateData['kitchen'] = is_array($aData['kitchen']) ? implode(',', $aData['kitchen']) : null;
        $aUpdateData['noise'] = is_array($aData['noise']) ? implode(',', $aData['noise']) : null;
        $aUpdateData['heat'] = is_array($aData['heat']) ? implode(',', $aData['heat']) : null;
        $aUpdateData['additional'] = is_array($aData['additional']) ? implode(',', $aData['additional']) : null;

        $this->update($aUpdateData, array('id_offer = ?' => $iOfferId));
    }

    public function remove($iId) {
        $this->delete(array('id_offer = ?' => $iId));
    }

    public function get($iOfferId) {
        return $this->select()
                        ->where('id_offer = ?', $iOfferId)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }

    public function getWantedByOfferData($aData) {


        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), null)
                ->join(array('w' => 'wanted'), 'w.id=p.id_offer', array('id_wanted' => 'id',
                            'id_type',
                            'price_from',
                            'price_to',
                            'surface_from',
                            'surface_to',
                            'info'))
                        ->join(array('c' => 'client'), 'c.id=w.id_client',
                                array('client_id' => 'c.id', 'client' => 'CONCAT_WS(" " , phone,  "\n" ,surname, name)'))
                ->order('w.created DESC');

               

        
        if (isset($aData['city']) && !empty($aData['city'])) {
            $oSelect->where('(w.city = "" OR ISNULL(w.city) OR "' . $aData['city'] . '" LIKE CONCAT("%" , w.city , "%") )');
        }

        if (isset($aData['district']) && !empty($aData['district'])) {
            $oSelect->where('(w.district = "" OR ISNULL(w.district) OR "' . $aData['district'] . '" LIKE CONCAT("%" , w.district , "%") )');
        }


        if (isset($aData['section']) && !empty($aData['section'])) {
            $oSelect->where('(w.section = "" OR ISNULL(w.section) OR w.section LIKE ("%'.$aData['section'].'%") )');
        }

        if (isset($aData['street']) && !empty($aData['street'])) {
            $oSelect->where('(w.street = "" OR ISNULL(w.street) OR "' . $aData['street'] . '" LIKE CONCAT("%" , w.street , "%") )');
        }

        if (isset($aData['price']) && !empty($aData['price'])) {
            $oSelect->where('w.price_from = "0.00" OR ISNULL(w.price_from) OR price_from <= "' . $aData['price'] . '"');
            $oSelect->where('w.price_to = "0.00" OR ISNULL(w.price_to) OR price_to >= "' . $aData['price'] . '"');
        }

        if (isset($aData['surface']) && !empty($aData['surface'])) {
            $oSelect->where('w.surface_from = "0.00" OR ISNULL(w.surface_from) OR surface_from <= "' . $aData['surface'] . '"');
            $oSelect->where('w.surface_to = "0.00" OR ISNULL(w.surface_to) OR surface_to >= "' . $aData['surface'] . '"');
        }



        if (isset($aData['rooms']) && !empty($aData['rooms'])) {
            $oSelect->where('p.rooms_from = "0" OR ISNULL(p.rooms_from) OR rooms_from <= "' . $aData['rooms'] . '"');
            $oSelect->where('p.rooms_to = "0" OR ISNULL(p.rooms_to) OR rooms_to >= "' . $aData['rooms'] . '"');
        }

        if (isset($aData['floor']) && !empty($aData['floor']) && $aData['floor'] != '0') {
            $oSelect->where('p.floor_from = "0" OR ISNULL(p.floor_from) OR floor_from <= "' . $aData['floor'] . '"');
            $oSelect->where('p.floor_to = "0" OR ISNULL(p.floor_to) OR floor_to >= "' . $aData['floor'] . '"');
        }

        if (isset($aData['levels']) && !empty($aData['levels'])) {
            $oSelect->where('p.levels_from = "0" OR ISNULL(p.levels_from) OR levels_from <= "' . $aData['levels'] . '"');
            $oSelect->where('p.levels_to = "0" OR ISNULL(p.levels_to) OR levels_to >= "' . $aData['levels'] . '"');
        }

        if (isset($aData['floors_in_building']) && !empty($aData['floors_in_building'])) {
            $oSelect->where('p.floors_in_building_from = "0" OR ISNULL(p.floors_in_building_from) OR floors_in_building_from <= "' . $aData['floors_in_building'] . '"');
            $oSelect->where('p.floors_in_building_to = "0" OR ISNULL(p.floors_in_building_to) OR floors_in_building_to >= "' . $aData['floors_in_building'] . '"');
        }


        if (isset($aData['year']) && !empty($aData['year'])) {
            $oSelect->where('p.year_from = "0000" OR ISNULL(p.year_from) OR year_from <= "' . $aData['year'] . '"');
            $oSelect->where('p.year_to = "0000" OR ISNULL(p.year_to) OR year_to >= "' . $aData['year'] . '"');
        }
         if (isset($aData['building_type']) && !empty($aData['building_type'])) {
             $oSelect->where('ISNULL(building_type) OR FIND_IN_SET(? , p.building_type) > 0', $aData['building_type']);
         }
         
                  if (isset($aData['quality']) && !empty($aData['quality'])) {
             $oSelect->where('ISNULL(quality) OR FIND_IN_SET(? , p.quality) > 0', $aData['quality']);
         }

                  if (isset($aData['installation_quality']) && !empty($aData['installation_quality'])) {
             $oSelect->where('ISNULL(installation_quality) OR FIND_IN_SET(? , p.installation_quality) > 0', $aData['installation_quality']);
         }

                  if (isset($aData['windows']) && !empty($aData['windows'])) {
             $oSelect->where('ISNULL(windows) OR FIND_IN_SET(? , p.windows) > 0', $aData['windows']);
         }

                  if (isset($aData['kitchen']) && !empty($aData['kitchen'])) {
             $oSelect->where('ISNULL(kitchen) OR FIND_IN_SET(? , p.kitchen) > 0', $aData['kitchen']);
         }

                  if (isset($aData['noise']) && !empty($aData['noise'])) {
             $oSelect->where('ISNULL(noise) OR FIND_IN_SET(? , p.noise) > 0', $aData['noise']);
         }

            if (isset($aData['heat']) && !empty($aData['heat'])) {
             $oSelect->where('ISNULL(heat) OR FIND_IN_SET(? , p.heat) > 0', $aData['heat']);
         }
         
             
        return $oSelect;
    }

    public function getFiltersQuery($aExtraFilters) {



        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=p.id_offer')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), array('o.id',
                    'created' => 'DATE(o.created)',
                    'location' => 'CONCAT(o.section, " " , o.street)',
                    'o.price',
                    'o.price_surface',
                    'p.rooms',
                    'o.surface',
                    'floors' => 'CONCAT(p.floor , "/" , p.floors_in_building)'))
                ->columns('(' . $oSubSelect . ') as cover')
                ->join(array('o' => 'offer'), 'p.id_offer=o.id', null)
                                ->where('o.status = ?' , Model_DbTable_Offer::STATUS_OPEN);


        if (isset($aExtraFilters['province']) && !empty($aExtraFilters['province'])) {
            $oSelect->where("province IN (?)", $aExtraFilters['province']);
        }

        if (isset($aExtraFilters['district']) && !empty($aExtraFilters['district'])) {
            $oSelect->where("district LIKE ?", '%' . $aExtraFilters['district'] . '%');
        }

        if (isset($aExtraFilters['city']) && !empty($aExtraFilters['city'])) {
            $oSelect->where("city LIKE ?", '%' . $aExtraFilters['city'] . '%');
        }

        if (isset($aExtraFilters['quality']) && !empty($aExtraFilters['quality'])) {
            $oSelect->where("quality IN (?)", $aExtraFilters['quality']);
        }

        if (isset($aExtraFilters['year_from']) && !empty($aExtraFilters['year_from'])) {
            $oSelect->where("year >= (?)", $aExtraFilters['year_from']);
        }


        if (isset($aExtraFilters['year_to']) && !empty($aExtraFilters['year_to'])) {
            $oSelect->where("year <= (?)", $aExtraFilters['year_to']);
        }

        if (isset($aExtraFilters['bulding_type']) && !empty($aExtraFilters['bulding_type'])) {
            $oSelect->where("bulding_type IN (?)", $aExtraFilters['bulding_type']);
        }

        if (isset($aExtraFilters['bulding_material']) && !empty($aExtraFilters['bulding_material'])) {
            $oSelect->where("bulding_material IN (?)", $aExtraFilters['bulding_material']);
        }

        if (isset($aExtraFilters['kitchen']) && !empty($aExtraFilters['kitchen'])) {
            $oSelect->where("kitchen IN (?)", $aExtraFilters['kitchen']);
        }

        if (isset($aExtraFilters['levels']) && !empty($aExtraFilters['levels'])) {
            $oSelect->where("levels = ?", $aExtraFilters['levels']);
        }

        if (isset($aExtraFilters['heat']) && !empty($aExtraFilters['heat'])) {
            $oSelect->where("heat IN (?)", $aExtraFilters['heat']);
        }

        if (isset($aExtraFilters['additional']) && !empty($aExtraFilters['additional'])) {

            foreach ($aExtraFilters['additional'] as $iAdditional) {
                $oSelect->where('FIND_IN_SET(? , p.additional) > 0', $iAdditional);
            }
        }

        if (isset($aExtraFilters['categories']) && !empty($aExtraFilters['categories'])) {

            $oSubQuery = $this->select()
                    ->setIntegrityCheck(false)
                    ->from('category_offer', array('id_offer'))
                    ->where('id_category IN(?)', $aExtraFilters['categories']);
            $oSelect->join(array('categories' => $oSubQuery), 'categories.id_offer=o.id', null);
        }

        return $oSelect;
    }

}

