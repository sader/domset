<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_DbTable_Wanted_Warehouse extends App_Db_Table {

    protected $_name = 'wanted_warehouse';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Wanted_Warehouse
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Wanted_Warehouse
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function add($aData) {

        $aInsertData = array();
        $aInsertData['id_offer'] = $aData['id_offer'];

        $aInsertData['purpose'] = implode(',', $aData['purpose']);
        $aInsertData['rooms_from'] = $aData['rooms_from'];
        $aInsertData['rooms_to'] = $aData['rooms_to'];

        $aInsertData['height_from'] = $aData['height_from'];
        $aInsertData['height_to'] = $aData['height_to'];

        $aInsertData['floor_from'] = $aData['floor_from'];
        $aInsertData['floor_to'] = $aData['floor_to'];

        $aInsertData['levels_from'] = $aData['levels_from'];
        $aInsertData['levels_to'] = $aData['levels_to'];

        $aInsertData['year_from'] = $aData['year_from'];
        $aInsertData['year_to'] = $aData['year_to'];

        $aInsertData['quality'] = implode(',', $aData['quality']);
        $aInsertData['heat'] = implode(',', $aData['heat']);
        $aInsertData['additional'] = implode(',', $aData['additional']);

        $this->insert($aInsertData);
    }

    public function edit($aData, $iOfferId) {

        $aUpdateData = array();

        $aUpdateData['purpose'] = implode(',', $aData['purpose']);
        $aUpdateData['rooms_from'] = $aData['rooms_from'];
        $aUpdateData['rooms_to'] = $aData['rooms_to'];

        $aUpdateData['height_from'] = $aData['height_from'];
        $aUpdateData['height_to'] = $aData['height_to'];

        $aUpdateData['floor_from'] = $aData['floor_from'];
        $aUpdateData['floor_to'] = $aData['floor_to'];

        $aUpdateData['levels_from'] = $aData['levels_from'];
        $aUpdateData['levels_to'] = $aData['levels_to'];

        $aUpdateData['year_from'] = $aData['year_from'];
        $aUpdateData['year_to'] = $aData['year_to'];

        $aUpdateData['quality'] = implode(',', $aData['quality']);
        $aUpdateData['heat'] = implode(',', $aData['heat']);
        $aUpdateData['additional'] = implode(',', $aData['additional']);


        $this->update($aUpdateData, array('id_offer = ?' => $iOfferId));
    }

    public function remove($iId) {

        $this->delete(array('id_offer = ?' => $iId));
    }

    public function get($iOfferId) {
        return $this->select()
                        ->where('id_offer = ?', $iOfferId)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }

    public function getQuery($aExtraFilters) {


        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=p.id_offer')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), array('o.id',
                    'created' => 'DATE(o.created)',
                    'o.section',
                    'o.price',
                    'o.price_surface',
                    'p.purpose',
                    'o.surface'))
                ->columns('(' . $oSubSelect . ') as cover')
                ->join(array('o' => 'offer'), 'p.id_offer=o.id', null)
                ->where('o.status = ?' , Model_DbTable_Offer::STATUS_OPEN);

        if (isset($aExtraFilters['city']) && !empty($aExtraFilters['city'])) {
            $oSelect->where("city LIKE ?", '%' . $aExtraFilters['city'] . '%');
        }

        if (isset($aExtraFilters['street']) && !empty($aExtraFilters['street'])) {
            $oSelect->where("street LIKE ?", '%' . $aExtraFilters['street'] . '%');
        }


        if (isset($aExtraFilters['year_from']) && !empty($aExtraFilters['year_from'])) {
            $oSelect->where("year >= (?)", $aExtraFilters['year_from']);
        }


        if (isset($aExtraFilters['year_to']) && !empty($aExtraFilters['year_to'])) {
            $oSelect->where("year <= (?)", $aExtraFilters['year_to']);
        }

        if (isset($aExtraFilters['height_from']) && !empty($aExtraFilters['height_from'])) {
            $oSelect->where("height >= (?)", $aExtraFilters['height_from']);
        }


        if (isset($aExtraFilters['height_to']) && !empty($aExtraFilters['height_to'])) {
            $oSelect->where("height <= (?)", $aExtraFilters['height_to']);
        }


        if (isset($aExtraFilters['quality']) && !empty($aExtraFilters['quality'])) {
            $oSelect->where("quality IN (?)", $aExtraFilters['quality']);
        }

        if (isset($aExtraFilters['categories']) && !empty($aExtraFilters['categories'])) {

            $oSubQuery = $this->select()
                    ->setIntegrityCheck(false)
                    ->from('category_offer', array('id_offer'))
                    ->where('id_category IN(?)', $aExtraFilters['categories']);
            $oSelect->join(array('categories' => $oSubQuery), 'categories.id_offer=o.id', null);
        }

        return $oSelect;
    }

    public function getWantedByOfferData($aData) {


        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), null)
                ->join(array('w' => 'wanted'), 'w.id=p.id_offer', array('id_wanted' => 'id',
                            'id_type',
                            'price_from',
                            'price_to',
                            'surface_from',
                            'surface_to',
                            'info'))
                        ->join(array('c' => 'client'), 'c.id=w.id_client',
                                array('client_id' => 'c.id', 'client' => 'CONCAT_WS(" " , phone,  "\n" ,surname, name)'))
                ->order('w.created DESC');


        if (isset($aData['city']) && !empty($aData['city'])) {
            $oSelect->where('(w.city = "" OR ISNULL(w.city) OR "' . $aData['city'] . '" LIKE CONCAT("%" , w.city , "%") )');
        }

        if (isset($aData['district']) && !empty($aData['district'])) {
            $oSelect->where('(w.district = "" OR ISNULL(w.district) OR "' . $aData['district'] . '" LIKE CONCAT("%" , w.district , "%") )');
        }


      if (isset($aData['section']) && !empty($aData['section'])) {
            $oSelect->where('(w.section = "" OR ISNULL(w.section) OR w.section LIKE ("%'.$aData['section'].'%") )');
        }

        if (isset($aData['street']) && !empty($aData['street'])) {
            $oSelect->where('(w.street = "" OR ISNULL(w.street) OR "' . $aData['street'] . '" LIKE CONCAT("%" , w.street , "%") )');
        }

        if (isset($aData['price']) && !empty($aData['price'])) {
            $oSelect->where('w.price_from = "0.00" OR ISNULL(w.price_from) OR price_from <= "' . $aData['price'] . '"');
            $oSelect->where('w.price_to = "0.00" OR ISNULL(w.price_to) OR price_to >= "' . $aData['price'] . '"');
        }

        if (isset($aData['surface']) && !empty($aData['surface'])) {
            $oSelect->where('w.surface_from = "0.00" OR ISNULL(w.surface_from) OR surface_from <= "' . $aData['surface'] . '"');
            $oSelect->where('w.surface_to = "0.00" OR ISNULL(w.surface_to) OR surface_to >= "' . $aData['surface'] . '"');
        }



        if (isset($aData['rooms']) && !empty($aData['rooms'])) {
            $oSelect->where('p.rooms_from = "0" OR ISNULL(p.rooms_from) OR rooms_from <= "' . $aData['rooms'] . '"');
            $oSelect->where('p.rooms_to = "0" OR ISNULL(p.rooms_to) OR rooms_to >= "' . $aData['rooms'] . '"');
        }

        if (isset($aData['height']) && !empty($aData['height'])) {
            $oSelect->where('p.height_from = "0" OR ISNULL(p.height_from) OR height_from <= "' . $aData['height'] . '"');
            $oSelect->where('p.height_to = "0" OR ISNULL(p.height_to) OR height_to >= "' . $aData['height'] . '"');
        }

        if (isset($aData['floor']) && !empty($aData['floor'])  && $aData['floor'] != '0') {
            $oSelect->where('p.floor_from = "0" OR ISNULL(p.floor_from) OR floor_from <= "' . $aData['floor'] . '"');
            $oSelect->where('p.floor_to = "0" OR ISNULL(p.floor_to) OR floor_to >= "' . $aData['floor'] . '"');
        }

        if (isset($aData['levels']) && !empty($aData['levels'])) {
            $oSelect->where('p.levels_from = "0" OR ISNULL(p.levels_from) OR levels_from <= "' . $aData['levels'] . '"');
            $oSelect->where('p.levels_to = "0" OR ISNULL(p.levels_to) OR levels_to >= "' . $aData['levels'] . '"');
        }

        if (isset($aData['year']) && !empty($aData['year'])) {
            $oSelect->where('p.year_from = "0000" OR ISNULL(p.year_from) OR year_from <= "' . $aData['year'] . '"');
            $oSelect->where('p.year_to = "0000" OR ISNULL(p.year_to) OR year_to >= "' . $aData['year'] . '"');
        }

        if (isset($aData['purpose']) && !empty($aData['purpose'])) {
            $oSelect->where('ISNULL(purpose) OR FIND_IN_SET(? , p.purpose) > 0', $aData['purpose']);
        }

        if (isset($aData['quality']) && !empty($aData['quality'])) {
            $oSelect->where('ISNULL(quality) OR FIND_IN_SET(? , p.quality) > 0', $aData['quality']);
        }

        if (isset($aData['heat']) && !empty($aData['heat'])) {
            $oSelect->where('ISNULL(heat) OR FIND_IN_SET(? , p.heat) > 0', $aData['heat']);
        }


        return $oSelect;
    }

}