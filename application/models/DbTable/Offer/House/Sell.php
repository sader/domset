<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_DbTable_Offer_House_Sell extends App_Db_Table {

    protected $_name = 'offer_house_sell';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Offer_House_Sell
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Offer_House_Sell
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function addOffer($aData) {
        $aInsertData = array(
            'id_offer' => $aData['id_offer'],
            'market' => $aData['market'],
            'market_visible' => $aData['market_visible'],
            'house_surface' => $aData['house_surface'],
            'terrain_surface' => $aData['terrain_surface'],
            'building_type' => $aData['building_type'],
            'rooms' => $aData['rooms'],
            'floors' => $aData['floors'],
            'attic' => $aData['attic'],
            'basement' => $aData['basement'],
            'year' => $aData['year'],
            'building_material' => $aData['building_material'],
            'quality' => $aData['quality'],
            'windows' => $aData['windows'],
            'roof' => $aData['roof'],
            'roof_type' => $aData['roof_type'],
            'heat' => $aData['heat'],
            'sewerage' => $aData['sewerage'],
            'fence' => $aData['fence'],
            'garage' => $aData['garage'],
            'access' => $aData['access'],
            'additional' => implode(',', $aData['additional'])
        );

        $this->insert($aInsertData);
    }

    public function editOffer($aData, $iOfferId) {
        $aUpdateData = array(
            'market' => $aData['market'],
            'market_visible' => $aData['market_visible'],
            'house_surface' => $aData['house_surface'],
            'terrain_surface' => $aData['terrain_surface'],
            'building_type' => $aData['building_type'],
            'rooms' => $aData['rooms'],
            'floors' => $aData['floors'],
            'attic' => $aData['attic'],
            'basement' => $aData['basement'],
            'year' => $aData['year'],
            'building_material' => $aData['building_material'],
            'quality' => $aData['quality'],
            'windows' => $aData['windows'],
            'roof' => $aData['roof'],
            'roof_type' => $aData['roof_type'],
            'heat' => $aData['heat'],
            'sewerage' => $aData['sewerage'],
            'fence' => $aData['fence'],
            'garage' => $aData['garage'],
            'access' => $aData['access'],
            'additional' => implode(',', $aData['additional']));

        $this->update($aUpdateData, array('id_offer = ?' => $iOfferId));
    }

    public function removeOffer($iId) {
        $this->delete(array('id_offer = ?' => $iId));
    }

    public function getOffer($iOfferId) {
        return $this->select()
                        ->where('id_offer = ?', $iOfferId)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }

    public function getOffersQuery($aExtraFilters) {


        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=p.id_offer')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), array('o.id',
                    'created' => 'DATE(o.created)',
                    'location' => 'CONCAT(o.section, " " , o.street)',
                    'o.price',
                    'o.price_surface',
                    'p.building_type',
                    'o.surface',
                    'p.terrain_surface',
                    'p.quality'))
                ->columns('(' . $oSubSelect . ') as cover')
                ->join(array('o' => 'offer'), 'p.id_offer=o.id', null)
                ->where('o.status  = ?' , Model_DbTable_Offer::STATUS_OPEN); 


        if (isset($aExtraFilters['province']) && !empty($aExtraFilters['province'])) {
            $oSelect->where("province IN (?)", $aExtraFilters['province']);
        }

        if (isset($aExtraFilters['district']) && !empty($aExtraFilters['district'])) {
            $oSelect->where("district LIKE ?", '%' . $aExtraFilters['district'] . '%');
        }

     if (isset($aExtraFilters['section']) && !empty($aExtraFilters['section'])) {
            $oSelect->where("section IN (?)", $aExtraFilters['section']);
        }

        if (isset($aExtraFilters['quality']) && !empty($aExtraFilters['quality'])) {
            $oSelect->where("quality IN (?)", $aExtraFilters['quality']);
        }

        if (isset($aExtraFilters['rooms_from']) && !empty($aExtraFilters['rooms_from'])) {
            $oSelect->where("rooms >= (?)", $aExtraFilters['rooms_from']);
        }


        if (isset($aExtraFilters['rooms_to']) && !empty($aExtraFilters['rooms_to'])) {
            $oSelect->where("rooms <= (?)", $aExtraFilters['rooms_to']);
        }


        if (isset($aExtraFilters['year_from']) && !empty($aExtraFilters['year_from'])) {
            $oSelect->where("year >= (?)", $aExtraFilters['year_from']);
        }


        if (isset($aExtraFilters['year_to']) && !empty($aExtraFilters['year_to'])) {
            $oSelect->where("year <= (?)", $aExtraFilters['year_to']);
        }

        if (isset($aExtraFilters['bulding_type']) && !empty($aExtraFilters['bulding_type'])) {
            $oSelect->where("bulding_type IN (?)", $aExtraFilters['bulding_type']);
        }

        if (isset($aExtraFilters['bulding_material']) && !empty($aExtraFilters['bulding_material'])) {
            $oSelect->where("bulding_material IN (?)", $aExtraFilters['bulding_material']);
        }

        if (isset($aExtraFilters['sewerage']) && !empty($aExtraFilters['sewerage'])) {
            $oSelect->where("sewerage IN (?)", $aExtraFilters['sewerage']);
        }

        if (isset($aExtraFilters['heat']) && !empty($aExtraFilters['heat'])) {
            $oSelect->where("heat IN (?)", $aExtraFilters['heat']);
        }

        if (isset($aExtraFilters['basement']) && !empty($aExtraFilters['basement'])) {
            $oSelect->where("basement IN (?)", $aExtraFilters['basement']);
        }

             if (isset($aExtraFilters['categories']) && !empty($aExtraFilters['categories'])) {


            foreach ($aExtraFilters['categories'] as $iKey => $iCat) {
                $iC = 'cat' . ++$iKey;
                $oSelect->join(array($iC => 'category_offer'), $iC.'.id_offer=o.id AND '.$iC.'.id_category = ' . $iCat, null);
            }
        }

        return $oSelect;
    }
    
          public function getOffersDisplayQuery($aExtraFilters) {

        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=o.id')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

 $oCategories = $this->select()
                     ->setIntegrityCheck(false)
                     ->from(array('co' => 'category_offer'), array('GROUP_CONCAT(CONCAT_WS("|" , c.name, c.info, co.data)  SEPARATOR "|x|")'))
                     ->join(array('c' => 'category'), 'co.id_category=c.id' , null)
         ->where('co.id_offer = o.id');
                
 
  $oNewPriceSelect = $this->select()->from(array('co' => 'category_offer'), array('data'))
                ->setIntegrityCheck(false)
                ->join(array('c' => 'category') , 'co.id_category=c.id' , null)
                ->where('c.name = ?' , 'Nowa cena')
                ->where('co.id_offer = o.id')
                ->limit(1);
 
 
        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name))
                ->columns('(' . $oSubSelect . ') as photo')
                ->join(array('o' => 'offer'), 'p.id_offer=o.id')
                ->columns('(' . $oNewPriceSelect . ') as new_price')
                ->columns('(' . $oCategories . ') as categories')
                ->where('o.status  = ?' , Model_DbTable_Offer::STATUS_OPEN); 
        
        
         if (isset($aExtraFilters['offer_id']) && !empty($aExtraFilters['offer_id'])) {
            $oSelect->where("id_offer = ?", $aExtraFilters['offer_id']);
        }
        
         if (isset($aExtraFilters['district']) && !empty($aExtraFilters['district'])) {
            $oSelect->where("district IN (?)", $aExtraFilters['district']);
        }
        
            if (isset($aExtraFilters['community']) && !empty($aExtraFilters['community'])) {
            $oSelect->where("community IN (?)", $aExtraFilters['community']);
        }

        if (isset($aExtraFilters['city']) && !empty($aExtraFilters['city'])) {
            $oSelect->where("city IN (?)", $aExtraFilters['city']);
        }

        if (isset($aExtraFilters['price_from']) && !empty($aExtraFilters['price_from']) && $aExtraFilters['price_from'] != '0.00') {
            $oSelect->where("price >= ?", $aExtraFilters['price_from']);
        }

        if (isset($aExtraFilters['price_to']) && !empty($aExtraFilters['price_to']) && $aExtraFilters['price_to'] != '0.00') {
            $oSelect->where("price <= ?", $aExtraFilters['price_to']);
        }

        if (isset($aExtraFilters['surface_from']) && !empty($aExtraFilters['surface_from']) && $aExtraFilters['surface_from'] != '0.00') {
            $oSelect->where("surface >= ?", $aExtraFilters['surface_from']);
        }


        if (isset($aExtraFilters['surface_to']) && !empty($aExtraFilters['surface_to']) && $aExtraFilters['surface_to'] != '0.00') {
            $oSelect->where("surface <= ?", $aExtraFilters['surface_to']);
        }


        if (isset($aExtraFilters['price_surface_from']) && !empty($aExtraFilters['price_surface_from']) && $aExtraFilters['price_surface_from'] != '0.00') {
            $oSelect->where("price_surface >= ?", $aExtraFilters['price_surface_from']);
        }


        if (isset($aExtraFilters['price_surface_to']) && !empty($aExtraFilters['price_surface_to']) && $aExtraFilters['price_surface_to'] != '0.00') {
            $oSelect->where("price_surface <= ?", $aExtraFilters['price_surface_to']);
        }

        if (isset($aExtraFilters['rooms_from']) && !empty($aExtraFilters['rooms_from']) && $aExtraFilters['rooms_from'] != '0') {
            $oSelect->where("rooms >= ?", $aExtraFilters['rooms_from']);
        }


        if (isset($aExtraFilters['rooms_to']) && !empty($aExtraFilters['rooms_to']) && $aExtraFilters['rooms_to'] != '0') {
            $oSelect->where("rooms <= ?", $aExtraFilters['rooms_to']);
        }

      
          if (isset($aExtraFilters['terrain_surface_to']) && !empty($aExtraFilters['terrain_surface_to']) && $aExtraFilters['terrain_surface_to'] != '0.00') {
            $oSelect->where("terrain_surface <= ?", $aExtraFilters['terrain_surface_to']);
        }


        if (isset($aExtraFilters['price_terrain_surface_from']) && !empty($aExtraFilters['price_terrain_surface_from']) && $aExtraFilters['price_terrain_surface_from'] != '0.00') {
            $oSelect->where("price_terrain_surface >= ?", $aExtraFilters['price_terrain_surface_from']);
        }
        
             if (isset($aExtraFilters['categories']) && !empty($aExtraFilters['categories'])) {


            foreach ($aExtraFilters['categories'] as $iKey => $iCat) {
                $iC = 'cat' . ++$iKey;
                $oSelect->join(array($iC => 'category_offer'), $iC.'.id_offer=o.id AND '.$iC.'.id_category = ' . $iCat, null);
            }
        }
        
        if (isset($aExtraFilters['sort']) && !empty($aExtraFilters['sort'])) {
            $aSort = Model_Logic_Offer::getInstance()->getSortingMethodById($aExtraFilters['sort']); 
            $oSelect->order($aSort['sql']);
        }
        else $oSelect->order('created DESC');

        return $oSelect;
    }

    public function getOffersForWanted($aExtraFilters) {


        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=o.id')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), array('o.id',
                    'o.city',
                    'o.street',
                    'o.price',
                    'o.surface',
                    'o.price_surface'))
                ->columns('(' . $oSubSelect . ') as cover')
                ->where('o.status = ?', Model_DbTable_Offer::STATUS_OPEN)
                ->join(array('o' => 'offer'), 'p.id_offer=o.id', null);



        if (isset($aExtraFilters['province']) && !empty($aExtraFilters['province'])) {
            $oSelect->where("province IN (?)", explode(',', $aExtraFilters['province']));
        }

        if (isset($aExtraFilters['district']) && !empty($aExtraFilters['district'])) {
            $oSelect->where("district LIKE ?", '%' . $aExtraFilters['district'] . '%');
        }

        if (isset($aExtraFilters['city']) && !empty($aExtraFilters['city'])) {
            $oSelect->where("city LIKE ?", '%' . $aExtraFilters['city'] . '%');
        }

        if (isset($aExtraFilters['section']) && !empty($aExtraFilters['section'])) {
            $oSelect->where("section IN(?)", explode(',', $aExtraFilters['section']));
        }

        if (isset($aExtraFilters['price_from']) && !empty($aExtraFilters['price_from']) && $aExtraFilters['price_from'] != '0.00') {
            $oSelect->where("price >= ?", $aExtraFilters['price_from']);
        }

        if (isset($aExtraFilters['price_to']) && !empty($aExtraFilters['price_to']) && $aExtraFilters['price_to'] != '0.00') {
            $oSelect->where("price <= ?", $aExtraFilters['price_to']);
        }

        if (isset($aExtraFilters['surface_from']) && !empty($aExtraFilters['surface_from']) && $aExtraFilters['surface_from'] != '0.00') {
            $oSelect->where("surface >= ?", $aExtraFilters['surface_from']);
        }


        if (isset($aExtraFilters['surface_to']) && !empty($aExtraFilters['surface_to']) && $aExtraFilters['surface_to'] != '0.00') {
            $oSelect->where("surface <= ?", $aExtraFilters['surface_to']);
        }


        if (isset($aExtraFilters['price_surface_from']) && !empty($aExtraFilters['price_surface_from']) && $aExtraFilters['price_surface_from'] != '0.00') {
            $oSelect->where("price_surface >= ?", $aExtraFilters['price_surface_from']);
        }


        if (isset($aExtraFilters['price_surface_to']) && !empty($aExtraFilters['price_surface_to']) && $aExtraFilters['price_surface_to'] != '0.00') {
            $oSelect->where("price_surface <= ?", $aExtraFilters['price_surface_to']);
        }

        if (isset($aExtraFilters['house_surface_from']) && !empty($aExtraFilters['house_surface_from']) && $aExtraFilters['house_surface_from'] != '0.00') {
            $oSelect->where("house_surface >= ?", $aExtraFilters['house_surface_from']);
        }


        if (isset($aExtraFilters['house_surface_to']) && !empty($aExtraFilters['house_surface_to']) && $aExtraFilters['house_surface_to'] != '0.00') {
            $oSelect->where("house_surface <= ?", $aExtraFilters['house_surface_to']);
        }

        if (isset($aExtraFilters['terrain_surface_from']) && !empty($aExtraFilters['terrain_surface_from']) && $aExtraFilters['terrain_surface_from'] != '0.00') {
            $oSelect->where("terrain_surface >= ?", $aExtraFilters['terrain_surface_from']);
        }


        if (isset($aExtraFilters['terrain_surface_to']) && !empty($aExtraFilters['terrain_surface_to']) && $aExtraFilters['terrain_surface_to'] != '0.00') {
            $oSelect->where("terrain_surface <= ?", $aExtraFilters['terrain_surface_to']);
        }

        if (isset($aExtraFilters['bulding_type']) && !empty($aExtraFilters['bulding_type'])) {
            $oSelect->where("bulding_type IN (?)", explode(',', $aExtraFilters['bulding_type']));
        }

        if (isset($aExtraFilters['rooms_from']) && !empty($aExtraFilters['rooms_from']) && $aExtraFilters['rooms_from'] != '0') {
            $oSelect->where("rooms >= ?", $aExtraFilters['rooms_from']);
        }


        if (isset($aExtraFilters['rooms_to']) && !empty($aExtraFilters['rooms_to']) && $aExtraFilters['rooms_to'] != '0') {
            $oSelect->where("rooms <= ?", $aExtraFilters['rooms_to']);
        }

        if (isset($aExtraFilters['floors_from']) && !empty($aExtraFilters['floors_from']) && $aExtraFilters['floors_from'] != '0') {
            $oSelect->where("floors >= ?", $aExtraFilters['floors_from']);
        }


        if (isset($aExtraFilters['floors_to']) && !empty($aExtraFilters['floors_to']) && $aExtraFilters['floors_to'] != '0') {
            $oSelect->where("floors <= ?", $aExtraFilters['floors_to']);
        }

        if (isset($aExtraFilters['attic']) && !empty($aExtraFilters['attic'])) {
            $oSelect->where("attic IN (?)", explode(',', $aExtraFilters['attic']));
        }


        if (isset($aExtraFilters['basement']) && !empty($aExtraFilters['basement'])) {
            $oSelect->where("basement IN (?)", explode(',', $aExtraFilters['basement']));
        }


        if (isset($aExtraFilters['year_from']) && !empty($aExtraFilters['year_from']) && $aExtraFilters['year_from'] != '0000') {
            $oSelect->where("year >= ?", $aExtraFilters['year_from']);
        }

        if (isset($aExtraFilters['year_to']) && !empty($aExtraFilters['year_to']) && $aExtraFilters['year_to'] != '0000') {
            $oSelect->where("year <= ?", $aExtraFilters['year_to']);
        }


        if (isset($aExtraFilters['building_material']) && !empty($aExtraFilters['building_material'])) {
            $oSelect->where("building_material IN (?)", explode(',', $aExtraFilters['building_material']));
        }



        if (isset($aExtraFilters['quality']) && !empty($aExtraFilters['quality'])) {
            $oSelect->where("quality IN (?)", explode(',', $aExtraFilters['quality']));
        }


        if (isset($aExtraFilters['windows']) && !empty($aExtraFilters['windows'])) {
            $oSelect->where("windows IN (?)", $aExtraFilters['windows']);
        }

        if (isset($aExtraFilters['roof']) && !empty($aExtraFilters['roof'])) {
            $oSelect->where("roof IN (?)", $aExtraFilters['roof']);
        }


        if (isset($aExtraFilters['roof_type']) && !empty($aExtraFilters['roof_type'])) {
            $oSelect->where("roof_type IN (?)", $aExtraFilters['roof_type']);
        }

        if (isset($aExtraFilters['heat']) && !empty($aExtraFilters['heat'])) {
            $oSelect->where("heat IN (?)", $aExtraFilters['heat']);
        }

        if (isset($aExtraFilters['sewerage']) && !empty($aExtraFilters['sewerage'])) {
            $oSelect->where("sewerage IN (?)", $aExtraFilters['sewerage']);
        }


        if (isset($aExtraFilters['fence']) && !empty($aExtraFilters['fence'])) {
            $oSelect->where("fence IN (?)", $aExtraFilters['fence']);
        }


        if (isset($aExtraFilters['garage']) && !empty($aExtraFilters['garage'])) {
            $oSelect->where("garage IN (?)", $aExtraFilters['garage']);
        }


        if (isset($aExtraFilters['access']) && !empty($aExtraFilters['access'])) {
            $oSelect->where("access IN (?)", $aExtraFilters['access']);
        }

        if (isset($aExtraFilters['additional']) && !empty($aExtraFilters['additional'])) {

            $aAdditionals = explode(',', $aExtraFilters['additional']);

            foreach ($aAdditionals as $iAdditional) {
                $oSelect->where('FIND_IN_SET(? , p.additional) > 0', $iAdditional);
            }
        }

        return $oSelect;
    }

}