<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_DbTable_Offer_Flat_Sell extends App_Db_Table {

    protected $_name = 'offer_flat_sell';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Offer_Flat_Sell
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Offer_Flat_Sell
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function addOffer($aData) {
        $aInsertData = array();
        $aInsertData['id_offer'] = $aData['id_offer'];
        $aInsertData['market'] = $aData['market'];
        $aInsertData['market_visible'] = $aData['market_visible'];
        $aInsertData['rooms'] = $aData['rooms'];
        $aInsertData['floor'] = $aData['floor'];
        $aInsertData['levels'] = $aData['levels'];
        $aInsertData['floors_in_building'] = $aData['floors_in_building'];
        $aInsertData['building_type'] = $aData['building_type'];
        $aInsertData['year'] = $aData['year'];
        $aInsertData['building_material'] = $aData['building_material'];
        $aInsertData['ownership'] = $aData['ownership'];
        $aInsertData['rent'] = $aData['rent'];
        $aInsertData['quality'] = $aData['quality'];
        $aInsertData['installation_quality'] = $aData['installation_quality'];
        $aInsertData['windows'] = $aData['windows'];
        $aInsertData['kitchen'] = $aData['kitchen'];
        $aInsertData['noise'] = $aData['noise'];
        $aInsertData['heat'] = $aData['heat'];
        $aInsertData['available'] = $aData['available'];
        $aInsertData['additional'] = implode(',', $aData['additional']);

        $this->insert($aInsertData);
    }

    public function editOffer($aData, $iOfferId) {
        $aUpdateData = array();
        $aUpdateData['market'] = $aData['market'];
        $aUpdateData['market_visible'] = $aData['market_visible'];
        $aUpdateData['rooms'] = $aData['rooms'];
        $aUpdateData['floor'] = $aData['floor'];
        $aUpdateData['levels'] = $aData['levels'];
        $aUpdateData['floors_in_building'] = $aData['floors_in_building'];
        $aUpdateData['building_type'] = $aData['building_type'];
        $aUpdateData['year'] = $aData['year'];
        $aUpdateData['building_material'] = $aData['building_material'];
        $aUpdateData['ownership'] = $aData['ownership'];
        $aUpdateData['rent'] = $aData['rent'];
        $aUpdateData['quality'] = $aData['quality'];
        $aUpdateData['installation_quality'] = $aData['installation_quality'];
        $aUpdateData['windows'] = $aData['windows'];
        $aUpdateData['kitchen'] = $aData['kitchen'];
        $aUpdateData['noise'] = $aData['noise'];
        $aUpdateData['heat'] = $aData['heat'];
        $aUpdateData['available'] = $aData['available'];
        $aUpdateData['additional'] = implode(',', $aData['additional']);

        $this->update($aUpdateData, array('id_offer = ?' => $iOfferId));
    }

    public function removeOffer($iId) {
        $this->delete(array('id_offer = ?' => $iId));
    }

    public function getOffer($iOfferId) {
        return $this->select()
                        ->where('id_offer = ?', $iOfferId)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }
    
    
    
      public function getPreviewQuery($aExtraFilters) {

        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=p.id_offer')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);
        

        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), array('p.id_offer',
                    'location' => 'CONCAT(o.section, " " , o.street)',
                    'o.price',
                    'o.surface',
                    'o.price_surface',
                    'p.rooms',
                    'floors' => 'CONCAT(p.floor , "/" , p.floors_in_building)'))
                ->columns('(' . $oSubSelect . ') as cover')
                ->join(array('o' => 'offer'), 'p.id_offer=o.id', null)
                ->join(array('pr' => 'presentation') , 'pr.id_offer=p.id_offer', array('presentation_date' => 'date'))
                ->join(array('c' => 'client') , 'pr.id_client=c.id', array('client_id' => 'id', 'client' =>  'CONCAT_WS(" ", name, surname)' , 'phone'))
                ->order('pr.date DESC')
                ->group('c.id');
                
        
        if (isset($aExtraFilters['province']) && !empty($aExtraFilters['province'])) {
            $oSelect->where("o.province IN (?)", $aExtraFilters['province']);
        }

        if (isset($aExtraFilters['district']) && !empty($aExtraFilters['district'])) {
            $oSelect->where("o.district LIKE ?", '%' . $aExtraFilters['district'] . '%');
        }

        if (isset($aExtraFilters['city']) && !empty($aExtraFilters['city'])) {
            $oSelect->where("o.city LIKE ?", '%' . $aExtraFilters['city'] . '%');
        }

        if (isset($aExtraFilters['quality']) && !empty($aExtraFilters['quality'])) {
            $oSelect->where("p.quality IN (?)", $aExtraFilters['quality']);
        }

        if (isset($aExtraFilters['year_from']) && !empty($aExtraFilters['year_from'])) {
            $oSelect->where("p.year >= (?)", $aExtraFilters['year_from']);
        }


        if (isset($aExtraFilters['year_to']) && !empty($aExtraFilters['year_to'])) {
            $oSelect->where("p.year <= (?)", $aExtraFilters['year_to']);
        }

        if (isset($aExtraFilters['bulding_type']) && !empty($aExtraFilters['bulding_type'])) {
            $oSelect->where("bulding_type IN (?)", $aExtraFilters['bulding_type']);
        }

        if (isset($aExtraFilters['bulding_material']) && !empty($aExtraFilters['bulding_material'])) {
            $oSelect->where("bulding_material IN (?)", $aExtraFilters['bulding_material']);
        }

        if (isset($aExtraFilters['kitchen']) && !empty($aExtraFilters['kitchen'])) {
            $oSelect->where("kitchen IN (?)", $aExtraFilters['kitchen']);
        }

        if (isset($aExtraFilters['levels']) && !empty($aExtraFilters['levels'])) {
            $oSelect->where("levels = ?", $aExtraFilters['levels']);
        }

        if (isset($aExtraFilters['heat']) && !empty($aExtraFilters['heat'])) {
            $oSelect->where("heat IN (?)", $aExtraFilters['heat']);
        }

        if (isset($aExtraFilters['additional']) && !empty($aExtraFilters['additional'])) {

            foreach ($aExtraFilters['additional'] as $iAdditional) {
                $oSelect->where('FIND_IN_SET(? , p.additional) > 0', $iAdditional);
            }
        }

            if (isset($aExtraFilters['categories']) && !empty($aExtraFilters['categories'])) {


            foreach ($aExtraFilters['categories'] as $iKey => $iCat) {
                $iC = 'cat' . ++$iKey;
                $oSelect->join(array($iC => 'category_offer'), $iC.'.id_offer=o.id AND '.$iC.'.id_category = ' . $iCat, null);
            }
        }

        return $oSelect;
    }
    
    
    
    

    public function getOffersQuery($aExtraFilters) {

        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=p.id_offer')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

        


        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), array('o.id',
                    'created' => 'DATE(o.created)',
                    'location' => 'CONCAT(o.section, " " , o.street)',
                    'o.price',
                    'o.surface',
                    'o.price_surface',
                    'p.rooms',
                    'floors' => 'CONCAT(p.floor , "/" , p.floors_in_building)'))
                ->columns('(' . $oSubSelect . ') as cover')
                ->join(array('o' => 'offer'), 'p.id_offer=o.id', null)
                ->where('o.status  = ?', Model_DbTable_Offer::STATUS_OPEN);


        if (isset($aExtraFilters['province']) && !empty($aExtraFilters['province'])) {
            $oSelect->where("province IN (?)", $aExtraFilters['province']);
        }

        if (isset($aExtraFilters['district']) && !empty($aExtraFilters['district'])) {
            $oSelect->where("district LIKE ?", '%' . $aExtraFilters['district'] . '%');
        }

        if (isset($aExtraFilters['city']) && !empty($aExtraFilters['city'])) {
            $oSelect->where("city LIKE ?", '%' . $aExtraFilters['city'] . '%');
        }

        if (isset($aExtraFilters['quality']) && !empty($aExtraFilters['quality'])) {
            $oSelect->where("quality IN (?)", $aExtraFilters['quality']);
        }

        if (isset($aExtraFilters['year_from']) && !empty($aExtraFilters['year_from'])) {
            $oSelect->where("year >= (?)", $aExtraFilters['year_from']);
        }


        if (isset($aExtraFilters['year_to']) && !empty($aExtraFilters['year_to'])) {
            $oSelect->where("year <= (?)", $aExtraFilters['year_to']);
        }

        if (isset($aExtraFilters['bulding_type']) && !empty($aExtraFilters['bulding_type'])) {
            $oSelect->where("bulding_type IN (?)", $aExtraFilters['bulding_type']);
        }

        if (isset($aExtraFilters['bulding_material']) && !empty($aExtraFilters['bulding_material'])) {
            $oSelect->where("bulding_material IN (?)", $aExtraFilters['bulding_material']);
        }

        if (isset($aExtraFilters['kitchen']) && !empty($aExtraFilters['kitchen'])) {
            $oSelect->where("kitchen IN (?)", $aExtraFilters['kitchen']);
        }

        if (isset($aExtraFilters['levels']) && !empty($aExtraFilters['levels'])) {
            $oSelect->where("levels = ?", $aExtraFilters['levels']);
        }

        if (isset($aExtraFilters['heat']) && !empty($aExtraFilters['heat'])) {
            $oSelect->where("heat IN (?)", $aExtraFilters['heat']);
        }

        if (isset($aExtraFilters['additional']) && !empty($aExtraFilters['additional'])) {

            foreach ($aExtraFilters['additional'] as $iAdditional) {
                $oSelect->where('FIND_IN_SET(? , p.additional) > 0', $iAdditional);
            }
        }

            if (isset($aExtraFilters['categories']) && !empty($aExtraFilters['categories'])) {


            foreach ($aExtraFilters['categories'] as $iKey => $iCat) {
                $iC = 'cat' . ++$iKey;
                $oSelect->join(array($iC => 'category_offer'), $iC.'.id_offer=o.id AND '.$iC.'.id_category = ' . $iCat, null);
            }
        }

        return $oSelect;
    }

    public function getOffersDisplayQuery($aExtraFilters) {


        

        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=o.id')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

        $oCategories = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('co' => 'category_offer'), array('GROUP_CONCAT(CONCAT_WS("|" , c.name, c.info, co.data)  SEPARATOR "|x|")'))
                ->join(array('c' => 'category'), 'co.id_category=c.id', null)
                ->where('co.id_offer = o.id');


        $oNewPriceSelect = $this->select()->from(array('co' => 'category_offer'), array('data'))
                ->setIntegrityCheck(false)
                ->join(array('c' => 'category'), 'co.id_category=c.id', null)
                ->where('c.name = ?', 'Nowa cena')
                ->where('co.id_offer = o.id')
                ->limit(1);


        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name))
                ->columns('(' . $oSubSelect . ') as photo')
                ->join(array('o' => 'offer'), 'p.id_offer=o.id')
                ->columns('(' . $oCategories . ') as categories')
                ->columns('(' . $oNewPriceSelect . ') as new_price')
                ->where('o.status  = ?', Model_DbTable_Offer::STATUS_OPEN);

        if (isset($aExtraFilters['offer_id']) && !empty($aExtraFilters['offer_id'])) {
            $oSelect->where("id_offer = ?", $aExtraFilters['offer_id']);
        }


        if (isset($aExtraFilters['city']) && !empty($aExtraFilters['city'])) {
            $oSelect->where("city IN (?)", $aExtraFilters['city']);
        }

        if (isset($aExtraFilters['section']) && !empty($aExtraFilters['section'])) {
            $oSelect->where("section IN (?)", $aExtraFilters['section']);
        }

        if (isset($aExtraFilters['price_from']) && !empty($aExtraFilters['price_from']) && $aExtraFilters['price_from'] != '0.00') {
            $oSelect->where("price >= ?", $aExtraFilters['price_from']);
        }

        if (isset($aExtraFilters['price_to']) && !empty($aExtraFilters['price_to']) && $aExtraFilters['price_to'] != '0.00') {
            $oSelect->where("price <= ?", $aExtraFilters['price_to']);
        }

        if (isset($aExtraFilters['surface_from']) && !empty($aExtraFilters['surface_from']) && $aExtraFilters['surface_from'] != '0.00') {
            $oSelect->where("surface >= ?", $aExtraFilters['surface_from']);
        }


        if (isset($aExtraFilters['surface_to']) && !empty($aExtraFilters['surface_to']) && $aExtraFilters['surface_to'] != '0.00') {
            $oSelect->where("surface <= ?", $aExtraFilters['surface_to']);
        }


        if (isset($aExtraFilters['price_surface_from']) && !empty($aExtraFilters['price_surface_from']) && $aExtraFilters['price_surface_from'] != '0.00') {
            $oSelect->where("price_surface >= ?", $aExtraFilters['price_surface_from']);
        }


        if (isset($aExtraFilters['price_surface_to']) && !empty($aExtraFilters['price_surface_to']) && $aExtraFilters['price_surface_to'] != '0.00') {
            $oSelect->where("price_surface <= ?", $aExtraFilters['price_surface_to']);
        }

        if (isset($aExtraFilters['rooms_from']) && !empty($aExtraFilters['rooms_from']) && $aExtraFilters['rooms_from'] != '0') {
            $oSelect->where("rooms >= ?", $aExtraFilters['rooms_from']);
        }


        if (isset($aExtraFilters['rooms_to']) && !empty($aExtraFilters['rooms_to']) && $aExtraFilters['rooms_to'] != '0') {
            $oSelect->where("rooms <= ?", $aExtraFilters['rooms_to']);
        }

      if (isset($aExtraFilters['floor_from']) && $aExtraFilters['floor_from'] != '') {
            $oSelect->where("floor >= ?", $aExtraFilters['floor_from']);
        }


        if (isset($aExtraFilters['floor_to']) && $aExtraFilters['floor_to'] != '') {
            $oSelect->where("floor <= ?", $aExtraFilters['floor_to']);
        }

        if (isset($aExtraFilters['quality']) && !empty($aExtraFilters['quality'])) {
            $oSelect->where("quality IN (?)", $aExtraFilters['quality']);
        }


        if (isset($aExtraFilters['categories']) && !empty($aExtraFilters['categories'])) {


            foreach ($aExtraFilters['categories'] as $iKey => $iCat) {
                $iC = 'cat' . ++$iKey;
                $oSelect->join(array($iC => 'category_offer'), $iC.'.id_offer=o.id AND '.$iC.'.id_category = ' . $iCat, null);
            }
        }

        if (isset($aExtraFilters['sort']) && !empty($aExtraFilters['sort'])) {
            $aSort = Model_Logic_Offer::getInstance()->getSortingMethodById($aExtraFilters['sort']);
            $oSelect->order($aSort['sql']);
        }
        else $oSelect->order('created DESC');
        
        return $oSelect;
    }

    public function getOffersForWanted($aExtraFilters) {


        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=o.id')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), array('o.id',
                    'o.city',
                    'o.street',
                    'o.price',
                    'o.surface',
                    'o.price_surface'))
                ->columns('(' . $oSubSelect . ') as cover')
                ->where('o.status = ?', Model_DbTable_Offer::STATUS_OPEN)
                ->join(array('o' => 'offer'), 'p.id_offer=o.id', null);



        if (isset($aExtraFilters['province']) && !empty($aExtraFilters['province'])) {
            $oSelect->where("province IN (?)", explode(',', $aExtraFilters['province']));
        }

        if (isset($aExtraFilters['district']) && !empty($aExtraFilters['district'])) {
            $oSelect->where("district LIKE ?", '%' . $aExtraFilters['district'] . '%');
        }

        if (isset($aExtraFilters['city']) && !empty($aExtraFilters['city'])) {
            $oSelect->where("city LIKE ?", '%' . $aExtraFilters['city'] . '%');
        }
        
        if (isset($aExtraFilters['section']) && !empty($aExtraFilters['section'])) {
            $oSelect->where("section IN (?)", explode(',' ,$aExtraFilters['section']));
        }

        if (isset($aExtraFilters['price_from']) && !empty($aExtraFilters['price_from']) && $aExtraFilters['price_from'] != '0.00') {
            $oSelect->where("price >= ?", $aExtraFilters['price_from']);
        }

        if (isset($aExtraFilters['price_to']) && !empty($aExtraFilters['price_to']) && $aExtraFilters['price_to'] != '0.00') {
            $oSelect->where("price <= ?", $aExtraFilters['price_to']);
        }

        if (isset($aExtraFilters['surface_from']) && !empty($aExtraFilters['surface_from']) && $aExtraFilters['surface_from'] != '0.00') {
            $oSelect->where("surface >= ?", $aExtraFilters['surface_from']);
        }


        if (isset($aExtraFilters['surface_to']) && !empty($aExtraFilters['surface_to']) && $aExtraFilters['surface_to'] != '0.00') {
            $oSelect->where("surface <= ?", $aExtraFilters['surface_to']);
        }


        if (isset($aExtraFilters['price_surface_from']) && !empty($aExtraFilters['price_surface_from']) && $aExtraFilters['price_surface_from'] != '0.00') {
            $oSelect->where("price_surface >= ?", $aExtraFilters['price_surface_from']);
        }


        if (isset($aExtraFilters['price_surface_to']) && !empty($aExtraFilters['price_surface_to']) && $aExtraFilters['price_surface_to'] != '0.00') {
            $oSelect->where("price_surface <= ?", $aExtraFilters['price_surface_to']);
        }

        if (isset($aExtraFilters['rooms_from']) && !empty($aExtraFilters['rooms_from']) && $aExtraFilters['rooms_from'] != '0') {
            $oSelect->where("rooms >= ?", $aExtraFilters['rooms_from']);
        }


        if (isset($aExtraFilters['rooms_to']) && !empty($aExtraFilters['rooms_to']) && $aExtraFilters['rooms_to'] != '0') {
            $oSelect->where("rooms <= ?", $aExtraFilters['rooms_to']);
        }

        if (isset($aExtraFilters['floor_from']) && $aExtraFilters['floor_from'] != '') {
            $oSelect->where("floor >= ?", $aExtraFilters['floor_from']);
        }


        if (isset($aExtraFilters['floor_to']) && $aExtraFilters['floor_to'] != '') {
            $oSelect->where("floor <= ?", $aExtraFilters['floor_to']);
        }

        if (isset($aExtraFilters['levels_from']) && !empty($aExtraFilters['levels_from']) && $aExtraFilters['levels_from'] != '0') {
            $oSelect->where("levels >= ?", $aExtraFilters['levels_from']);
        }


        if (isset($aExtraFilters['levels_to']) && !empty($aExtraFilters['levels_to']) && $aExtraFilters['levels_to'] != '0') {
            $oSelect->where("levels <= ?", $aExtraFilters['levels_to']);
        }

        if (isset($aExtraFilters['floors_in_building_from']) && !empty($aExtraFilters['floors_in_building_from']) && $aExtraFilters['floors_in_building_from'] != '0') {
            $oSelect->where("floors_in_building >= ?", $aExtraFilters['floors_in_building_from']);
        }


        if (isset($aExtraFilters['floors_in_building_to']) && !empty($aExtraFilters['floors_in_building_to']) && $aExtraFilters['floors_in_building_to'] != '0') {
            $oSelect->where("floors_in_building <= ?", $aExtraFilters['floors_in_building_to']);
        }

        if (isset($aExtraFilters['year_from']) && !empty($aExtraFilters['year_from']) && $aExtraFilters['year_from'] != '0000') {
            $oSelect->where("year >= ?", $aExtraFilters['year_from']);
        }


        if (isset($aExtraFilters['year_to']) && !empty($aExtraFilters['year_to']) && $aExtraFilters['year_to'] != '0000') {
            $oSelect->where("year <= ?", $aExtraFilters['year_to']);
        }


        if (isset($aExtraFilters['quality']) && !empty($aExtraFilters['quality'])) {
            $oSelect->where("quality IN (?)", explode(',', $aExtraFilters['quality']));
        }

        if (isset($aExtraFilters['installation_quality']) && !empty($aExtraFilters['installation_quality'])) {
            $oSelect->where("installation_quality IN (?)", explode(',', $aExtraFilters['installation_quality']));
        }

        if (isset($aExtraFilters['bulding_type']) && !empty($aExtraFilters['bulding_type'])) {
            $oSelect->where("bulding_type IN (?)", explode(',', $aExtraFilters['bulding_type']));
        }


        if (isset($aExtraFilters['kitchen']) && !empty($aExtraFilters['kitchen'])) {
            $oSelect->where("kitchen IN (?)", explode(',', $aExtraFilters['kitchen']));
        }


        if (isset($aExtraFilters['windows']) && !empty($aExtraFilters['windows'])) {
            $oSelect->where("windows IN (?)", explode(',', $aExtraFilters['windows']));
        }

        if (isset($aExtraFilters['noise']) && !empty($aExtraFilters['noise'])) {
            $oSelect->where("noise IN (?)", explode(',', $aExtraFilters['noise']));
        }

        if (isset($aExtraFilters['heat']) && !empty($aExtraFilters['heat'])) {
            $oSelect->where("heat IN (?)", explode(',', $aExtraFilters['heat']));
        }

        if (isset($aExtraFilters['additional']) && !empty($aExtraFilters['additional'])) {

            $aAdditionals = explode(',', $aExtraFilters['additional']);

            foreach ($aAdditionals as $iAdditional) {
                $oSelect->where('FIND_IN_SET(? , p.additional) > 0', $iAdditional);
            }
        }
        return $oSelect;
        
    }

}

