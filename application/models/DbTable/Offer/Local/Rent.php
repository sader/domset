<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_DbTable_Offer_Local_Rent extends App_Db_Table {

    protected $_name = 'offer_local_rent';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Offer_Local_Rent
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Offer_Local_Rent
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function addOffer($aData) {
        $aInsertData = array();
        $aInsertData['id_offer'] = $aData['id_offer'];
        $aInsertData['purpose'] = $aData['purpose'];
        $aInsertData['rent_additional'] = $aData['rent_additional'];
        $aInsertData['additional_payment'] = $aData['additional_payment'];
        $aInsertData['building_type'] = $aData['building_type'];
        $aInsertData['entry'] = $aData['entry'];
        $aInsertData['height'] = $aData['height'];
        $aInsertData['rooms'] = $aData['rooms'];
        $aInsertData['floor'] = $aData['floor'];
        $aInsertData['levels'] = $aData['levels'];
        $aInsertData['floors_in_building'] = $aData['floors_in_building'];
        $aInsertData['year'] = $aData['year'];
        $aInsertData['quality'] = $aData['quality'];
        $aInsertData['windows'] = $aData['windows'];
        $aInsertData['heat'] = $aData['heat'];
        $aInsertData['available'] = $aData['available'];
        $aInsertData['additional'] = implode(',', $aData['additional']);

        $this->insert($aInsertData);
    }

    public function editOffer($aData, $iOfferId) {
        $aUpdateData = array();
        $aUpdateData['purpose'] = $aData['purpose'];
        $aUpdateData['rent_additional'] = $aData['rent_additional'];
        $aUpdateData['additional_payment'] = $aData['additional_payment'];
        $aUpdateData['building_type'] = $aData['building_type'];
        $aUpdateData['entry'] = $aData['entry'];
        $aUpdateData['height'] = $aData['height'];
        $aUpdateData['rooms'] = $aData['rooms'];
        $aUpdateData['floor'] = $aData['floor'];
        $aUpdateData['levels'] = $aData['levels'];
        $aUpdateData['floors_in_building'] = $aData['floors_in_building'];
        $aUpdateData['year'] = $aData['year'];
        $aUpdateData['quality'] = $aData['quality'];
        $aUpdateData['windows'] = $aData['windows'];
        $aUpdateData['heat'] = $aData['heat'];
        $aUpdateData['available'] = $aData['available'];
        $aUpdateData['additional'] = implode(',', $aData['additional']);

        $this->update($aUpdateData, array('id_offer = ?' => $iOfferId));
    }

    public function removeOffer($iId) {

        $this->delete(array('id = ?' => $iId));
    }

    public function getOffer($iOfferId) {
        return $this->select()
                        ->where('id_offer = ?', $iOfferId)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }

    public function getOffersQuery($aExtraFilters) {


        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=p.id_offer')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), array('o.id',
                    'created' => 'DATE(o.created)',
                    'o.section',
                    'o.price',
                    'o.price_surface',
                    'p.purpose',
                    'o.surface',
                    'p.building_type'))
                ->columns('(' . $oSubSelect . ') as cover')
                ->join(array('o' => 'offer'), 'p.id_offer=o.id', null)
                ->where('o.status  = ?' , Model_DbTable_Offer::STATUS_OPEN); 

        if (isset($aExtraFilters['city']) && !empty($aExtraFilters['city'])) {
            $oSelect->where("city LIKE ?", '%' . $aExtraFilters['city'] . '%');
        }

        if (isset($aExtraFilters['street']) && !empty($aExtraFilters['street'])) {
            $oSelect->where("street LIKE ?", '%' . $aExtraFilters['street'] . '%');
        }


        if (isset($aExtraFilters['year_from']) && !empty($aExtraFilters['year_from'])) {
            $oSelect->where("year >= ?", $aExtraFilters['year_from']);
        }


        if (isset($aExtraFilters['year_to']) && !empty($aExtraFilters['year_to'])) {
            $oSelect->where("year <= ?", $aExtraFilters['year_to']);
        }

        if (isset($aExtraFilters['height_from']) && !empty($aExtraFilters['height_from'])) {
            $oSelect->where("height >= ?", $aExtraFilters['height_from']);
        }


        if (isset($aExtraFilters['height_to']) && !empty($aExtraFilters['height_to'])) {
            $oSelect->where("height <= ?", $aExtraFilters['height_to']);
        }


            if (isset($aExtraFilters['categories']) && !empty($aExtraFilters['categories'])) {


            foreach ($aExtraFilters['categories'] as $iKey => $iCat) {
                $iC = 'cat' . ++$iKey;
                $oSelect->join(array($iC => 'category_offer'), $iC.'.id_offer=o.id AND '.$iC.'.id_category = ' . $iCat, null);
            }
        }

        return $oSelect;
    }
    
         public function getOffersDisplayQuery($aExtraFilters) {



        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=o.id')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

 $oCategories = $this->select()
                     ->setIntegrityCheck(false)
                     ->from(array('co' => 'category_offer'), array('GROUP_CONCAT(CONCAT_WS("|" , c.name, c.info, co.data)  SEPARATOR "|x|")'))
                     ->join(array('c' => 'category'), 'co.id_category=c.id' , null)
         ->where('co.id_offer = o.id');
 
 
  $oNewPriceSelect = $this->select()->from(array('co' => 'category_offer'), array('data'))
                ->setIntegrityCheck(false)
                ->join(array('c' => 'category') , 'co.id_category=c.id' , null)
                ->where('c.name = ?' , 'Nowa cena')
                ->where('co.id_offer = o.id')
                ->limit(1);
 
                
        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name))
                ->columns('(' . $oSubSelect . ') as photo')
                ->join(array('o' => 'offer'), 'p.id_offer=o.id')
                ->columns('(' . $oCategories . ') as categories')
                ->columns('(' . $oNewPriceSelect . ') as new_price')
                ->where('o.status  = ?' , Model_DbTable_Offer::STATUS_OPEN); 
        
            if (isset($aExtraFilters['offer_id']) && !empty($aExtraFilters['offer_id'])) {
            $oSelect->where("id_offer = ?", $aExtraFilters['offer_id']);
        }
        
        
                if (isset($aExtraFilters['city']) && !empty($aExtraFilters['city'])) {
            $oSelect->where("city IN (?)", $aExtraFilters['city']);
        }


       if (isset($aExtraFilters['section']) && !empty($aExtraFilters['section'])) {
            $oSelect->where("section IN (?)", $aExtraFilters['section']);
        }

        if (isset($aExtraFilters['price_from']) && !empty($aExtraFilters['price_from']) && $aExtraFilters['price_from'] != '0.00') {
            $oSelect->where("price >= ?", $aExtraFilters['price_from']);
        }

        if (isset($aExtraFilters['price_to']) && !empty($aExtraFilters['price_to']) && $aExtraFilters['price_to'] != '0.00') {
            $oSelect->where("price <= ?", $aExtraFilters['price_to']);
        }

        if (isset($aExtraFilters['surface_from']) && !empty($aExtraFilters['surface_from']) && $aExtraFilters['surface_from'] != '0.00') {
            $oSelect->where("surface >= ?", $aExtraFilters['surface_from']);
        }


        if (isset($aExtraFilters['surface_to']) && !empty($aExtraFilters['surface_to']) && $aExtraFilters['surface_to'] != '0.00') {
            $oSelect->where("surface <= ?", $aExtraFilters['surface_to']);
        }


        if (isset($aExtraFilters['price_surface_from']) && !empty($aExtraFilters['price_surface_from']) && $aExtraFilters['price_surface_from'] != '0.00') {
            $oSelect->where("price_surface >= ?", $aExtraFilters['price_surface_from']);
        }


        if (isset($aExtraFilters['price_surface_to']) && !empty($aExtraFilters['price_surface_to']) && $aExtraFilters['price_surface_to'] != '0.00') {
            $oSelect->where("price_surface <= ?", $aExtraFilters['price_surface_to']);
        }

        
        if (isset($aExtraFilters['purpose']) && !empty($aExtraFilters['purpose'])) {
            $oSelect->where("purpose IN (?)", $aExtraFilters['purpose']);
        }
        
        
          if (isset($aExtraFilters['building_type']) && !empty($aExtraFilters['building_type'])) {
            $oSelect->where("building_type IN (?)", $aExtraFilters['building_type']);
        }
       

        if (isset($aExtraFilters['quality']) && !empty($aExtraFilters['quality'])) {
            $oSelect->where("quality IN (?)", $aExtraFilters['quality']);
        }


             if (isset($aExtraFilters['categories']) && !empty($aExtraFilters['categories'])) {


            foreach ($aExtraFilters['categories'] as $iKey => $iCat) {
                $iC = 'cat' . ++$iKey;
                $oSelect->join(array($iC => 'category_offer'), $iC.'.id_offer=o.id AND '.$iC.'.id_category = ' . $iCat, null);
            }
        }
        
        if (isset($aExtraFilters['sort']) && !empty($aExtraFilters['sort'])) {
            $aSort = Model_Logic_Offer::getInstance()->getSortingMethodById($aExtraFilters['sort']); 
            $oSelect->order($aSort['sql']);
        }
        else $oSelect->order('created DESC');

        return $oSelect;
    }
    
      public function getOffersForWanted($aExtraFilters)
       {
           return Model_DbTable_Offer_Local_Sell::getInstance()->getOffersForWanted($aExtraFilters); 
       }

}

