<?php

class Model_DbTable_Admin extends Zend_Db_Table_Abstract {

    protected $_name = 'admin';

    const ACCOUNT_TYPE_STANDARD = 1;
    const ACCOUNT_TYPE_MASTER = 2;

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Admin
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Admin
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getListOfAdmins() {
   
        return $this->getListOfAdminsQuery()
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    
    public function getListOfAdminsQuery()
    {
        return $this->select()
                    ->from(array('p' => $this->_name), 
                          array( 'id' , 
                                'login' , 
                                'CONCAT(surname, " ", name) as name' , 
                                'type' , 
                                'phone' , 
                                'email'))
                    ->order('login'); 
    }

    public function getAccountById($iId)
    {
        return $this->select()->where('id = ?' , $iId)->query()->fetch(Zend_Db::FETCH_ASSOC); 
    }



    public function addAccount($aData = array()) {

        $aInsertData = array(
        'login' => $aData['login'],
        'password' => $aData['crypted_password'],
        'name' => $aData['name'],
        'surname' => $aData['surname'],
        'email' => $aData['email'],
        'phone' => $aData['phone'],
        'type' => $aData['type']
        ); 
        
        return $this->insert($aInsertData); 
    }
    
    public function editAccount($iId, $aValidatedData)
    {
        
        $this->update($aValidatedData, array('id = ?' => $iId)); 
        
    }
    
    public function changePasswordToAccount($iId, $sPassword)
    {
        
        $this->update(array('password' => $sPassword) , array('id = ?' => $iId)); 
    }
    
    public function removeAccount($iId)
    {
        $this->delete(array('id = ?' => $iId)); 
    }
}

