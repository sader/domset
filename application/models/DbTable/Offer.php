<?php

class Model_DbTable_Offer extends Zend_Db_Table_Abstract {

    protected $_name = 'offer';

    const STATUS_OPEN = 1;
    const STATUS_FINISHED = 2;
    const STATUS_CANCELLED = 3;
    const STATUS_DELETED = 4;

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Offer
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Offer
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function addOffer($aOfferData) {


        $aTableData = array(
            'id_type' => $aOfferData['type'],
            'title' => $aOfferData['title'],
            'id_admin' => $aOfferData['id_admin'],
            'province' => $aOfferData['province'],
            'district' => $aOfferData['district'],
            'community' => $aOfferData['community'],
            'section' => $aOfferData['section'],
            'city' => $aOfferData['city'],
            'street' => $aOfferData['street'],
            'surface' => $aOfferData['surface'],
            'price' => $aOfferData['price'],
            'price_surface' => $aOfferData['price_surface'],
            'price_negotiable' => $aOfferData['price_negotiable'],
            'description' => $aOfferData['description'],
            'description_pdf' => $aOfferData['description_pdf'],
            'status' => self::STATUS_OPEN,
            'created' => date('Y-m-d H:i:s')
        );

        return $this->insert($aTableData);
    }

    public function editOffer($aOfferData, $iOfferId) {


        $aTableData = array(
            'id_type' => $aOfferData['type'],
            'title' => $aOfferData['title'],
            'id_admin' => $aOfferData['id_admin'],
            'province' => $aOfferData['province'],
            'district' => $aOfferData['district'],
            'community' => $aOfferData['community'],
            'section' => $aOfferData['section'],
            'city' => $aOfferData['city'],
            'street' => $aOfferData['street'],
            'surface' => $aOfferData['surface'],
            'price' => $aOfferData['price'],
            'price_surface' => $aOfferData['price_surface'],
            'price_negotiable' => $aOfferData['price_negotiable'],
            'description' => $aOfferData['description'],
            'description_pdf' => $aOfferData['description_pdf'],
        );

        return $this->update($aTableData, array('id = ?' => $iOfferId));
    }

    public function setMapCoordinates($iId, $aCoord) {
        $aTableData = array(
            'lat' => $aCoord['lat'],
            'lng' => $aCoord['lng']
        );
        $this->update($aTableData, array('id = ?' => $iId));
    }

    public function getOfferBasicInformationById($iId) {

        $oGropSubSelect = $this->select()->from(array('g' => 'gallery'), array('covers' => 'GROUP_CONCAT(name)'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=p.id')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC');

        $oNewPriceSelect = $this->select()->from(array('co' => 'category_offer'), array('data'))
                ->setIntegrityCheck(false)
                ->join(array('c' => 'category'), 'co.id_category=c.id', null)
                ->where('c.name = ?', 'Nowa cena')
                ->where('co.id_offer = ?', $iId)
                ->limit(1);

        $oSubSelect = $this->select()->from(array('g' => 'gallery'), array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=p.id')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

        return $this->select()
                        ->setIntegrityCheck(false)
                        ->from(array('p' => $this->_name))
                        ->join(array('a' => 'admin'), 'a.id=p.id_admin', array('name', 'login', 'surname', 'phone', 'email'))
                        ->columns('(' . $oSubSelect . ') as cover')
                        ->columns('(' . $oGropSubSelect . ') as covers')
                        ->columns('(' . $oNewPriceSelect . ') as new_price')
                        ->where('p.id = ?', $iId)
                        ->query()
                        ->fetch(zend_Db::FETCH_ASSOC);
    }

    public function changeOfferStatus($iOfferId, $iStatus) {
        try {
            $this->update(array('status' => $iStatus, 'updated' => date('Y-m-d H:i:s')), array('id = ?' => $iOfferId));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getOffersArchiveQuery() {

        return $this->select()
                        ->from($this->_name, array('id', 'id_type', 'location' => 'CONCAT_WS(" " , city, section, street)', 'price', 'surface', 'price_surface', 'updated', 'status'))
                        ->columns('(SELECT name from gallery where id_offer = offer.id and type = 1 order by ord ASC LIMIT 1) as cover')
                        ->where('status != ?', self::STATUS_OPEN)
                        ->order('created DESC');
    }

    public function getNewestQuery() {

        return $this->select()
                        ->from($this->_name)
                        ->columns('(SELECT name from gallery where id_offer = offer.id and type = 1 order by ord ASC LIMIT 1) as photo')
                        ->where('status = ?', self::STATUS_OPEN)
                        ->order('created DESC');
    }

    public function getUniqueCitiesByType($iType) {
        
        return $this->select()
                        ->distinct(true)
                        ->from($this->_name, array('city'))
                        ->where('id_type = ?', $iType)
                        ->where('status = ?', self::STATUS_OPEN)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function getListOfActiveOffersQuery() {
        return $this->select()
                        ->from($this->_name, array('id',
                            'id_type',
                            'city',
                            'street',
                            'price',
                            'surface',
                            'price_surface'))
                        ->where('status = ?', self::STATUS_OPEN)
                        ->columns('(SELECT name from gallery where id_offer = offer.id and type = 1 order by ord ASC LIMIT 1) as cover')
                        ->order('created DESC');
    }

    public function getListOfOffersByType($sType) {
        return $this->select()
                        ->from($this->_name, array('id',
                            'CONCAT_WS(" " , p.city, p.section, p.street)',
                            'price',
                            'surface',
                            'price_surface',
                            'status'))
                        ->where('id_type = ?', $sType)
                        ->columns('(SELECT name from gallery where id_offer = offer.id and type = 1 order by ord ASC LIMIT 1) as photo')
                        ->order('created DESC');
    }

    public function getListOfActiveOffers() {
        return $this->select()
                        ->from($this->_name, array('id', 'id_type', 'location' => 'CONCAT_WS(" " , city, section, street)', 'price', 'surface', 'price_surface', 'status'))
                        ->columns('(SELECT name from gallery where id_offer = offer.id and type = 1 order by ord ASC LIMIT 1) as cover')
                        ->columns('(SELECT count(id_offer) as offer from promoted  where id_offer = offer.id) as promoted')
                        ->where('status = ?', self::STATUS_OPEN)
                        ->order('title ASC');
    }

    public function getSectionsByCity($aCities, $iType) {
        return $this->select()
                        ->distinct()
                        ->from($this->_name, array('section'))
                        ->where('city IN (?)', $aCities)
                        ->where('id_type = ?', $iType)
                        ->where('status = ?' , self::STATUS_OPEN)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function getCitiesByCommunities($aCommunities, $iType) {
        return $this->select()
                        ->distinct()
                        ->from($this->_name, array('city'))
                        ->where('community IN (?)', $aCommunities)
                        ->where('status = ?' , self::STATUS_OPEN)
                        ->where('id_type = ?', $iType)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function getCommunnitiesByDistrict($aDistricts, $iType) {
        return $this->select()
                        ->distinct()
                        ->from($this->_name, array('community'))
                        ->where('district IN (?)', $aDistricts)
                        ->where('status = ?' , self::STATUS_OPEN)
                        ->where('id_type = ?', $iType)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function getUniqueDistrictsByType($iType) {
        return $this->select()
                        ->distinct(true)
                        ->from($this->_name, array('district'))
                        ->where('id_type = ?', $iType)
                        ->where('status = ?', self::STATUS_OPEN)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
        
    }

}

