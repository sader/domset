<?php

class Model_DbTable_Client extends Zend_Db_Table_Abstract {

    protected $_name = 'client';

    const ACCOUNT_TYPE_CALLED = 4;
    const ACCOUNT_TYPE_OWNER = 3;
    const ACCOUNT_TYPE_LOOKER = 2;
    const ACCOUNT_TYPE_VISITER = 1;

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Client
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Client
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }


   
    public function getClientById($iId)
    {
        return $this->select()->where('id = ?' , $iId)->query()->fetch(zend_Db::FETCH_ASSOC); 
    }
    
    public function getClientByPhone($sPhone)
    {
        return $this->select()->where('phone LIKE ?' , $sPhone.'%')
                             ->orWhere('phone_alt LIKE ?' , $sPhone.'%')
                             ->orWhere('surname LIKE ?', $sPhone.'%')
                             ->query()
                             ->fetchAll(zend_Db::FETCH_ASSOC); 
    }
    
       
    
    
    
      public function getClientsById($aId)
    {
        return $this->select()->where('id IN(?)' , $aId)->query()->fetchAll(zend_Db::FETCH_ASSOC); 
    }
    
    public function editClient($aData , $iId)
    {
        $aData['type'] = implode(',' , $aData['type']); 
        
        $this->update($aData, array('id = ?' => $iId)); 
    }
    



        public function addClient($aData = array()) {
        
            $aData['type'] = implode(',' , $aData['type']); 
            
            
            return $this->insert($aData); 
    }
    
    public function getClientsByType($iType)
    {
        return $this->select()
                    ->from($this->_name, array('id' , 'phone' , 'CONCAT_WS(" " , phone, surname, name) as client'))
                    ->where('FIND_IN_SET(? , type) > 0', $iType)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC); 
    }
    
    public function getClients()
    {
         return $this->select()
             ->from($this->_name, array('id',
                                        'client' => 'CONCAT(phone, " - " , COALESCE(surname, ""), " " , COALESCE(name, ""))'))
                                         ->order('client ASC')
                                         ->query()
                                         ->fetchAll(Zend_Db::FETCH_ASSOC); 
               
        
        
    }

        public function getSellers()
    {
        
        return $this->select()
             ->from($this->_name, array('id' , 'CONCAT(surname , " ", name, " - " , docid) as person'))
             ->where('type = ?' , self::ACCOUNT_TYPE_OWNER)
             ->order('person DESC')
             ->query()
             ->fetchAll(Zend_Db::FETCH_ASSOC); 
    }
    
        public function getBuyers()
    {
        
        return $this->select()
             ->from($this->_name, array('id' , 'CONCAT("surname" , " ", "name" " - " , "docid") as person'))
             ->where('type = ?' , self::ACCOUNT_TYPE_LOOKER)
             ->order('person DESC')
             ->query()
             ->fetchAll(Zend_Db::FETCH_ASSOC); 
    }
    
    public function getListOfClients()
    {
        
        return $this->select()
             ->from($this->_name, array('id',
                                        'client' => 'CONCAT(surname, " " , name)' , 
                                        'address' => 'TRIM(CONCAT(postcode, " " , city, " " , street, " " , house, " " , flat))' , 
                                        'email' ,
                                        'pesel' ,
                                        'phone',
                                        'dowod_osobisty' => 'docid',
                                        'type'))
                                         ->order('client ASC');
             
    }
    
}

