<?php

class Model_DbTable_Visibility extends Zend_Db_Table_Abstract {

    protected $_name = 'visibility';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Visibility
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Visibility
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function addVisibility($iOfferId, $sData) {

        $sql = "INSERT INTO visibility (id_offer, data) VALUES (?, ?)
                ON DUPLICATE KEY UPDATE id_offer = ?, data = ?";
        $values = array("id_offer" => $iOfferId, "data" => $sData);
        $this->getAdapter()->query($sql, array_merge(array_values($values), array_values($values)));

        return true;
    }

    public function getVisibility($iOfferId) {
        return $this->select()
                        ->where('id_offer = ?', $iOfferId)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }

}

