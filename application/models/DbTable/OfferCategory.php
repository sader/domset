<?php

class Model_DbTable_OfferCategory extends Zend_Db_Table_Abstract {
    const CATEGORY_HAS_ADDITIONAL_DATA = 1;
    const CATEGORY_HAS_NOT_ADDITIONAL_DATA = 0;

    protected $_name = 'category_offer';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_OfferCategory
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_OfferCategory
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function deleteCategoriesByIdOffer($iId) {
        $this->delete(array('id_offer = ?' => $iId));
    }
    
    public function getCategoriesForOffer($iOfferId)
    {
        return $this->select()
                    ->where('id_offer = ?' , $iOfferId)
                    ->query()
                    ->fetchAll(Zend_db::FETCH_ASSOC); 
    }
    
        public function getCategoriesDataForOffer($iOfferId)
    {
        return $this->select()
                    ->setIntegrityCheck(false)
                    ->from($this->_name)
                    ->where('id_offer = ?' , $iOfferId)
                    ->join('category' , 'category.id=category_offer.id_category')
                    ->query()
                    ->fetchAll(Zend_db::FETCH_ASSOC); 
    }

    public function addCategoriesForOffer($aData, $iOfferId) {

        foreach ($aData as $aCategoryBinder) {
            $aInsertData = array(
                'id_category' => $aCategoryBinder['id_category'],
                'id_offer' => $iOfferId,
                'data' => $aCategoryBinder['data']
            );

            $this->insert($aInsertData);
        }
    }
    
    public function removeCategoriesByOfferId($iOfferId)
    {
        $this->delete('id_offer = '.$iOfferId); 
    }
    
    public function isOfferMarkerAsNoProvision($iOfferId)
    {
        return $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('co' => $this->_name))
                    ->join(array('c' => 'category') , 'c.id=co.id_category', null)
                    ->where('c.name = ?' , Model_Logic_Category::NO_PROVISION)
                    ->where('co.id_offer = ?' , $iOfferId)
                    ->query()
                    ->fetch(Zend_Db::FETCH_ASSOC); 
        
        
    }

}

