<?php

class Model_DbTable_Job extends Zend_Db_Table_Abstract {

    protected $_name = 'jobs';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Job
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Job
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function add($aData) {

        $aData['added'] = date('Y-m-d H:i:s');
        $aData['updated'] = date('Y-m-d H:i:s');
        return $this->insert($aData);
    }

    public function edit($aData, $iId) {

        $aData['updated'] = date('Y-m-d H:i:s');

        return $this->update($aData, array('id = ?' => $iId));
    }

    public function remove($iId) {
        return $this->delete(array('id = ?' => $iId));
    }

    public function getListOfJobs($sSpecificDate = null) {



        $oSelect = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name),
                        array('p.id',
                    'id_client' => 'c.id',
                    'client' => 'CONCAT_WS(" ", c.phone, c.surname, c.name)',
                    'location' => 'CONCAT_WS(" " , p.city, p.section, p.street)',
                    'offer_type',
                    'surface',
                    'rooms',
                    'floor',
                    'price',
                    'price_surface',
                    'status',
                    'zadanie' => 'CONCAT_WS(" ",  date, time)',
                    'dodane' => 'added',
                    'aktualizowane' => 'updated'))
                ->join(array('c' => 'client'), 'c.id=p.id_client', null);


        if ($sSpecificDate !== null) {
            $oSelect->where('date <= ?', $sSpecificDate)
                    ->where('date != ?' , '0000-00-00')
                    ->order('time DESC');
        } else {
            $oSelect->order('updated DESC');
        }

        return $oSelect;
    }

    public function getListOfJobsForClient($iClientId) {
        return $this->select()
                        ->setIntegrityCheck(false)
                        ->from(array('p' => $this->_name),
                                array('p.id',
                            'c.phone',
                            'location' => 'CONCAT_WS(" " , p.city, p.section, p.street)',
                            'surface',
                            'rooms',
                            'floor',
                            'offer_type',
                            'price',
                            'price_surface',
                            'status',
                            'zadanie' => 'CONCAT_WS(" ", date, time)',
                            'dodane' => 'added',
                            'aktualizowane' => 'updated'))
                        ->join(array('c' => 'client'), 'c.id=p.id_client', null)
                        ->where('p.id_client = ?', $iClientId)
                        ->order('p.added');
    }

    public function getJob($iId) {
        return $this->select()->where('id = ?', $iId)->query()->fetch(Zend_Db::FETCH_ASSOC);
    }

}

