<?php

class Model_DbTable_Presentation extends Zend_Db_Table_Abstract {

    protected $_name = 'presentation';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Presentation
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Presentation
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function add($aData) {

        $aData['added'] = date('Y-m-d H:i:s');
        return $this->insert($aData);
    }

    public function edit($aData, $iId) {
        return $this->update($aData, array('id = ?' => $iId));
    }

    public function remove($iId) {
        return $this->delete(array('id = ?' => $iId));
    }

    public function getListOfPresentationsForOffer($iOfferId) {


       return $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), array('id', 'status' , 'info' , 'CONCAT_WS(" ", date, time) as event' , 'id_offer'))
                ->join(array('c' => 'client'), 'c.id=p.id_client', array('client' => 'CONCAT_WS(" ", c.phone, c.surname, c.name)' , 'id_client' => 'c.id'))
                ->where('p.id_offer  = ?', $iOfferId); 


    }
    
    public function getPresentationForClient($iClientId)
    {
        
           return $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name), array('id', 'status' , 'info' , 'CONCAT_WS(" ", date, time) as event' , 'id_offer'))
                ->join(array('o' => 'offer'), 'o.id=p.id_offer'  , array('offer_id' => 'id' , 'local' => 'CONCAT_WS(" " , city, section, street)'))
                ->columns('(SELECT name from gallery where id_offer = o.id and type = 1 order by ord ASC LIMIT 1) as photo')
                ->where('p.id_client  = ?', $iClientId); 
        
        
    }
    
    public function getPresentation($iId)
    {
        return $this->select()->where('id = ?' , $iId)->query()->fetch(Zend_Db::FETCH_ASSOC); 
    }

}

