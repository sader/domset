<?php

class Model_DbTable_News extends Zend_Db_Table_Abstract {

    protected $_name = 'news';

    

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_News
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_News
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    /**
     * Lista eventów
     * @return Zend_Db_Select 
     */
    public function getListQuery()
    {
        return $this->select(); 
    }
    
    
     /**
     * Lista eventów
     * @return Zend_Db_Select 
     */
    public function getPageListQuery()
    {
        return $this->select()->where('display  < ?' , date('Y-m-d H:i:s'))->order('display DESC'); 
    }
    
    
    /**
     * Dodaj event
     * @param array $aEvent
     * @return integer (last_inserted_id) 
     */
    public function add($aEvent)
    {
        return $this->insert($aEvent); 
    }
    
    public function edit($aEvent, $iEventId)
    {
        $this->update($aEvent, array('id = ?' => $iEventId)); 
    }


    
    /**
     * Znajdź po Id
     * @param integer $iId
     * @return array 
     */
    public function getById($iId)
    {
        return $this->select()
                    ->where('id = ?' , $iId)
                    ->query()
                    ->fetch(Zend_db::FETCH_ASSOC); 
    }
    
    public function remove($iId)
    {
        $this->delete(array('id = ?' => $iId)); 
    }
    
}