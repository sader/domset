<?php

class Model_DbTable_Category extends Zend_Db_Table_Abstract {
    const CATEGORY_HAS_ADDITIONAL_DATA = 1;
    const CATEGORY_HAS_NOT_ADDITIONAL_DATA = 0;

    protected $_name = 'category';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Category
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Category
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getListOfCategories() {
        return $this->select()
                        ->order('name')
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }
    
    public function addCategory($aData)
    {
        $aData['type'] = implode(',', $aData['type']); 
        
        $this->insert($aData); 
    }
    
    public function editCategory($aData, $iId)
    {
        
        $aData['type'] = implode(',', $aData['type']); 
        $this->update($aData, array('id = ?' => $iId)); 
        
    }
    
    public function removeCategory($iId)
    {
        $this->delete(array('id = ?' => $iId)); 
    }
    
    public function getCategoryById($iId)
    {
        return $this->select()
                    ->where('id = ?' , $iId)
                    ->query()
                    ->fetch(Zend_Db::FETCH_ASSOC); 
    }
    
    public function getListOfCategoriesQuery()
    {
        return $this->select()
                    ->from($this->_name)
                            
                        ->order('name'); 
        
    }
    
    public function getByType($iType)
    {
        return $this->select()
                    ->where('FIND_IN_SET(? , type) > 0' , $iType)
                    ->order('name')
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC); 
    }

}

