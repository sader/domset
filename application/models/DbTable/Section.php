<?php

class Model_DbTable_Section extends Zend_Db_Table_Abstract {
    
    protected $_name = 'section';
    
    
    /**
     * Instancja klasy.
     * 
     * @var  Model_DbTable_Section
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return  Model_DbTable_Section
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    public function getListOfSectionForDefaultCity()
    {
        return $this->select()
                    ->order('name')
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC); 
    }
    
    public function addSection($sName)
    {
        return $this->insert(array('name' => $sName)); 
    }

    
}

