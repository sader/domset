<?php

class Model_DbTable_Agreement extends Zend_Db_Table_Abstract {
    

    protected $_name = 'agreement';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Agreement
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Agreement
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getAgreementForOffer($iOfferId) {
      
        return $this->select()
                    ->setIntegrityCheck(false)
                        ->from($this->_name)
                        ->where('id_offer = ?' , $iOfferId)
                        ->columns('(SELECT GROUP_CONCAT(filename) from agreement_files where id_offer = "'.$iOfferId.'") as files')
                        ->columns('(SELECT GROUP_CONCAT(id_owner) from agreement_client where id_offer = "'.$iOfferId.'") as owners')
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }
    
    
      
    
    
    public function editAgreement($aData, $iId)
    {
        $aUpdateData = array(
        'postcode'  => $aData['postcode'],
	'city'   => $aData['city'],
	'street'  => $aData['street'],
	'house'  => $aData['house'],
	'flat'   => $aData['flat'],
	'record_number'   => $aData['record_number'],
	'land_and_mortgage_register'   => $aData['land_and_mortgage_register'],
	'agreement_type'   => $aData['agreement_type'],
	'commission_precentage'   => $aData['commission_precentage'],
	'commission_value'   => $aData['commission_value'],
	'agreement_date'   => $aData['agreement_date'],
	'agreement_end'   => $aData['agreement_end'],
	'information'   => $aData['information'],
	'time'   => $aData['time'],
	'person'   => $aData['person'],
	'phone'   => $aData['phone'],
	'email'   => $aData['email']
        ); 
        $this->update($aUpdateData, array('id_offer = ?' => $iId));  
    }
    
    public function addAgreement($aData)
    {   
        $aInsertData = array(
        'id_offer' => $aData['id_offer'],
        'postcode'  => $aData['postcode'],
	'city'   => $aData['city'],
	'street'  => $aData['street'],
	'house'  => $aData['house'],
	'flat'   => $aData['flat'],
	'record_number'   => $aData['record_number'],
	'land_and_mortgage_register'   => $aData['land_and_mortgage_register'],
	'agreement_type'   => $aData['agreement_type'],
	'commission_precentage'   => $aData['commission_precentage'],
	'commission_value'   => $aData['commission_value'],
	'agreement_date'   => $aData['agreement_date'],
	'agreement_end'   => $aData['agreement_end'],
	'information'   => $aData['information'],
	'time'   => $aData['time'],
	'person'   => $aData['person'],
	'phone'   => $aData['phone'],
	'email'   => $aData['email']
        );
        
        $this->insert($aInsertData); 
    }

}

