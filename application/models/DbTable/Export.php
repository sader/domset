<?php

class Model_DbTable_Export extends Zend_Db_Table_Abstract {

    const TYPE_ADDED = 1;
    const TYPE_EDITED = 2;
    const TYPE_RELOADED = 3;
    const TYPE_DELETED = 4;

    protected $_name = 'export';

    /**
     * Instancja klasy.
     *
     * @var Model_DbTable_Export
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------

    /**
     * Zwraca instancje klasy.
     *
     * @return Model_DbTable_Export
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getExportsForOffer($iOfferId) {
        return $this->select()
                        ->from($this->_name)
                        ->where('id_offer  = ?', $iOfferId)
                        ->order('date DESC');
    }

    public function addExport($iOfferId, $iExportId, $iRemoteId = null, $aData = null , $iType = self::TYPE_ADDED) {

        $aInsertArray = array(
            'id_offer' => $iOfferId,
            'type' => $iType,
            'id_export' => $iExportId,
            'date' => date('Y-m-d H:i:s'),
            'remote_id' => $iRemoteId,
            'data' => is_array($aData) ? serialize($aData) : $aData
        );
        return $this->insert($aInsertArray);
    }

    public function editExport($iId, $iType, $aData = array(), $iRemoteId = null, $sDate = null) {

        $aUpdateArray = array(
            'type' => $iType,
            'data' => empty($aData) ? '' : serialize($aData)
        );

        if ($iRemoteId !== null) {
            $aUpdateArray['remote_id'] = $iRemoteId;
        }


         if ($sDate !== null) {
            $aUpdateArray['date'] = $sDate;
        }

        $this->update($aUpdateArray, array('id = ?' => $iId));
    }

    public function isExportExist($iOfferId) {
        $aOffer = $this->select()
                ->where('id_offer = ? ', $iOfferId)
                ->query()
                ->fetch(Zend_Db::FETCH_NUM);
        return (!empty($aOffer));
    }

    public function getExportById($iId) {
        return $this->select()
                        ->from($this->_name)
                        ->where('id = ?', $iId)
                        ->query()
                        ->fetch(Zend_Db::FETCH_ASSOC);
    }

    public function getExportsByAdapter($iId, $sDate = null) {



        $aQuery = $this->select()
                        ->from($this->_name)
                        ->where('id_export = ?', $iId);
            
        if($sDate !== null)
        {
            $aQuery->where('date > ?' , $sDate); 
        }
        
        return $aQuery->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    public function getActiveOffersByAdapter($iId) {
        return $this->select()
                        ->from($this->_name, 'data')
                        ->where('id_export = ?', $iId)
                        ->where('type != ?', self::TYPE_DELETED)
                        ->query()
                        ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

}

