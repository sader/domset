<?php

class Model_DbTable_Wanted extends Zend_Db_Table_Abstract {

    protected $_name = 'wanted';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Wanted
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Offer
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function addWanted($aData) {

        $aTableData = array(
            'id_type' => $aData['type'],
            'id_client' => $aData['id_client'],
            'province' => is_array($aData['province']) ? implode(',',
                            $aData['province']) : null,
            'district' => $aData['district'],
            'section' => is_array($aData['section']) ? implode(',', $aData['section']) : null,
            'city' => $aData['city'],
            'street' => $aData['street'],
            'surface_from' => $aData['surface_from'],
            'surface_to' => $aData['surface_to'],
            'price_from' => $aData['price_from'],
            'price_to' => $aData['price_to'],
            'price_surface_from' => $aData['price_surface_from'],
            'price_surface_to' => $aData['price_surface_to'],
            'info' => $aData['info'],
            'created' => date('Y-m-d H:i:s')
        );

        return $this->insert($aTableData);
    }

    public function edit($aData, $iOfferId) {

        $aTableData = array(
            'id_type' => $aData['type'],
            'id_client' => $aData['id_client'],
            'province' => is_array($aData['province']) ? implode(',',
                            $aData['province']) : null,
            'district' => $aData['district'],
            'section' => is_array($aData['section']) ? implode(',', $aData['section']) : null,
            'city' => $aData['city'],
            'street' => $aData['street'],
            'surface_from' => $aData['surface_from'],
            'surface_to' => $aData['surface_to'],
            'price_from' => $aData['price_from'],
            'price_to' => $aData['price_to'],
            'price_surface_from' => $aData['price_surface_from'],
            'price_surface_to' => $aData['price_surface_to'],
            'info' => $aData['info'],
            'created' => date('Y-m-d H:i:s')
        );

        return $this->update($aTableData, array('id = ?' => $iOfferId));
    }

    public function remove($iId) {
        return $this->delete(array('id = ?' => $iId));
    }

    public function getWantedBasicInformationById($iId) {

        $oGropSubSelect = $this->select()->from(array('g' => 'gallery'),
                        array('covers' => 'GROUP_CONCAT(name)'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=p.id')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC');

        $oSubSelect = $this->select()->from(array('g' => 'gallery'),
                        array('name'))
                ->setIntegrityCheck(false)
                ->where('g.id_offer=p.id')
                ->where('g.type = ?', Model_DbTable_Gallery::TYPE_VISIBLE)
                ->order('ord ASC')
                ->limit(1);

        return $this->select()
                        ->setIntegrityCheck(false)
                        ->from(array('p' => $this->_name))
                        ->join(array('a' => 'client'), 'a.id=p.id_client',
                                array('name', 'surname', 'phone', 'email'))
                        ->columns('(' . $oSubSelect . ') as cover')
                        ->columns('(' . $oGropSubSelect . ') as covers')
                        ->where('p.id = ?', $iId)
                        ->query()
                        ->fetch(zend_Db::FETCH_ASSOC);
    }

    

    public function getListOfWantedForUser($iUserId) {

        return $this->select()
                        ->from($this->_name,
                                array('id_wanted' => 'id',
                            'id_type',
                            'price_from',
                            'price_to',
                            'surface_from',
                            'surface_to',
                            'info'))
                        ->where('id_client = ?', $iUserId)
                        ->order('created DESC');
    }

    public function getListOfWantedOffersQuery() {

        return $this->select()
                        ->setIntegrityCheck(false)
                        ->from(array('p' => $this->_name),
                                array('id_wanted' => 'id',
                            'id_type',
                            'price_from',
                            'price_to',
                            'surface_from',
                            'surface_to',
                            'info'))
                        ->join(array('c' => 'client'), 'c.id=p.id_client',
                                array('client_id' => 'c.id', 'client' => 'CONCAT_WS(" " , phone,  "\n" ,surname, name)'))
                        ->order('p.created DESC');
                
    }

}

