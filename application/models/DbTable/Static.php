<?php

class Model_DbTable_Static extends Zend_Db_Table_Abstract {

    protected $_name = 'pages';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Static
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Static
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    /**
     * Lista Staticów
     * @return Model_DbTable_Static 
     */
    public function getListQuery() {
        return $this->select()->order('name ASC');
    }

    public function getList() {
        return $this->getListQuery()->query()->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    /**
     * Dodaj Static
     * @param array $aStatic
     * @return integer (last_inserted_id) 
     */
    public function add($aStatic) {
        return $this->insert($aStatic);
    }

    public function edit($aStatic, $iStaticId) {
        $this->update($aStatic, array('id = ?' => $iStaticId));
    }

    /**
     * Znajdź po Id
     * @param integer $iId
     * @return array 
     */
    public function getById($iId) {
        return $this->select()
                        ->where('id = ?', $iId)
                        ->query()
                        ->fetch(Zend_db::FETCH_ASSOC);
    }

    /**
     * Znajdź po url
     * @param integer $iId
     * @return array 
     */
    public function getByUrl($iId) {
        return $this->select()
                        ->where('name = ?', $iId)
                        ->query()
                        ->fetch(Zend_db::FETCH_ASSOC);
    }

    public function remove($iId) {
        $this->delete(array('id = ?' => $iId));
    }

}