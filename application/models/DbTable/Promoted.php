<?php

class Model_DbTable_Promoted extends Zend_Db_Table_Abstract {

    protected $_name = 'promoted';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_Admin
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_Promoted
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function clearPromoted() {
        $this->getAdapter()->query('delete from promoted');
    }

    public function addPromoted($iId) {
        $this->insert(array('id_offer' => $iId));
    }

    public function removePromoted($iId) {
        $this->delete(array('id_offer = ?' => $iId));
    }

    public function getListOfPromotedOffers() {
        $this->select()
                ->setIntegrityCheck(false)
                ->from(array('p' => $this->_name))
                ->join(array('o' => 'offer'), 'p.id = p.id_offer',
                        array('title'))
                ->order('p.id ASC')
                ->query()
                ->fetchAll(Zend_Db::FETCH_ASSOC);
    }

    
        
        public function getPromotions() {

       
        return $this->select()
                        ->setIntegrityCheck(false)
                        ->from(array('p' => $this->_name))
                        ->join(array('o' => 'offer') , 'o.id=p.id_offer')
                        ->columns('(SELECT name from gallery where id_offer = p.id_offer and type = 1 order by ord ASC LIMIT 1) as photo')
                        ->order('p.id ASC')
                        ->query()
                        ->fetchAll(zend_Db::FETCH_ASSOC);
    }
    
    
      public function getPromotionsQuery() {

       
        return $this->select()
                        ->setIntegrityCheck(false)
                        ->from(array('p' => $this->_name))
                        ->join(array('o' => 'offer') , 'o.id=p.id_offer')
                        ->columns('(SELECT name from gallery where id_offer = p.id_offer and type = 1 order by ord ASC LIMIT 1) as photo')
                        ->order('p.id ASC'); 
                        
                        
    }


}

