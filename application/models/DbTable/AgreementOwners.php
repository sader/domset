<?php

class Model_DbTable_AgreementOwners extends Zend_Db_Table_Abstract {

    protected $_name = 'agreement_client';

    /**
     * Instancja klasy.
     * 
     * @var Model_DbTable_AgreementOwners
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_DbTable_AgreementOwners
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function addOwnersToAgrement($aTranslatedData) {
        foreach ($aTranslatedData as $aData) {
            $this->insert($aData);
        }
    }

    public function removeOwnersFromAgreement($iOfferId) {
        $this->delete(array('id_offer = ?' => $iOfferId));
    }

    public function getOffersByAgreementClient($iClientId) {
 

     return $this->select()->setIntegrityCheck(false)
                        ->from(array('a' => $this->_name), null)
                        ->join(array('o' => 'offer'), 'a.id_offer=o.id', array('id',
                                'id_type', 'location' => 'CONCAT_WS(" " , city, section, street)',
                                'price', 'surface', 'price_surface', 'status'))
                        ->columns('(SELECT name from gallery where id_offer = o.id and type = 1 order by ord ASC LIMIT 1) as cover')
                        ->where('a.id_owner = ?', $iClientId);
       
    }

}

