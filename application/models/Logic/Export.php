<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Export extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Export
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Export
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public static function getRegisteredAdapters() {
        return array(
            Model_Logic_Export_Gratka::ID => array('model' => 'Model_Logic_Export_Gratka',
                'label' => 'Gratka.pl',
                'id' => Model_Logic_Export_Gratka::ID),
            Model_Logic_Export_Otodom::ID => array('model' => 'Model_Logic_Export_Otodom',
                'label' => 'Otodom.pl',
                'id' => Model_Logic_Export_Otodom::ID),
            Model_Logic_Export_Olx::ID => array('model' => 'Model_Logic_Export_Olx',
                'label' => 'Olx.pl',
                'id' => Model_Logic_Export_Olx::ID),
            Model_Logic_Export_Tablica::ID => array('model' => 'Model_Logic_Export_Tablica',
                'label' => 'Tablica.pl',
                'id' => Model_Logic_Export_Tablica::ID),
            Model_Logic_Export_Domimporta::ID => array('model' => 'Model_Logic_Export_Domimporta',
                'label' => 'Domimporta.pl',
                'id' => Model_Logic_Export_Domimporta::ID),
        );
    }

    public static function getRegisteredAdatersLabels() {
        return array(
            Model_Logic_Export_Gratka::ID => 'Gratka.pl',
            Model_Logic_Export_Otodom::ID => 'Otodom.pl',
            Model_Logic_Export_Olx::ID => 'Olx.pl',
            Model_Logic_Export_Tablica::ID => 'Tablica.pl',
            Model_Logic_Export_Domimporta::ID => 'Domimporta.pl'
        );
    }

    public static function getAdapterById($iId) {

        $aAdapters = self::getRegisteredAdapters();
        return isset($aAdapters[$iId]['model']) ? new $aAdapters[$iId]['model']() : null;
    }

    public function manageAdd(App_Controller_Admin_Abstract $oCtrl) {
        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }
        else
            $oCtrl->view->id_offer = $iId;

        if (($iAdapterId = $oCtrl->getRequest()->getParam('adapter', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }
         $oCtrl->view->offer = Model_Logic_Offer::getInstance()->getDetailedOffer($iId);
        $oExportAdapter = $this->getAdapterById($iAdapterId);
        $oExportAdapter->manageAdd($oCtrl);
    }

    public function manageEdit(App_Controller_Admin_Abstract $oCtrl) {
        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }
        if (($aExport = Model_DbTable_Export::getInstance()->getExportById($iId)) == null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }

        $oCtrl->view->id_offer = $aExport['id_offer']; 

         $oCtrl->view->offer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);
        $oExportAdapter = $this->getAdapterById($aExport['id_export']);
        $oExportAdapter->manageEdit($oCtrl, $aExport);
    }

    public function manageRemove(App_Controller_Admin_Abstract $oCtrl) {
        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }

        if (($aExport = Model_DbTable_Export::getInstance()->getExportById($iId)) == null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }
        
       //  $this->view->offer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_export']);
        
        $oExportAdapter = $this->getAdapterById($aExport['id_export']);
        $oExportAdapter->manageRemove($oCtrl, $aExport);
    }

    public function manageReload(App_Controller_Admin_Abstract $oCtrl) {
        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }
       
        if (($aExport = Model_DbTable_Export::getInstance()->getExportById($iId)) == null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }

        $oCtrl->view->offer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);
        
        $oExportAdapter = $this->getAdapterById($aExport['id_export']);
        $oExportAdapter->manageReload($oCtrl, $aExport);
    }

    public function manageList(App_Controller_Admin_Abstract $oCtrl) {

        if (($iOfferId = $oCtrl->getRequest()->getParam('offer', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }
       
        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($iOfferId);

        $oGrid = new Admin_Grid_Exports($iOfferId);
        return $this->set('adapters', self::getRegisteredAdapters())
                        ->set('id_offer', $iOfferId)
                        ->set('offer', $aOffer)
                        ->set('list', $oGrid->deploy())->getView();
    }

}
