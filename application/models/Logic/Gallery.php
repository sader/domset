<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Gallery extends Model_Logic_Abstract {
    const IMAGE_WIDTH = 640;
    const IMAGE_HEIGHT = 480;
    const IMAGE_THUMB_WIDTH = 128;
    const IMAGE_THUMB_HEIGHT = 96;
    const IMAGE_EXTENSTION = 'jpg';
    const IMAGE_PATH = '/images/offer/';

    private $_sUploadFieldName = 'fd-file';
    private $_iOfferId = null;

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Gallery
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Gallery
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function setOfferId($iId) {
        $this->_iOfferId = $iId;
    }

    public function getOfferId() {
        return $this->_iOfferId;
    }

    public function isOfferIdSet() {
        return!empty($this->_iOfferId);
    }

    public function displayImage(Zend_Controller_Action $oCtrl) {
        $sImage = $oCtrl->getRequest()->getParam('name');
        $iOfferId = $oCtrl->getRequest()->getParam('id');
        $sWidth = $oCtrl->getRequest()->getParam('width', self::IMAGE_WIDTH);
        $sHeight = $oCtrl->getRequest()->getParam('height', self::IMAGE_HEIGHT);


        $sCalledFile = $this->getImageFullPath($sImage, $iOfferId);
        $sFileToDisplay = file_exists($sCalledFile) ? $sCalledFile : APPLICATION_PATH . '/../public_html/images/empty.jpg';



        include('WideImage/WideImage.php');
        return WideImage::loadFromFile($sFileToDisplay)->resize($sWidth, $sHeight);
    }

    public function manageOfferGalleryForAdmin(App_Controller_Admin_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }
        $this->setOfferId($iId);

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($iId);

        $oForm = new Admin_Form_Gallery();

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {
            $this->saveGallery($oCtrl->getRequest()->getPost()) ? $oCtrl->successMessage('Galeria zapisana') : $oCtrl->errorMessage('Problem z zapisaniem galerii');
        }

        $aGalleryImages = Model_DbTable_Gallery::getInstance()->getGalleryForOffer($iId);
        $aImages = $this->translateDbDataToForm($aGalleryImages);
      //  $this->cleanUpGalleryDir($aGalleryImages);
        $oForm->populate($aImages);


        $oCtrl->view->headScript()
                ->appendScript($this->__setJsInput($oCtrl))
                ->appendFile('/scripts/filedrop-min.js')
                ->appendFile('/scripts/fancybox/jquery.fancybox.pack.js')
                ->appendFile('/scripts/manager/admin/offer/gallery.js');

        $oCtrl->view->headLink()->appendStylesheet('/scripts/fancybox/jquery.fancybox.css');


        return $this->set('form', $oForm)
                        ->set('offer', $aOffer)
                        ->set('id', $this->getOfferId())->getView();
    }

    private function translateFormDataToDb($aData) {

        $aReturn = array();
        $iId = $this->getOfferId();

        if (!empty($aData['visible'])) {
            $aVisible = explode('|', $aData['visible']);
            foreach ($aVisible as $iKey => $sV) {
                $aReturn[] = array('id_offer' => $iId,
                    'name' => $sV,
                    'type' => Model_DbTable_Gallery::TYPE_VISIBLE,
                    'ord' => $iKey);
            }
        }

        if (!empty($aData['hidden'])) {
            $aHidden = explode('|', $aData['hidden']);
            foreach ($aHidden as $iKey => $sV) {
                $aReturn[] = array('id_offer' => $iId,
                    'name' => $sV,
                    'type' => Model_DbTable_Gallery::TYPE_HIDDEN,
                    'ord' => $iKey);
            }
        }
        return $aReturn;
    }

    private function translateDbDataToForm($aData) {

        $aReturn = array('visible' => array(), 'hidden' => array());

        foreach ($aData as $aRow) {

            switch ($aRow['type']) {
                case Model_DbTable_Gallery::TYPE_VISIBLE:
                    $aReturn['visible'][] = $aRow['name'];
                    break;
                case Model_DbTable_Gallery::TYPE_HIDDEN:
                    $aReturn['hidden'][] = $aRow['name'];
                    break;
                default:
                    $aReturn['visible'][] = $aRow['name'];
                    break;
            }
        }

        return array('visible' => implode('|', $aReturn['visible']),
            'hidden' => implode('|', $aReturn['hidden']));
    }

    private function saveGallery($aImages) {
        try {
            Model_DbTable_Gallery::getInstance()->cleanGalleryForOffer($this->getOfferId());
            $aData = $this->translateFormDataToDb($aImages);
            Model_DbTable_Gallery::getInstance()->saveImages($aData);
            $this->manageNoProvisionMarker($this->getOfferId()); 
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function cleanUpGalleryDir($aGalleryNames = array()) {
        
        $aGalleryFilesSaved = array();

        if (!empty($aGalleryNames)) {
            foreach ($aGalleryNames as $aGalleryRow) {
                $aGalleryFilesSaved[] = $aGalleryRow['name'];
            }
        }


        $aImageExisted = $this->getImagesFromOfferDir();
        $aUnused = array_diff($aImageExisted, $aGalleryFilesSaved);
        if (empty($aUnused))
            return true;

        foreach ($aUnused as $sFile) {
            try {
                unlink($this->getImagePath() . DIRECTORY_SEPARATOR . $sFile . '.' . self::IMAGE_EXTENSTION);
            } catch (Exception $e) {
                $this->getLog()->log($e, Zend_Log::ERR);
                //pozwalamy aplikacji działać dalej. 
            }
        }
    }

    private function getImagesFromOfferDir() {
        $aData = glob($this->getImagePath() . "/*." . self::IMAGE_EXTENSTION);
        $aReturn = array();

        if (!empty($aData)) {
            foreach ($aData as $aRow) {
                $aReturn[] = substr(basename($aRow), 0, -4);
            }
        }
        return $aReturn;
    }

    private function __setJsInput($oCtrl) {

        $aData = array('image' => array(
                'uploadUrl' => $oCtrl->getHelper('url')->url(array('controller' => 'gallery',
                    'action' => 'upload', 'id' => $this->getOfferId())),
                'width' => self::IMAGE_WIDTH,
                'height' => self::IMAGE_HEIGHT,
                'extension' => self::IMAGE_EXTENSTION,
                'path' => self::IMAGE_PATH . $this->getOfferId()),
            'thumb' => array(
                'url' => $oCtrl->getHelper('url')->url(array('action' => 'thumb',
                    'id' => $this->getOfferId())),
                'width' => self::IMAGE_THUMB_WIDTH,
                'height' => self::IMAGE_THUMB_HEIGHT
            )
        );

        return sprintf($this->getJsDataTemplate(), Zend_Json::encode($aData));
    }

    public function manageUploadImageFile(App_Controller_Admin_Abstract $oCtrl) {


        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
            throw new Model_Logic_Exception('No offer id specified', 500);

        $this->setOfferId($iId);

        $sUploadedFileData = $this->isFilePosted() ? $this->getImageFromGlobalFile() : $this->getFileFromStandardInput();

        try {
            $sUploadedFileName = $this->getFileNewName($sUploadedFileData);
            $this->saveImage($sUploadedFileData, $sUploadedFileName);
        } catch (Exception $e) {
            return array('status' => false,
                'content' => array('message' => $e->getMessage()));
        }

        return array(
            'status' => true,
            'content' => array(
                'hash' => $sUploadedFileName,
                'src' => $this->getImageSrc($sUploadedFileName)
            )
        );
    }

    public function manageRemoveImageFile(App_Controller_Admin_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
            throw new Model_Logic_Exception('No offer id specified', 500);

        $this->setOfferId($iId);

        if (($sHash = $oCtrl->getRequest()->getParam('hash', null)) === null)
            throw new Model_Logic_Exception('No image file hash specified', 500);


        $this->removeFile($sHash);

        return $this->set('status', true)->getView();
    }

    private function removeFile($sHash) {
        $sFullPath = $this->getImageFullPath($sHash);

        if (is_file($sFullPath))
            unlink($sFullPath);
    }

    public function getImageFullPath($sFileName = null, $iOfferId = null) {
        return $this->getImagePath($iOfferId) .
                DIRECTORY_SEPARATOR .
                $sFileName .
                '.' .
                self::IMAGE_EXTENSTION;
    }

    public function getTempImageFullPath($sFileName = null, $iOfferId = null) {
        return $this->getImagePath($iOfferId) .
                DIRECTORY_SEPARATOR .
                $sFileName .
                '_temp' .
                '.' .
                self::IMAGE_EXTENSTION;
    }

    public function getImagePath($iId = null) {


        if ($iId === null && !$this->isOfferIdSet())
            throw new Model_Logic_Exception('No offer id specified');


        $iOfferId = !empty($iId) ? $iId : $this->getOfferId();


        return APPLICATION_PATH .
                DIRECTORY_SEPARATOR .
                '..' .
                DIRECTORY_SEPARATOR .
                'public_html' .
                DIRECTORY_SEPARATOR .
                self::IMAGE_PATH .
                $iOfferId;
    }

    public function getImageSrc($sFileName, $iId = null) {


        $iOfferId = !empty($iId) ? $iId : $this->getOfferId();

        return self::IMAGE_PATH .
                $iOfferId .
                '/' .
                $sFileName .
                '.' .
                self::IMAGE_EXTENSTION;
    }

    private function saveImage($sUploadedFile = null, $sUploadedFileName = null) {

        if (empty($sUploadedFile))
            throw new Model_Logic_Exception('No image data specified');

        if (empty($sUploadedFileName))
            throw new Model_Logic_Exception('No image filename specified');

        $this->createImagePath();


        $sImageFullPath = $this->getImageFullPath($sUploadedFileName);

        if (file_exists($sImageFullPath))
            throw new Exception('Ten plik już istneje dla tej ofery');


        require_once ('WideImage/WideImage.php');

        $oImg = WideImage::loadFromString($sUploadedFile);

        $sWidth = $oImg->getWidth();
        $sHeight = $oImg->getHeight();
        $oWatermark = WideImage::load(APPLICATION_PATH .
                        DIRECTORY_SEPARATOR .
                        'data' .
                        DIRECTORY_SEPARATOR .
                        'domset_zw.png');


        if ($this->isImageVertical($sWidth, $sHeight)) {

            $oBg = WideImage::load(APPLICATION_PATH .
                            DIRECTORY_SEPARATOR .
                            'data' .
                            DIRECTORY_SEPARATOR .
                            'bg.png');

            $oResized = $oImg->resizeDown(null, self::IMAGE_HEIGHT)->merge($oWatermark, 'center', 'center');

            $oFinal =  $oBg->merge($oResized, 'center', 'center');
           
            $oFinal->saveToFile($sImageFullPath);
        } else {

            $oImg->resize(self::IMAGE_WIDTH, self::IMAGE_HEIGHT)
                    ->merge($oWatermark, 'center', 'center')
                    ->saveToFile($sImageFullPath);
        }
    }

    private function isImageVertical($sWidth, $sHeight) {
        return ($sHeight > $sWidth);
    }

    private function createImagePath() {
        $sOfferImagePath = $this->getImagePath();

        if (!$this->isOfferIdSet())
            throw new Model_Logic_Exception('No offer id specified');

        try {
            if (!is_dir($sOfferImagePath)) {
                mkdir($sOfferImagePath, 0755, true);
            }
        } catch (Exception $e) {
            throw new Model_Logic_Exception('Mkdir Exception', 500);
        }
    }

    private function getFileNewName($sFileData) {
        return md5(microtime());
    }

    private function isFilePosted() {
        return!empty($_FILES[$this->_sUploadFieldName]) && is_uploaded_file($_FILES[$this->_sUploadFieldName]['tmp_name']);
    }

    private function getImageFromGlobalFile() {
        return $this->_sUploadFieldName;
    }

    private function getFileFromStandardInput() {
        return file_get_contents("php://input");
    }

    public function getImagesDataForOffer($iId, $iLimit = 20, $sPrefix = '') {

        $aReturn = array();
        $aGallery = Model_DbTable_Gallery::getInstance()->getVisibleGalleryForOffer($iId, $iLimit);

        if (!empty($aGallery)) {
            foreach ($aGallery as $aImage) {
                $sFilePath = $this->getImageFullPath($aImage['name'], $aImage['id_offer']);
                $sData = file_get_contents($sFilePath);
                $aReturn[$aImage['name']] = $sPrefix . base64_encode($sData);
            }
        }

        return $aReturn;
    }

    public function getImagesLinksForOffer($iId, $iLimit = 20) {

        $aGallery = Model_DbTable_Gallery::getInstance()->getVisibleGalleryForOffer($iId, $iLimit);

        if (!empty($aGallery)) {
            $aReturn = array();

            foreach ($aGallery as $aImage) {
                  $aReturn[] = 'http://' . $_SERVER['SERVER_NAME'] . $this->getImageSrc($aImage['name'], $aImage['id_offer']);
            }
            return $aReturn;
        }
        else
            return null;
    }


     public function getImagesPathForOffer($iId, $iLimit = 20) {

        $aGallery = Model_DbTable_Gallery::getInstance()->getVisibleGalleryForOffer($iId, $iLimit);

        if (!empty($aGallery)) {
            $aReturn = array();

            foreach ($aGallery as $aImage) {
                  $aReturn[] = $this->getImageFullPath($aImage['name'], $aImage['id_offer']);
            }
            return $aReturn;
        }
        else
            return null;
    }


    public function manageNoProvisionMarker($iOfferId) {
        
        $aOffer = Model_DbTable_OfferCategory::getInstance()->isOfferMarkerAsNoProvision($iOfferId); 

        $aGraph = Model_DbTable_Gallery::getInstance()->getVisibleGalleryForOffer($iOfferId);
            $this->resetImages($aGraph); 

        if (!empty($aOffer)) {
            $this->addNoProvisionMarker($aGraph[0]); 
        }
        
    }

    private function resetImages($aImages) {

        foreach ($aImages as $aImage) {

            $sImageTmp = $this->getTempImageFullPath($aImage['name'], $aImage['id_offer']);

            if (file_exists($sImageTmp))
            {
            $sImageName = $this->getImageFullPath($aImage['name'], $aImage['id_offer']);
            unlink($sImageName);
            rename($sImageTmp, $sImageName);
            }
        }
    }
    
    private function addNoProvisionMarker($aImage)
    {
         require_once ('WideImage/WideImage.php');

        
        $sImageTmp = $this->getTempImageFullPath($aImage['name'], $aImage['id_offer']);
        $sImageName = $this->getImageFullPath($aImage['name'], $aImage['id_offer']);
        
        $oFile = WideImage::loadFromFile($sImageName); 
        $oFilew = WideImage::loadFromFile($sImageName); 
        unlink($sImageName);
        
          $oMarker = WideImage::load(APPLICATION_PATH .
                        DIRECTORY_SEPARATOR .
                        'data' .
                        DIRECTORY_SEPARATOR .
                        'prowizja.png');
          
          $oFile->saveToFile($sImageTmp); 

          $oFilew->merge($oMarker, "right - 10", "top - 10")->saveToFile($sImageName); 
          
    }

}