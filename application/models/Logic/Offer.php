<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Offer extends Model_Logic_Abstract {

    private $_sRatePrecentage = 0.035;
    private $_sRates = 360;

    public function countRate($iCost) {

        $q = 1 + ($this->_sRatePrecentage / 12);
        return ($iCost * pow($q, $this->_sRates)) * (($q - 1) / (pow($q, $this->_sRates) - 1));
    }

    private function getRegisteredAdapters() {
        return array(
            Model_Logic_Offer_Flat_Sell::ID => 'Model_Logic_Offer_Flat_Sell',
            Model_Logic_Offer_Flat_Rent::ID => 'Model_Logic_Offer_Flat_Rent',
            Model_Logic_Offer_House_Sell::ID => 'Model_Logic_Offer_House_Sell',
            Model_Logic_Offer_House_Rent::ID => 'Model_Logic_Offer_House_Rent',
            Model_Logic_Offer_Land_Sell::ID => 'Model_Logic_Offer_Land_Sell',
            Model_Logic_Offer_Land_Rent::ID => 'Model_Logic_Offer_Land_Rent',
            Model_Logic_Offer_Local_Sell::ID => 'Model_Logic_Offer_Local_Sell',
            Model_Logic_Offer_Local_Rent::ID => 'Model_Logic_Offer_Local_Rent',
            Model_Logic_Offer_Warehouse_Sell::ID => 'Model_Logic_Offer_Warehouse_Sell',
            Model_Logic_Offer_Warehouse_Rent::ID => 'Model_Logic_Offer_Warehouse_Rent');
    }

    public function getOfferTypeNames() {
        return array(
            Model_Logic_Offer_Flat_Sell::ID => 'mieszkanie, sprzedaż',
            Model_Logic_Offer_Flat_Rent::ID => 'mieszkanie, wynajem',
            Model_Logic_Offer_House_Sell::ID => 'dom, sprzedaż',
            Model_Logic_Offer_House_Rent::ID => 'dom, wynajem',
            Model_Logic_Offer_Land_Sell::ID => 'działka, sprzedaż',
            Model_Logic_Offer_Land_Rent::ID => 'działka, dzierżawa',
            Model_Logic_Offer_Local_Sell::ID => 'lokal, sprzedaż',
            Model_Logic_Offer_Local_Rent::ID => 'lokale, wynajem',
            Model_Logic_Offer_Warehouse_Sell::ID => 'przemysłowe, sprzedaż',
            Model_Logic_Offer_Warehouse_Rent::ID => 'przemysłowe, wynajem');
    }

    public function getOfferRoutes() {
        return array(
            Model_Logic_Offer_Flat_Sell::ID => 'list_flat_sell',
            Model_Logic_Offer_Flat_Rent::ID => 'list_flat_rent',
            Model_Logic_Offer_House_Sell::ID => 'list_house_sell',
            Model_Logic_Offer_House_Rent::ID => 'list_house_rent',
            Model_Logic_Offer_Land_Sell::ID => 'list_land_sell',
            Model_Logic_Offer_Land_Rent::ID => 'list_land_rent',
            Model_Logic_Offer_Local_Sell::ID => 'list_local_sell',
            Model_Logic_Offer_Local_Rent::ID => 'list_local_rent',
            Model_Logic_Offer_Warehouse_Sell::ID => 'list_warehouse_sell',
            Model_Logic_Offer_Warehouse_Rent::ID => 'list_warehouse_rent');
    }

    public function getSortingMethods() {
        return array(
            1 => array('id' => 1, 'label' => 'Data dodania (najnowsze)', 'sql' => 'created DESC'),
            2 => array('id' => 2, 'label' => 'Data dodania (najstarsze)', 'sql' => 'created ASC'),
            3 => array('id' => 3, 'label' => 'Cena (rosnąco)', 'sql' => 'price ASC'),
            4 => array('id' => 4, 'label' => 'Cena (malejąco)', 'sql' => 'price DESC'),
            5 => array('id' => 5, 'label' => 'Cena za m2 (rosnąco)', 'sql' => 'price_surface ASC'),
            6 => array('id' => 6, 'label' => 'Cena za m2 (malejąco)', 'sql' => 'price_surface DESC'),
            7 => array('id' => 7, 'label' => 'Powierzchnia (rosnąco)', 'sql' => 'surface ASC'),
            8 => array('id' => 8, 'label' => 'Powierzchnia (malejąco)', 'sql' => 'surface DESC')
        );
    }

    public function getSortingMethodById($iId) {
        $aMehods = $this->getSortingMethods();
        return isset($aMehods[$iId]) ? $aMehods[$iId] : '';
    }

    public function getRoute($iType) {
        $aRoutes = $this->getOfferRoutes();
        return isset($aRoutes[$iType]) ? $aRoutes[$iType] : '';
    }

    public function getOfferTypeName($iId) {

        $aTypes = $this->getOfferTypeNames();

        return isset($aTypes[$iId]) ? $aTypes[$iId] : '';
    }

    public function getStatuses() {
        return array(
            Model_DbTable_Offer::STATUS_OPEN => 'Aktualne',
            Model_DbTable_Offer::STATUS_FINISHED => 'Zakończone',
            Model_DbTable_Offer::STATUS_CANCELLED => 'Anulowane',
        );
    }

    public function initAdapterById($iId = null) {

        if ($this->isAdapterRegistered($iId)) {
            $aAdapters = $this->getRegisteredAdapters();
            return new $aAdapters[$iId]();
        }
        else
            throw new Model_Logic_Exception('Offer adapter failed', 500);
    }

    private function isAdapterRegistered($iId = null) {
        $aAdapters = $this->getRegisteredAdapters();
        return (!empty($iId) && isset($aAdapters[$iId]));
    }

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Offer
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Offer
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getDetailedOffer($iId) {
        $aOfferBasicData = Model_DbTable_Offer::getInstance()->getOfferBasicInformationById($iId);
        $oOfferAdapter = $this->initAdapterById($aOfferBasicData['id_type']);
        $aOfferDetailData = $oOfferAdapter->getTableModel()->getOffer($iId);
        return array_merge($aOfferBasicData, $aOfferDetailData);
    }

    public function manageIndex(App_Controller_Admin_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($iId);

        return $this->set('offer', $aOffer)
                        ->getView();
    }

    public function manageOfferDetails(App_Controller_Admin_Abstract $oCtrl) {


        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }

        $aOffer = $this->getDetailedOffer($iId);
        $oOfferAdapter = $this->initAdapterById($aOffer['id_type']);

        $oForm = new Admin_Form_Mask();

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            if (Model_DbTable_Visibility::getInstance()->addVisibility($iId, $oForm->getValue('fields'))) {
                $oCtrl->successMessage('Oferta dodana poprawnie.');
            } else {
                $oCtrl->errorMessage('Nieudane dodanie oferty.');
            }
        }

        $sVisibility = Model_DbTable_Visibility::getInstance()->getVisibility($iId);
        $aCategories = Model_DbTable_OfferCategory::getInstance()->getCategoriesDataForOffer($iId);
        $aMask = empty($sVisibility['data']) ? array() : explode(',', $sVisibility['data']);

        $oCtrl->view->headLink()->appendStylesheet('/styles/admin/jquery.galleryview-3.0-dev.css');

        $oCtrl->view->headScript()->appendFile('/scripts/jquery.easing.1.3.js')
                ->appendFile('/scripts/jquery.galleryview-3.0-dev.js')
                ->appendFile('/scripts/jquery.timers-1.2.js')
                ->appendFile('/scripts/jquery.mousewheel.min.js')
                ->appendFile('/scripts/manager/admin/offer/show.js');

        return $this->set('offer', $aOffer)
                        ->set('mask', $aMask)
                        ->set('form', $oForm)
                        ->set('categories', $aCategories)
                        ->set('template', $oOfferAdapter->getDetailViewPartial())
                        ->getView();
    }

    public function manageAddOffer(App_Controller_Admin_Abstract $oCtrl) {
        $iOfferType = $oCtrl->getRequest()->getParam('type', Model_Logic_Offer_Flat_Sell::ID);

        $oForm = new Admin_Form_Offer();

        $oOfferAdapter = $this->initAdapterById($iOfferType);
        $oForm->setOfferType($oOfferAdapter)->setDefaultAutor(Model_Logic_Admin::getInstance()->getActiveAdminId());

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $iOfferId = $this->saveValidatedOffer($oForm->getValues(), $oOfferAdapter);

            if ($iOfferId !== null) {
                $oCtrl->successMessage('Oferta dodana poprawnie.');
                $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'map', 'id' => $iOfferId)));
            } else {
                $oCtrl->errorMessage('Nieudane dodanie oferty.');
                $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list', 'type' => $oOfferAdapter->getId())));
            }
        }

        $oCtrl->view->headScript()->appendFile('/scripts/jquery.price_format.1.7.min.js')
                ->appendFile('/scripts/jquery.maskedinput.min.js')
                ->appendFile('/scripts/ckeditor/ckeditor.js')
                ->appendFile('/scripts/manager/admin/offer/details.js');


        return $this->set('form', $oForm)->getView();
    }

    public function manageEditOffer(App_Controller_Admin_Abstract $oCtrl) {

        if (($iOfferId = $oCtrl->getRequest()->getParam('id')) === null)
            throw new Model_Logic_Exception('Offer ID failed', 500);

        $aData = $this->getDetailedOffer($iOfferId);
        $oOfferAdapter = $this->initAdapterById($aData['id_type']);

        $oForm = new Admin_Form_Offer();
        $oForm->setOfferType($oOfferAdapter)->editMode();

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $this->editValidatedOffer($iOfferId, $oForm->getValues(), $oOfferAdapter) ? $oCtrl->successMessage('Udana edycja oferty.') : $oCtrl->errorMessage('Nieudana edycjas oferty.');

            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('module' => 'admin',
                        'controller' => 'offer',
                        'action' => 'index',
                        'id' => $iOfferId), 'home', true));
        }

        $oForm->populate($this->transformDataToForm($aData, $iOfferId));

        $oCtrl->view->headScript()->appendFile('/scripts/jquery.price_format.1.7.min.js')
                ->appendFile('/scripts/jquery.maskedinput.min.js')
                ->appendFile('/scripts/ckeditor/ckeditor.js')
                ->appendFile('/scripts/manager/admin/offer/details.js');

        return $this->set('form', $oForm)->set('offer', $aData)->getView();
    }

    public function manageStatus(App_Controller_Admin_Abstract $oCtrl) {
        if (($iOfferId = $oCtrl->getRequest()->getParam('id')) === null)
            throw new Model_Logic_Exception('Offer ID failed', 500);

        if (($iStatus = $oCtrl->getRequest()->getParam('status')) === null)
            throw new Model_Logic_Exception('Offer ID failed', 500);

        Model_DbTable_Offer::getInstance()->changeOfferStatus($iOfferId, $iStatus) ? $oCtrl->successMessage('Status oferty zmieniony.') : $oCtrl->errorMessage('Nieudana zmiana statusu oferty.');
        if($iStatus != Model_DbTable_Offer::STATUS_OPEN)
        {
            Model_DbTable_Promoted::getInstance()->removePromoted($iOfferId); 
        }
        
        $oCtrl->getHelper('Redirector')->goToUrl($_SERVER['HTTP_REFERER']);
    }

    private function transformDataToForm($aData, $iOfferId) {

        $aData['additional'] = explode(',', $aData['additional']);
        $aCategories = Model_DbTable_OfferCategory::getInstance()->getCategoriesForOffer($iOfferId);

        return array_merge($aData, Model_Logic_Category::getInstance()->translateCategoriesToForm($aCategories));
    }
    
    public function managePresentations(App_Controller_Admin_Abstract $oCtrl)
    {
        
        
        $iType = $oCtrl->getRequest()->getParam('type', 1);
       
        
        $oOfferAdapter = $this->initAdapterById($iType);

        $oGrid = new Admin_Grid_Preview($oCtrl->getRequest()->getParams());
        $oCtrl->view->headScript()->appendFile('/scripts/jquery.masonry.min.js')
                ->appendFile('/scripts/manager/admin/offer/list.js');

        return $this->set('list', $oGrid->deploy())->set('filter', $oOfferAdapter->getFilter())->getView();
    }


    
    public function manageList(App_Controller_Admin_Abstract $oCtrl) {


        $iType = $oCtrl->getRequest()->getParam('type', null);
        $oOfferAdapter = $this->initAdapterById($iType);

        $oGrid = new Admin_Grid_Offers($iType, $oCtrl->getRequest()->getParams());
        $oCtrl->view->headScript()->appendFile('/scripts/jquery.masonry.min.js')
                ->appendFile('/scripts/manager/admin/offer/list.js');

        return $this->set('list', $oGrid->deploy())->set('filter', $oOfferAdapter->getFilter())->getView();
    }

    public function managePageList(App_Controller_Page_Abstract $oCtrl) {


        $iType = $oCtrl->getRequest()->getParam('type', null);
        $oOfferAdapter = $this->initAdapterById($iType);
        $aQuery = $oCtrl->getRequest()->getQuery();
        

        $oFilter = $oOfferAdapter->getPageFilter($iType);
        $oFilter->populate($aQuery);

        $oAdapterQuery = $oOfferAdapter->getTableModel()->getOffersDisplayQuery($aQuery);
        $oNewPaginationAdapter = new Zend_Paginator_Adapter_DbSelect($oAdapterQuery);
        $oNewPagination = new Zend_Paginator($oNewPaginationAdapter);
        $oNewPagination->setCurrentPageNumber($oCtrl->getRequest()->getParam('page', 1));


        $oCtrl->view->headScript()->appendFile('/scripts/manager/page/list.js');
        $oCtrl->view->headScript()->appendScript('var get=' . Zend_Json::encode($aQuery) . ';');

        $oCtrl->view->headMeta()->appendName('keywords', Model_Logic_Meta::getListMeta('keywords', $iType));
        $oCtrl->view->headMeta()->appendName('description', Model_Logic_Meta::getListMeta('description', $iType));
        $oCtrl->view->headTitle(Model_Logic_Meta::getListMeta('title', $iType));
        $oCtrl->view->executedQuery = count(array_keys($aQuery));
        $oCtrl->view->data = $oNewPagination;
        $oCtrl->view->form = $oFilter;
        $oCtrl->view->type = $iType;
        $oCtrl->view->partial = $oOfferAdapter->getPageOfferListPartial();
    }

    public function managePageOfferDetail(App_Controller_Page_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }

        $aQuery = $oCtrl->getRequest()->getQuery();

        if (isset($aQuery['ask'])) {
            $oCtrl->view->ask = true;
        }

        $aVisible = Model_DbTable_Visibility::getInstance()->getVisibility($iId);
        $oContactForm = new Page_Form_Contact();
        if ($oCtrl->getRequest()->isPost()) {
            if ($oContactForm->isValid($oCtrl->getRequest()->getPost())) {

                Model_Logic_Mail::getInstance()->sendContactEmail($oContactForm->getValues());
                $oCtrl->view->success = true;
                $oContactForm->reset();
            }
            else
                $oCtrl->view->formFailed = true;
        }


        $aOffer = $this->getDetailedOffer($iId);
        $oOfferAdapter = $this->initAdapterById($aOffer['id_type']);

        $aLocDetails = array('lat' => $aOffer['lat'], 'lng' => $aOffer['lng']);

        $oCtrl->view->headScript()->appendFile('https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyDy0CKx15WJ7PlB9_7RNSZviGTFAlMNNdo')
                ->appendScript('var locator = ' . Zend_Json::encode($aLocDetails) . ';')
                ->appendFile('/scripts/manager/page/map.js');

        $oCtrl->view->headMeta()->appendName('keywords', sprintf(Model_Logic_Meta::getDetailMeta('keywords'), $aOffer['title'], $aOffer['id']));
        $oCtrl->view->headMeta()->appendName('description', sprintf(Model_Logic_Meta::getDetailMeta('description'), $aOffer['title'], $aOffer['id']));
        $oCtrl->view->headTitle(sprintf(Model_Logic_Meta::getDetailMeta('title'), $aOffer['title'], $aOffer['id']));



        $oCtrl->view->form = $oContactForm;
        $oCtrl->view->data = $aOffer;
        $oCtrl->view->blocked = empty($aVisible) ? array() : explode(',', $aVisible['data']);
        $oCtrl->view->partial = $oOfferAdapter->getDetailTemplate();
    }

    public function manageArchive(App_Controller_Admin_Abstract $oCtrl) {

        $oGrid = new Admin_Grid_Offers(0, $oCtrl->getRequest()->getParams());
        $oCtrl->view->headScript()->appendFile('/scripts/jquery.masonry.min.js')
                ->appendFile('/scripts/manager/admin/offer/list.js');

        return $this->set('list', $oGrid->deploy())->getView();
    }

    private function saveValidatedOffer($aOfferData, $oOfferAdapter) {

        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction();


            $aOfferData['surface'] = preg_replace('/,/', '.', $aOfferData['surface']);
            $aOfferData['price'] = preg_replace('/,/', '.', $aOfferData['price']);

            $iOfferBaseId = Model_DbTable_Offer::getInstance()->addOffer($aOfferData);
            $aCategories = Model_Logic_Category::getInstance()->translateCategoriesToDb($aOfferData);
            Model_DbTable_OfferCategory::getInstance()->addCategoriesForOffer($aCategories, $iOfferBaseId);

            $oOfferAdapter->add(array_merge(array('id_offer' => $iOfferBaseId), $aOfferData));
            Zend_Db_Table::getDefaultAdapter()->commit();
            return $iOfferBaseId;
        } catch (Exception $e) {
            Zend_Db_Table::getDefaultAdapter()->rollback();
            $this->getLog()->log($e, Zend_Log::CRIT);
            return null;
        }
    }

    private function editValidatedOffer($iOfferId, $aOfferData, $oOfferAdapter) {


        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction();

            $aOfferData['surface'] = preg_replace('/,/', '.', $aOfferData['surface']);
            $aOfferData['price'] = preg_replace('/,/', '.', $aOfferData['price']);

            Model_DbTable_Offer::getInstance()->editOffer($aOfferData, $iOfferId);

            if ($oOfferAdapter->getId() != $aOfferData['type']) {
                $oChangedAdapter = $this->initAdapterById($aOfferData['type']);
                $oOfferAdapter->removeOfferTypeById($iOfferId);
                $oChangedAdapter->add(array_merge(array('id_offer' => $iOfferId), $aOfferData));
            } else {
                $oOfferAdapter->edit($aOfferData, $iOfferId);
            }

            $aCategories = Model_Logic_Category::getInstance()->translateCategoriesToDb($aOfferData);
            Model_DbTable_OfferCategory::getInstance()->removeCategoriesByOfferId($iOfferId);
            Model_DbTable_OfferCategory::getInstance()->addCategoriesForOffer($aCategories, $iOfferId);

            Zend_Db_Table::getDefaultAdapter()->commit();
            return true;
        } catch (Exception $e) {
            Zend_Db_Table::getDefaultAdapter()->rollback();
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    public function manageChangeOfferType(App_Controller_Admin_Abstract $oCtrl) {
        $iOfferType = $oCtrl->getRequest()->getParam('type', null);
        $oForm = new Admin_Form_Offer();
        try {
            if ($iOfferType !== null) {
                $oOfferAdapter = $this->initAdapterById($iOfferType);
                $oForm->setOfferType($oOfferAdapter);
                $aResponse = array('status' => true,
                    'content' => $oForm->populate($oCtrl->getRequest()->getPost())->render());
            }
        } catch (Exception $e) {
            $aResponse = array('status' => false, 'message' => $e->getMessage());
        }


        return $aResponse;
    }

    public function manageAddSection(App_Controller_Admin_Abstract $oCtrl) {

        $oForm = new Admin_Form_Section();

        if ($oCtrl->getRequest()->isPost()) {

            if ($oForm->isValid($oCtrl->getRequest()->getPost())) {
                $aResponse['value'] = $oForm->getValue('section');
                $aResponse['id'] = Model_DbTable_Section::getInstance()->addSection($aResponse['value']);
                return array('status' => true, 'content' => $aResponse);
            }
            else
                return array('status' => false, 'content' => $oForm->render());
        }
        else
            return array('status' => true, 'content' => $oForm->render());
    }

    public function manageGetOffer(App_Controller_Admin_Abstract $oCtrl) {

        if (($iOfferId = $oCtrl->getRequest()->getParam('id')) === null)
            return array('status' => false, 'content' => 'Nie wysłano identyfikatora');


        if (($aOfferData = Model_DbTable_Offer::getInstance()->getOfferBasicInformationById($iOfferId)) == null)
            return array('status' => false, 'content' => 'Nie oferty o podanym identyfikatorze');


        return array('status' => true, 'content' => $aOfferData);
    }

    public function manageReloadPromotions(App_Controller_Admin_Abstract $oCtrl) {

        
        $sType = $oCtrl->getRequest()->getParam('type', 'existed');

        if ($sType == 'new') {
            $sPromotions = $oCtrl->getRequest()->getParam('promotions', null);
            if (!$this->saveValidatedPromotedOffers($sPromotions)) {
                return array('status' => false, 'content' => "Błąd przy przeładowaniu");
            }
        }

        $aOffers = Model_DbTable_Promoted::getInstance()->getPromotions();
        return array('status' => true, 'content' => $aOffers);
    }

    public function managePromoted(App_Controller_Admin_Abstract $oCtrl) {

        $oGrid = new Admin_Grid_Offers(Admin_Grid_Offers::OFFER_FOR_PROMOTED_ID);

        $oCtrl->view->headScript()->appendFile('/scripts/manager/admin/offer/promoted.js');


        return $this->set('grid', $oGrid->deploy())->getView();
    }

    public function manageMainPage(App_Controller_Page_Abstract $oCtrl) {

        $oPromotedPaginationAdapter = new Zend_Paginator_Adapter_DbSelect(Model_DbTable_Promoted::getInstance()->getPromotionsQuery());
        $oPromotedPagination = new Zend_Paginator($oPromotedPaginationAdapter);
        $oPromotedPagination->setItemCountPerPage(6)
                ->setCurrentPageNumber($oCtrl->getRequest()->getParam('promo', 1))
                ->setDefaultPageRange(3);



        $oNewPaginationAdapter = new Zend_Paginator_Adapter_DbSelect(Model_DbTable_Offer::getInstance()->getNewestQuery());
        $oNewPagination = new Zend_Paginator($oNewPaginationAdapter);
        $oNewPagination->setCurrentPageNumber($oCtrl->getRequest()->getParam('last', 1))
                ->setItemCountPerPage(6)
                ->setDefaultPageRange(3);

        $oCtrl->view->promoted = $oPromotedPagination;
        $oCtrl->view->last = $oNewPagination;
        $oCtrl->view->headTitle('Nieruchomości DOMSET - mieszkania na sprzedaż, działki, lokale, wycena nieruchomości oraz kredyty.');
        $oCtrl->view->headMeta()->appendName('keywords', 'nieruchomości, kredyty, wycena, mieszkania na sprzedaż w ełku, nowe mieszkania, działki, lokale do wynajęcia.');
        $oCtrl->view->headMeta()->appendName('description', 'Agencja nieruchomości i pośrednictwa DOMSET posiada w swojej ofercie nieruchomości na terenie Ełku oraz okolicach Giżycko, Wydminy, Woszczele, Stare Juchy, Prostki. Oferty NIERUCHOMOŚCI: mieszkania, domy, działki, wille, i grunty oraz nieruchomości komercyjne, tereny inwestycyjne.');


    }

    private function saveValidatedPromotedOffers($sOffers = "") {

         if (!empty($sOffers)) {
        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction();
            Model_DbTable_Promoted::getInstance()->clearPromoted();
           
                $aOffers = explode('|', $sOffers);
                foreach ($aOffers as $iId) {
                    Model_DbTable_Promoted::getInstance()->addPromoted($iId);
                }
          
            Zend_Db_Table::getDefaultAdapter()->commit();
            return true;
        } catch (Exception $e) {

            Zend_Db_Table::getDefaultAdapter()->rollBack();
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
          }
    }

    public function manageCity(App_Controller_Page_Abstract $oCtrl) {


        $sCities = $oCtrl->getRequest()->getParam('cities');
      
        
        if (( $iType = $oCtrl->getRequest()->getParam('type')) === null) {
            throw new Exception('Id fail', 500);
        }
        
        
        
        if (empty($sCities)) {
            return array();
        } else {
            return Model_DbTable_Offer::getInstance()->getSectionsByCity(explode('|', $sCities), $iType);
        }
    }

    public function manageCommunity(App_Controller_Page_Abstract $oCtrl) {


        $sCommunities = $oCtrl->getRequest()->getParam('communities');
        
         
        if (( $iType = $oCtrl->getRequest()->getParam('type')) === null) {
            throw new Exception('Id fail', 500);
        }
        
        if (empty($sCommunities)) {
            return array();
        } else {
            return Model_DbTable_Offer::getInstance()->getCitiesByCommunities(explode('|', $sCommunities), $iType);
        }
    }

    public function manageDistrict(App_Controller_Page_Abstract $oCtrl) {


        $sDistrict = $oCtrl->getRequest()->getParam('districts');
        
         
        if (( $iType = $oCtrl->getRequest()->getParam('type')) === null) {
            throw new Exception('Id fail', 500);
        }
        
        if (empty($sDistrict)) {
            return array();
        } else {
            return Model_DbTable_Offer::getInstance()->getCommunnitiesByDistrict(explode('|', $sDistrict), $iType);
        }
    }

    public function managePrint(App_Controller_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id')) === null) {
            throw new Exception('Id fail', 500);
        }

        $sMode = $oCtrl->getRequest()->getParam('mode', 'P');

        $oCtrl->getHelper('layout')->disableLayout();
        $oCtrl->getHelper('viewRenderer')->setNoRender(true);

        $aOffer = $this->getDetailedOffer($iId);
        $oOfferAdapter = $this->initAdapterById($aOffer['id_type']);

        include('mpdf/mpdf.php');

        $oCtrl->view->addScriptPath(APPLICATION_PATH .
                DIRECTORY_SEPARATOR .
                'modules' .
                DIRECTORY_SEPARATOR .
                'page' .
                DIRECTORY_SEPARATOR .
                'views' .
                DIRECTORY_SEPARATOR .
                'scripts');
        
        

        switch($sMode)
        {
        case 'P':
                $oMpdf = new mPDF('', 'A4', 0, '', 0, 0, 0, 0, 0, 0, 'P');
                $oMpdf->dpi = 150;
                $oMpdf->WriteHTML($oCtrl->view->partial($oOfferAdapter->getPrintTemplate(), array('offer' => $aOffer)));
                break;
            case 'L':
             $oMpdf = new mPDF('', 'A4-L', 0, '', 0, 0, 0, 0, 0, 0, 'L');
             $oMpdf->dpi = 150;
                $oMpdf->WriteHTML($oCtrl->view->partial($oOfferAdapter->getPrintLandscapeTemplate(), array('offer' => $aOffer)));
                break;
            default:
                throw new Exception('No mode');
                break;
        }



        $oMpdf->Output('domset_oferta_' . $aOffer['id'] . '.pdf', 'D');
        exit;
    }

}