<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Mail extends Model_Logic_Abstract {

   

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Mail
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Mail
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    
    public function sendContactEmail($aData)
    {
        
        
        $sContent = sprintf('Od : %s '."\n" . 
                            'Ze strony : %s' ."\n" .
                            'Email : %s '."\n" .
                            'Telefon : %s '."\n" .
                            'Treść : %s '."\n" , 
                            $aData['name'],
                            $aData['url'],
                            $aData['email'], 
                            $aData['phone'], 
                            $aData['body']); 
        
        $oMail = new Zend_Mail('utf-8'); 
        $oMail->addTo('biuro1@domset.pl')
              ->setSubject('Wiadomość z Domset.pl')
              ->setBodyText($sContent, 'utf-8')
              ->send();
        
        
        
    }
    
}