<?php

class Model_Logic_Export_Olx_Api extends Model_Logic_Abstract {

    const CONTACT_EMAIL = 'biuro@domset.pl';
    const LOCATION_COUNTRY_POLAND = 'PL';
    const CURRENCY_PLN = 'PLN';

    /**
     * Instancja klasy.
     *
     * @var Model_Logic_Export_Olx_Api
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------

    /**
     * Zwraca instancje klasy.
     *
     * @return Model_Logic_Export_Olx_Api
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    private static function cdata($sData) {
        return sprintf('<![CDATA[%s]]>', $sData);
    }

    public function buildData($iExportId, $aData, $aImages = array()) {

        $aDatax = array(); 
        $aDatax['ID'] = $iExportId;
        $aDatax['EMAIL'] = self::CONTACT_EMAIL;
        $aDatax['CURRENCY'] = self::CURRENCY_PLN;
        $aDatax['IMAGE_URL'] = $aImages;

        return array_merge($aDatax, $aData);
    }

    public function markAsRemoved($aExport) {

        $aData = unserialize($aExport['data']);
        $aData['STATUS'] = 'D';

        return $aData;
    }

    public function displayXml() {

        $aXmlData = Model_DbTable_Export::getInstance()->getExportsByAdapter(Model_Logic_Export_Olx::ID);

        if (!empty($aXmlData)) {

            $oXml = new DomDocument('1.0', 'UTF-8');
            $oAds = $oXml->createElement('ads');

            foreach ($aXmlData as $aSingleExport) {


                if (empty($aSingleExport))
                    continue;

                $aDataToXml = unserialize($aSingleExport['data']);


                $aDataToXml['DESCRIPTION'] = str_replace('&nbsp', '', $aDataToXml['DESCRIPTION']);

                if (!is_array($aDataToXml))
                    continue;

                $oNode = $oXml->createElement('ad');

              //  $oNode->appendChild($oXml->createElement('id', self::cdata($aSingleExport['id'])));


                foreach ($aDataToXml as $sKey => $sValue) {

                    if($sKey == 'STATE')
                    {
                        $sKey = 'STATUS'; 
                    }

                    if (!empty($sValue)) {
                        if (is_array($sValue)) {
                            foreach ($sValue as $sSubvalue) {
                                $oNode->appendChild($oXml->createElement($sKey, self::cdata($sSubvalue)));
                            }
                        }
                        else
                        {
                            $sValue = str_replace('&oacute;', 'ó', $sValue);
                            $oNode->appendChild($oXml->createElement($sKey, self::cdata($sValue)));
                        }
                    }
                }
                $oAds->appendChild($oNode);
            }
            $oXml->appendChild($oAds);
            header('Content-Type: text/xml');
            echo $oXml->saveXML();
        }
    }

}

