<?php

class Model_Logic_Export_Olx_Translate {

    const CONTACT_NAME = 'DOMSET Nieruchomości';
    const CONTACT_EMAIL = 'biuro@domset.pl';
    const STATUS_ACTIVE = 1;
    const COUNTRY_POLAND = 1;
    const PRICE_BRUTTO = 0;
    const PRICE_CURRENCY_PLN = 1;
    const TYPE_FLAT = 0;
    const TYPE_HOUSE = 1;
    const TYPE_LAND = 2;
    const TYPE_LOCAL = 4;
    const TYPE_WAREHOUSE = 5;
    const OFFER_TYPE_SELL = 0;
    const OFFER_TYPE_RENT = 1;
    const TITLE_LENGHT_LIMIT = 50;

    public static function translate($aData) {

        return array(
            'TITLE' => self::translateTitle($aData['title']),
            'DESCRIPTION' => self::translateDescription($aData['description'], $aData),
            'DATE' => $aData['created'],
            'LOCATION_STATE' => self::translateProvince($aData['province']),
            'LOCATION_CITY' => $aData['city'],
            'COUNTRY' => 170,
            'ADDRESS' => $aData['street'],
            'PRICE' => $aData['price'],
            'SURFACE' => $aData['surface'],
            'PHONE' => $aData['phone'],
            'CATEGORY' => self::translateCategory($aData['id_type']),
            'STATE' => self::translateState($aData['state'])
        );
    }

    private static function translateTitle($sTitle) {
        return (mb_strlen($sTitle) < self::TITLE_LENGHT_LIMIT) ? $sTitle : mb_substr($sTitle, 0, self::TITLE_LENGHT_LIMIT);
    }

    private static function translateDescription($sDesc = '', $aData = array()) {

        $sAddText = PHP_EOL . PHP_EOL . 'Numer licencji pośrednika odpowiedzialnego zawodowo za wykonanie umowy pośrednictwa: 15295' . PHP_EOL . PHP_EOL .
                'Kontakt do Agenta' . PHP_EOL .
                $aData['name'] . ' ' . $aData['surname'] . PHP_EOL .
                'tel. kom.: ' . $aData['phone'] . PHP_EOL .
                'DOMSET Nieruchomości' . PHP_EOL .
                'Ełk, ul Wojska Polskiego 43 lok. 3' . PHP_EOL .
                'tel.: 87 610 88 88' . PHP_EOL;


        return trim($sDesc) . $sAddText;


        return trim($sDesc) . $sAddText;
    }

    private static function translateCategory($iCategoryId) {

        $aKeyTranslate = array(
            Model_Logic_Offer_Flat_Sell::ID => 367,
            Model_Logic_Offer_Flat_Rent::ID => 363,
            Model_Logic_Offer_House_Sell::ID => 367,
            Model_Logic_Offer_House_Rent::ID => 363,
            Model_Logic_Offer_Local_Sell::ID => 415,
            Model_Logic_Offer_Local_Rent::ID => 415,
            Model_Logic_Offer_Land_Sell::ID => 410,
            Model_Logic_Offer_Land_Rent::ID => 410,
            Model_Logic_Offer_Warehouse_Sell::ID => 415,
            Model_Logic_Offer_Warehouse_Rent::ID => 415
        );

        return isset($aKeyTranslate[$iCategoryId]) ? $aKeyTranslate[$iCategoryId] : '';
    }

    private static function translateProvince($iProvinceId) {
        $aKeyTranslate = array(
            1 => 'WMK',
            2 => 'DLS',
            3 => 'KPK',
            4 => 'LBK',
            5 => 'LSK',
            6 => 'LDK',
            7 => 'MPK',
            8 => 'MZK',
            9 => 'OPK',
            10 => 'PKK',
            11 => 'PLK',
            12 => 'PMK',
            13 => 'SLK',
            14 => 'SKK',
            15 => 'WPK',
            16 => 'ZPK');

        return isset($aKeyTranslate[$iProvinceId]) ? $aKeyTranslate[$iProvinceId] : '';
    }

    private static function translateState($iState) {
        $aKeyTranslate = array(
            Model_DbTable_Export::TYPE_ADDED => 'N',
            Model_DbTable_Export::TYPE_RELOADED => 'N',
            Model_DbTable_Export::TYPE_EDITED => 'U',
            Model_DbTable_Export::TYPE_DELETED => 'D',
        );

        return isset($aKeyTranslate[$iState]) ? $aKeyTranslate[$iState] : '';
    }

}

