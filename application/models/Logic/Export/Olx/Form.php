<?php

class Model_Logic_Export_Olx_Form extends Form_Abstract {

    public function __construct() {
        parent::__construct(null);


        $this->_aFields['TITLE'] = new Zend_Form_Element_Text('TITLE');
        $this->_aFields['TITLE']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Tytuł')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));

        $this->_aFields['DESCRIPTION'] = new Zend_Form_Element_Textarea('DESCRIPTION');
        $this->_aFields['DESCRIPTION']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Opis')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));

           $this->_aFields['CATEGORY'] = new Zend_Form_Element_Hidden('CATEGORY');
        $this->_aFields['CATEGORY']->setDecorators(array('viewHelper')); 
                
        
        $this->_aFields['DATE'] = new Zend_Form_Element_Text('DATE');
        $this->_aFields['DATE']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Data dodania')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));

        $this->_aFields['LOCATION_STATE'] = new Zend_Form_Element_Select('LOCATION_STATE');
        $this->_aFields['LOCATION_STATE']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Województwo')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));

        $this->_aFields['LOCATION_CITY'] = new Zend_Form_Element_Text('LOCATION_CITY');
        $this->_aFields['LOCATION_CITY']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Miasto');

        $this->_aFields['ZIP_CODE'] = new Zend_Form_Element_Text('ZIP_CODE');
        $this->_aFields['ZIP_CODE']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Kod pocztowy');

        $this->_aFields['ADDRESS'] = new Zend_Form_Element_Text('ADDRESS');
        $this->_aFields['ADDRESS']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Adres');

        $this->_aFields['PRICE'] = new Zend_Form_Element_Text('PRICE');
        $this->_aFields['PRICE']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Cena');

        $this->_aFields['BEDROOMS'] = new Zend_Form_Element_Text('BEDROOMS');
        $this->_aFields['BEDROOMS']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Ilość sypialni');

        $this->_aFields['BATHROOMS'] = new Zend_Form_Element_Text('BATHROOMS');
        $this->_aFields['BATHROOMS']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Ilość łazienek');

        $this->_aFields['PHONE'] = new Zend_Form_Element_Hidden('PHONE');
        $this->_aFields['PHONE']->setDecorators(array('ViewHelper')); 
        
        $this->_aFields['STATE'] = new Zend_Form_Element_Hidden('STATE');
        $this->_aFields['STATE']->setDecorators(array('ViewHelper')); 

        $this->_aFields['SURFACE'] = new Zend_Form_Element_Text('SURFACE');
        $this->_aFields['SURFACE']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Powierzchnia');
        
        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setDecorators($this->_aFormSubmitTableDecorator)
                ->setLabel('Wyślij');
        
        $this->addElements($this->_aFields)
             ->setDecorators($this->_aFormTableDecorator)   
             ->setMethod(Zend_FORM::METHOD_POST); 
        
        $this->populateSelect(Model_Logic_Export_Olx_Translate_Province::getProvinces(), 'LOCATION_STATE', false, true); 
    }

}