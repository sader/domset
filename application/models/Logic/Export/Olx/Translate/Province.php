<?php

class Model_Logic_Export_Olx_Translate_Province {
    
  
        
       private static $aProvinces = array(
                'DLS' => 'Dolnośląskie',
                'KPK' => 'Kujawsko-pomorskie',
                'LBK' => 'Lubelskie',
                'LSK' => 'Lubuskie',
                'MZK' => 'Mazowieckie',
                'MPK' => 'Małopolskie',
                'OPK' => 'Opolskie',
                'PKK' => 'Podkarpackie',
                'PLK' => 'Podlaskie',
                'PMK' => 'Pomorskie',
                'SLK' => 'Śląskie',
                'SKK' => 'Świętokrzyskie',
                'WMK' => 'Warmińsko-mazurskie',
                'WPK' => 'Wielkopolskie',
                'ZPK' => 'Zachodniopomorskie',
                'LDK' => 'Łódzkie'

           );   
        
 
    
    
    
    public static function getProvinces()
    {
        return self::$aProvinces; 
        
    }
}


