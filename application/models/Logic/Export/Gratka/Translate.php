<?php

class Model_Logic_Export_Gratka_Translate {

    const ID_COUNTRY_POLAND = 136;
    const CONTACT_NAME =  ''; //'DOMSET Nieruchomości';
    const CONTACT_EMAIL = 'biuro@domset.pl';
    const Flat_Sell = 397;
    const Flat_Rent = 401;
    const House_Sell = 402;
    const House_Rent = 406;
    const Land_Sell = 407;
    const Land_Rent = 411;
    const Local_Sell = 417;
    const Local_Rent = 421;
    const Warehouse_Sell = 417;
    const Warehouse_Rent = 421;

    private static function getDomsetOfferTypeToGratkaCategoryMapper() {
        return array(
            Model_Logic_Offer_Flat_Sell::ID => self::Flat_Sell,
            Model_Logic_Offer_Flat_Rent::ID => self::Flat_Rent,
            Model_Logic_Offer_House_Sell::ID => self::House_Sell,
            Model_Logic_Offer_House_Rent::ID => self::House_Rent,
            Model_Logic_Offer_Land_Sell::ID => self::Land_Sell,
            Model_Logic_Offer_Land_Rent::ID => self::Land_Rent,
            Model_Logic_Offer_Local_Sell::ID => self::Local_Sell,
            Model_Logic_Offer_Local_Rent::ID => self::Local_Rent,
            Model_Logic_Offer_Warehouse_Sell::ID => self::Local_Sell,
            Model_Logic_Offer_Warehouse_Rent::ID => self::Local_Rent
        );
    }

    public static function getGratkaCategoryId($iId) {

        $aTypes = self::getDomsetOfferTypeToGratkaCategoryMapper();
        return isset($aTypes[$iId]) ? $aTypes[$iId] : null;
    }

    public static function translate($iCategoryId, $aData) {


        switch ($iCategoryId) {
            case self::Flat_Sell:
                return self::getFlatSell($aData);
                break;
            case self::Flat_Rent:
                return self::getFlatRent($aData);
                break;
            case self::House_Sell:
                return self::getHouseSell($aData);
                break;
            case self::House_Rent:
                return self::getHouseRent($aData);
                break;
            case self::Local_Sell:
                return self::getLocalSell($aData);
                break;
            case self::Local_Rent:
                return self::getLocalRent($aData);
                break;
            case self::Land_Sell:
                return self::getLandSell($aData);
                break;
            case self::Land_Rent:
                return self::getLandRent($aData);
                break;
            default:
                throw new Exception('No offer translate adapted for type ' . $iCategoryId);
                break;
        }
    }

    private static function getFlatSell($aData) {
        return array(
            'cena' => $aData['price'],
            'powierzchnia' => $aData['surface'],
            'cena_za_metr' => $aData['price_surface'],
            'garaz_bit' => self::translateAdditional($aData['additional']),
            'id_panstwo' => self::ID_COUNTRY_POLAND,
            'id_region' => self::translateRegion($aData['province']),
            'gmina' => $aData['community'],
            'powiat' => $aData['district'],
            'miejscowosc' => $aData['city'],
            'dzielnica' => $aData['section'],
            'id_glosnosc' => self::translateNoise($aData['noise']),
            'id_forma_wlasnosci' => self::translateOwnership($aData['ownership']),
            'id_liczba_pieter' => $aData['floors_in_building'],
            'id_liczba_pokoi' => $aData['rooms'],
            'id_liczba_poziomow' => $aData['levels'],
            'id_material' => self::translateMaterial($aData['building_material']),
            'id_okna' => self::translateWindows($aData['windows']),
            'id_pietro' => self::translateFloor($aData['floor']),
            'id_stan_instalacji' => self::translateInstalationQuality($aData['installation_quality']),
            'id_stan_mieszkania' => self::translateQuality($aData['quality']),
            'id_typ_zabudowy' => self::translateFlatBuildingType($aData['building_type']),
            'kontakt_email' => self::CONTACT_EMAIL,
            'kontakt_osoba' => self::CONTACT_NAME,
            'kontakt_telefon' => $aData['phone'],
            'czy_kontakt_agencja' => 1,
            'id_jednostka_pow' => 1,
            'id_waluta' => 1,
            'numer_oferty' => $aData['id'],
            'opis' => self::translateDescription($aData['description'], $aData),
            'rok_budowy' => $aData['year'],
            'ulica' => $aData['street'],
            'wspolrzedne_x' => $aData['lat'],
            'wspolrzedne_y' => $aData['lng'],
            'wysokosc_czynszu' => $aData['rent']
        );
    }

    private static function getFlatRent($aData) {
        return array(
            'cena' => $aData['price'],
            'powierzchnia' => $aData['surface'],
            'cena_za_metr' => $aData['price_surface'],
            'garaz_bit' => self::translateAdditional($aData['additional']),
            'id_panstwo' => self::ID_COUNTRY_POLAND,
            'id_region' => self::translateRegion($aData['province']),
            'gmina' => $aData['community'],
            'powiat' => $aData['district'],
            'miejscowosc' => $aData['city'],
            'dzielnica' => $aData['section'],
            'id_glosnosc' => self::translateNoise($aData['noise']),
            'id_liczba_pieter' => $aData['floors_in_building'],
            'id_liczba_pokoi' => $aData['rooms'],
            'id_liczba_poziomow' => $aData['levels'],
            'id_okna' => self::translateWindows($aData['windows']),
            'id_pietro' => self::translateFloor($aData['floor']),
            'id_stan_instalacji' => self::translateInstalationQuality($aData['installation_quality']),
            'id_stan_mieszkania' => self::translateQuality($aData['quality']),
            'id_typ_zabudowy' => self::translateFlatBuildingType($aData['building_type']),
            'kontakt_email' => self::CONTACT_EMAIL,
            'kontakt_osoba' => self::CONTACT_NAME,
            'kontakt_telefon' => $aData['phone'],
            'czy_kontakt_agencja' => 1,
            'id_jednostka_pow' => 1,
            'id_waluta' => 1,
            'id_platnosc_za' => 1,
            'numer_oferty' => $aData['id'],
            'opis' => self::translateDescription($aData['description'], $aData),
            'rok_budowy' => $aData['year'],
            'ulica' => $aData['street'],
            'wspolrzedne_x' => $aData['lat'],
            'wspolrzedne_y' => $aData['lng']
        );
    }

    private static function getHouseSell($aData) {
        return array(
            'cena' => $aData['price'],
            'powierzchnia' => $aData['surface'],
            'cena_za_metr' => $aData['price_surface'],
            'garaz_bit' => self::translateAdditional($aData['additional']),
            'id_panstwo' => self::ID_COUNTRY_POLAND,
            'id_region' => self::translateRegion($aData['province']),
            'gmina' => $aData['community'],
            'powiat' => $aData['district'],
            'miejscowosc' => $aData['city'],
            'dzielnica' => $aData['section'],
            'id_liczba_pokoi' => $aData['rooms'],
            'id_liczba_pieter' => $aData['floors'],
            'okna_bit' => self::translateWindowsBit($aData['windows']),
            'id_material' => self::translateHouseMaterial($aData['building_material']),
            'id_typ_budynku' => self::translateHouseType($aData['building_type']),
            'id_stan_budynku' => self::translateHouseState($aData['quality']),
            'id_typ_zabudowy' => $aData['building_type'],
            'kontakt_email' => self::CONTACT_EMAIL,
            'kontakt_osoba' => self::CONTACT_NAME,
            'kontakt_telefon' => $aData['phone'],
            'czy_kontakt_agencja' => 1,
            'id_jednostka_pow' => 1,
            'id_waluta' => 1,
            'id_dach' => self::translateRoofType($aData['roof_type']),
            'powierzchnia_dzialki' => $aData['terrain_surface'],
            'id_ogrodzenie' => self::translateFence($aData['fence']),
            'id_podpiwniczenie' => $aData['basement'],
            'id_poddasze' => $aData['attic'],
            'id_dojazd' => self::translateAccess($aData['access']),
            'numer_oferty' => $aData['id'],
            'opis' => self::translateDescription($aData['description'], $aData),
            'rok_budowy' => $aData['year'],
            'ulica' => $aData['street'],
            'wspolrzedne_x' => $aData['lat'],
            'wspolrzedne_y' => $aData['lng'],
        );
    }

    private static function getHouseRent($aData) {

        return array(
            'cena' => $aData['price'],
            'powierzchnia' => $aData['surface'],
            'cena_za_metr' => $aData['price_surface'],
            'garaz_bit' => self::translateAdditional($aData['additional']),
            'id_panstwo' => self::ID_COUNTRY_POLAND,
            'id_region' => self::translateRegion($aData['province']),
            'gmina' => $aData['community'],
            'powiat' => $aData['district'],
            'miejscowosc' => $aData['city'],
            'dzielnica' => $aData['section'],
            'id_liczba_pokoi' => $aData['rooms'],
            'id_liczba_pieter' => $aData['floors'],
            'id_material' => self::translateHouseMaterial($aData['building_material']),
            'id_typ_budynku' => self::translateHouseType($aData['building_type']),
            'id_stan_budynku' => self::translateHouseState($aData['quality']),
            'id_typ_zabudowy' => $aData['building_type'],
            'kontakt_email' => self::CONTACT_EMAIL,
            'kontakt_osoba' => self::CONTACT_NAME,
            'kontakt_telefon' => $aData['phone'],
            'czy_kontakt_agencja' => 1,
            'id_jednostka_pow' => 1,
            'id_waluta' => 1,
            'okna_bit' => self::translateWindowsBit($aData['windows']),
            'id_dach' => self::translateRoofType($aData['roof_type']),
            'powierzchnia_dzialki' => $aData['terrain_surface'],
            'id_ogrodzenie' => self::translatFence($aData['fence']),
            'id_podpiwniczenie' => $aData['basement'],
            'id_poddasze' => $aData['attic'],
            'id_dojazd' => self::translateAccess($aData['access']),
            'numer_oferty' => $aData['id'],
            'opis' => self::translateDescription($aData['description'], $aData),
            'rok_budowy' => $aData['year'],
            'ulica' => $aData['street'],
            'wspolrzedne_x' => $aData['lat'],
            'wspolrzedne_y' => $aData['lng'],
        );
    }

    private static function getLocalSell($aData) {


        if ($aData['id_type'] == Model_Logic_Offer_Local_Rent::ID ||
                $aData['id_type'] == Model_Logic_Offer_Local_Sell::ID
        ) {
            $iPurpose = self::translatePurpose($aData['purpose']);
        } else {
            $iPurpose = self::translateWarehousePurpose($aData['purpose']);
        }

        return array(
            'cena' => $aData['price'],
            'powierzchnia' => $aData['surface'],
            'cena_za_metr' => $aData['price_surface'],
            'garaz_bit' => self::translateAdditional($aData['additional']),
            'id_panstwo' => self::ID_COUNTRY_POLAND,
            'id_region' => self::translateRegion($aData['province']),
            'gmina' => $aData['community'],
            'powiat' => $aData['district'],
            'miejscowosc' => $aData['city'],
            'dzielnica' => $aData['section'],
            // 'id_glosnosc' => $aData['noise'],
            'id_forma_wlasnosci' => self::translateOwnership($aData['ownership']),
            'id_liczba_pieter' => isset($aData['floors_in_building']) ? $aData['floors_in_building'] : '',
            'id_liczba_pokoi' => $aData['rooms'],
            'id_liczba_poziomow' => $aData['levels'],
            // 'id_material' => $aData['building_material'],
            'id_okna' => isset($aData['windows']) ? self::translateWindows($aData['windows']) : '',
            'id_pietro' => self::translateFloor($aData['floor']),
            // 'id_stan_instalacji' => $aData['installation_quality'],
            'id_stan_mieszkania' => $aData['quality'],
            'id_wejscie' => isset($aData['entry']) ? self::translateEntry($aData['entry']) : '',
            'wysokosc_lokalu' => $aData['height'],
            'id_umiejscowienie' => isset($aData['building_type']) ? self::translateLocalType($aData['building_type']) : '',
            'kontakt_email' => self::CONTACT_EMAIL,
            'kontakt_osoba' => self::CONTACT_NAME,
            'kontakt_telefon' => $aData['phone'],
            'czy_kontakt_agencja' => 1,
            'id_jednostka_pow' => 1,
            'id_waluta' => 1,
            'id_przeznaczenie' => $iPurpose,
            'numer_oferty' => $aData['id'],
            'opis' => self::translateDescription($aData['description'], $aData),
            'rok_budowy' => $aData['year'],
            'ulica' => $aData['street'],
            'wspolrzedne_x' => $aData['lat'],
            'wspolrzedne_y' => $aData['lng'],
            'wysokosc_czynszu' => isset($aData['rent']) ? $aData['rent'] : ''
        );
    }

    private static function getLocalRent($aData) {


        if ($aData['id_type'] == Model_Logic_Offer_Local_Rent::ID ||
                $aData['id_type'] == Model_Logic_Offer_Local_Sell::ID
        ) {
            $iPurpose = self::translatePurpose($aData['purpose']);
        } else {
            $iPurpose = self::translateWarehousePurpose($aData['purpose']);
        }

        return array(
            'cena' => $aData['price'],
            'powierzchnia' => $aData['surface'],
            'cena_za_metr' => $aData['price_surface'],
            'garaz_bit' => self::translateAdditional($aData['additional']),
            'id_panstwo' => self::ID_COUNTRY_POLAND,
            'id_region' => self::translateRegion($aData['province']),
            'gmina' => $aData['community'],
            'powiat' => $aData['district'],
            'miejscowosc' => $aData['city'],
            'dzielnica' => $aData['section'],
            'id_liczba_pieter' => isset($aData['floors_in_building']) ? $aData['floors_in_building'] : '',
            'id_liczba_pokoi' => $aData['rooms'],
            'id_liczba_poziomow' => $aData['levels'],
            'id_okna' => isset($aData['windows']) ? self::translateWindows($aData['windows']) : '',
            'id_pietro' => self::translateFloor($aData['floor']),
            'id_stan_mieszkania' => $aData['quality'],
            'id_wejscie' => isset($aData['entry']) ? self::translateEntry($aData['entry']) : '',
            'wysokosc_lokalu' => $aData['height'],
            'id_umiejscowienie' => isset($aData['building_type']) ? self::translateLocalType($aData['building_type']) : '',
            'kontakt_email' => self::CONTACT_EMAIL,
            'kontakt_osoba' => self::CONTACT_NAME,
            'kontakt_telefon' => $aData['phone'],
            'czy_kontakt_agencja' => 1,
            'id_jednostka_pow' => 1,
            'id_waluta' => 1,
            'id_przeznaczenie' => self::translatePurpose($aData['purpose']),
            'numer_oferty' => $aData['id'],
            'opis' => self::translateDescription($aData['description'], $aData),
            'rok_budowy' => $aData['year'],
            'ulica' => $aData['street'],
            'wspolrzedne_x' => $aData['lat'],
            'wspolrzedne_y' => $aData['lng'],
        );
    }

    private static function getLandSell($aData) {
        return array(
            'cena' => $aData['price'],
            'powierzchnia' => $aData['surface'],
            'cena_za_metr' => $aData['price_surface'],
            'id_panstwo' => self::ID_COUNTRY_POLAND,
            'id_region' => self::translateRegion($aData['province']),
            'gmina' => $aData['community'],
            'powiat' => $aData['district'],
            'miejscowosc' => $aData['city'],
            'dzielnica' => $aData['section'],
            'kontakt_email' => self::CONTACT_EMAIL,
            'kontakt_osoba' => self::CONTACT_NAME,
            'kontakt_telefon' => $aData['phone'],
            'id_rodzaj_dzialki' => self::translateLandType($aData['land_type']),
            'czy_kontakt_agencja' => 1,
            'id_jednostka_pow' => 1,
            'id_waluta' => 1,
            'dlugosc_dzialki' => $aData['width'],
            'szerokosc_dzialki' => $aData['height'],
            'id_ogrodzenie' => self::translateLandFence($aData['fence']),
            'id_dojazd' => self::translateAccess($aData['access']),
            'id_ksztalt' => self::translateLandShape($aData['shape']),
            'numer_oferty' => $aData['id'],
            'opis' => self::translateDescription($aData['description'], $aData),
            'ulica' => $aData['street'],
            'wspolrzedne_x' => $aData['lat'],
            'wspolrzedne_y' => $aData['lng']
        );
    }

    private static function getLandRent($aData) {
        return array(
            'cena' => $aData['price'],
            'powierzchnia' => $aData['surface'],
            'cena_za_metr' => $aData['price_surface'],
            'id_panstwo' => self::ID_COUNTRY_POLAND,
            'id_region' => self::translateRegion($aData['province']),
            'gmina' => $aData['community'],
            'powiat' => $aData['district'],
            'miejscowosc' => $aData['city'],
            'dzielnica' => $aData['section'],
            'kontakt_email' => self::CONTACT_EMAIL,
            'kontakt_osoba' => self::CONTACT_NAME,
            'kontakt_telefon' => $aData['phone'],
            'id_rodzaj_dzialki' => self::translateLandType($aData['land_type']),
            'czy_kontakt_agencja' => 1,
            'id_jednostka_pow' => 1,
            'id_waluta' => 1,
            'dlugosc_dzialki' => $aData['width'],
            'szerokosc_dzialki' => $aData['height'],
            'id_ogrodzenie' => self::translateLandFence($aData['fence']),
            'id_dojazd' => self::translateAccess($aData['access']),
            'id_ksztalt' => self::translateLandShape($aData['shape']),
            'numer_oferty' => $aData['id'],
            'opis' => self::translateDescription($aData['description'], $aData),
            'ulica' => $aData['street'],
            'wspolrzedne_x' => $aData['lat'],
            'wspolrzedne_y' => $aData['lng']
        );
    }

    private static function translateEntry($iId) {

        $aKeyTranslate = array(
            1 => 1,
            2 => 3,
            3 => 2
        );


        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateInstalationQuality($iId) {

        $aKeyTranslate = array(
            1 => 4,
            2 => 3,
            3 => 2,
            4 => 1
        );


        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateFloor($mFloor) {


        if ($mFloor == '')
            return $mFloor;
        elseif ($mFloor == 'parter' && $mFloor == 0)
            return 1;
        else
            return $mFloor + 1;
    }

    private static function translateNoise($iId) {

        $aKeyTranslate = array(
            1 => 4,
            2 => 3,
            3 => 2,
            4 => 1
        );




        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateQuality($iId) {

        $aKeyTranslate = array(
            1 => 9,
            2 => 8,
            3 => 12,
            4 => 10,
            5 => 4,
            6 => 2,
            7 => 7
        );


        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateRegion($iRegionId = null) {

        $aKeyTranslate = array(
            1 => 14,
            2 => 1,
            3 => 2,
            4 => 4,
            5 => 5,
            6 => 6,
            7 => 7,
            8 => 8,
            9 => 3,
            10 => 9,
            11 => 10,
            12 => 11,
            13 => 12,
            14 => 13,
            16 => 15,
            17 => 16);

        return isset($aKeyTranslate[$iRegionId]) ? $aKeyTranslate[$iRegionId] : '';
    }

    private static function translateWindowsBit($iWindowsId = null) {

        $aKeyTranslate = array(
            1 => 1,
            2 => 2,
            3 => 1,
            4 => 2,
            5 => 1,
            6 => 2,
            7 => 3);

        return isset($aKeyTranslate[$iWindowsId]) ? array($aKeyTranslate[$iWindowsId]) : '';
    }

    private static function translateWindows($iWindowsId = null, $bBitField = false) {

        $aKeyTranslate = array(
            1 => 2,
            2 => 3,
            3 => 2,
            4 => 3,
            5 => 1,
            6 => 1);

        $iPickedValue = isset($aKeyTranslate[$iWindowsId]) ? $aKeyTranslate[$iWindowsId] : '';

        return $bBitField ? array($iPickedValue) : $iPickedValue;
    }

    private static function translateAdditional($sAdditional = null) {
        $aReturn = array();

        if (empty($sAdditional)) {
            return $aReturn;
        }
        $aData = explode(',', $sAdditional);

        foreach ($aData as $iId) {

            if (isset($aKeyTranslate[$iId]))
                $aReturn[] = $aKeyTranslate[$iId];
        }
    }

    private static function translateHouseType($iBuildingTypeId = null) {
        $aKeyTranslate = array(
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 6,
            6 => 9,
            7 => 12,
            8 => 1
        );

        return isset($aKeyTranslate[$iBuildingTypeId]) ? $aKeyTranslate[$iBuildingTypeId] : '';
    }

    private static function translateMaterial($iMaterialId) {
        $aKeyTranslate = array(
            1 => 3,
            2 => 1,
            3 => 2,
            4 => 6,
            6 => 2);

        return isset($aKeyTranslate[$iMaterialId]) ? $aKeyTranslate[$iMaterialId] : '';
    }

    private static function translateHouseMaterial($iMaterialId = null) {

        $aKeyTranslate = array(
            1 => 1,
            2 => 2,
            3 => 5,
            4 => 7,
            5 => 6,
            6 => 8,
            7 => 8
        );
        return isset($aKeyTranslate[$iMaterialId]) ? $aKeyTranslate[$iMaterialId] : '';
    }

    private static function translateHouseState($iStateId = null) {

        $aKeyTranslate = array(
            1 => 14,
            2 => 2,
            3 => 3,
            4 => 3,
            5 => 8,
            6 => 4,
            7 => 6);

        return isset($aKeyTranslate[$iStateId]) ? $aKeyTranslate[$iStateId] : '';
    }

    private static function translateRoofType($iRoofTypeId = null) {
        $aKeyTranslate = array(
            1 => 7,
            2 => 1,
            3 => 2,
            4 => 3
        );

        return isset($aKeyTranslate[$iRoofTypeId]) ? $aKeyTranslate[$iRoofTypeId] : '';
    }

    private static function translateAccess($iAccessId = null) {

        $aKeyTranslate = array(
            1 => 2,
            3 => 1,
            4 => 3);

        return isset($aKeyTranslate[$iAccessId]) ? $aKeyTranslate[$iAccessId] : '';
    }

    private static function translatePurpose($iPurposeId) {

        $aKeyTranslate = array(
            1 => 1,
            2 => 2,
            3 => 5,
            4 => 3,
            5 => 5);



        return isset($aKeyTranslate[$iPurposeId]) ? $aKeyTranslate[$iPurposeId] : '';
    }

    private static function translateWarehousePurpose($iPurposeId) {

        $aKeyTranslate = array(
            1 => 1,
            2 => 4,
            3 => 3,
            4 => 2,
            5 => 5
        );



        return isset($aKeyTranslate[$iPurposeId]) ? $aKeyTranslate[$iPurposeId] : '';
    }

    private static function translateLocalType($iLocalTypeId) {
        $aKeyTranslate = array(
            1 => 4,
            2 => 1,
            3 => 6,
            4 => 7,
            5 => 2,
            6 => 5,
            7 => 3,
            8 => 10,
            9 => 11,
            10 => 15);

        return isset($aKeyTranslate[$iLocalTypeId]) ? $aKeyTranslate[$iLocalTypeId] : '';
    }

    private static function translateLandType($iLandTypeId) {
        $aKeyTranslate = array(
            1 => 1,
            2 => 2,
            3 => 4,
            4 => 5,
            5 => 3,
            6 => 6,
            7 => 7,
            8 => 9,
            9 => 8,
            10 => 11);

        return isset($aKeyTranslate[$iLandTypeId]) ? $aKeyTranslate[$iLandTypeId] : '';
    }

    private static function translateLandShape($iLandShapeId) {

        $aKeyTranslate = array(
            1 => 1,
            2 => 2,
            3 => 5,
            4 => 3,
            5 => 4,
            6 => 6
        );
        return isset($aKeyTranslate[$iLandShapeId]) ? $aKeyTranslate[$iLandShapeId] : '';
    }

    private static function translateFence($iFenceId) {

        $aKeyTranslate = array(
            1 => 8,
            2 => 2,
            3 => 3,
            4 => 5,
            5 => 4,
            6 => 6,
        );
        return isset($aKeyTranslate[$iFenceId]) ? $aKeyTranslate[$iFenceId] : '';
    }

    private static function translateLandFence($iFenceId) {

        $aKeyTranslate = array(
            1 => 8,
            2 => 2,
            3 => 4,
            4 => 1,
            5 => 5,
            6 => 6
        );
        return isset($aKeyTranslate[$iFenceId]) ? $aKeyTranslate[$iFenceId] : '';
    }

    private static function translateOwnership($iOwnerShip) {

        $aKeyTranslate = array(
            1 => 1,
            2 => 2,
            3 => 5,
            4 => 4,
        );
    }

    private static function translateFlatBuildingType($iType) {
        $aKeyTranslate = array(
            1 => 1,
            2 => 2,
            3 => 5,
            4 => 4,
            5 => 3
        );

        return isset($aKeyTranslate[$iType]) ? $aKeyTranslate[$iType] : '';
    }

    private static function translateDescription($sDesc = '', $aData = array()) {

        $sDesc = strip_tags($sDesc);

        $sAddText = PHP_EOL . PHP_EOL . 'Numer licencji pośrednika odpowiedzialnego zawodowo za wykonanie umowy pośrednictwa: 15295' . PHP_EOL . PHP_EOL .
                'LINK DO STRONY' . PHP_EOL .
                'http://www.domset.pl/' . $aData['id'] . PHP_EOL . PHP_EOL .
                'Kontakt do Agenta' . PHP_EOL .
                $aData['name'] . ' ' . $aData['surname'] . PHP_EOL .
                'tel. kom.: ' . $aData['phone'] . PHP_EOL .
                'e-mail: ' . $aData['email'] . PHP_EOL . PHP_EOL .
                'DOMSET Nieruchomości' . PHP_EOL .
                'Ełk, ul Wojska Polskiego 43 lok. 3' . PHP_EOL .
                'tel.: 87 610 88 88' . PHP_EOL;


        return trim($sDesc) . $sAddText;
    }

}