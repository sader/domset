<?php

class Model_Logic_Export_Gratka_Form extends Model_Logic_Abstract{
   
    
    private $oForm; 
    
        /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Export_Gratka_Form
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Export_Gratka_Form
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
   
    public function getForm($iCategoryId) {

        
        if (($aFields = Model_Logic_Export_Gratka_Api::getInstance($iCategoryId)->getFields()) === false)
            return false;

        $this->oForm = new Form_Abstract();
        $this->oForm->setDecorators($this->oForm->_aFormTableDecorator);

        foreach ($aFields as $oField) {

            $oElem = $this->getElementType($oField, $iCategoryId);
            if ($oElem === false)
                return false;

            $this->oForm->addElement($oElem);
        }

        $this->oForm->addElements($this->getAdditionalFields());

        return $this->oForm;
    }

    private function getAdditionalFields() {

        $aElem['images'] = new Zend_Form_Element_Hidden('images');
        $aElem['images']->setDecorators(array('ViewHelper'));

        

        $aElem['submit'] = new Zend_Form_Element_Submit('submit');
        $aElem['submit']->setDecorators($this->oForm->_aFormSubmitTableDecorator)
                ->setLabel('Wyślij');
        
        return $aElem; 
        
    }

    private function getElementType($oField, $iCategoryId) {

        /**
        if (!Model_Logic_Export_Gratka_Translate::isUsable($oField->klucz, $iCategoryId)) {
            return $this->getHiddenElemet($oField);
        }
        */

        if ($oField->wartosc->czy_tablica_bitowa) {
            return $this->getMulticheckboxElemet($oField, $iCategoryId);
        }

        if ($oField->wartosc->czy_slownik) {
            return $this->getSelectElemet($oField, $iCategoryId);
        }
        
        if($oField->klucz == 'opis')
        {
            return $this->getTextareaElemet($oField);
        }
        
        return $this->getTextElemet($oField);
    }

    private function getTextElemet($oField) {
        $oElem = new Zend_Form_Element_Text($oField->klucz);

        $oElem->setLabel($this->getLabelFromName($oField->klucz))
                ->setDecorators($this->oForm->_aFormElementTableDecorator);


        if ($oField->wartosc->czy_wymagane) {
            $oElem->setRequired(true)
                    ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymanage'));
        }

        return $oElem;
    }
    
    
    private function getTextareaElemet($oField) {
        $oElem = new Zend_Form_Element_Textarea($oField->klucz);

        $oElem->setLabel($this->getLabelFromName($oField->klucz))
                ->setDecorators($this->oForm->_aFormElementTableDecorator);


        if ($oField->wartosc->czy_wymagane) {
            $oElem->setRequired(true)
                    ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymage'));
        }

        return $oElem;
    }

    private function getSelectElemet($oField, $iCategoryId) {
        $oElem = new Zend_Form_Element_Select($oField->klucz);

        if (($aData = Model_Logic_Export_Gratka_Api::getInstance($iCategoryId)->getDictionary($oField->klucz)) === false) {
            return false;
        }   

        if ($oField->wartosc->czy_wymagane) {
            $oElem->setRequired(true)
                    ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));
            
        }
        else $oElem->addMultiOption('');

        foreach ($aData as $oSlownik) {
            $oElem->addMultiOption($oSlownik->klucz, $oSlownik->wartosc);
        }

        $oElem->setLabel($this->getLabelFromName($oField->klucz))
                ->setDecorators($this->oForm->_aFormElementTableDecorator);

        return $oElem;
    }

    private function getHiddenElemet($oField) {
        $oElem = new Zend_Form_Element_Hidden($oField->klucz);

        $oElem->setLabel($this->getLabelFromName($oField->klucz))
                ->setDecorators(array('ViewHelper'));


        if ($oField->wartosc->czy_wymagane) {
            $oElem->setRequired(true)
                    ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymanage'));
        }

        return $oElem;
    }

    private function getMulticheckboxElemet($oField, $iCategoryId) {
        $oElem = new Zend_Form_Element_MultiCheckbox($oField->klucz);

        if (($aData = Model_Logic_Export_Gratka_Api::getInstance($iCategoryId)->getDictionary($oField->klucz)) === false) {
            return false;
        }

        foreach ($aData as $oSlownik) {
            $oElem->addMultiOption($oSlownik->klucz, $oSlownik->wartosc);
        }

        $oElem->setLabel($this->getLabelFromName($oField->klucz))
                ->setDecorators($this->oForm->_aFormElementTableDecorator);

        return $oElem;
    }

    private function getLabelFromName($sName) {

        return ucfirst(str_replace('_', ' ', preg_replace('(id_|_bit)', '', $sName)));
    }

   
}

