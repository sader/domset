<?php

class Model_Logic_Export_Gratka_Api extends Model_Logic_Abstract {
    
    
    private $sLogin = 'biuro@domset.pl';
   // private $sPassword = 'domset321';
   private $sPassword = 'sampras85';
    private $sKey = 'a7c35dc715e6e943e735593410c57fa4';
    private $oClient;
    private $sSessionId; 
    private $iApiVersion  = 2; 
    private $iCategoryId;
    private $loginId = 382;
    
      /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Export_Gratka_Api
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Export_Gratka_Api
     */
    static public function getInstance($iCategoryId) {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self($iCategoryId);
        }
        return self::$_oInstance;
    }
    


    public function __construct($iCategoryId) {
        $this->oClient = new Zend_Soap_Client($this->getUrl(),
                        array('compression' => SOAP_COMPRESSION_ACCEPT));
     //   $this->oClient->setSoapVersion(SOAP_1_1);
        
        
        $this->iCategoryId = $iCategoryId; 
        
        if(empty($this->sSessionId)) $this->login(); 
        
    }
    
    
     private function getUrl() {
        $aSoapUrls = array('production' => 'http://soap.webapi-beta.gratka.pl/dom.html?wsdl',
            'development' => 'http://soaptest.webapi-beta.gratka.pl/dom.html?wsdl',
            'test' => 'http://soaptest.webapi-beta.gratka.pl/dom.html?wsdl',
            'stagging' => 'http://soaptest.webapi-beta.gratka.pl/dom.html?wsdl');

        return isset($aSoapUrls[APPLICATION_ENV]) ? $aSoapUrls[APPLICATION_ENV] : $aSoapUrls['development'];
    }
    
    
   public function login() {
      try
      {
      $aResponse =   $this->oClient->zaloguj($this->sLogin, 
                                              $this->sPassword, 
                                              $this->sKey, 
                                              $this->loginId,
                                              $this->iApiVersion); 
      
      if( isset($aResponse['sesja']) && !empty ($aResponse['sesja']))
        $this->sSessionId = $aResponse['sesja'];
      else throw new Exception ('Problem z identyfikatorem sesji', 500); 
      }
      catch(SoapFault $e)
      {
          throw new Exception($e->getMessage(), 500); 
      }
      
    }

   public function getFields() {

        $sCacheKey = md5('gratka_fields' . $this->iCategoryId);

        if (($aFields = $this->getCache()->load($sCacheKey)) === false) {
            try {
                $aFields = $this->oClient->pobierz_pola($this->sSessionId, $this->iCategoryId);
            } catch (SoapFault $e) {
                $this->getLog()->log($e, Zend_Log::CRIT);
                return false;
            }
            if (empty($aFields['bledy'])) {
                $this->getCache()->save($aFields, $sCacheKey);
            }
        }
        return $aFields;
    }

   public function getDictionary($sName) {

        $sCacheKey = md5('gratka_dictionary' . $sName . $this->iCategoryId);

        if (($aDictionary = $this->getCache()->load($sCacheKey)) === false) {
            try {
                $aDictionary = $this->oClient->pobierz_slownik($this->sSessionId, $sName, $this->iCategoryId);
            } catch (SoapFault $e) {
                $this->getLog()->log($e, Zend_Log::CRIT);
                return false;
            }
            if (empty($aDictionary['bledy'])) {
                $this->getCache()->save($aDictionary, $sCacheKey);
            }
        }
        return $aDictionary;
    }
    
   public function exportImages($aImages, $iRemoteId) {
        
        $aExportedImagesIds = array();

        if (!empty($aImages)) {
            foreach ($aImages as $sKey => $sImage) {
                try {
                     $oResult =   $this->oClient->dodaj_zdjecie_base64($this->sSessionId, $this->iCategoryId, $iRemoteId, $sImage);
                    if($oResult['czy_sukces'])
                    $aExportedImagesIds[$sKey] = $oResult['id_zdjecie']; 
                } catch (SoapFault $e) {
                    throw new Exception('Problem z dodaniem zdjęcia [' . $e->getMessage() . ']', 500);
                }
            }
        }
        
        return $aExportedImagesIds;
    }
    
    public function removeImages($aImages = array(), $iRemoteId) {

        if (!empty($aImages)) {
            foreach ($aImages as $sKey => $sImage) {
                try {
                     $this->oClient->usun_zdjecie_id($this->sSessionId, 
                                                     $this->iCategoryId, 
                                                     $iRemoteId, 
                                                     $sImage);
                } catch (SoapFault $e) {
                    throw new Exception('Problem z dodaniem zdjęcia [ ' . $e->getMessage() . ' ]', 500);
                }
            }
            return;
        }
    }
    
   public function exportOffer($aData) {
        try {
            
            
            $iResponse = $this->oClient->dodaj_ogloszenie($this->sSessionId, $this->iCategoryId, $aData); 
             return (int)$iResponse;
            
        } catch (SoapFault $e) {
            throw new Exception('Problem z dodaniem oferty [ ' . $e->getMessage() . ' ]', 500);
        }
    }
    
   public function editOffer($aData) {
        try {
            $bResponse = $this->oClient->aktualizuj_ogloszenie($this->sSessionId, $this->iCategoryId, $aData); 
            
            if(!$bResponse) throw new Exception('Odpowiedź z serwera Gratki.pl negatywna'); 
            
            return $bResponse; 
            
        } catch (SoapFault $e) {
            throw new Exception('Problem z dodaniem oferty [' . $e->getMessage() . ']', 500);
        }
    }
    
    public function getOffer($iOfferId)
    {
         try {
            
            $aResponse = $this->oClient->pobierz_ogloszenie($this->sSessionId, $this->iCategoryId, $iOfferId); 
             return $aResponse; 
            
        } catch (SoapFault $e) {
            throw new Exception('Problem z dodaniem oferty [ ' . $e->getMessage() . ' ]', 500);
        }
    }

  public function removeOffer($iRemoteId)
  {
        try {
            
            $bResponse = $this->oClient->usun_ogloszenie($this->sSessionId, $this->iCategoryId, (int)$iRemoteId); 
             
        } catch (SoapFault $e) {
            
            
            //Ogłoszenie nei zostało znalezione, czyli nam to pasi
            if($e->getCode() === 0) return true; 
            
            throw new Exception('Problem z dodaniem oferty [' . $e->getMessage() . ']', 500);
        }
    }
      


  /** DECAPITEADED
     * @param type $aData
     * @return Model_Logic_Export_Gratka_Ogloszenie 
     */
    private function getOgloszenie($aData = array()) {
        $oOgloszenie = new Model_Logic_Export_Gratka_Ogloszenie();

        foreach ($aData as $sKey => $sVal) {
               if (property_exists('Model_Logic_Export_Gratka_Ogloszenie', $sKey)) {
            $oOgloszenie->$sKey = $sVal;
             }
        }

        return $oOgloszenie;
    }
    
}