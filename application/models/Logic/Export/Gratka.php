<?php

class Model_Logic_Export_Gratka extends Model_Logic_Export_Abstract implements Model_Logic_Export_Interface {
    const ID = 1;
    const IMAGE_EXPORT_LIMIT = 15;

    private $iOrderId;
    private $iCategoryId;

    public function getOfferId() {
        return $this->iOrderId;
    }

    public function setOfferId($iOrderId) {
        $this->iOrderId = $iOrderId;
    }

    public function getCategoryId() {
        return $this->iCategoryId;
    }

    public function setCategoryId($iOfferId = null) {
        $this->iCategoryId = Model_Logic_Export_Gratka_Translate::getGratkaCategoryId($iOfferId);


        if ($this->iCategoryId === null) {
            throw new Exception('Brak poprawnego identyfikatora kategorii Gratka.pl');
        }
    }

    public function manageAdd(App_Controller_Admin_Abstract $oCtrl) {

        if (($iOfferId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed');
        } else {
            $this->setOfferId($iOfferId);
        }

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($iOfferId);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        } else {
            $this->setCategoryId($aOffer['id_type']);
        }

        if (($oForm = Model_Logic_Export_Gratka_Form::getInstance()->getForm($this->getCategoryId())) === false) {
            throw new Exception('Problem z pobraniem listy pól z gratki i wygenerowaniem formularza');
        }

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            if ($this->addValidatedExport($oForm->getValues())) {
                $oCtrl->successMessage('Operacja eksportu zakończona pomyślnie');
            } else {
                $oCtrl->errorMessage('Wystąpił błąd podczas próby eksportu do gratka.pl. Spróbój później.');
            }

            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array('module' => 'admin',
                                'controller' => 'export',
                                'action' => 'list',
                                'offer' => $this->getOfferId()), 'home', true));
        }

        $aTranslatedOfferData = Model_Logic_Export_Gratka_Translate::translate($this->getCategoryId(), $aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    public function manageEdit(App_Controller_Admin_Abstract $oCtrl, $aExport) {


        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        } else {
            $this->setOfferId($aOffer['id_offer']);
            $this->setCategoryId($aOffer['id_type']);
        }


        if (($oForm = Model_Logic_Export_Gratka_Form::getInstance()->getForm($this->getCategoryId())) === false) {
            throw new Exception('Problem z pobraniem listy pól z gratki i wygenerowaniem formularza');
        }

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $this->editValidatedExport($oForm->getValues(), $aExport) ? $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie') : $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji eksportu do gratka.pl. Spróbój później.');


            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array('module' => 'admin',
                                'controller' => 'export',
                                'action' => 'list',
                                'offer' => $aExport['id_offer']), 'home', true));
        }

        $aTranslatedOfferData = Model_Logic_Export_Gratka_Translate::translate($this->getCategoryId(), $aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    public function manageRemove(App_Controller_Admin_Abstract $oCtrl, $aExport) {

        if (($iOfferId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed');
        }

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        } else {
            $this->setCategoryId($aOffer['id_type']);
        }
        if ($this->removeValidatedExport($aExport)) {
            $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie');
        } else {
            $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji eksportu do gratka.pl. Spróbój później.');
        }
        $oCtrl->getHelper('Redirector')
                ->goToUrl($oCtrl->view->url(array('module' => 'admin',
                            'controller' => 'export',
                            'action' => 'list',
                            'offer' => $aExport['id_offer']), 'home', true));
    }

    public function manageReload(App_Controller_Admin_Abstract $oCtrl, $aExport) {

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        } else {
            $this->setOfferId($aOffer['id_offer']);
            $this->setCategoryId($aOffer['id_type']);
        }

        if (($oForm = Model_Logic_Export_Gratka_Form::getInstance()->getForm($this->getCategoryId())) === false) {
            throw new Exception('Problem z pobraniem listy pól z gratki i wygenerowaniem formularza');
        }

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            if ($this->reloadValidatedExport($oForm->getValues(), $aExport)) {
                $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie');
            } else {
                $oCtrl->errorMessage('Wystąpił błąd podczas próby przeładowania eksportu do gratka.pl. Spróbój później.');
            }

            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array('module' => 'admin',
                                'controller' => 'export',
                                'action' => 'list',
                                'offer' => $aExport['id_offer']), 'home', true));
        }
        $aTranslatedOfferData = Model_Logic_Export_Gratka_Translate::translate($this->getCategoryId(), $aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    private function addValidatedExport($aData) {

        try {
            $iExportedRemoteId = Model_Logic_Export_Gratka_Api::getInstance($this->getCategoryId())->exportOffer($aData);
            $aImages = Model_Logic_Gallery::getInstance()->getImagesDataForOffer($this->getOfferId(), self::IMAGE_EXPORT_LIMIT);
            $aExportedImages = Model_Logic_Export_Gratka_Api::getInstance($this->getCategoryId())->exportImages($aImages, $iExportedRemoteId);
            Model_DbTable_Export::getInstance()->addExport($this->getOfferId(), self::ID, $iExportedRemoteId, $aExportedImages);
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function editValidatedExport($aData, $aExport) {

        try {
            $aData['id'] = $aExport['remote_id'];
            $aData['status'] = 1; 
            Model_Logic_Export_Gratka_Api::getInstance($this->getCategoryId())->editOffer($aData);
            $aImages = Model_Logic_Gallery::getInstance()->getImagesDataForOffer($this->getOfferId(), self::IMAGE_EXPORT_LIMIT);

            if(!empty($aExport['data']))
            {
            Model_Logic_Export_Gratka_Api::getInstance($this->getCategoryId())->removeImages(unserialize($aExport['data']), $aExport['remote_id']);
            }
             $aExportedImages = empty($aImages) ? array() : Model_Logic_Export_Gratka_Api::getInstance($this->getCategoryId())->exportImages($aImages, $aExport['remote_id']);
            
             Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_EDITED, $aExportedImages);
             return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function removeValidatedExport($aExport) {

        try {
            Model_Logic_Export_Gratka_Api::getInstance($this->getCategoryId())->removeOffer($aExport['remote_id']);
            Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_DELETED);
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function reloadValidatedExport($aData, $aExport) {

        try {

            Model_Logic_Export_Gratka_Api::getInstance($this->getCategoryId())->removeOffer($aExport['remote_id']);
            $iExportedRemoteId = Model_Logic_Export_Gratka_Api::getInstance($this->getCategoryId())->exportOffer($aData);
            $aImages = Model_Logic_Gallery::getInstance()->getImagesDataForOffer($this->getOfferId(), self::IMAGE_EXPORT_LIMIT);
            $aExportedImages = Model_Logic_Export_Gratka_Api::getInstance($this->getCategoryId())->exportImages($aImages, $iExportedRemoteId);
            Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_RELOADED, $aExportedImages, $iExportedRemoteId);

            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function groupImagesByAction($aImagesToAdd, $sExportedImages) {

        $aExport = empty($sExportedImages) ? array() : unserialize($sExportedImages);

        return array('add' => array_diff_key($aImagesToAdd, $aExport),
            'remove' => array_diff_key($aExport, $aImagesToAdd),
            'unmoved' => array_intersect_key($aImagesToAdd, $aExport));
    }

}