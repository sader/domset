<?php

class Model_Logic_Export_Tablica_Api_Client {

    private $url = null;

    public function __construct($url) {
        $this->url = $url;
    }

    public function __call($name, $arguments) {
        $id = uniqid();
        $request = array(
            'jsonrpc' => '2.0',
            'method' => $name,
            'params' => $arguments,
            'id' => $id
        );

        $jsonRequest = json_encode($request);
        $ch = curl_init($this->url);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonRequest);
       //  curl_setopt($ch, CURLOPT_PROXY, '59.173.12.236:8090');
           // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders($jsonRequest));

        $jsonResponse = curl_exec($ch);
        $error = curl_errno($ch);
        $errorMessage = curl_error($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        if ($error != 0) {
            throw new Exception($errorMessage);
        }

        if ($info['http_code'] != 200 || empty($jsonResponse)) {
            throw new Exception('Cant fetch data from URL');
        }

        $response = json_decode($jsonResponse, true);
        if (empty($response)) {
            throw new Exception('JSON cannot be decoded');
        }

        if (empty($response['id']) || $response['id'] != $id) {
            throw new Exception('Mismatched JSON-RPC IDs');
        }

        if (!empty($response['error'])) {
            throw new Exception(print_r($response['error'], true));
        } else if (isset($response['result'])) {
            return $response['result'];
        } else {
            throw new Exception('Invalid JSON-RPC response');
        }
    }

    protected function getHeaders($jsonRequest) {
        return array();
    }

}