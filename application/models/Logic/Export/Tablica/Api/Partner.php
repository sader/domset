<?php
class Model_Logic_Export_Tablica_Api_Partner extends Model_Logic_Export_Tablica_Api_Client
{
	private $partnerCode = null;
	private $partnerKey = null;

	public function setPartnerCredentials($partnerCode, $partnerKey)
	{
		$this->partnerCode = $partnerCode;
		$this->partnerKey = $partnerKey;
	}

	protected function getHeaders($jsonRequest)
	{
		$headers = array(
        	"Authentication: partner-code={$this->partnerCode},hmac-sha1=" . hash_hmac('sha1', $jsonRequest, $this->partnerKey)
		);

		return $headers;
	}
}