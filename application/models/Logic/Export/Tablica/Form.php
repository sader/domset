<?php

class Model_Logic_Export_Tablica_Form extends Form_Abstract {

           /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Export_Tablica_Form
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Export_Tablica_Form
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    
    
    public function getForm($iType)
    {
        switch ($iType) {
                case Model_Logic_Offer_Flat_Sell::ID:
                $oForm = $this->flat(); 
                break;
            case Model_Logic_Offer_Flat_Rent::ID:
                $oForm = $this->flat(); 
                break;
            case Model_Logic_Offer_House_Sell::ID:
                $oForm = $this->house(); 
                break;
            case Model_Logic_Offer_House_Rent::ID:
                $oForm = $this->house(); 
                break;
            case Model_Logic_Offer_Local_Sell::ID:
                $oForm = $this->local(); 
                break;
            case Model_Logic_Offer_Local_Rent::ID:
                $oForm = $this->local(); 
                break;
            case Model_Logic_Offer_Land_Sell::ID:
                    $oForm = $this->land(); 
                break;
            case Model_Logic_Offer_Land_Rent::ID:
                $oForm = $this->warehouse(); 
                break;
            case Model_Logic_Offer_Warehouse_Sell::ID:
                $oForm = $this->warehouse(); 
                break;
            case Model_Logic_Offer_Warehouse_Rent::ID:
                $oForm = $this->warehouse(); 
                break;
            default:
                throw new Exception('Brak formularza dla tej kategorii');
                break;
        }
        
         $oElem = new Zend_Form_Element_Submit('submit'); 
         $oElem->setDecorators($this->_aFormSubmitTableDecorator)
                                ->setLabel('Wyślij');
        
        
        $oForm->addElement($oElem); 
        return $oForm; 
    }
    
        
        public function flat() 
        {

            $oFrom = new Form_Abstract(); 
             $this->_aFields['external_id'] = new Zend_Form_Element_Hidden('external_id'); 
             $this->_aFields['external_id']->setDecorators(array('ViewHelper')); 
            $this->_aFields['region_id'] = new Zend_Form_Element_Select('region_id'); 
            $this->_aFields['region_id']->setLabel('Województwo')
                                        ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['subregion_id'] = new Zend_Form_Element_Select('subregion_id'); 
            $this->_aFields['subregion_id']->setLabel('Miasto')
                                           ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['param_m'] = new Zend_Form_Element_Text('param_m'); 
            $this->_aFields['param_m']->setLabel('Powierzchnia')
                                ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['param_rooms'] = new Zend_Form_Element_Select('param_rooms');  
            $this->_aFields['param_rooms']->setLabel('Pokoje')
                                          ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['param_price'] = new Zend_Form_Element_Text('param_price');
            $this->_aFields['param_price']->setLabel('Cena')
                                           ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['title'] = new Zend_Form_Element_text('title');
            $this->_aFields['title']->setLabel('Tytuł')
                                    ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['description'] = new Zend_Form_Element_Textarea('description');
            $this->_aFields['description']->setLabel('Opis')
                                          ->setDecorators($oFrom->_aFormElementTableDecorator); 
        
            $oFrom->addElements($this->_aFields)
                  ->setMethod(Zend_Form::METHOD_POST)
                  ->setDecorators($oFrom->_aFormTableDecorator); 
            
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_Province::getProvinces(), 'region_id' , false, false); 
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_City::getCities(), 'subregion_id' , false, false); 
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_Rooms::getRooms(), 'param_rooms' , false, false); 
        
        return $oFrom; 
    }
    
    public function house()
    {
        $oFrom = new Form_Abstract(); 
             $this->_aFields['external_id'] = new Zend_Form_Element_Hidden('external_id'); 
             $this->_aFields['external_id']->setDecorators(array('ViewHelper')); 
            $this->_aFields['region_id'] = new Zend_Form_Element_Select('region_id'); 
            $this->_aFields['region_id']->setLabel('Województwo')
                                        ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['subregion_id'] = new Zend_Form_Element_Select('subregion_id'); 
            $this->_aFields['subregion_id']->setLabel('Miasto')
                                           ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['param_m'] = new Zend_Form_Element_Text('param_m'); 
            $this->_aFields['param_m']->setLabel('Powierzchnia')
                                ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['param_price'] = new Zend_Form_Element_Text('param_price');
            $this->_aFields['param_price']->setLabel('Cena')
                                           ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['title'] = new Zend_Form_Element_text('title');
            $this->_aFields['title']->setLabel('Tytuł')
                                    ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['description'] = new Zend_Form_Element_Textarea('description');
            $this->_aFields['description']->setLabel('Opis')
                                          ->setDecorators($oFrom->_aFormElementTableDecorator); 
        
            $oFrom->addElements($this->_aFields)
                  ->setMethod(Zend_Form::METHOD_POST)
                  ->setDecorators($oFrom->_aFormTableDecorator); 
            
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_Province::getProvinces(), 'region_id' , false, false); 
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_City::getCities(), 'subregion_id' , false, false); 
        
        return $oFrom; 
    }
    
    public function local()
    {
        $oFrom = new Form_Abstract(); 
             $this->_aFields['external_id'] = new Zend_Form_Element_Hidden('external_id'); 
             $this->_aFields['external_id']->setDecorators(array('ViewHelper')); 
            $this->_aFields['region_id'] = new Zend_Form_Element_Select('region_id'); 
            $this->_aFields['region_id']->setLabel('Województwo')
                                        ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['subregion_id'] = new Zend_Form_Element_Select('subregion_id'); 
            $this->_aFields['subregion_id']->setLabel('Miasto')
                                           ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['param_m'] = new Zend_Form_Element_Text('param_m'); 
            $this->_aFields['param_m']->setLabel('Powierzchnia')
                                ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['param_floor'] = new Zend_Form_Element_Select('param_floor');  
            $this->_aFields['param_floor']->setLabel('Piętro')
                                          ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['param_price'] = new Zend_Form_Element_Text('param_price');
            $this->_aFields['param_price']->setLabel('Cena')
                                           ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['title'] = new Zend_Form_Element_text('title');
            $this->_aFields['title']->setLabel('Tytuł')
                                    ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['description'] = new Zend_Form_Element_Textarea('description');
            $this->_aFields['description']->setLabel('Opis')
                                          ->setDecorators($oFrom->_aFormElementTableDecorator); 
        
            $oFrom->addElements($this->_aFields)
                  ->setMethod(Zend_Form::METHOD_POST)
                  ->setDecorators($oFrom->_aFormTableDecorator); 
            
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_Province::getProvinces(), 'region_id' , false, false); 
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_City::getCities(), 'subregion_id' , false, false); 
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_Floor::getFloor(), 'param_floor' , false, false); 
        
        return $oFrom; 
    }
    
    public function warehouse()
    {
        $oFrom = new Form_Abstract(); 
             $this->_aFields['external_id'] = new Zend_Form_Element_Hidden('external_id'); 
             $this->_aFields['external_id']->setDecorators(array('ViewHelper')); 
            $this->_aFields['region_id'] = new Zend_Form_Element_Select('region_id'); 
            $this->_aFields['region_id']->setLabel('Województwo')
                                        ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['subregion_id'] = new Zend_Form_Element_Select('subregion_id'); 
            $this->_aFields['subregion_id']->setLabel('Miasto')
                                           ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['param_m'] = new Zend_Form_Element_Text('param_m'); 
            $this->_aFields['param_m']->setLabel('Powierzchnia')
                                ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['param_price'] = new Zend_Form_Element_Text('param_price');
            $this->_aFields['param_price']->setLabel('Cena')
                                           ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['title'] = new Zend_Form_Element_text('title');
            $this->_aFields['title']->setLabel('Tytuł')
                                    ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['description'] = new Zend_Form_Element_Textarea('description');
            $this->_aFields['description']->setLabel('Opis')
                                          ->setDecorators($oFrom->_aFormElementTableDecorator); 
        
            $oFrom->addElements($this->_aFields)
                  ->setMethod(Zend_Form::METHOD_POST)
                  ->setDecorators($oFrom->_aFormTableDecorator); 
            
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_Province::getProvinces(), 'region_id' , false, false); 
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_City::getCities(), 'subregion_id' , false, false); 
        
        return $oFrom; 
    }
    
    public function land()
    {
        $oFrom = new Form_Abstract(); 
             $this->_aFields['external_id'] = new Zend_Form_Element_Hidden('external_id'); 
             $this->_aFields['external_id']->setDecorators(array('ViewHelper')); 
            $this->_aFields['region_id'] = new Zend_Form_Element_Select('region_id'); 
            $this->_aFields['region_id']->setLabel('Województwo')
                                        ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['subregion_id'] = new Zend_Form_Element_Select('subregion_id'); 
            $this->_aFields['subregion_id']->setLabel('Miasto')
                                           ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['param_m'] = new Zend_Form_Element_Text('param_m'); 
            $this->_aFields['param_m']->setLabel('Powierzchnia')
                                ->setDecorators($oFrom->_aFormElementTableDecorator)
                                             ->setDecorators($oFrom->_aFormElementTableDecorator); 
           
            $this->_aFields['type'] = new Zend_Form_Element_Select('type'); 
            $this->_aFields['type']->setLabel('Typ działki')
                                ->setDecorators($oFrom->_aFormElementTableDecorator); 
             $this->_aFields['param_price'] = new Zend_Form_Element_Text('param_price');
            $this->_aFields['param_price']->setLabel('Cena')
                                           ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['title'] = new Zend_Form_Element_text('title');
            $this->_aFields['title']->setLabel('Tytuł')
                                    ->setDecorators($oFrom->_aFormElementTableDecorator); 
            $this->_aFields['description'] = new Zend_Form_Element_Textarea('description');
            $this->_aFields['description']->setLabel('Opis')
                                          ->setDecorators($oFrom->_aFormElementTableDecorator); 
        
            $oFrom->addElements($this->_aFields)
                  ->setMethod(Zend_Form::METHOD_POST)
                  ->setDecorators($oFrom->_aFormTableDecorator); 
            
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_Province::getProvinces(), 'region_id' , false, false); 
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_City::getCities(), 'subregion_id' , false, false); 
        $oFrom->populateSelect(Model_Logic_Export_Tablica_Translate_LandType::getLandTypes(), 'type' , false, false); 
        
        return $oFrom; 
    }
}