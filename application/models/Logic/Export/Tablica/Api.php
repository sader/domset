<?php

class Model_Logic_Export_Tablica_Api extends Model_Logic_Abstract {

    private $sLogin = 'DOMSET_nieruchomosci';
    private $sPassKey = '180c6cbbb1e2f89c66578c6695a4aa25';
    private $sUrl = 'http://olx.pl/api/v1/';

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Export_Tablica_Api
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Export_Tablica_Api
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function sendExport($aData) {
        $oClient = new Model_Logic_Export_Tablica_Api_Partner($this->sUrl);
        $oClient->setPartnerCredentials($this->sLogin, $this->sPassKey);
        $aResponse = $oClient->ads_put($aData);
        
        if(!isset($aResponse['id'])) throw new Exception (serialize($aResponse['errors']), 500); 
        else return $aResponse['id'];
    }

    public function removeExport($iId) {

        $oClient = new Model_Logic_Export_Tablica_Api_Partner($this->sUrl);
        $oClient->setPartnerCredentials($this->sLogin, $this->sPassKey);
        return $oClient->ads_removeAd($iId);
    }
    
    public function  categories_parameters($iId, $iType = 'both')
    {
       
        $oClient = new Model_Logic_Export_Tablica_Api_Partner($this->sUrl);
        $oClient->setPartnerCredentials($this->sLogin, $this->sPassKey);
      var_dump($oClient->categories_get()); 
    }

}