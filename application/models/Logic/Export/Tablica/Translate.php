<?php

class Model_Logic_Export_Tablica_Translate {

    const Flat_Sell = 14;
    const Flat_Rent = 15;
    const House_Sell = 18;
    const House_Rent = 20;
    const Land_Sell = 24;
    const Land_Rent = 32;
    const Local_Sell = 125;
    const Local_Rent = 127;
    const Warehouse_Sell = 32;
    const Warehouse_Rent = 32;

    public static function translate($aOffer)
    {
        
        switch ($aOffer['id_type']) {
                case Model_Logic_Offer_Flat_Sell::ID:
                return self::translateFlat($aOffer); 
                break;
            case Model_Logic_Offer_Flat_Rent::ID:
                return self::translateFlat($aOffer); 
                break;
            case Model_Logic_Offer_House_Sell::ID:
                return self::translateHouse($aOffer); 
                break;
            case Model_Logic_Offer_House_Rent::ID:
                return self::translateHouse($aOffer); 
                break;
            case Model_Logic_Offer_Local_Sell::ID:
                return self::translateLocal($aOffer); 
                break;
            case Model_Logic_Offer_Local_Rent::ID:
                return self::translateLocal($aOffer);
                break;
            case Model_Logic_Offer_Land_Sell::ID:
                return self::translateLand($aOffer);
                break;
            case Model_Logic_Offer_Land_Rent::ID:
                return self::translateLand($aOffer);
                break;
            case Model_Logic_Offer_Warehouse_Sell::ID:
                return self::translateWarehouse($aOffer);
                break;
            case Model_Logic_Offer_Warehouse_Rent::ID:
                return self::translateWarehouse($aOffer);
                break;
            default:
                throw new Exception('Brak formularza dla tej kategorii');
                break;
        }
    }


    
    public static function translateCategory($iCategoryId) {
        
        
        
       $aKeyTranslate = array(
            Model_Logic_Offer_Flat_Sell::ID => self::Flat_Sell,
            Model_Logic_Offer_Flat_Rent::ID => self::Flat_Rent,
            Model_Logic_Offer_House_Sell::ID => self::House_Sell,
            Model_Logic_Offer_House_Rent::ID => self::House_Rent,
            Model_Logic_Offer_Land_Sell::ID => self::Land_Sell,
            Model_Logic_Offer_Land_Rent::ID => self::Land_Rent,
            Model_Logic_Offer_Local_Sell::ID => self::Local_Sell,
            Model_Logic_Offer_Local_Rent::ID => self::Local_Rent,
            Model_Logic_Offer_Warehouse_Sell::ID => self::Local_Sell,
            Model_Logic_Offer_Warehouse_Rent::ID => self::Local_Rent
        );
        
              return isset($aKeyTranslate[$iCategoryId]) ? $aKeyTranslate[$iCategoryId] : '';
    }

    public static function translateFlat($aData) {
        
        $aReturn = array();
        $aReturn['region_id'] = self::translateRegion($aData['province']);
        $aReturn['subregion_id'] = self::translateCity($aData['city']);
        $aReturn['param_m'] = $aData['surface'];
        $aReturn['param_rooms'] = self::translateRooms($aData['rooms']);
        $aReturn['param_price'] = $aData['price'];
        $aReturn['title'] = self::translateTitle($aData['title']);
        $aReturn['description'] = self::translateDescription(urldecode($aData['description']), $aData['id']);
        
        return $aReturn; 
    }

    public static function translateHouse($aData) {
     
        
           $aReturn = array();
        $aReturn['region_id'] = self::translateRegion($aData['province']);
        $aReturn['subregion_id'] = self::translateCity($aData['city']);
        $aReturn['param_m'] = $aData['surface'];
        $aReturn['param_price'] = $aData['price'];
        $aReturn['title'] = self::translateTitle($aData['title']);
        $aReturn['description'] = self::translateDescription($aData['description'], $aData['id']);
        
        return $aReturn; 
        
    }

    public static function translateLocal($aData) {
           $aReturn = array();
        $aReturn['region_id'] = self::translateRegion($aData['province']);
        $aReturn['subregion_id'] = self::translateCity($aData['city']);
        $aReturn['param_m'] = $aData['surface'];
        $aReturn['param_floor'] = self::translateFloor($aData['floor']);
        $aReturn['param_price'] = $aData['price'];
        $aReturn['title'] = self::translateTitle($aData['title']);
        $aReturn['description'] = self::translateDescription(urldecode($aData['description']), $aData['id']);
        
           return $aReturn; 
    }

    public static function translateLand($aData) {
          $aReturn = array();
         $aReturn['region_id'] = self::translateRegion($aData['province']);
        $aReturn['subregion_id'] = self::translateCity($aData['city']);
        $aReturn['param_m'] = $aData['surface'];
        $aReturn['param_price'] = $aData['price'];
        $aReturn['title'] = self::translateTitle($aData['title']);
        $aReturn['description'] = self::translateDescription(urldecode($aData['description']), $aData['id']);
        
           return $aReturn; 
    }

    public static function translateWarehouse($aData) {
           $aReturn = array();
        $aReturn['region_id'] = self::translateRegion($aData['province']);
        $aReturn['subregion_id'] = self::translateCity($aData['city']);
        $aReturn['param_m'] = $aData['surface'];
        $aReturn['param_price'] = $aData['price'];
        $aReturn['title'] = self::translateTitle($aData['title']);
        $aReturn['description'] = self::translateDescription($aData['description'], $aData['id']);
        
           return $aReturn; 
    }

    private static function translateRegion($iProvince) {
        $aKeyTranslate = array(
            1 => 14,
            2 => 3,
            3 => 15,
            4 => 8,
            5 => 9,
            6 => 7,
            7 => 4,
            8 => 2,
            9 => 12,
            10 => 17,
            11 => 18,
            12 => 5,
            13 => 6,
            14 => 13,
            15 => 1,
            16 => 11);


        return isset($aKeyTranslate[$iProvince]) ? $aKeyTranslate[$iProvince] : '';
    }

    private static function translateCity($sCity) {
        $iSearched =  array_search($sCity, Model_Logic_Export_Tablica_Translate_City:: getCities());
        return empty($iSearched) ? 43 : $iSearched;

    }

    private static function translateRooms($iRooms) {
        if (empty($iRooms)) return '';
        elseif ($iRooms == 1) return 'one';
        elseif ($iRooms == 2) return 'two';
        elseif ($iRooms == 3) return 'three';
        elseif ($iRooms >= 4) return 'four';
    }
    
    
      private static function translateFloor($iFloor) {
        
        if ($iFloor == 'Parter' && $iFloor == '0') return 'parter';
        elseif ($iFloor == 1) return 'one';
        elseif ($iFloor == 2) return 'two';
        elseif ($iFloor == 3) return 'three';
        elseif ($iFloor >= 4) return 'four';
    }

    private static function translateTitle($sTitle) {
        return (mb_strlen($sTitle) < Model_Logic_Export_Tablica::TITLE_LENGHT_LIMIT) ? $sTitle : mb_substr($sTitle, 0, Model_Logic_Export_Tablica::TITLE_LENGHT_LIMIT);
    }

    private static function translateDescription($sDesc, $iId) {

        
        $sFooter = PHP_EOL .PHP_EOL . 'Numer licencji pośrednika odpowiedzialnego zawodowo za wykonanie umowy pośrednictwa: 15295'. PHP_EOL . PHP_EOL .
                'LINK DO STRONY' . PHP_EOL .
                   'http://www.domset.pl/'.$iId. PHP_EOL .
                   'DOMSET Nieruchomości'. PHP_EOL .
                   'Ełk, ul Wojska Polskiego 43 lok. 3';
        
        return preg_replace('/&nbsp;/' , '' , $sDesc).PHP_EOL.$sFooter; 
    }
    
}

