<?php

class Model_Logic_Export_Tablica_Translate_LandType {

    private static $aLandTypes = array(
          'dzialki-budowlane' => 'Działki budowlane',
          'dzialki-rolne' => 'Działki rolne',
          'dzialki-lesne' => 'Działki leśne',
          'dzialki-inwestycyjne' => 'Działki inwestycyjne',
          'dzialki-rolno-budowlane' => 'Działki rolno-budowlane',
          'dzialka-siedliskowa' => 'Działka siedliskowa'
    );
  
    public static function getLandTypes() {
        return self::$aLandTypes; 
    }

}

