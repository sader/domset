<?php

class Model_Logic_Export_Tablica_Translate_Floor {

    private static $aFloor = array(
        'parter' => 'Parter',
        'one' => '1',
        'two' => '2',
        'three' => '3',
        'four' => '4 i wyżej'
    );

    public static function getFloor() {

        return self::$aFloor;
    }

}

