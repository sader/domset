<?php

class Model_Logic_Export_Tablica_Translate_Province {

    private static $aProvinces = array(
        3 => "Dolnośląskie",
        15 => "Kujawsko-pomorskie",
         8 => "Lubelskie",
         9 => "Lubuskie",
         7 => "Łódzkie",
         4 => "Małopolskie",
         2 => "Mazowieckie",
        12 => "Opolskie",
        17 => "Podkarpackie",
        18 => "Podlaskie",
         5 => "Pomorskie",
         6 => "Śląskie",
        13 => "Świętokrzyskie",
        14 => "Warmińsko-mazurskie",
         1 => "Wielkopolskie",
        11 => "Zachodniopomorskie"
    );

    public static function getProvinces() {
        return self::$aProvinces;
    }

}

