<?php

class Model_Logic_Export_Tablica_Translate_Rooms {

    private static $aRooms = array(
        'one' => '1 pokój',
        'two' => '2 pokoje',
        'three' => '3 pokoje',
        'four' => '4 i więcej'
    );

    public static function getRooms() {

        return self::$aRooms;
    }

}

