<?php

class Model_Logic_Export_Domimporta_Api {

    const CURRENCY_PLN = 'PLN';
    const SELLER_LICENCE = '15295';
    const SELLER_NAME = 'Marcin Brochadzki';
    const PACKAGE_TYPE_FULL = 'całość';
    const PACKAGE_TYPE_NEW = 'przyrost';
    const PACKAGE_TYPE_NOIMG = 'całość bez zdjęć';
    const FILENAME = 'dane';

    private $sFtpHost = 'importy.trader.pl';
    private $sFtpLogin = 'nieruchdomset1';
    private $sFtpPassword = 'q6A4Bpr3ZH';
    private $sFtpPath = '/';
    private $sTimeStampFile = null;

    /* całość, przyrost, całość bez zdjęć
     * Instancja klasy.
     *
     * @var  Model_Logic_Export_Domimporta_Api
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------

    /**
     * Zwraca instancje klasy.
     *
     * @return  Model_Logic_Export_Domimporta_Api
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function __construct() {

        $this->sTimeStampFile = APPLICATION_PATH . '/models/Logic/Export/Domimporta/timestamp';
    }

    public function prepareDataForXml($aOffer, $aFormData, $iExportId) {
        $aReturn = array();

        foreach ($aFormData as $sKey => $sValue) {

                $aReturn[$sKey] = $sValue;
        }

        $aReturn['ID'] = $iExportId;
        $aReturn['Nr_oferty'] = $aOffer['id'];
        $aReturn['Nr_licencji'] = self::SELLER_LICENCE;
        $aReturn['Kategoria'] = Model_Logic_Export_Domimporta_Translate::translateCategory($aOffer);
        $aReturn['Operacja'] = Model_Logic_Export_Domimporta_Translate::translateOperationType($aOffer['id_type']);
        $aReturn['Waluta'] = self::CURRENCY_PLN;


        if (!empty($aOffer['lat']) && !empty($aOffer['lng'])) {
            $aReturn['GeoLokalizacja']['Szerokosc'] = $aOffer['lat'];
            $aReturn['GeoLokalizacja']['Dlugosc'] = $aOffer['lng'];
        }
        return $aReturn;
    }

    public function sendOffers() {
        try {
            $aExports = Model_DbTable_Export::getInstance()->getExportsByAdapter(Model_Logic_Export_Domimporta::ID, $this->getDate());
            if (empty($aExports)) {
                $this->saveTimestamp();
                echo 'Nic do wyslania';
                return false;
            }

            $aData = array();

            foreach ($aExports as $aExport) {

                switch ($aExport['type']) {
                    case Model_DbTable_Export::TYPE_ADDED:
                    case Model_DbTable_Export::TYPE_EDITED:
                    case Model_DbTable_Export::TYPE_RELOADED:
                        $aFormData = unserialize($aExport['data']);
                        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);
                        $aData['existed'][] = array('data' => $this->prepareDataForXml($aOffer, $aFormData, $aExport['id']),
                            'images' => Model_Logic_Gallery::getInstance()->getImagesPathForOffer($aExport['id_offer']));
                        break;
                    case Model_DbTable_Export::TYPE_DELETED:
                        $aData['deleted'][] = $aExport['id'];
                        break;
                    default:
                        throw new Exception('Niezidentyfikowany typ eksportu', 500);
                        break;
                }
            }

            $sFullXmlFile = $this->createXml($aData, self::PACKAGE_TYPE_NEW);
            $sSavedZipPackage = $this->saveToZip($sFullXmlFile, self::PACKAGE_TYPE_NEW, $aData);
            $this->saveTimestamp();
            $this->sendToFtp($sSavedZipPackage);
            echo 'Wyslano '.count($aExports).' ofert';
        } catch (Exception $e) {
            Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('log')->log($e, Zend_Log::CRIT);
        }
    }

    private function saveToZip($sData, $sFileName, $aData) {

        $sFilePath = APPLICATION_PATH .
                DIRECTORY_SEPARATOR .
                'data' .
                DIRECTORY_SEPARATOR .
                'trader-' . date('Ymd') . '-' . date('His') . '-' . $sFileName . '.zip';

        $oZip = new ZipArchive();
        $oZip->open($sFilePath, ZIPARCHIVE::OVERWRITE);
        $oZip->addFromString(self::FILENAME . '.xml', $sData);

        foreach ($aData['existed'] as $aElements) {
            foreach ($aElements['images'] as $imagePath) {
                $oZip->addFile($imagePath, basename($imagePath));
            }
        }

        $oZip->close();
        return $sFilePath;
    }

    private function createXml($aOffers, $sPackageType = 'całość') {

        $oDocument = new DOMDocument();
        $oDocument->formatOutput = true;

        $oRootElement = $oDocument->createElement("Trader");
        $oDocument->appendChild($oRootElement);

        $oInf = $oDocument->createElement('Informacje');
        $oPack = $oDocument->createElement('RodzajPaczki');
        $oPack->appendChild($oDocument->createTextNode($sPackageType));
        $oInf->appendChild($oPack);
        $oRootElement->appendChild($oInf);

        if (isset($aOffers['deleted']) AND count($aOffers['deleted']) > 0) {

            $oDelete = $oDocument->createElement('Usun');

            foreach ($aOffers['deleted'] as $iDeletedOfferId) {
                $oId = $oDocument->createElement('ID');
                $oId->appendChild($oDocument->createTextNode($iDeletedOfferId));
                $oDelete->appendChild($oId);
            }

            $oRootElement->appendChild($oDelete);
        }

        $oOffers = $oDocument->createElement('Oferty');
        $oRootElement->appendChild($oOffers);

        $oType = $oDocument->createElement('Nieruchomosci');
        $oOffers->appendChild($oType);

        foreach ($aOffers['existed'] as $mData) {

            $oOffer = $oDocument->createElement('Oferta');

            foreach ($mData['data'] as $sKey => $mVal) {
                switch ($sKey) {

                    case 'Przeznaczenie':
                        $oPurpose = $oDocument->createElement('Przeznaczenie');

                        foreach ($mVal as $sPurpose) {
                            $oPurposeElem = $oDocument->createElement('PrzeznaczenieElement');
                            $oPurposeElem->appendChild($oDocument->createTextNode($sPurpose));
                            $oPurpose->appendChild($oPurposeElem);
                        }

                        $oOffer->appendChild($oPurpose);

                        break;
                    case 'GeoLokalizacja':
                        if (isset($mVal['lat'], $mVal['lng'])) {
                            $oGeo = $oDocument->createElement('GeoLokalizacja');
                            $oLat = $oDocument->createElement('Szerokosc');
                            $oLat->appendChild($oDocument->createTextNode($mVal['lat']));
                            $oLng = $oDocument->createElement('Dlugosc');
                            $oLng->appendChild($oDocument->createTextNode($mVal['lng']));

                            $oGeo->appendChild($oLat);
                            $oGeo->appendChild($oLng);
                            $oOffer->appendChild($oGeo);
                        }

                        break;
                    default:
                        $oElem = $oDocument->createElement($sKey);
                        $oElem->appendChild($oDocument->createTextNode($mVal));
                        $oOffer->appendChild($oElem);
                        break;
                }
            }
            $oImgs = $oDocument->createElement('Zdjecia');
            $oOffer->appendChild($oImgs);

            foreach ($mData['images'] as $sImagePath) {
                $oImg = $oDocument->createElement('Zdjecie');
                $oImageName = $oDocument->createElement('NazwaZdjecia');
                $oImageName->appendChild($oDocument->createTextNode(basename($sImagePath)));
                $oImg->appendChild($oImageName);
                $oImgs->appendChild($oImg);
            }


            $oType->appendChild($oOffer);
        }


        return $oDocument->saveXML();
    }

    private function sendToFtp($aPackage) {
        if (($oConnectionId = ftp_connect($this->sFtpHost, 21)) === false) {
            throw new Exception('Błąd połączenia do FTP domiporta.pl', 500);
        }


        if (!ftp_login($oConnectionId, $this->sFtpLogin, $this->sFtpPassword)) {
            throw new Exception('Błąd podczas zalogowania do FTP domiporta.pl', 500);
        }



        if (!ftp_put($oConnectionId, $this->sFtpPath . basename($aPackage), $aPackage, FTP_BINARY)) {
            throw new Exception('Błąd podczas transferu pliku do FTP domiporta.pl', 500);
        }

         if (!ftp_rename($oConnectionId, $this->sFtpPath . basename($aPackage), $this->sFtpPath . 'ok-' . basename($aPackage))) {
            throw new Exception('Zmiana nazwy nieudana', 500);
        }


        return true;
    }

    public function getDate() {

        if (file_exists($this->sTimeStampFile)) {
            $sDate = file_get_contents($this->sTimeStampFile);
            return trim($sDate);
        }
        else
            return null;
    }

    private function saveTimestamp() {
        file_put_contents($this->sTimeStampFile, date('Y-m-d H:i:s'));
    }

}

