<?php

class Model_Logic_Export_Domimporta_Form_Land extends Form_Abstract {

    public function __construct() {

        parent::__construct();

        $this->_aFields['Informacje_dodatkowe'] = new Zend_Form_Element_Text('Informacje_dodatkowe');
        $this->_aFields['Informacje_dodatkowe']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Tytuł');


        $this->_aFields['Wojewodztwo'] = new Zend_Form_Element_Select('Wojewodztwo');
        $this->_aFields['Wojewodztwo']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Województwo')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));

        $this->_aFields['Powiat'] = new Zend_Form_Element_Text('Powiat');
        $this->_aFields['Powiat']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Powiat')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));


        $this->_aFields['Gmina'] = new Zend_Form_Element_Text('Gmina');
        $this->_aFields['Gmina']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Gmina')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));


        $this->_aFields['Miasto'] = new Zend_Form_Element_Text('Miasto');
        $this->_aFields['Miasto']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Miasto')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));


        $this->_aFields['Dzielnica'] = new Zend_Form_Element_Text('Dzielnica');
        $this->_aFields['Dzielnica']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Dzielnica')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));


        $this->_aFields['Ulica'] = new Zend_Form_Element_Text('Ulica');
        $this->_aFields['Ulica']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Ulica')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));


        $this->_aFields['Powierzchnia'] = new Zend_Form_Element_Text('Powierzchnia');
        $this->_aFields['Powierzchnia']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Powierzchnia')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));


        $this->_aFields['Cena'] = new Zend_Form_Element_Text('Cena');

        $this->_aFields['Cena']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Cena')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));

        $this->_aFields['Cena_metra'] = new Zend_Form_Element_Text('Cena_metra');
        $this->_aFields['Cena_metra']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Cena_metra')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));


        
        $this->_aFields['Opis'] = new Zend_Form_Element_Textarea('Opis');
        $this->_aFields['Opis']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Opis')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Te pole jest wymagane'));

     
        $this->_aFields['Do_negocjacji'] = new Zend_Form_Element_Checkbox('Do_negocjacji');
        $this->_aFields['Do_negocjacji']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Cena do negocjacji');
        
          $this->_aFields['Ogrodzenie'] = new Zend_Form_Element_Select('Ogrodzenie');
        $this->_aFields['Ogrodzenie']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Ogrodzenie');
        
        
        $this->_aFields['Droga_dojazdowa'] = new Zend_Form_Element_Select('Droga_dojazdowa');
        $this->_aFields['Droga_dojazdowa']->setDecorators($this->_aFormElementTableDecorator)
                ->setLabel('Dojazd');

        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setDecorators($this->_aFormSubmitTableDecorator)
                ->setLabel('Wyślij');

        $this->addElements($this->_aFields)
                ->setDecorators($this->_aFormTableDecorator)
                ->setMethod(Zend_FORM::METHOD_POST);

        $this->populateSelect(Model_Logic_Export_Domimporta_Translate_Province::getProvinces(), 'Wojewodztwo', false, true);
        
        $this->populateSelect(Model_Logic_Export_Domimporta_Translate_Access::getAccess(), 'Droga_dojazdowa', false, true);
        $this->populateSelect(Model_Logic_Export_Domimporta_Translate_Fence::getFence(), 'Ogrodzenie', false, true);
    }

}

