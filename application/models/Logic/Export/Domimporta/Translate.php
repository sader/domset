<?php

class Model_Logic_Export_Domimporta_Translate {

    const CONTACT_NAME = 'DOMSET Nieruchomości';
    const CONTACT_EMAIL = 'biuro@domset.pl';

    public static function translateOperationType($iId) {
        switch ($iId) {
            case Model_Logic_Offer_Flat_Sell::ID:
            case Model_Logic_Offer_House_Sell::ID:
            case Model_Logic_Offer_Local_Sell::ID:
            case Model_Logic_Offer_Land_Sell::ID:
            case Model_Logic_Offer_Warehouse_Sell::ID:
                return 'sprzedaż';
                break;
            case Model_Logic_Offer_Flat_Rent::ID:
            case Model_Logic_Offer_House_Rent::ID:
            case Model_Logic_Offer_Local_Rent::ID:
            case Model_Logic_Offer_Land_Rent::ID:
            case Model_Logic_Offer_Warehouse_Rent::ID:
                return 'wynajem';
                break;
        }
    }

    public static function translateCategory($aOffer) {
        switch ($aOffer['id_type']) {
            case Model_Logic_Offer_Flat_Sell::ID:
            case Model_Logic_Offer_Flat_Rent::ID:

                switch ($aOffer['rooms']) {
                    case 1:
                        return 30796;
                        break;
                    case 2:
                        return 30797;
                        break;
                    case 3:
                        return 30798;
                        break;
                    case 4:
                        return 30799;
                        break;
                    case 5:
                        return 30800;
                        break;
                    default:
                        return 308001;
                        break;
                }
                break;
            case Model_Logic_Offer_House_Sell::ID:
            case Model_Logic_Offer_House_Rent::ID:

                switch ($aOffer['building_type']) {
                    case 1:
                        return 30802;
                        break;
                    case 2:
                    case 3:
                        return 30803;
                        break;
                    case 4:
                        return 30804;
                        break;
                    case 5:
                        return 30805;
                        break;
                    case 6:
                        return 30807;
                        break;
                    case 7:
                        return 30809;
                        break;
                    case 8:
                        return 30811;
                        break;
                    default:
                        return 30811;
                        break;
                }
            case Model_Logic_Offer_Local_Sell::ID:
            case Model_Logic_Offer_Local_Rent::ID:
            case Model_Logic_Offer_Warehouse_Sell::ID:
            case Model_Logic_Offer_Warehouse_Rent::ID:
                return 197;
                break;
            case Model_Logic_Offer_Land_Sell::ID:
            case Model_Logic_Offer_Land_Rent::ID:
                switch ($aOffer['land_type']) {
                    case 1:
                        return 30812;
                        break;
                    case 2:
                        return 30818;
                        break;
                    case 3:
                        return 30813;
                        break;
                    case 4:
                        return 30816;
                        break;
                    case 5:
                        return 30820;
                        break;
                    case 6:
                        return 30817;
                        break;
                    case 7:
                        return 30814;
                        break;
                    case 8:
                        return 30819;
                        break;
                    case 9:
                        return 30819;
                        break;
                    case 10:
                        return 30812;
                        break;
                    case 11:
                        return 30821;
                        break;
                    default:
                        return 30821;
                        break;
                }
                break;
            default:
                break;
        }
    }

    public static function translate($iCategoryId, $aData) {

        switch ($iCategoryId) {
            case Model_Logic_Offer_Flat_Sell::ID:
            case Model_Logic_Offer_Flat_Rent::ID:
                return self::translateFlat($aData);
                break;
            case Model_Logic_Offer_House_Sell::ID:
            case Model_Logic_Offer_House_Rent::ID:
                return self::translateHouse($aData);
                break;
            case Model_Logic_Offer_Land_Sell::ID:
            case Model_Logic_Offer_Land_Rent::ID:
                return self::translateLand($aData);
                break;
            case Model_Logic_Offer_Local_Sell::ID:
            case Model_Logic_Offer_Local_Rent::ID:
            case Model_Logic_Offer_Warehouse_Sell::ID:
            case Model_Logic_Offer_Warehouse_Rent::ID:
                return self::translateLocal($aData);
                break;
            default:
                throw new Exception('No offer translate adapted for type ' . $iCategoryId);
                break;
        }
    }

    private static function translateFlat($aData) {
        return array(
            // 'Informacje_dodatkowe' => $aData['title'],
            'Wojewodztwo' => self::translateProvince($aData['province']),
            'Powiat' => $aData['district'],
            'Gmina' => $aData['city'],
            'Miasto' => $aData['city'],
            'Dzielnica' => $aData['section'],
            'Ulica' => $aData['street'],
            'Powierzchnia' => $aData['surface'],
            'Cena' => $aData['price'],
            'Cena_metra' => $aData['price_surface'],
            'Rok' => $aData['year'],
            'Liczba_pieter' => $aData['floors_in_building'],
            'Pietro' => $aData['floor'],
            'Opis' => self::translateDescription($aData['description'], $aData),
            'Przeznaczenie' => self::translateBuildingType($aData['building_type']),
            'Material' => self::translateFlatMaterial($aData['building_material'])
        );
    }

    private static function translateHouse($aData) {
        return array(
            // 'Informacje_dodatkowe' => $aData['title'],
            'Wojewodztwo' => self::translateProvince($aData['province']),
            'Powiat' => $aData['district'],
            'Gmina' => $aData['city'],
            'Miasto' => $aData['city'],
            'Dzielnica' => $aData['section'],
            'Ulica' => $aData['street'],
            'Powierzchnia' => $aData['surface'],
            'Cena' => $aData['price'],
            'Cena_metra' => $aData['price_surface'],
            'Rok' => $aData['year'],
            'Liczba_pieter' => $aData['floors'],
            'Opis' => self::translateDescription($aData['description'], $aData),
            'Material' => self::translateHouseMaterial($aData['building_material']),
            'Dzialka_powierzchnia' => $aData['terrain_surface']
        );
    }

    private static function translateLocal($aData) {
               
        return array(
            // 'Informacje_dodatkowe' => $aData['title'],
            'Wojewodztwo' => self::translateProvince($aData['province']),
            'Powiat' => $aData['district'],
            'Gmina' => $aData['city'],
            'Miasto' => $aData['city'],
            'Dzielnica' => $aData['section'],
            'Ulica' => $aData['street'],
            'Powierzchnia' => $aData['surface'],
            'Cena' => $aData['price'],
            'Cena_metra' => $aData['price_surface'],
            'Liczba_pieter' => $aData['floors_in_building'],
            'Liczba_pomieszczen' => $aData['rooms'],
            'Rok' => $aData['year'],
            'Opis' => self::translateDescription($aData['description'], $aData),
            'Material' => self::translateFlatMaterial($aData['building_material']),
            'Przeznaczenie' => self::translatePurpouse($aData['purpose'])
        );
    }

    private static function translateLand($aData) {

        return array(
            //  'Informacje_dodatkowe' => $aData['title'],
            'Wojewodztwo' => self::translateProvince($aData['province']),
            'Powiat' => $aData['district'],
            'Gmina' => $aData['city'],
            'Miasto' => $aData['city'],
            'Dzielnica' => $aData['section'],
            'Ulica' => $aData['street'],
            'Powierzchnia' => $aData['surface'],
            'Cena' => $aData['price'],
            'Cena_metra' => $aData['price_surface'],
            'Opis' => self::translateDescription($aData['description'], $aData),
            'Material' => self::translateFlatMaterial($aData['building_material']),
            'Ogrodzenie' => self::translateFence($aData['fence']),
            'Droga_dojazdowa' => self::translateAccess($aData['access'])
        );
    }

    private static function translateDescription($sDesc = '', $aData = array()) {

        $sAddText = PHP_EOL . PHP_EOL . 'Numer licencji pośrednika odpowiedzialnego zawodowo za wykonanie umowy pośrednictwa: 15295' . PHP_EOL . PHP_EOL .
                'LINK DO STRONY' . PHP_EOL .
                'http://www.domset.pl/' . $aData['id'] . PHP_EOL . PHP_EOL .
                'Kontakt do Agenta' . PHP_EOL .
                $aData['name'] . ' ' . $aData['surname'] . PHP_EOL .
                'tel. kom.: ' . $aData['phone'] . PHP_EOL .
                'e-mail: ' . $aData['email'] . PHP_EOL . PHP_EOL .
                'DOMSET Nieruchomości' . PHP_EOL .
                'Ełk, ul Wojska Polskiego 43 lok. 3' . PHP_EOL .
                'tel.: 87 610 88 88' . PHP_EOL;


        return trim($sDesc) . $sAddText;
    }

    private static function translateFlatMaterial($iId) {

        $aKeyTranslate = array(
            1 => 247,
            2 => 241,
            3 => 243,
            4 => 240,
            5 => 242,
            6 => 334,
            7 => 247
        );

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateHouseMaterial($iId) {

        $aKeyTranslate = array(
            1 => 241,
            2 => 243,
            3 => 245,
            4 => 330,
            5 => 247,
            6 => 247,
            7 => 247
        );

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateBuildingType($iId) {

        $aKeyTranslate = array(
            1 => 235,
            2 => 237,
            3 => 239,
            4 => 236,
            5 => 238,
            6 => 239
        );
        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateProvince($iId) {

        $aKeyTranslate = array(
            1 => 30,
            2 => 17,
            3 => 18,
            4 => 19,
            5 => 20,
            6 => 21,
            7 => 22,
            8 => 23,
            9 => 24,
            10 => 25,
            11 => 26,
            12 => 27,
            13 => 28,
            14 => 29,
            15 => 31,
            16 => 32
        );
        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateFence($iId) {

        $aKeyTranslate = array(
            1 => 355,
            2 => 349,
            3 => 350,
            4 => 351,
            5 => 354,
            6 => 352,
            7 => 354
        );
        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateAccess($iId) {

        $aKeyTranslate = array(
            1 => 466,
            2 => 468,
            3 => 465,
            4 => 468
        );
        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    public static function translatePurpouse($iId) {

        $aKeyTranslate = array(
            1 => 902,
            2 => 905,
            3 => 904,
            4 => 916,
            5 => 916
        );

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }
}
