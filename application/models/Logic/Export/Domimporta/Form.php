<?php

class Model_Logic_Export_Domimporta_Form extends Form_Abstract {

     
        /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Export_Domimporta_Form
     */
    static private $_oInstance;
    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Export_Domimporta_Form
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }
    
    
    
    public function getForm($iType)
    {
        switch ($iType) {
                case Model_Logic_Offer_Flat_Sell::ID:
                return new Model_Logic_Export_Domimporta_Form_Flat();
                break;
            case Model_Logic_Offer_Flat_Rent::ID:
                return new Model_Logic_Export_Domimporta_Form_Flat();
                break;
            case Model_Logic_Offer_House_Sell::ID:
                return new Model_Logic_Export_Domimporta_Form_House();
                break;
            case Model_Logic_Offer_House_Rent::ID:
                return new Model_Logic_Export_Domimporta_Form_House();
                break;
            case Model_Logic_Offer_Local_Sell::ID:
                return new Model_Logic_Export_Domimporta_Form_Local();
                break;
            case Model_Logic_Offer_Local_Rent::ID:
                return new Model_Logic_Export_Domimporta_Form_Local();
                break;
            case Model_Logic_Offer_Land_Sell::ID:
                return new Model_Logic_Export_Domimporta_Form_Land();
                break;
            case Model_Logic_Offer_Land_Rent::ID:
                return new Model_Logic_Export_Domimporta_Form_Land();
                break;
            case Model_Logic_Offer_Warehouse_Sell::ID:
                return new Model_Logic_Export_Domimporta_Form_Local();
                break;
            case Model_Logic_Offer_Warehouse_Rent::ID:
                return new Model_Logic_Export_Domimporta_Form_Local();
                break;
            default:
                throw new Exception('Brak formularza dla tej kategorii');
                break;
        }
    }
    
}