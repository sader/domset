<?php

class Model_Logic_Export_Domimporta_Translate_Province {

    private static $aProvinces = array(
        17 => "Dolnośląskie",
        18 => "Kujawsko-pomorskie",
        19 => "Lubelskie",
        20 => "Lubuskie",
        21 => "Łódzkie",
        22 => "Małopolskie",
        23 => "Mazowieckie",
        24 => "Opolskie",
        25 => "Podkarpackie",
        26 => "Podlaskie",
        27 => "Pomorskie",
        28 => "Śląskie",
        29 => "Świętokrzyskie",
        30 => "Warmińsko-mazurskie",
        31 => "Wielkopolskie",
        32 => "Zachodniopomorskie"
    );

    public static function getProvinces() {
        return self::$aProvinces;
    }

}

