<?php

class Model_Logic_Export_Domimporta_Translate_Purpouse {

    private static $aPurpouse = array(
        902 => 'Biuro',
        910 => 'Magazyn',
        903 => 'Usługi',
        905 => 'Handel',
        904 => 'Gastronomia',
        906 => 'Gabinet',
        907 => 'Warsztat',
        914 => 'Pawilon',
        909 => 'Hala',
        908 => 'Centrum dystrybucyjne',
        912 => 'Centrum handlowe',
        913 => 'Hotel-pensjonat',
        911 => 'Produkcja',
        916 => 'Inne');

    public static function getPurpouse() {
        return self::$aPurpouse;
    }

}

