<?php

class Model_Logic_Export_Domimporta_Translate_Type {

    private static $aFlatTypes = array(
        235 => 'Blok',
        236 => 'Apartamentowiec',
        237 => 'Kamienica',
        238 => 'Dom wielorodzinny',
        239 => 'Inny'
    );

    public static function getFlatTypes() {
        return self::$aFlatTypes;
    }

}

