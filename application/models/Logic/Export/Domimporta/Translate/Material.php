<?php

class Model_Logic_Export_Domimporta_Translate_Material {

    private static $aMaterial = array(
        240 => 'Beton',
        241 => 'Cegła',
        242 => 'Drewno',
        243 => 'Pustak',
        244 => 'Rama H',
        245 => 'Wielka płyta',
        246 => 'Ytong',
        815 => 'Suporeks',
        247 => 'Inny'
    );

    public static function getMaterial() {
        return self::$aMaterial;
    }

}

