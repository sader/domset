<?php

class Model_Logic_Export_Domimporta_Translate_Access {

    private static $aAccess = array(
        465 => 'asfaltowa',
        466 => 'utwardzana',
        467 => 'polna',
        468 => 'brak'
    );

    public static function getAccess() {
        return self::$aAccess;
    }

}

