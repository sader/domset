<?php

class Model_Logic_Export_Domimporta_Translate_Fence {

    private static $aFence = array(
        349 => 'siatka',
        350 => 'murowane',
        351 => 'drewniane',
        352 => 'żywopłot',
        353 => 'metalowe',
        354 => 'inne',
        355 => 'brak',
    );

    public static function getFence() {
        return self::$aFence;
    }

}

