<?php

interface Model_Logic_Export_Interface {
    public function manageAdd(App_Controller_Admin_Abstract $oCtrl);
    public function manageEdit(App_Controller_Admin_Abstract $oCtrl, $aExport); 
    public function manageRemove(App_Controller_Admin_Abstract $oCtrl, $aExport);
    public function manageReload(App_Controller_Admin_Abstract $oCtrl , $aExport); 
    
}

