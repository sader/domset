<?php

class Model_Logic_Export_Olx extends Model_Logic_Export_Abstract implements Model_Logic_Export_Interface {

    const ID = 3;
    const IMAGE_EXPORT_LIMIT = 20; 
    
    private $iOrderId;
    private $iCategoryId;

    public function getOfferId() {
        return $this->iOrderId;
    }

    public function setOfferId($iOrderId) {
        $this->iOrderId = $iOrderId;
    }

    public function getCategoryId() {
        return $this->iCategoryId;
    }

    public function setCategoryId($iOfferId = null) {
        $this->iCategoryId = Model_Logic_Export_Gratka_Translate::getGratkaCategoryId($iOfferId);


        if ($this->iCategoryId === null) {
            throw new Exception('Brak poprawnego identyfikatora kategorii Gratka.pl');
        }
    }

    public function manageAdd(App_Controller_Admin_Abstract $oCtrl) {

        if (($iOfferId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed');
        }

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($iOfferId);
        $aOffer['STATUS'] = Model_DbTable_Export::TYPE_ADDED;
        
        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        } 

        $oForm = new Model_Logic_Export_Olx_Form(); 

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {
            
            $aValidatedData = $oForm->getValues(); 

            $this->addValidatedExport($iOfferId, $aValidatedData) 
                    ? $oCtrl->successMessage('Operacja eksportu zakończona pomyślnie') 
                    : $oCtrl->errorMessage('Wystąpił błąd podczas próby eksportu do olx.pl.');

            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array('controller' => 'export',
                                'action' => 'list',
                                'offer' => $aOffer['id_offer']), null, true));
        }
        
        $aTranslatedOfferData = Model_Logic_Export_Olx_Translate::translate($aOffer);
        
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    public function manageEdit(App_Controller_Admin_Abstract $oCtrl, $aExport) {


        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        }
        $aOffer['STATUS'] = Model_DbTable_Export::TYPE_EDITED;

        $oForm = new Model_Logic_Export_Olx_Form(); 

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $this->editValidatedExport($oForm->getValues(), $aExport) 
                    ? $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie') 
                    : $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji eksportu do olx.pl. Spróbój później.');

            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array('controller' => 'export',
                                'action' => 'list',
                                'offer' => $aExport['id_offer']), null, true));
        }

        $aTranslatedOfferData = Model_Logic_Export_Olx_Translate::translate($aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    public function manageRemove(App_Controller_Admin_Abstract $oCtrl, $aExport) {

        
        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        } else {
            $this->setCategoryId($aOffer['id_type']);
        }

        $this->removeValidatedExport($aExport) 
                ? $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie') 
                : $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji eksportu do olx.pl. Spróbój później.');

        $oCtrl->getHelper('Redirector')
                ->goToUrl($oCtrl->view->url(array('controller' => 'export',
                            'action' => 'list',
                            'offer' => $aExport['id_offer']), null, true));
    }

    public function manageReload(App_Controller_Admin_Abstract $oCtrl, $aExport) {

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        } else {
            $this->setCategoryId($aOffer['id_type']);
        }

        if (($oForm = Model_Logic_Export_Gratka_Form::getInstance()->getForm($this->getCategoryId())) === false) {
            throw new Exception('Problem z pobraniem listy pól z gratki i wygenerowaniem formularza');
        }

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $this->reloadValidatedExport($oForm->getValues(), $aExport) 
                    ? $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie') 
                    : $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji eksportu do gratka.pl. Spróbój później.');

            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array('controller' => 'export',
                                'action' => 'list',
                                'offer' => $aExport['id_offer']), null, true));
        }

        $aTranslatedOfferData = Model_Logic_Export_Gratka_Translate::translate($this->getCategoryId(), $aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    private function addValidatedExport($iOfferId, $aData) {

        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction(); 
            $iExportId = Model_DbTable_Export::getInstance()->addExport($iOfferId, self::ID);
            $aImages =  Model_Logic_Gallery::getInstance()->getImagesLinksForOffer($iOfferId, self::IMAGE_EXPORT_LIMIT); 
            $aBuildedData = Model_Logic_Export_Olx_Api::getInstance()->buildData($iExportId, $aData, $aImages);
            Model_DbTable_Export::getInstance()->editExport($iExportId, Model_DbTable_Export::TYPE_ADDED, $aBuildedData, $iExportId);
            Zend_Db_Table::getDefaultAdapter()->commit(); 
            return true;
        } catch (Exception $e) {
            Zend_Db_Table::getDefaultAdapter()->rollback(); 
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function editValidatedExport($aData, $aExport) {

           try {
            $aImages =  Model_Logic_Gallery::getInstance()->getImagesLinksForOffer($aExport['id_offer'], self::IMAGE_EXPORT_LIMIT); 
            $aBuildedData = Model_Logic_Export_Olx_Api::getInstance()->buildData($aExport['id'], $aData, $aImages);
            Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_EDITED, $aBuildedData);
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function removeValidatedExport($aExport) {

        try {
            $aXmlData = Model_Logic_Export_Olx_Api::getInstance()->markAsRemoved($aExport);
            Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_DELETED, $aXmlData);
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }
    
    
      private function reloadValidatedExport($aData, $aExport) {

        try {
            $aXmlData = Model_Logic_Export_Olx_Api::getInstance()->markAsRemoved($aExport);
            Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_DELETED, $aXmlData);
            
            $aImages =  Model_Logic_Gallery::getInstance()->getImagesLinksForOffer($this->getOfferId(), self::IMAGE_EXPORT_LIMIT); 
            $aData = Model_Logic_Export_Olx_Api::getInstance()->buildData($this->getOfferId(), $aData, $aImages);
            Model_DbTable_Export::getInstance()->addExport($this->getOfferId(), self::ID, null, $aData);
            
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }
}