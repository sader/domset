<?php

class Model_Logic_Export_Otodom_Form_Land extends Form_Abstract {

    public $oSubForm;

    public function __construct($aDictionaries = array()) {
        parent::__construct(null);

        $this->oSubForm = new Zend_Form_SubForm();
       $this->oSubForm->setDecorators(array('FormElements', 'Form'));
        
        $oElem = new Zend_Form_Element_Select('Type');
        $oElem->setLabel('Typ działki')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

       
        $oElem = new Zend_Form_Element_Text('Dimensions');
        $oElem->setLabel('Wymiary')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('AccessMask');
        $oElem->setLabel('Dojazd')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

         $oElem = new Zend_Form_Element_MultiCheckbox('MediaMask');
        $oElem->setLabel('Media')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);
        
        $oElem = new Zend_Form_Element_Select('Fence');
        $oElem->setLabel('Ogrodzenie')
                ->setDecorators($this->_aFormElementTableDecorator)
                ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie')); 
        $this->oSubForm->addElement($oElem); 
                       
        
        $oElem = new Zend_Form_Element_MultiCheckbox('VicinityMask');
        $oElem->setLabel('Okolica')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $this->addDictionary($aDictionaries);
    }

    public function addDictionary($aDictionaries) {
        foreach ($aDictionaries->Fields as $oField) {
            if (($oElem = $this->oSubForm->getElement($oField->Name)) !== null) {
                if ($oElem instanceof Zend_Form_Element_Select) {
                    $oElem->addMultiOption('', '');
                }

                foreach ($oField->Values as $oVal) {
                    $oElem->addMultiOption($oVal->ID, $oVal->Value);
                }
            }
        }
    }

}