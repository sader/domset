<?php

class Model_Logic_Export_Otodom_Form_House extends Form_Abstract {

    public $oSubForm;

    public function __construct($aDictionaries = array()) {
        parent::__construct(null);

        $this->oSubForm = new Zend_Form_SubForm();

     $this->oSubForm->setDecorators(array('FormElements', 'Form'));

        $oElem = new Zend_Form_Element_Select('BuildingType');
        $oElem->setLabel('Typ budowy')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('BuildingMaterial');
        $oElem->setLabel('Materiał budowy')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('TerrainArea');
        $oElem->setLabel('Powierzchnia działki m2')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('BuildYear');
        $oElem->setLabel('Rok budowy')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('RoofType');
        $oElem->setLabel('Dach')
              ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Type');
        $oElem->setLabel('Typ')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('FloorsNum');
        $oElem->setLabel('Liczba pięter')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('RoomsNum');
        $oElem->setLabel('Liczba pokoi')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);



        $oElem = new Zend_Form_Element_Select('GarretType');
        $oElem->setLabel('Poddasze')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('WindowsType');
        $oElem->setLabel('Typ okien')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('MediaMask');
        $oElem->setLabel('Media')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('HeatingMask');
        $oElem->setLabel('Ogrzewanie')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('FenceMask');
        $oElem->setLabel('Ogrodzenie')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Location');
        $oElem->setLabel('Położenie')
              ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Roofing');
        $oElem->setLabel('Pokrycie dachu')
              ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);



        $oElem = new Zend_Form_Element_Text('FreeForm');
        $oElem->setLabel('Dostępny od')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);



        $oElem = new Zend_Form_Element_Select('Furnished');
        $oElem->setLabel('Czy umeblowane')
                ->addMultioptions(array('' => '', 'y' => 'Tak', 'n' => 'Nie'))
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);



        $oElem = new Zend_Form_Element_Text('Rent');
        $oElem->setLabel('Wysokość czysznu')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Hidden('RentCurrency');
        $oElem->setDecorators(array('ViewHelper'))
                ->setValue(0);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('PriceIncludeRent');
        $oElem->setLabel('Cena zawiera czynsz')
                ->addMultioptions(array('' => '', 'y' => 'Tak', 'n' => 'Nie'))
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);



        $oElem = new Zend_Form_Element_MultiCheckbox('VicinityMask');
        $oElem->setLabel('Okolica')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);



        $oElem = new Zend_Form_Element_MultiCheckbox('AccessMask');
        $oElem->setLabel('Dojazd')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);



        $oElem = new Zend_Form_Element_MultiCheckbox('SecurityMask');
        $oElem->setLabel('Zabezpieczenia')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $this->addDictionary($aDictionaries);
    }

    public function addDictionary($aDictionaries) {
        foreach ($aDictionaries->Fields as $oField) {
            if (($oElem = $this->oSubForm->getElement($oField->Name)) !== null) {
                if ($oElem instanceof Zend_Form_Element_Select) {
                    $oElem->addMultiOption('', '');
                }

                foreach ($oField->Values as $oVal) {
                    $oElem->addMultiOption($oVal->ID, $oVal->Value);
                }
            }
        }
    }

}