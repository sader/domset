<?php

class Model_Logic_Export_Otodom_Form_Local extends Form_Abstract {

    public $oSubForm;

    public function __construct($aDictionaries = array()) {
        parent::__construct(null);

        $this->oSubForm = new Zend_Form_SubForm();
    
       $this->oSubForm->setDecorators(array('FormElements', 'Form'));
        
        
        $oElem = new Zend_Form_Element_MultiCheckbox('PropertyUseMask');
        $oElem->setLabel('Przeznaczenie')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('ExtrasMask');
        $oElem->setLabel('Informacje dodatkowe')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);
        
        
                $oElem = new Zend_Form_Element_Select('Floor');
        $oElem->setLabel('Piętro')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);
        
        $oElem = new Zend_Form_Element_Text('FreeForm');
        $oElem->setLabel('Dostępny od')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        
        $oElem = new Zend_Form_Element_Select('Furnished');
        $oElem->setLabel('Czy umeblowane')
                ->addMultioptions(array('' => '', 'y' => 'Tak', 'n' => 'Nie'))
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        
         $oElem = new Zend_Form_Element_Select('BuildingType');
        $oElem->setLabel('Umiejscowienie lokalu')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        
        $oElem = new Zend_Form_Element_Text('TerrainArea');
        $oElem->setLabel('Powierzchnia działki m2')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);
        
        
          
        $oElem = new Zend_Form_Element_Select('ConstructionStatus');
        $oElem->setLabel('Stan wykończenia')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        
        $oElem = new Zend_Form_Element_Text('BuildYear');
        $oElem->setLabel('Rok budowy')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);



        $oElem = new Zend_Form_Element_MultiCheckbox('MediaMask');
        $oElem->setLabel('Media')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('SecurityMask');
        $oElem->setLabel('Zabezpieczenia')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $this->addDictionary($aDictionaries);
    }

    public function addDictionary($aDictionaries) {
        foreach ($aDictionaries->Fields as $oField) {
            if (($oElem = $this->oSubForm->getElement($oField->Name)) !== null) {
                if ($oElem instanceof Zend_Form_Element_Select) {
                    $oElem->addMultiOption('', '');
                }

                foreach ($oField->Values as $oVal) {
                    $oElem->addMultiOption($oVal->ID, $oVal->Value);
                }
            }
        }
    }

}