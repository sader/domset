<?php

class Model_Logic_Export_Otodom_Form_Flat extends Form_Abstract {

    public $oSubForm;

    public function  __construct($aDictionaries = array()) {
        parent::__construct(null);

        $this->oSubForm = new Zend_Form_SubForm();
        
        $this->oSubForm->setDecorators(array('FormElements', 'Form'));
        
        $oElem = new Zend_Form_Element_Select('BuildingType');
        $oElem->setLabel('Typ budowy')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('BuildingMaterial');
        $oElem->setLabel('Materiał budowy')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('BuildingOwnership');
        $oElem->setLabel('Forma własności')->setDecorators($this->_aFormElementTableDecorator);

        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('FloorNo');
        $oElem->setLabel('Piętro')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('RoomsNum');
        $oElem->setLabel('Liczba pokoi')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('BuildYear');
        $oElem->setLabel('Rok budowy')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('ExtrasMask');
        $oElem->setLabel('Informacje dodatkowe')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Heating');
        $oElem->setLabel('Rodzaj ogrzewania')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('FreeForm');
        $oElem->setLabel('Dostępny od')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Furnished');
        $oElem->setLabel('Czy umeblowane')
                ->addMultioptions(array('' => '', 'y' => 'Tak', 'n' => 'Nie'))
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('Rent');
        $oElem->setLabel('Wysokość czysznu')
                 ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

 $oElem = new Zend_Form_Element_Hidden('RentCurrency');
        $oElem->setDecorators(array('ViewHelper'))
                    ->setValue(0); 
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('PriceIncludeRent');
        $oElem->setLabel('Cena zawiera czynsz')
                ->addMultioptions(array('' => '', 'y' => 'Tak', 'n' => 'Nie'))
                    ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('ConstructionStatus');
        $oElem->setLabel('Stan wykończenia')
                    ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('WindowsType');
        $oElem->setLabel('Typ okien')
                    ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('SecurityMask');
        $oElem->setLabel('Zabezpieczenia')
                    ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('MediaMask');
        $oElem->setLabel('Media')
                    ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('EquipmentMask');
        $oElem->setLabel('Wyposażenie')
                    ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('RentToStudents');
        $oElem->setLabel('Wynajmę również studentom')
                ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
                 ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem)->

        $oElem = new Zend_Form_Element_Text('Deposit');
        $oElem->setLabel('Wysokość kaucji')
                    ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Hidden('DepositCurrency');
        $oElem->setDecorators(array('ViewHelper'))
                    ->setValue(0); 
        $this->oSubForm->addElement($oElem);
        

        $this->addDictionary($aDictionaries);
    }

    public function addDictionary($aDictionaries) {
        foreach ($aDictionaries->Fields as $oField) {
            if (($oElem = $this->oSubForm->getElement($oField->Name)) !== null) {
                if ($oElem instanceof Zend_Form_Element_Select) {
                    $oElem->addMultiOption('', '');
                }

                foreach ($oField->Values as $oVal) {
                    $oElem->addMultiOption($oVal->ID, $oVal->Value);
                }
            }
        }
    }

}