<?php

class Model_Logic_Export_Otodom_Form_Warehouse extends Form_Abstract {

    public $oSubForm;

    public function __construct($aDictionaries = array()) {
        parent::__construct(null);

        $this->oSubForm = new Zend_Form_SubForm();
    $this->oSubForm->setDecorators(array('FormElements', 'Form'));

        $oElem = new Zend_Form_Element_Text('Height');
        $oElem->setLabel('Wysokość')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);



        $oElem = new Zend_Form_Element_MultiCheckbox('AccessMask');
        $oElem->setLabel('Dojazd')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('Parking');
        $oElem->setLabel('Parking (Usytuowanie)')
                ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('MediaMask');
        $oElem->setLabel('Media')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);



        $oElem = new Zend_Form_Element_Select('Structure');
        $oElem->setLabel('Konstrukcja')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Heating');
        $oElem->setLabel('Ogrzewanie')
                ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('UseMask');
        $oElem->setLabel('Przeznaczenie')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Heating');
        $oElem->setLabel('Ogrzewanie')
                ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('ParkingType');
        $oElem->setLabel('Typ parkingu')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Flooring');
        $oElem->setLabel('Flooring')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('ConstructionStatus');
        $oElem->setLabel('Stan wykończenia')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('OfficeSpace');
        $oElem->setLabel('Pomieszczenia biurowe')
                ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('SocialFacilities');
        $oElem->setLabel('Zaplecze socjalne')
                ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('Ramp');
        $oElem->setLabel('Rampa')
                ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('SecurityMask');
        $oElem->setLabel('Zabezpieczenia')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Fence');
        $oElem->setLabel('Ogrodzenie')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->oSubForm->addElement($oElem);




        $this->addDictionary($aDictionaries);
    }

    public function addDictionary($aDictionaries) {
        foreach ($aDictionaries->Fields as $oField) {
            if (($oElem = $this->oSubForm->getElement($oField->Name)) !== null) {
                if ($oElem instanceof Zend_Form_Element_Select) {
                    $oElem->addMultiOption('', '');
                }

                foreach ($oField->Values as $oVal) {
                    $oElem->addMultiOption($oVal->ID, $oVal->Value);
                }
            }
        }
    }

}