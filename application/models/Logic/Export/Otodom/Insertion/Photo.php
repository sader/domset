<?php

class Model_Logic_Export_Otodom_Insertion_Photo {
	/**
	 * @access public
	 * @var string
	 */
	public $Position;
	/**
	 * @access public
	 * @var string
	 */
	public $Url;
	/**
	 * @access public
	 * @var string
	 */
	public $UrlMini;
	/**
	 * @access public
	 * @var string
	 */
	public $Data;

}
