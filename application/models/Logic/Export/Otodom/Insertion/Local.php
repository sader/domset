<?php

class Model_Logic_Export_Otodom_Insertion_Local {
	/**
	 * @access public
	 * @var integer[]
	 */
	public $PropertyUseMask;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $ExtrasMask;
	/**
	 * @access public
	 * @var integer
	 */
	public $Floor;
	/**
	 * @access public
	 * @var string
	 */
	public $FreeFrom;
	/**
	 * @access public
	 * @var string
	 */
	public $Furnished;
	/**
	 * @access public
	 * @var integer
	 */
	public $BuildingType;
	/**
	 * @access public
	 * @var integer
	 */
	public $TerrainArea;
	/**
	 * @access public
	 * @var integer
	 */
	public $ConstructionStatus;
	/**
	 * @access public
	 * @var integer
	 */
	public $BuildYear;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $MediaMask;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $SecurityMask;

}