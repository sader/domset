<?php

class Model_Logic_Export_Otodom_Insertion_Warehouse {
	/**
	 * @access public
	 * @var integer
	 */
	public $Height;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $AccessMask;
	/**
	 * @access public
	 * @var boolean
	 */
	public $Parking;
	/**
	 * @access public
	 * @var integer
	 */
	public $Structure;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $MediaMask;
	/**
	 * @access public
	 * @var boolean
	 */
	public $Heating;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $UseMask;
	/**
	 * @access public
	 * @var integer
	 */
	public $ParkingType;
	/**
	 * @access public
	 * @var integer
	 */
	public $Flooring;
	/**
	 * @access public
	 * @var integer
	 */
	public $ConstructionStatus;
	/**
	 * @access public
	 * @var boolean
	 */
	public $OfficeSpace;
	/**
	 * @access public
	 * @var boolean
	 */
	public $SocialFacilities;
	/**
	 * @access public
	 * @var boolean
	 */
	public $Ramp;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $SecurityMask;
	/**
	 * @access public
	 * @var integer
	 */
	public $Fence;


}