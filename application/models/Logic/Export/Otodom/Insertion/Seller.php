<?php

class Model_Logic_Export_Otodom_Insertion_Seller {
	/**
	 * @access public
	 * @var integer
	 */
	public $ID;
	/**
	 * @access public
	 * @var string
	 */
	public $Name;
	/**
	 * @access public
	 * @var string
	 */
	public $Street;
	/**
	 * @access public
	 * @var string
	 */
	public $Postcode;
	/**
	 * @access public
	 * @var string
	 */
	public $City;
	/**
	 * @access public
	 * @var integer
	 */
	public $Province;
	/**
	 * @access public
	 * @var string
	 */
	public $Country;
	/**
	 * @access public
	 * @var ContactInfoObject[]
	 */
	public $Contacts;
	/**
	 * @access public
	 * @var integer
	 */
	public $InsertionsNum;
}
