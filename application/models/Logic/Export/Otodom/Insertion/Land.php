<?php

class Model_Logic_Export_Otodom_Insertion_Land {
	/**
	 * @access public
	 * @var integer
	 */
	public $Type;
	/**
	 * @access public
	 * @var string
	 */
	public $Dimensions;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $MediaMask;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $AccessMask;
	/**
	 * @access public
	 * @var boolean
	 */
	public $Fence;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $VicinityMask;
	/**
	 * @access public
	 * @var integer
	 */
	public $Location;

}