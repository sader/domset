<?php

class Model_Logic_Export_Otodom_Insertion_Base {
public $ID;
public $Status;
public $Country;
public $Province;
public $District;
public $City;
public $Quarter;
public $Street;
public $LocationDescription;
public $ExpirationDate;
public $ModificationDate;
public $RemoteId;
public $Price;
public $PriceType;
public $PriceCurrency;
public $Area;
public $MarketType;
public $ObjectName;
public $OfferType;
public $Description;
public $Investment;
public $SellerInfo;
public $AdditionalContact;
public $AdditionalAddress;
public $FlatDetails;
public $HouseDetails;
public $TerrainDetails;
public $RoomDetails;
public $CommercialPropertyDetails;
public $HallDetails;
public $GarageDetails;
public $Photos;
public $Url;
public $Title;

}