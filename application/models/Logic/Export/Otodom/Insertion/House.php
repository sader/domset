<?php

class Model_Logic_Export_Otodom_Insertion_House {
	/**
	 * @access public
	 * @var integer
	 */
	public $BuildingType;
	/**
	 * @access public
	 * @var integer
	 */
	public $BuildingMaterial;
	/**
	 * @access public
	 * @var integer
	 */
	public $TerrainArea;
	/**
	 * @access public
	 * @var integer
	 */
	public $ConstructionStatus;
	/**
	 * @access public
	 * @var integer
	 */
	public $BuildYear;
	/**
	 * @access public
	 * @var integer
	 */
	public $RoofType;
	/**
	 * @access public
	 * @var integer
	 */
	public $Type;
	/**
	 * @access public
	 * @var integer
	 */
	public $FloorsNum;
	/**
	 * @access public
	 * @var integer
	 */
	public $RoomsNum;
	/**
	 * @access public
	 * @var integer
	 */
	public $GarretType;
	/**
	 * @access public
	 * @var integer
	 */
	public $WindowsType;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $MediaMask;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $HeatingMask;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $FenceMask;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $ExtrasMask;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $VicinityMask;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $AccessMask;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $SecurityMask;
	/**
	 * @access public
	 * @var integer
	 */
	public $Location;
	/**
	 * @access public
	 * @var string
	 */
	public $Roofing;
	/**
	 * @access public
	 * @var string
	 */
	public $FreeFrom;
	/**
	 * @access public
	 * @var string
	 */
	public $Furnished;
	/**
	 * @access public
	 * @var double
	 */
	public $Rent;
	/**
	 * @access public
	 * @var integer
	 */
	public $RentCurrency;
	/**
	 * @access public
	 * @var string
	 */
	public $PriceIncludeRent;

}