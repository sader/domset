<?php

class Model_Logic_Export_Otodom_Insertion_Flat {
	/**
	 * @access public
	 * @var integer
	 */
	public $BuildingType;
	/**
	 * @access public
	 * @var integer
	 */
	public $BuildingMaterial;
	/**
	 * @access public
	 * @var integer
	 */
	public $BulidingFloorsNum;
	/**
	 * @access public
	 * @var integer
	 */
	public $BuildingOwnership;
	/**
	 * @access public
	 * @var integer
	 */
	public $ConstructionStatus;
	/**
	 * @access public
	 * @var integer
	 */
	public $FloorNo;
	/**
	 * @access public
	 * @var integer
	 */
	public $RoomsNum;
	/**
	 * @access public
	 * @var integer
	 */
	public $BuildYear;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $ExtrasMask;
	/**
	 * @access public
	 * @var string
	 */
	public $Heating;
	/**
	 * @access public
	 * @var string
	 */
	public $FreeFrom;
	/**
	 * @access public
	 * @var string
	 */
	public $Furnished;
	/**
	 * @access public
	 * @var double
	 */
	public $Rent;
	/**
	 * @access public
	 * @var integer
	 */
	public $RentCurrency;
	/**
	 * @access public
	 * @var double
	 */
	public $Deposit;
	/**
	 * @access public
	 * @var integer
	 */
	public $DepositCurrency;
	/**
	 * @access public
	 * @var string
	 */
	public $PriceIncludeRent;
	/**
	 * @access public
	 * @var string
	 */
	public $WindowsType;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $SecurityMask;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $MediaMask;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $EquipmentMask;
	/**
	 * @access public
	 * @var boolean
	 */
	public $RentToStudents;

}