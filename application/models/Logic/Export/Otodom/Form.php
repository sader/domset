<?php

class Model_Logic_Export_Otodom_Form extends Form_Abstract
{

    private $dictionary;

    public function __construct($iOfferType, $aDictionaries = array())
    {
        parent::__construct(null);


        $this->dictionary = new Model_Logic_Export_Otodom_Dictonary();

        $this->_aFields['Title'] = new Zend_Form_Element_Text('Title');
        $this->_aFields['Title']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Tytuł');

        $this->_aFields['Status'] = new Zend_Form_Element_Select('Status');
        $this->_aFields['Status']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Status');


        $this->_aFields['Country'] = new Zend_Form_Element_Select('Country');
        $this->_aFields['Country']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Kraj');


        $this->_aFields['Province'] = new Zend_Form_Element_Select('Province');
        $this->_aFields['Province']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Województwo');

        $this->_aFields['District'] = new Zend_Form_Element_Select('District');
        $this->_aFields['District']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Powiat');

        $this->_aFields['City'] = new Zend_Form_Element_Text('City');
        $this->_aFields['City']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Miasto');


        $this->_aFields['Quarter'] = new Zend_Form_Element_Text('Quarter');
        $this->_aFields['Quarter']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Dzielnica');

        $this->_aFields['Street'] = new Zend_Form_Element_Text('Street');
        $this->_aFields['Street']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Ulica');


        $this->_aFields['Price'] = new Zend_Form_Element_Text('Price');
        $this->_aFields['Price']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Cena');

        $this->_aFields['PriceType'] = new Zend_Form_Element_Select('PriceType');
        $this->_aFields['PriceType']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Typ ceny');


        $this->_aFields['PriceCurrency'] = new Zend_Form_Element_Select('PriceCurrency');
        $this->_aFields['PriceCurrency']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Waluta');

        $this->_aFields['LocationDescription'] = new Zend_Form_Element_Textarea('LocationDescription');
        $this->_aFields['LocationDescription']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Dodatkowy opis lokalizacji');


        $this->_aFields['Area'] = new Zend_Form_Element_Text('Area');
        $this->_aFields['Area']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Powierzchnia');

        $this->_aFields['MarketType'] = new Zend_Form_Element_Select('MarketType');
        $this->_aFields['MarketType']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Rynek');

        $this->_aFields['OfferType'] = new Zend_Form_Element_Select('OfferType');
        $this->_aFields['OfferType']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Typ oferty');

        $this->_aFields['Description'] = new Zend_Form_Element_Textarea('Description');
        $this->_aFields['Description']->setDecorators($this->_aFormElementTableDecorator)
            ->setLabel('Opis');

        $this->addDictionary($this->_aFields, 'Common');

        $oMainSubForm = new Zend_Form_SubForm();

        $oMainSubForm->addElements($this->_aFields)
            ->setDecorators($this->_aSubFormTableDecorator);


        $this->addSubForm($oMainSubForm, 'base')->setMethod(Zend_Form::METHOD_POST)->setDecorators($this->_aFormTableDecorator);
        //   $this->addDictionary(Model_Logic_Export_Otodom_Api::getInstance()->getDictionary('Common', $aDictionaries));
        $this->getTypeSubform($iOfferType, $aDictionaries);


        $oElem = new Zend_Form_Element_Submit('submit');
        $oElem->setDecorators($this->_aFormSubmitTableDecorator)
            ->setLabel('Wyślij');

        $this->addElement($oElem);

        //wybierz odpowiedni subform w zaleznosci od typu // wykorzystaj translator
    }

    public function addDictionary($oFields, $section = 'Common')
    {
        foreach ($oFields as $fieldName => $field) {
            if ($field instanceof Zend_Form_Element_Select) {
                $field->addMultiOption('', '');
                $field->addMultiOptions($this->dictionary->get($fieldName, $section));
            }
        }

    }

    public function getTypeSubform($iOfferTypeId, $aDictionaries)
    {

        switch ($iOfferTypeId) {
            case Model_Logic_Offer_Flat_Rent::ID:
            case Model_Logic_Offer_Flat_Sell::ID:
                //       $aDictionary = Model_Logic_Export_Otodom_Api::getInstance()->getDictionary('FlatDetails', $aDictionaries);
                $this->addElements($this->flatSubform()->getElements());
                break;
            case Model_Logic_Offer_House_Rent::ID:
            case Model_Logic_Offer_House_Sell::ID:
                //       $aDictionary = Model_Logic_Export_Otodom_Api::getInstance()->getDictionary('HouseDetails', $aDictionaries);
                $this->addElements($this->houseSubform()->getElements());
                break;
            case Model_Logic_Offer_Local_Rent::ID:
            case Model_Logic_Offer_Local_Sell::ID:
                //       $aDictionary = Model_Logic_Export_Otodom_Api::getInstance()->getDictionary('CommercialPropertyDetails', $aDictionaries);
                $this->addElements($this->localSubform()->getElements());
                break;
            case Model_Logic_Offer_Land_Rent::ID:
            case Model_Logic_Offer_Land_Sell::ID:
                //        $aDictionary = Model_Logic_Export_Otodom_Api::getInstance()->getDictionary('TerrainDetails', $aDictionaries);
                $this->addElements($this->landSubform()->getElements());
                break;
            case Model_Logic_Offer_Warehouse_Rent::ID:
            case Model_Logic_Offer_Warehouse_Sell::ID:
                //       $aDictionary = Model_Logic_Export_Otodom_Api::getInstance()->getDictionary('HallDetails', $aDictionaries);
                $this->addElements($this->warehouseSubform()->getElements());
                break;
            default:
                throw new Exception('Brak formularza dla tego typu zamówienia dla Otodom', 500);
                break;
        }
    }

    private function flatSubform()
    {

        $oSubForm = new Zend_Form_SubForm();
        $oSubForm->addElementPrefixPath('App_Validate', 'App/Validate', 'validate');
        $oSubForm->addElementPrefixPath('App_Filter', 'App/Filter', 'filter');
        $oSubForm->addElementPrefixPath('App_Form_Decorator', 'App/Form/Decorator', 'decorator');
        $oSubForm->setDecorators(array('FormElements', 'Form'));

        $oElem = new Zend_Form_Element_Select('BuildingType');
        $oElem->setLabel('Typ budowy')
            ->setDecorators($this->_aFormElementTableDecorator);

        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('BuildingMaterial');
        $oElem->setLabel('Materiał budowy')
            ->setDecorators($this->_aFormElementTableDecorator);

        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('BuildingOwnership');
        $oElem->setLabel('Forma własności')->setDecorators($this->_aFormElementTableDecorator);

        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('FloorNo');
        $oElem->setLabel('Piętro')
            ->setDecorators($this->_aFormElementTableDecorator);

        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('RoomsNum');
        $oElem->setLabel('Liczba pokoi')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('BuildYear');
        $oElem->setLabel('Rok budowy')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('ExtrasMask');
        $oElem->setLabel('Informacje dodatkowe')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Heating');
        $oElem->setLabel('Rodzaj ogrzewania')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('FreeForm');
        $oElem->setLabel('Dostępny od')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Furnished');
        $oElem->setLabel('Czy umeblowane')
            ->addMultioptions(array('' => '', 'y' => 'Tak', 'n' => 'Nie'))
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('Rent');
        $oElem->setLabel('Wysokość czysznu')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Hidden('RentCurrency');
        $oElem->setDecorators(array('ViewHelper'))
            ->setValue(0);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('PriceIncludeRent');
        $oElem->setLabel('Cena zawiera czynsz')
            ->addMultioptions(array('' => '', 'y' => 'Tak', 'n' => 'Nie'))
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('ConstructionStatus');
        $oElem->setLabel('Stan wykończenia')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('WindowsType');
        $oElem->setLabel('Typ okien')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('SecurityMask');
        $oElem->setLabel('Zabezpieczenia')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('MediaMask');
        $oElem->setLabel('Media')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('EquipmentMask');
        $oElem->setLabel('Wyposażenie')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('RentToStudents');
        $oElem->setLabel('Wynajmę również studentom')
            ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);
        $oElem = new Zend_Form_Element_Text('Deposit');
        $oElem->setLabel('Wysokość kaucji')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Hidden('DepositCurrency');
        $oElem->setDecorators(array('ViewHelper'))
            ->setValue(0);
        $oSubForm->addElement($oElem);

        $this->addDictionary($oSubForm->getElements(), 'FlatDetails');

        foreach ($oSubForm->getElements() as $oElem) {
            $oElem->setBelongsTo('details');
        }

        return $oSubForm;
    }

    private function houseSubform()
    {

        $oSubForm = new Zend_Form_SubForm();
        $oSubForm->addElementPrefixPath('App_Validate', 'App/Validate', 'validate');
        $oSubForm->addElementPrefixPath('App_Filter', 'App/Filter', 'filter');
        $oSubForm->addElementPrefixPath('App_Form_Decorator', 'App/Form/Decorator', 'decorator');
        $oSubForm->setDecorators(array('FormElements', 'Form'));


        $oElem = new Zend_Form_Element_Select('BuildingType');
        $oElem->setLabel('Typ budowy')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('BuildingMaterial');
        $oElem->setLabel('Materiał budowy')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('TerrainArea');
        $oElem->setLabel('Powierzchnia działki m2')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('BuildYear');
        $oElem->setLabel('Rok budowy')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('RoofType');
        $oElem->setLabel('Dach')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Type');
        $oElem->setLabel('Typ')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('FloorsNum');
        $oElem->setLabel('Liczba pięter')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('RoomsNum');
        $oElem->setLabel('Liczba pokoi')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('GarretType');
        $oElem->setLabel('Poddasze')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('WindowsType');
        $oElem->setLabel('Typ okien')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('MediaMask');
        $oElem->setLabel('Media')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('HeatingMask');
        $oElem->setLabel('Ogrzewanie')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('FenceMask');
        $oElem->setLabel('Ogrodzenie')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Location');
        $oElem->setLabel('Położenie')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Roofing');
        $oElem->setLabel('Pokrycie dachu')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('FreeForm');
        $oElem->setLabel('Dostępny od')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Furnished');
        $oElem->setLabel('Czy umeblowane')
            ->addMultioptions(array('' => '', 'y' => 'Tak', 'n' => 'Nie'))
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('Rent');
        $oElem->setLabel('Wysokość czysznu')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Hidden('RentCurrency');
        $oElem->setDecorators(array('ViewHelper'))
            ->setValue(0);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('PriceIncludeRent');
        $oElem->setLabel('Cena zawiera czynsz')
            ->addMultioptions(array('' => '', 'y' => 'Tak', 'n' => 'Nie'))
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('VicinityMask');
        $oElem->setLabel('Okolica')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('AccessMask');
        $oElem->setLabel('Dojazd')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('SecurityMask');
        $oElem->setLabel('Zabezpieczenia')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $this->addDictionary($oSubForm->getElements(), 'HouseDetails');

        foreach ($oSubForm->getElements() as $oElem) {
            $oElem->setBelongsTo('details');
        }

        return $oSubForm;

    }

    private function landSubform()
    {

        $oSubForm = new Zend_Form_SubForm();
        $oSubForm->addElementPrefixPath('App_Validate', 'App/Validate', 'validate');
        $oSubForm->addElementPrefixPath('App_Filter', 'App/Filter', 'filter');
        $oSubForm->addElementPrefixPath('App_Form_Decorator', 'App/Form/Decorator', 'decorator');
        $oSubForm->setDecorators(array('FormElements', 'Form'));

        $oElem = new Zend_Form_Element_Select('Type');
        $oElem->setLabel('Typ działki')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('Dimensions');
        $oElem->setLabel('Wymiary')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('AccessMask');
        $oElem->setLabel('Dojazd')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('MediaMask');
        $oElem->setLabel('Media')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('Fence');
        $oElem->setLabel('Ogrodzenie')
            ->setDecorators($this->_aFormElementTableDecorator)
            ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'));
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('VicinityMask');
        $oElem->setLabel('Okolica')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $this->addDictionary($oSubForm->getElements(), 'TerrainDetails');

        foreach ($oSubForm->getElements() as $oElem) {
            $oElem->setBelongsTo('details');
        }


        return $oSubForm;


    }

    private function localSubform()
    {

        $oSubForm = new Zend_Form_SubForm();
        $oSubForm->addElementPrefixPath('App_Validate', 'App/Validate', 'validate');
        $oSubForm->addElementPrefixPath('App_Filter', 'App/Filter', 'filter');
        $oSubForm->addElementPrefixPath('App_Form_Decorator', 'App/Form/Decorator', 'decorator');
        $oSubForm->setDecorators(array('FormElements', 'Form'));

        $oElem = new Zend_Form_Element_MultiCheckbox('PropertyUseMask');
        $oElem->setLabel('Przeznaczenie')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('ExtrasMask');
        $oElem->setLabel('Informacje dodatkowe')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Floor');
        $oElem->setLabel('Piętro')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Text('FreeForm');
        $oElem->setLabel('Dostępny od')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Furnished');
        $oElem->setLabel('Czy umeblowane')
            ->addMultioptions(array('' => '', 'y' => 'Tak', 'n' => 'Nie'))
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('BuildingType');
        $oElem->setLabel('Umiejscowienie lokalu')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('TerrainArea');
        $oElem->setLabel('Powierzchnia działki m2')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('ConstructionStatus');
        $oElem->setLabel('Stan wykończenia')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('BuildYear');
        $oElem->setLabel('Rok budowy')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('MediaMask');
        $oElem->setLabel('Media')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('SecurityMask');
        $oElem->setLabel('Zabezpieczenia')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $this->addDictionary($oSubForm->getElements(), 'CommercialPropertyDetails');

        foreach ($oSubForm->getElements() as $oElem) {
            $oElem->setBelongsTo('details');
        }

        return $oSubForm;
    }

    private function warehouseSubform()
    {
        $oSubForm = new Zend_Form_SubForm();
        $oSubForm->addElementPrefixPath('App_Validate', 'App/Validate', 'validate');
        $oSubForm->addElementPrefixPath('App_Filter', 'App/Filter', 'filter');
        $oSubForm->addElementPrefixPath('App_Form_Decorator', 'App/Form/Decorator', 'decorator');
        $oSubForm->setDecorators(array('FormElements', 'Form'));

        $oElem = new Zend_Form_Element_Text('Height');
        $oElem->setLabel('Wysokość')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('AccessMask');
        $oElem->setLabel('Dojazd')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('Parking');
        $oElem->setLabel('Parking (Usytuowanie)')
            ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('MediaMask');
        $oElem->setLabel('Media')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Structure');
        $oElem->setLabel('Konstrukcja')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Heating');
        $oElem->setLabel('Ogrzewanie')
            ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('UseMask');
        $oElem->setLabel('Przeznaczenie')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Heating');
        $oElem->setLabel('Ogrzewanie')
            ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('ParkingType');
        $oElem->setLabel('Typ parkingu')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Flooring');
        $oElem->setLabel('Flooring')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('ConstructionStatus');
        $oElem->setLabel('Stan wykończenia')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('OfficeSpace');
        $oElem->setLabel('Pomieszczenia biurowe')
            ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('SocialFacilities');
        $oElem->setLabel('Zaplecze socjalne')
            ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('Ramp');
        $oElem->setLabel('Rampa')
            ->addMultioptions(array('' => '', 1 => 'Tak', 0 => 'Nie'))
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_MultiCheckbox('SecurityMask');
        $oElem->setLabel('Zabezpieczenia')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('Fence');
        $oElem->setLabel('Ogrodzenie')
            ->setDecorators($this->_aFormElementTableDecorator);
        $oSubForm->addElement($oElem);


        foreach ($oSubForm->getElements() as $oElem) {
            $oElem->setBelongsTo('details');
        }

        $this->addDictionary($oSubForm->getElements(), 'HallDetails');

        return $oSubForm;

    }


}