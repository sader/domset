<?php

class Model_Logic_Export_Otodom_Translate {
    const CONTACT_NAME = 'DOMSET Nieruchomości';
    const CONTACT_EMAIL = 'biuro@domset.pl';
    const STATUS_ACTIVE = 1;
    const COUNTRY_POLAND = 1;
    const PRICE_BRUTTO = 0;
    const PRICE_CURRENCY_PLN = 1;
    const TYPE_FLAT = 0;
    const TYPE_HOUSE = 1;
    const TYPE_LAND = 2;
    const TYPE_LOCAL = 4;
    const TYPE_WAREHOUSE = 5;
    const OFFER_TYPE_SELL = 0;
    const OFFER_TYPE_RENT = 1;
    const TITLE_LENGHT_LIMIT = 50;

    public static function translate($iTypeId, $aData) {

        switch ($iTypeId) {
            case Model_Logic_Offer_Flat_Rent::ID:
                return self::translateFlat($aData, self::OFFER_TYPE_RENT);
                break;
            case Model_Logic_Offer_Flat_Sell::ID:
                return self::translateFlat($aData, self::OFFER_TYPE_SELL);
                break;
            case Model_Logic_Offer_House_Rent::ID:
                return self::translateHouse($aData, self::OFFER_TYPE_RENT);
                break;
            case Model_Logic_Offer_House_Sell::ID:
                return self::translateHouse($aData, self::OFFER_TYPE_SELL);
                break;
            case Model_Logic_Offer_Local_Rent::ID:
                return self::translateLocal($aData, self::OFFER_TYPE_RENT);
                break;
            case Model_Logic_Offer_Local_Sell::ID:
                return self::translateLocal($aData, self::OFFER_TYPE_SELL);
                break;
            case Model_Logic_Offer_Land_Rent::ID:
                return self::translateLand($aData, self::OFFER_TYPE_RENT);
                break;
            case Model_Logic_Offer_Land_Sell::ID:
                return self::translateLand($aData, self::OFFER_TYPE_SELL);
                break;
            case Model_Logic_Offer_Warehouse_Rent::ID:
                return self::translateWarehouse($aData, self::OFFER_TYPE_RENT);
                break;
            case Model_Logic_Offer_Warehouse_Sell::ID:
                return self::translateWarehouse($aData, self::OFFER_TYPE_SELL);
                break;
            default:
                throw new Exception('Brak formularza dla tego typu zamówienia dla Otodom', 500);
                break;
        }
    }

    private static function translateFlat($aData, $iOfferType) {

        $aReturn = self::translateUniversal($aData, $iOfferType);

        $aReturn['ObjectName'] = self::TYPE_FLAT;
        $aReturn['BuildingType'] = self::translateBuildingType($aData['building_type']);
        $aReturn['BulidingFloorsNum'] = $aData['floors_in_building'];
        $aReturn['FloorNo'] = self::translateFloor($aData['floor']);
        $aReturn['RoomsNum'] = $aData['rooms'];
        $aReturn['BuildYear'] = $aData['year'];
        $aReturn['Heating'] = self::translateFlatHeat($aData['heat']);
        $aReturn['ConstructionStatus'] = self::translateQuality($aData['quality']);
        $aReturn['WindowsTypes'] = self::translateWindows($aData['windows']);

        if ($iOfferType == self::OFFER_TYPE_SELL) {
            $aReturn['BuildingMaterial'] = self::translateFlatMaterial($aData['building_material']);
            $aReturn['BuildingOwnership'] = self::translateOwnership($aData['ownership']);
        } else {
            $aReturn['Deposit'] = $aData['caution'];
            $aReturn['Rent'] = $aData['additional_payment'];
        }

        return $aReturn;
    }

    private static function translateHouse($aData, $iOfferType) {

        $aReturn = self::translateUniversal($aData, $iOfferType);

        $aReturn['ObjectName'] = self::TYPE_HOUSE;
        $aReturn['BuildingType'] = self::translateHouseType($aData['building_type']);
        $aReturn['TerrainArea'] = $aData['terrain_surface'];
        $aReturn['FloorsNum'] = self::translateFloors($aData['floors']);
        $aReturn['RoomsNum'] = $aData['rooms'];
        $aReturn['BuildYear'] = $aData['year'];
        $aReturn['GarretType'] = self::translateAttic($aData['attic']);

        $aReturn['FenceMask'] = self::translateFence($aData['fence']);
        $aReturn['RoofType'] = self::translateRoof($aData['roof']);
        $aReturn['Roofing'] = self::translateRoofType($aData['roof_type']);
        $aReturn['AccessMask'] = self::translateAccess($aData['access']);


        $aReturn['Heating'] = self::translateHouseHeat($aData['heat']);
        $aReturn['ConstructionStatus'] = self::translateQuality($aData['quality']);
        $aReturn['WindowsType'] = self::translateHouseWindows($aData['windows']);

        if ($iOfferType == self::OFFER_TYPE_SELL) {
            $aReturn['BuildingMaterial'] = self::translateHouseMaterial($aData['building_material']);
        }

        return $aReturn;
    }

    private static function translateLand($aData, $iOfferType) {
        $aReturn = self::translateUniversal($aData, $iOfferType);

        $aReturn['ObjectName'] = self::TYPE_LAND;
        $aReturn['Type'] = self::translateLandType($aData['land_type']);
        $aReturn['Dimensions'] = self::translateDimensions($aData['width'], $aData['height']);
        $aReturn['Fence'] = self::translateLandFence($aData['fence']);
        $aReturn['AccessMask'] = self::translateAccess($aData['access']);
        return $aReturn;
    }

    private static function translateLocal($aData, $iOfferType) {

        $aReturn = self::translateUniversal($aData, $iOfferType);

        $aReturn['ObjectName'] = self::TYPE_LOCAL;
        $aReturn['PropertyUseMask'] = self::translatePurpose($aData['purpose']);
        $aReturn['Floor'] = self::translateFloor($aData['floor']);
        $aReturn['BuildingType'] = self::translateLocalType($aData['building_type']);
        $aReturn['ConstructionStatus'] = self::translateQuality($aData['quality']);
        $aReturn['BuildYear'] = $aData['year'];

        return $aReturn;
    }

    private static function translateWarehouse($aData, $iOfferType) {
        $aReturn = self::translateUniversal($aData, $iOfferType);

        $aReturn['ObjectName'] = self::TYPE_WAREHOUSE;
        $aReturn['Height'] =  preg_replace('/\./' , ',', (string)($aData['height'] / 100));
        $aReturn['Heating'] = self::translateWarehouseHeating($aData['heat']);
        $aReturn['UseMask'] = self::translateWarehousePurpose($aData['purpose']);
        $aReturn['ConstructionStatus'] = self::translateQuality($aData['quality']);

        return $aReturn;
    }

    private static function translateUniversal($aData, $iOfferType) {

        $aReturn = array();

        $aReturn['Title'] = self::translateTitle($aData['title']);
        $aReturn['Status'] = self::STATUS_ACTIVE;
        $aReturn['Country'] = self::COUNTRY_POLAND;
        $aReturn['Province'] = self::translateProvince($aData['province']);
        $aReturn['District'] = Model_Logic_Export_Otodom_Translate_District::getDistrictIdByName($aData['district']);
        $aReturn['City'] = $aData['city'];
        $aReturn['Quarter'] = $aData['section'];
        $aReturn['Street'] = $aData['street'];
        $aReturn['Price'] = $aData['price'];
        $aReturn['PriceType'] = self::PRICE_BRUTTO;
        $aReturn['PriceCurrency'] = self::PRICE_CURRENCY_PLN;
        $aReturn['Area'] = $aData['surface'];
        $aReturn['MarketType'] = 1;
        $aReturn['OfferType'] = $iOfferType;
        $aReturn['Description'] = self::translateDescription($aData['description'], $aData);

        return $aReturn;
    }

    private static function translateTitle($sTitle) {
        return (mb_strlen($sTitle) < self::TITLE_LENGHT_LIMIT) ? $sTitle : mb_substr($sTitle, 0, self::TITLE_LENGHT_LIMIT);
    }

    private static function translateDescription($sDesc = '', $aData = array()) {
        
         $sAddText =  PHP_EOL .PHP_EOL . 'Numer licencji pośrednika odpowiedzialnego zawodowo za wykonanie umowy pośrednictwa: 15295'. PHP_EOL . PHP_EOL .
                'LINK DO STRONY' . PHP_EOL .
                'http://www.domset.pl/'. $aData['id'] . PHP_EOL . PHP_EOL .
                'Kontakt do Agenta' . PHP_EOL .
                $aData['name'].' '.$aData['surname'] . PHP_EOL .
                'tel. kom.: '.$aData['phone'] . PHP_EOL .
                'e-mail: '.$aData['email'] . PHP_EOL . PHP_EOL .
                'DOMSET Nieruchomości' . PHP_EOL .
                'Ełk, ul Wojska Polskiego 43 lok. 3' . PHP_EOL .
                'tel.: 87 610 88 88' . PHP_EOL;


        return trim($sDesc) . $sAddText;
    }

    private static function translateProvince($iProvinceId) {


        $aKeyTranslate = array(
            1 => 14,
            2 => 1,
            3 => 2,
            4 => 3,
            5 => 4,
            6 => 5,
            7 => 6,
            8 => 7,
            9 => 8,
            10 => 9,
            11 => 10,
            12 => 11,
            13 => 12,
            14 => 13,
            15 => 15,
            16 => 16);

        return isset($aKeyTranslate[$iProvinceId]) ? $aKeyTranslate[$iProvinceId] : '';
    }

    private static function translateBuildingType($iId) {

        $aKeyTranslate = array(
            1 => 0,
            2 => 1,
            4 => 5);

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateFlatMaterial($iId) {

        $aKeyTranslate = array(
            1 => 4,
            2 => 0,
            3 => 2,
            4 => 5,
            5 => 1,
            6 => 2,
            7 => 6);

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateHouseMaterial($iId) {

        $aKeyTranslate = array(
            1 => 0,
            2 => 2,
            3 => 4,
            3 => 8,
            4 => 1,
            5 => 7,
            6 => 6,
            7 => 6);

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateFloor($sFloor) {

        if (($sFloor != '')) {
            if ($sFloor == 'suterena' || $sFloor == 'parter')
                return $sFloor;
            if (is_numeric($sFloor))
                return (int) $sFloor + 1;
        }

        return '';
    }

    private static function translateFlatHeat($iId) {

        $aKeyTranslate = array(
            1 => 0,
            2 => 3,
            3 => 1,
            4 => 2,
            5 => 5,
            6 => 4,
            7 => 4);

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateHouseHeat($iId) {

        $aKeyTranslate = array(
            1 => 0,
            2 => 3,
            3 => 1,
            4 => 2,
            5 => 5,
            6 => 4,
            7 => 4);

        return isset($aKeyTranslate[$iId]) ? array($aKeyTranslate[$iId]) : array();
    }

    private static function translateQuality($iId) {

        $aKeyTranslate = array(
            1 => 0,
            2 => 0,
            3 => 0,
            5 => 2,
            6 => 2,
            7 => 1);

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateWindows($iId) {

        $aKeyTranslate = array(
            1 => 0,
            2 => 1,
            3 => 0,
            4 => 1,
            5 => 0,
            6 => 1,
            7 => 2);

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateHouseWindows($iId) {

        $aKeyTranslate = array(
            1 => 1,
            2 => 2,
            3 => 1,
            4 => 2,
            5 => 1,
            6 => 2,
            7 => 3);

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateOwnership($iId) {

        $aKeyTranslate = array(
            1 => 2,
            2 => 0,
            3 => 1,
            4 => 3
        );

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateAttic($iId) {

        $aKeyTranslate = array(
            1 => 1,
            2 => 2,
            3 => 0
        );

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateFloors($iId) {
        
            
        
        if ($iId <= 3)
            return $iId - 1;
        elseif ($iId > 3)
            return 3;
        else
            return '';
    }

    private static function translateFence($iId) {
        $aKeyTranslate = array(
            2 => 1,
            3 => 0,
            4 => 3,
            5 => 4,
            6 => 5,
            7 => 6
        );

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateRoof($iId) {
        $aKeyTranslate = array(
            1 => 1,
            2 => 2,
            3 => 2
        );

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateRoofType($iId) {

        $aKeyTranslate = array(
            1 => 5,
            2 => 1,
            3 => 0,
            4 => 0,
            5 => 7
        );

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateAccess($iId) {

        $aKeyTranslate = array(
            1 => 0,
            3 => 2,
            4 => 1
        );

        return isset($aKeyTranslate[$iId]) ? array($aKeyTranslate[$iId]) : array();
    }

    private static function translateHouseType($iId) {
        $aKeyTranslate = array(
            1 => 0,
            2 => 2,
            3 => 2,
            4 => 1,
            5 => 3,
            6 => 4,
            7 => 0,
            8 => 5
        );

        return isset($aKeyTranslate[$iId]) ? array($aKeyTranslate[$iId]) : array();
    }

    private static function translateLandType($iId) {
        $aKeyTranslate = array(
            1 => 0,
            2 => 1,
            3 => 3,
            4 => 3,
            5 => 3,
            6 => 2,
            7 => 4,
            8 => 6,
            9 => 5,
            10 => 6,
            11 => 5
        );

        return isset($aKeyTranslate[$iId]) ? array($aKeyTranslate[$iId]) : array();
    }

    private static function translateDimensions($sWidth, $sHeight) {


        if (!empty($sWidth) && !empty($sHeight)) {
            return sprintf('%s x %s', $sWidth, $sHeight);
        }
        else
            return '';
    }

    private static function translateLandFence($iId) {
        $aKeyTranslate = array(
            1 => 0,
            2 => 1,
            3 => 1,
            4 => 1,
            5 => 1,
            6 => 1,
            7 => 1);

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translatePurpose($iId) {
        $aKeyTranslate = array(
            1 => array(1),
            2 => array(0, 2),
            3 => array(3),
            4 => array(4));

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : array();
    }

    private static function translateLocalType($iId) {

        $aKeyTranslate = array(
            1 => 0,
            2 => 1,
            3 => 2,
            4 => 3,
            12 => 6
        );

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateWarehouseHeating($iId) {

        $aKeyTranslate = array(
            1 => 1,
            2 => 1,
            3 => 1,
            4 => 1,
            5 => 1,
            6 => 1,
            7 => 1,
            8 => 0);


        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

    private static function translateWarehousePurpose($iId) {

        $aKeyTranslate = array(
            1 => 2,
            2 => 0,
            3 => 1,
            4 => 3,
        );

        return isset($aKeyTranslate[$iId]) ? $aKeyTranslate[$iId] : '';
    }

}

