<?php

class Model_Logic_Export_Otodom_Api extends Model_Logic_Abstract
{

    const CURRENCY_PLN = 'PLN';
    const SELLER_LICENCE = '15295';
    const SELLER_NAME = 'Marcin Brochadzki';
    const PACKAGE_TYPE_FULL = 'całość';
    const PACKAGE_TYPE_NEW = 'przyrost';
    const PACKAGE_TYPE_NOIMG = 'całość bez zdjęć';
    const FILENAME = 'dane';

    private $sFtpHost = 'ftp.otodom.pl';
    private $sFtpLogin = 'biuro@domset.pl';
    private $sFtpPassword = 'SxpNxaEC';
    private $sFtpPath = '/';
    private $sTimeStampFile = null;

    /* całość, przyrost, całość bez zdjęć
     * Instancja klasy.
     *
     * @var  Model_Logic_Export_Otodom_Api
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------

    /**
     * Zwraca instancje klasy.
     *
     * @return  Model_Logic_Export_Otodom_Api
     */
    static public function getInstance()
    {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function __construct()
    {

        $this->sTimeStampFile = APPLICATION_PATH . '/data/otodom-timestamp';
    }


    public function sendOffers()
    {
        try {
            $aExports = Model_DbTable_Export::getInstance()->getExportsByAdapter(Model_Logic_Export_Otodom::ID, $this->getDate());
            if (empty($aExports)) {
                $this->saveTimestamp();
                echo 'Nic do wyslania';
                return false;
            }

            $aData = array();

            foreach ($aExports as $aExport) {

                switch ($aExport['type']) {
                    case Model_DbTable_Export::TYPE_ADDED:
                    case Model_DbTable_Export::TYPE_EDITED:
                    case Model_DbTable_Export::TYPE_RELOADED:
                        $aFormData = unserialize($aExport['data']);
                        $aData['existed'][] = array('data' => $aFormData,
                            'export' => $aExport,
                            'images' => Model_Logic_Gallery::getInstance()->getImagesPathForOffer($aExport['id_offer']));
                        break;
                    case Model_DbTable_Export::TYPE_DELETED:
                        $aData['deleted'][] = $aExport['id'];
                        break;
                    default:
                        throw new Exception('Niezidentyfikowany typ eksportu', 500);
                        break;
                }
            }

            $sFullXmlFile = $this->createXml($aData);
            $sSavedZipPackage = $this->saveToZip($sFullXmlFile, $aData);
            $this->saveTimestamp();
            $this->sendToFtp($sSavedZipPackage);
            echo 'Wyslano ' . count($aExports) . ' ofert';
        } catch (Exception $e) {
            Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('log')->log($e, Zend_Log::CRIT);
        }
    }

    private function saveToZip($sData, $aData)
    {

        $sFilePath = APPLICATION_PATH .
            DIRECTORY_SEPARATOR .
            'data' .
            DIRECTORY_SEPARATOR .
            'trader-' . date('Ymd') . '-' . date('His') . '.zip';

        $oZip = new ZipArchive();
        $oZip->open($sFilePath, ZIPARCHIVE::OVERWRITE);
        $oZip->addFromString(self::FILENAME . '.xml', $sData);

        if (isset($aData['existed'])) {
            foreach ($aData['existed'] as $aElements) {
                foreach ($aElements['images'] as $key => $imagePath) {
                    $keyname = $key + 1;
                    $oZip->addFile($imagePath, $aElements['export']['id'] . '-' . (string)$keyname . '.jpg');
                }
            }
        }

        $oZip->close();
        return $sFilePath;
    }

    private function append($oDocument, $name, $value)
    {

        $oNode = $oDocument->createElement($name);
        $oNode->appendChild($oDocument->createTextNode($value));
        return $oNode;
    }

    private function pickDetailName($id)
    {

        $aOffer = Model_DbTable_Offer::getInstance()->getOfferBasicInformationById($id);

        switch ($aOffer['id_type']) {
            case Model_Logic_Offer_Flat_Rent::ID:
            case Model_Logic_Offer_Flat_Sell::ID:
                return 'Flat';
            case Model_Logic_Offer_House_Rent::ID:
            case Model_Logic_Offer_House_Sell::ID:
                return 'House';
            case Model_Logic_Offer_Land_Rent::ID:
            case Model_Logic_Offer_Land_Sell::ID:
                return 'Terrain';
            case Model_Logic_Offer_Local_Rent::ID:
            case Model_Logic_Offer_Local_Sell::ID:
                return 'CommercialProperty';
            case Model_Logic_Offer_Warehouse_Rent::ID:
            case Model_Logic_Offer_Warehouse_Sell::ID:
                return 'Hall';

        }


    }


    private function createXml($aOffers)
    {


        $oDocument = new DOMDocument();
        $oDocument->formatOutput = true;

        $oRootElement = $oDocument->createElement("otoDom");
        $oDocument->appendChild($oRootElement);

        $oRootElement->appendChild($this->append($oDocument, 'Agency', 'kontakt@domset.pl'));
        $oRootElement->appendChild($this->append($oDocument, 'Date', date('Y-m-d')));
        $oRootElement->appendChild($this->append($oDocument, 'ImportType', 'incremental'));

        $oDataGrid = $oDocument->createElement('Insertions');

        if (isset($aOffers['existed']) AND count($aOffers['existed']) > 0) {
            foreach ($aOffers['existed'] as $aOffer) {
                $offer = $oDocument->createElement('Insertion');
                $offer->appendChild($this->append($oDocument, "ID", $aOffer['export']['id']));
                $offer->appendChild($this->append($oDocument, "Action", 0));

                $detailName = $this->pickDetailName($aOffer['export']['id_offer']);

                $offer->appendChild($this->append($oDocument, "ObjectName", $detailName));

                foreach ($aOffer['data']['base'] as $sKey => $mVal) {

                    $offer->appendChild($this->append($oDocument, $sKey, $mVal));

                }
                $oDetails = $oDocument->createElement($detailName . 'Details');

                foreach ($aOffer['data']['details'] as $sKey => $mVal) {
                    $oDetails->appendChild($this->append($oDocument, $sKey, $mVal));
                }
                $offer->appendChild($oDetails);


                $oImages = $oDocument->createElement('Photos');

                foreach ($aOffer['images'] as $key => $image) {
                    $oImage = $oDocument->createElement('Photo');
                    $id = $key + 1;
                    $oImage->appendChild($this->append($oDocument, 'Position', $id));
                    $oImage->appendChild($this->append($oDocument, 'File', $aOffer['export']['id'] . '-' . $id . '.jpg'));

                    $oImages->appendChild($oImage);
                }

                $offer->appendChild($oImages);
                $oDataGrid->appendChild($offer);
            }
        }

        if (isset($aOffers['deleted']) AND count($aOffers['deleted']) > 0) {
            foreach ($aOffers['deleted'] as $deletedOffer) {
                $offer = $oDocument->createElement('Insertion');
                $offer->appendChild($this->append($oDocument, "ID", $deletedOffer));
                $offer->appendChild($this->append($oDocument, "Action", 2));
                $oDataGrid->appendChild($offer);
            }
        }

        $oRootElement->appendChild($oDataGrid);
        return $oDocument->saveXML();
    }

    private function sendToFtp($aPackage)
    {
        if (($oConnectionId = ftp_connect($this->sFtpHost, 21)) === false) {
            throw new Exception('Błąd połączenia do FTP otodom.pl', 500);
        }

        if (!ftp_login($oConnectionId, $this->sFtpLogin, $this->sFtpPassword)) {
            throw new Exception('Błąd podczas zalogowania do FTP otodom.pl', 500);
        }

        if (!ftp_put($oConnectionId, $this->sFtpPath . basename($aPackage), $aPackage, FTP_BINARY)) {
            throw new Exception('Błąd podczas transferu pliku do FTP otodom.pl', 500);
        }

        if (!ftp_rename($oConnectionId, $this->sFtpPath . basename($aPackage), $this->sFtpPath . 'ok-' . basename($aPackage))) {
            throw new Exception('Zmiana nazwy nieudana', 500);
        }


        return true;
    }

    public function getDate()
    {

        if (file_exists($this->sTimeStampFile)) {
            $sDate = file_get_contents($this->sTimeStampFile);
            return trim($sDate);
        } else
            return null;
    }

    private function saveTimestamp()
    {
        file_put_contents($this->sTimeStampFile, date('Y-m-d H:i:s'));
    }


}