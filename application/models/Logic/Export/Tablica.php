<?php

class Model_Logic_Export_Tablica extends Model_Logic_Export_Abstract implements Model_Logic_Export_Interface {

    const ID = 4;
    const IMAGE_EXPORT_LIMIT = 8;
    const TITLE_LENGHT_LIMIT = 70;
    const CONTACT_EMAIL = 'biuro@domset.pl';
    const PERSON = 'DOMSET Nieruchomości';

    public function manageAdd(App_Controller_Admin_Abstract $oCtrl) {

      
        if (($iOfferId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed');
        }

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($iOfferId);
        
        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        }

        $oForm = Model_Logic_Export_Tablica_Form::getInstance()->getForm($aOffer['id_type']);

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            if ($this->addValidatedExport($iOfferId, $aOffer, $oForm->getValues())) {
                $oCtrl->successMessage('Operacja eksportu zakończona pomyślnie');
            } else {
                $oCtrl->errorMessage('Wystąpił błąd podczas próby eksportu do gratka.pl. Spróbój później.');
            }

            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array(
                                'module' => 'admin',
                                'controller' => 'export',
                                'action' => 'list',
                                'offer' => $iOfferId), 'home', true));
        }
        $aTranslatedOfferData = Model_Logic_Export_Tablica_Translate::translate($aOffer);
        $oForm->populate($aTranslatedOfferData);
        $oCtrl->view->form = $oForm;
    }

    public function manageEdit(App_Controller_Admin_Abstract $oCtrl, $aExport) {


        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        }


        $oForm = Model_Logic_Export_Tablica_Form::getInstance()->getForm($aOffer['id_type']);

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            if ($this->editValidatedExport($aOffer, $oForm->getValues(), $aExport)) {
                $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie');
            } else {
                $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji eksportu do gratka.pl. Spróbój później.');
            }

            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array(
                                'module' => 'admin',
                                'controller' => 'export',
                                'action' => 'list',
                                'offer' => $aExport['id_offer']), 'home', true));
        }

        $aTranslatedOfferData = Model_Logic_Export_Tablica_Translate::translate($aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    public function manageRemove(App_Controller_Admin_Abstract $oCtrl, $aExport) {

        if (($iOfferId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed');
        }

        if ($this->removeValidatedExport($aExport)) {
            $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie');
        } else {
            $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji eksportu do tablica.pl. Spróbój później.');
        }

        $oCtrl->getHelper('Redirector')
                ->goToUrl($oCtrl->view->url(array(
                            'module' => 'admin',
                            'controller' => 'export',
                            'action' => 'list',
                            'offer' => $aExport['id_offer']), 'home', true));
    }

    public function manageReload(App_Controller_Admin_Abstract $oCtrl, $aExport) {

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        }

        $oForm = Model_Logic_Export_Tablica_Form::getInstance()->getForm($aOffer['id_type']);

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            if ($this->reloadValidatedExport($oForm->getValues(), $aExport, $aOffer)) {
                $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie');
            } else {
                $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji eksportu do tablica.pl. Spróbój później.');
            }
            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array(
                                'module' => 'admin',
                                'controller' => 'export',
                                'action' => 'list',
                                'offer' => $aExport['id_offer']), 'home', true));
        }

        $aTranslatedOfferData = Model_Logic_Export_Tablica_Translate::translate($aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    private function addValidatedExport($iOfferId, $aOffer, $aData) {

        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction();
            $iExportId = Model_DbTable_Export::getInstance()->addExport($iOfferId, self::ID);

            $aData['created_at'] = date('Y-m-d H:i:s');
            $aData['category_id'] = Model_Logic_Export_Tablica_Translate::translateCategory($aOffer['id_type']);
            $aData['email'] = self::CONTACT_EMAIL;
            $aData['person'] = self::PERSON;
            $aData['phone'] = $aOffer['phone'];
            if(isset($aData['type']))
            {
                $aData['param_type'] = $aData['type']; 
            }
            $aData['private_business'] = 'business';
            $aData['offer_seek'] = 'offer';
            $aData['agree'] = true;
            $aData['param_price'] = array('price', $aData['param_price']);
            $aData['external_id'] = $iExportId;
            $aData['images'] = Model_Logic_Gallery::getInstance()->getImagesLinksForOffer($iOfferId, self::IMAGE_EXPORT_LIMIT);
            //ToDO TESTING
            $aData['images'] = array_merge(array('false') , $aData['images']); 

            $iRemoteId = Model_Logic_Export_Tablica_Api::getInstance()->sendExport($aData);
            
            $aData['images'] = array_keys($aData['images']); 
            Model_DbTable_Export::getInstance()->editExport($iExportId, Model_DbTable_Export::TYPE_ADDED, $aData, $iRemoteId);

            Zend_Db_Table::getDefaultAdapter()->commit();
            return true;
        } catch (Exception $e) {
            Zend_Db_Table::getDefaultAdapter()->rollBack();
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function editValidatedExport($aOffer, $aData, $aExport) {



        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction();
            
            $aData['id'] = $aExport['remote_id'];
            $aData['category_id'] = Model_Logic_Export_Tablica_Translate::translateCategory($aOffer['id_type']);
            $aData['email'] = self::CONTACT_EMAIL;
            $aData['person'] = self::PERSON;
            $aData['phone'] = $aOffer['phone']; 
            $aData['private_business'] = 'business';
            $aData['offer_seek'] = 'offer';
            $aData['agree'] = true;
            $aData['param_price'] = array('price', $aData['param_price']);
            $aData['external_id'] = $aExport['id'];
            $aData['images'] = Model_Logic_Gallery::getInstance()->getImagesLinksForOffer($aExport['id_offer'], self::IMAGE_EXPORT_LIMIT);

            $iRemoteId = Model_Logic_Export_Tablica_Api::getInstance()->sendExport($aData);
            Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_EDITED, $aData);
            Zend_Db_Table::getDefaultAdapter()->commit();
            return true;
        } catch (Exception $e) {
            Zend_Db_Table::getDefaultAdapter()->rollBack();
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function removeValidatedExport($aExport) {

        try {
            Model_Logic_Export_Tablica_Api::getInstance()->removeExport($aExport['remote_id']);
            Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_DELETED);
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function reloadValidatedExport($aData, $aExport, $aOffer) {

        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction();

            Model_Logic_Export_Tablica_Api::getInstance()->removeExport($aExport['remote_id']);
            $aData['created_at'] = date('Y-m-d H:i:s');
            $aData['category_id'] = Model_Logic_Export_Tablica_Translate::translateCategory($aOffer['id_type']);
            $aData['email'] = self::CONTACT_EMAIL;
            $aData['person'] = self::PERSON;
            $aData['phone'] = $aOffer['phone'];
            $aData['private_business'] = 'business';
            $aData['offer_seek'] = 'offer';
            $aData['agree'] = true;
            $aData['param_price'] = array('price', $aData['param_price']);
            $aData['external_id'] = $aExport['id'];
            $aData['images'] = Model_Logic_Gallery::getInstance()->getImagesLinksForOffer($aOffer['id_offer'], self::IMAGE_EXPORT_LIMIT);

            $iRemoteId = Model_Logic_Export_Tablica_Api::getInstance()->sendExport($aData);
            Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_RELOADED, $aData, $iRemoteId);

            Zend_Db_Table::getDefaultAdapter()->commit();
            return true;
        } catch (Exception $e) {
            Zend_Db_Table::getDefaultAdapter()->rollBack();
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }
}