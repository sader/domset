<?php

class Model_Logic_Export_Domimporta extends Model_Logic_Export_Abstract implements Model_Logic_Export_Interface {

    const ID = 5;
    const IMAGE_EXPORT_LIMIT = 15;

    private $iOrderId;

    public function getOfferId() {
        return $this->iOrderId;
    }

    public function setOfferId($iOrderId) {
        $this->iOrderId = $iOrderId;
    }

    public function manageAdd(App_Controller_Admin_Abstract $oCtrl) {

        if (($iOfferId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed');
        } else {
            $this->setOfferId($iOfferId);
        }

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($iOfferId);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        }

        if (($oForm = Model_Logic_Export_Domimporta_Form::getInstance()->getForm($aOffer['id_type'])) === false) {
            throw new Exception('Problem z pobraniem listy pól z domimporta i wygenerowaniem formularza');
        }

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            if ($this->addValidatedExport($oForm->getValues(), $aOffer)) {
                $oCtrl->successMessage('Operacja eksportu zakończona pomyślnie. Pamiętaj, że domimporta aktualizuje ogłoszenia raz  na dobę tak więc nie są widoczne od razu');
            } else {
                $oCtrl->errorMessage('Wystąpił błąd podczas próby eksportu do domimporta. Spróbój później.');
            }


            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array('controller' => 'export',
                                'action' => 'list',
                                'offer' => $this->getOfferId()), null, true));
        }

        $aTranslatedOfferData = Model_Logic_Export_Domimporta_Translate::translate($aOffer['id_type'], $aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    public function manageEdit(App_Controller_Admin_Abstract $oCtrl, $aExport) {

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        } else {
            $this->setOfferId($aOffer['id']);
        }

        if (($oForm = Model_Logic_Export_Domimporta_Form::getInstance()->getForm($aOffer['id_type'])) === false) {
            throw new Exception('Problem z pobraniem listy pól z domimporta i wygenerowaniem formularza');
        }

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $this->editValidatedExport($oForm->getValues(), $aExport) ? $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie. Pamiętaj, że domimporta aktualizuje ogłoszenia raz  na dobę tak więc nie są widoczne od razu') : $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji eksportu do domimporta');

            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array('controller' => 'export',
                                'action' => 'list',
                                'offer' => $aExport['id_offer']), null, true));
        }

        $aTranslatedOfferData = Model_Logic_Export_Domimporta_Translate::translate($aOffer['id_type'], $aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    public function manageRemove(App_Controller_Admin_Abstract $oCtrl, $aExport) {

        if (($iOfferId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed');
        }

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        } else {
               $this->setOfferId($aOffer['id']);
        }

        $this->removeValidatedExport($aExport) ? $oCtrl->successMessage('Operacja usunięcia eksportu zakończona pomyślnie. Pamiętaj, że domimporta aktualizuje ogłoszenia raz  na dobę tak więc nie są widoczne od razu') : $oCtrl->errorMessage('Wystąpił błąd podczas próby usunięcia eksportu do domimporta.');

        $oCtrl->getHelper('Redirector')
                ->goToUrl($oCtrl->view->url(array('controller' => 'export',
                            'action' => 'list',
                            'offer' => $aExport['id_offer']), null, true));
    }

    public function manageReload(App_Controller_Admin_Abstract $oCtrl, $aExport) {

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        } else {
            $this->setOfferId($aOffer['id']);
        }

        if (($oForm = Model_Logic_Export_Domimporta_Form::getInstance()->getForm($aOffer['id_type'])) === false) {
            throw new Exception('Problem z pobraniem listy pól z domimporta i wygenerowaniem formularza');
        }


        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $this->reloadValidatedExport($oForm->getValues(), $aExport) ? $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie. Pamiętaj, że domimporta aktualizuje ogłoszenia raz  na dobę tak więc nie są widoczne od razu') : $oCtrl->errorMessage('Wystąpił błąd podczas próby przeładowania eksportu do domimporta.');

            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array('controller' => 'export',
                                'action' => 'list',
                                'offer' => $aExport['id_offer']), null, true));
        }

        $aTranslatedOfferData = Model_Logic_Export_Domimporta_Translate::translate($aOffer['id_type'], $aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    private function addValidatedExport($aFormData) {

        try {
            Model_DbTable_Export::getInstance()->addExport($this->getOfferId(), self::ID, null, $aFormData);
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function editValidatedExport($aData, $aExport) {
        try {
            Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_EDITED, $aData, null, date('Y-m-d H:i:s'));
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function removeValidatedExport($aExport) {

        try {
            $sLastExport = Model_Logic_Export_Domimporta_Api::getInstance()->getDate();

            if ($sLastExport === null OR (strtotime($sLastExport) < strtotime($aExport['date']))) {
                Model_DbTable_Export::getInstance()->delete(array('id = ?' => $aExport['id']));
            } else {
                Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_DELETED, null, null, date('Y-m-d H:i:s'));
            }
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function reloadValidatedExport($aData, $aExport) {

        try {
            Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_DELETED, null, null, date('Y-m-d H:i:s'));
            Model_DbTable_Export::getInstance()->addExport($this->getOfferId(), self::ID, null, $aData, Model_DbTable_Export::TYPE_RELOADED);
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }
}