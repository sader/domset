<?php

class Model_Logic_Export_Otodom extends Model_Logic_Export_Abstract implements Model_Logic_Export_Interface {

    const ID = 2;
    const IMAGE_EXPORT_LIMIT = 15;

    private $iOrderId;

    public function getOfferId() {
        return $this->iOrderId;
    }

    public function setOfferId($iOrderId) {
        $this->iOrderId = $iOrderId;
    }

    public function manageAdd(App_Controller_Admin_Abstract $oCtrl) {

        if (($iOfferId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed');
        }

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($iOfferId);

        $oForm = new Model_Logic_Export_Otodom_Form($aOffer['id_type'], array());

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {


            $aGeo = array('lat' => $aOffer['lat'], 'lng' => $aOffer['lng']);


            $aFormValidatedData = $oForm->getValues();

            if ($this->addValidatedExport($iOfferId, $aFormValidatedData, $aGeo)) {
                $oCtrl->successMessage('Operacja eksportu zakończona pomyślnie');
            } else {
                $oCtrl->errorMessage('Wystąpił błąd podczas próby eksportu do otodom.pl. Spróbój później.');
            }
            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array('module' => 'admin',
                                'controller' => 'export',
                                'action' => 'list',
                                'offer' => $iOfferId), 'home', true));
        }

        $aTranslatedOfferData = Model_Logic_Export_Otodom_Translate::translate($aOffer['id_type'], $aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    public function manageEdit(App_Controller_Admin_Abstract $oCtrl, $aExport) {


        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        }


      //  $aDictionaries = Model_Logic_Export_Otodom_Api::getInstance()->getDictionaries();

        $oForm = new Model_Logic_Export_Otodom_Form($aOffer['id_type'], array());

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {


            $aGeo = array('lat' => $aOffer['lat'], 'lng' => $aOffer['lng']);

            if ($this->editValidatedExport($aExport['id_offer'], $oForm->getValues(), $aExport, $aGeo)) {
                $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie');
            } else {
                $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji eksportu do otodom.pl. Spróbój później.');
            }
            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array(
                                'module' => 'admin',
                                'controller' => 'export',
                                'action' => 'list',
                                'offer' => $aExport['id_offer']), 'home', true));
        }

        $aTranslatedOfferData = Model_Logic_Export_Otodom_Translate::translate($aOffer['id_type'], $aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    public function manageRemove(App_Controller_Admin_Abstract $oCtrl, $aExport) {

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($aExport['id_offer']);

        if (empty($aOffer)) {
            throw new Model_Logic_Exception('Niewłaściwy identyfikator oferty', 500);
        }

        if ($this->removeValidatedExport($aExport)) {

            $oCtrl->successMessage('Operacja edycji eksportu zakończona pomyślnie');
        } else {
            $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji eksportu do otodom.pl. Spróbój później.');
        }

        $oCtrl->getHelper('Redirector')
                ->goToUrl($oCtrl->view->url(array(
                            'module' => 'admin',
                            'controller' => 'export',
                            'action' => 'list',
                            'offer' => $aExport['id_offer']), 'home', true));
    }

    public function manageReload(App_Controller_Admin_Abstract $oCtrl, $aExport) {

        $iOfferId = $aExport['id_offer'];

        $aOffer = Model_Logic_Offer::getInstance()->getDetailedOffer($iOfferId);
        // $aDictionaries = Model_Logic_Export_Otodom_Api::getInstance()->getDictionaries();

        $oForm = new Model_Logic_Export_Otodom_Form($aOffer['id_type'], array());

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {


            $aGeo = array('lat' => $aOffer['lat'], 'lng' => $aOffer['lng']);

            if ($this->reloadValidatedExport($iOfferId, $oForm->getValues(), $aExport, $aGeo)) {
                $oCtrl->successMessage('Operacja przeładowania eksportu zakończona pomyślnie');
            } else {
                $oCtrl->errorMessage('Wystąpił błąd podczas próby przeładowania do otodom.pl. Spróbój później.');
            }
            $oCtrl->getHelper('Redirector')
                    ->goToUrl($oCtrl->view->url(array(
                                'module' => 'admin',
                                'controller' => 'export',
                                'action' => 'list',
                                'offer' => $iOfferId), 'home', true));
        }

        $aTranslatedOfferData = Model_Logic_Export_Otodom_Translate::translate($aOffer['id_type'], $aOffer);
        $oForm->populate($aTranslatedOfferData);

        $oCtrl->view->form = $oForm;
    }

    private function addValidatedExport($id_offer, $aFormData) {

        try {
            Model_DbTable_Export::getInstance()->addExport($id_offer, self::ID, null, $aFormData);
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function editValidatedExport($aData, $aExport) {
        try {
            Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_EDITED, $aData, null, date('Y-m-d H:i:s'));
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function removeValidatedExport($aExport) {

        try {
            $sLastExport = Model_Logic_Export_Otodom_Api::getInstance()->getDate();



            if ($sLastExport === null OR (strtotime($sLastExport) < strtotime($aExport['date']))) {
                Model_DbTable_Export::getInstance()->delete(array('id = ?' => $aExport['id']));
            } else {
                Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_DELETED, null, null, date('Y-m-d H:i:s'));
            }
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function reloadValidatedExport($iOfferId, $aData, $aExport) {

        try {
            Model_DbTable_Export::getInstance()->editExport($aExport['id'], Model_DbTable_Export::TYPE_DELETED, null, null, date('Y-m-d H:i:s'));
            Model_DbTable_Export::getInstance()->addExport($iOfferId, self::ID, null, $aData, Model_DbTable_Export::TYPE_RELOADED);
            return true;
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }
}