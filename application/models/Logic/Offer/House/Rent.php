<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Offer_House_Rent extends Model_Logic_Offer_Abstract implements Model_Logic_Offer_Interface {
    const ID = 4;

       public function __construct() {
        parent::__construct();
        
        $this->oTableModel = $this->getTableModel(); 
        $this->sDetailViewTemplate = 'offer_house_rent.phtml'; 
        $this->sPageOfferList = '_partial/offer/house/list.phtml';
        $this->sDetailTemplate = '_partial/offer/house/detail_rent.phtml';
        $this->sPrintTemplate = '_partial/offer/house/print.phtml';
        $this->sPrintLandscapeTemplate = '_partial/offer/house/print_l.phtml';
        
    }
    
    
    public function getFormElements() {
        $oForm = new Admin_Form_Offer_House_Rent();
        return $oForm->getElements();
    }

    public function getId() {
        return self::ID;
    }

    public function getTableModel() {
        return Model_DbTable_Offer_House_Rent::getInstance();
    }
    
      public function getFilter()
    {
        return new Admin_Form_Offer_House_Filter(self::ID); 
    }
    
     public function getPageFilter($iType)
    {
        return new Page_Form_FilterHouse($iType); 
    }
    

}
