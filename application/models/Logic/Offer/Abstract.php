<?php

class Model_Logic_Offer_Abstract extends Model_Logic_Abstract {

    protected $oTableModel;
    protected $sDetailViewTemplate; 
    protected $sPageOfferList; 
    protected $sDetailTemplate; 
    protected $sPrintTemplate; 

    public function add($aValidatedData) {
        $this->oTableModel->addOffer($aValidatedData);
    }
    
    
    public function edit($aValidatedData, $iId) {
        $this->oTableModel->editOffer($aValidatedData, $iId);
    }

    public function removeType($iId) {
        $this->oTableModel->removeOffer($iId);
    }
    
       public function get($iId) {
        $this->oTableModel->get($iId);
    }
    
     public function getDetailViewPartial()
    {
        return $this->sDetailViewTemplate; 
    }
    
    public function getPageOfferListPartial()
    {
        return $this->sPageOfferList; 
    }
    
    public function  getDetailTemplate()
    {
        return $this->sDetailTemplate; 
        
    }
    
     public function  getPrintTemplate()
    {
        return $this->sPrintTemplate; 
        
    }
    
         public function  getPrintLandscapeTemplate()
    {
        return $this->sPrintLandscapeTemplate; 
        
    }



    public function getOffersForWanted($aFilters)
    {
        return $this->getTableModel()->getOffersForWanted($aFilters); 
    }

}

