<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Offer_Local_Sell extends Model_Logic_Offer_Abstract implements Model_Logic_Offer_Interface {
    const ID = 7;

    public function __construct() {
        parent::__construct();

        $this->oTableModel = $this->getTableModel();
        $this->sDetailViewTemplate = 'offer_local_sell.phtml';
        $this->sPageOfferList = '_partial/offer/local/list.phtml';
        $this->sDetailTemplate = '_partial/offer/local/detail_sell.phtml';
        $this->sPrintTemplate = '_partial/offer/local/print.phtml';
        $this->sPrintLandscapeTemplate = '_partial/offer/local/print_l.phtml';
    }

    public function getFormElements() {
        $oForm = new Admin_Form_Offer_Local_Sell();
        return $oForm->getElements();
    }

    public function getId() {
        return self::ID;
    }

    public function getTableModel() {
        return Model_DbTable_Offer_Local_Sell::getInstance();
    }

    public function getFilter() {
        return new Admin_Form_Offer_Local_Filter(self::ID);
    }
    public function getPageFilter($iType)
    {
        return new Page_Form_FilterLocal($iType); 
    }

}

