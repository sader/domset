<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Offer_Warehouse_Rent extends Model_Logic_Offer_Abstract implements Model_Logic_Offer_Interface {
    const ID = 10;

       public function __construct() {
        parent::__construct();
        
        $this->oTableModel = $this->getTableModel(); 
        $this->sDetailViewTemplate = 'offer_warehouse_rent.phtml'; 
        $this->sPageOfferList = '_partial/offer/warehouse/list.phtml';
        $this->sDetailTemplate = '_partial/offer/warehouse/detail_rent.phtml';
        $this->sPrintTemplate = '_partial/offer/warehouse/print.phtml';
        $this->sPrintLandscapeTemplate = '_partial/offer/warehouse/print_l.phtml';
        
    }
    
    public function getFormElements() {
        $oForm = new Admin_Form_Offer_Warehouse_Rent();
        return $oForm->getElements();
    }

    public function getId() {
        return self::ID;
    }

    public function getTableModel() {
        return Model_DbTable_Offer_Warehouse_Rent::getInstance();
    }
    
     public function getFilter()
    {
        return new Admin_Form_Offer_Warehouse_Filter(self::ID); 
    }
    
    public function getPageFilter($iType)
    {
        return new Page_Form_FilterWarehouse($iType); 
    }

}