<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Offer_Flat_Sell extends Model_Logic_Offer_Abstract implements Model_Logic_Offer_Interface {
    
    
    const ID = 1;
    
    public function getId()
    {
        return self::ID; 
    }
    
    public function __construct() {
        parent::__construct();
        
        $this->oTableModel = $this->getTableModel();
        $this->sDetailViewTemplate = 'offer_flat_sell.phtml'; 
        $this->sPageOfferList = '_partial/offer/flat/list.phtml';
        $this->sDetailTemplate = '_partial/offer/flat/detail_sell.phtml';
        $this->sPrintTemplate = '_partial/offer/flat/print.phtml';
        $this->sPrintLandscapeTemplate = '_partial/offer/flat/print_l.phtml';
    }

    public function getTableModel()
    {
        return Model_DbTable_Offer_Flat_Sell::getInstance(); 
    }


    public function getFormElements() {
        $oForm = new Admin_Form_Offer_Flat_Sell();
        return $oForm->getElements();
    }

    public function getFilter()
    {
        return new Admin_Form_Offer_Flat_Filter(self::ID); 
    }
    
        public function getPageFilter($iType)
    {
        return new Page_Form_FilterFlat($iType); 
    }
    
  

}
