<?php

interface Model_Logic_Offer_Interface {

    public function getId();
    public function getFormElements();
    public function getTableModel();
    public function getFilter(); 
   // public function getPageFilter(); 
}

