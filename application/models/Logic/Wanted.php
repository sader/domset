<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Wanted extends Model_Logic_Abstract {

    const FILTER_WANTED = 1;
    const FILTER_OFFER = 2;

    private function getRegisteredAdapters() {
        return array(
            Model_Logic_Offer_Flat_Sell::ID => 'Model_Logic_Wanted_Flat',
            Model_Logic_Offer_Flat_Rent::ID => 'Model_Logic_Wanted_Flat',
            Model_Logic_Offer_House_Sell::ID => 'Model_Logic_Wanted_House',
            Model_Logic_Offer_House_Rent::ID => 'Model_Logic_Wanted_House',
            Model_Logic_Offer_Land_Sell::ID => 'Model_Logic_Wanted_Land',
            Model_Logic_Offer_Land_Rent::ID => 'Model_Logic_Wanted_Land',
            Model_Logic_Offer_Local_Sell::ID => 'Model_Logic_Wanted_Local',
            Model_Logic_Offer_Local_Rent::ID => 'Model_Logic_Wanted_Local',
            Model_Logic_Offer_Warehouse_Sell::ID => 'Model_Logic_Wanted_Warehouse',
            Model_Logic_Offer_Warehouse_Rent::ID => 'Model_Logic_Wanted_Warehouse');
    }

    public function initAdapterById($iId = null) {

        if ($this->isAdapterRegistered($iId)) {
            $aAdapters = $this->getRegisteredAdapters();
            return new $aAdapters[$iId]();
        }
        else
            throw new Model_Logic_Exception('Offer adapter failed', 500);
    }

    private function isAdapterRegistered($iId = null) {
        $aAdapters = $this->getRegisteredAdapters();
        return (!empty($iId) && isset($aAdapters[$iId]));
    }

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Wanted
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Wanted 
     *    */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getDetailedOffer($iId) {
        $aWantedBasicData = Model_DbTable_Wanted::getInstance()->getWantedBasicInformationById($iId);
        $oWantedAdapter = $this->initAdapterById($aWantedBasicData['id_type']);
        $aWantedDetailData = $oWantedAdapter->getTableModel()->get($iId);
        return array_merge($aWantedBasicData, $aWantedDetailData);
    }

    public function manageAddWanted(App_Controller_Admin_Abstract $oCtrl) {

        $iOfferType = $oCtrl->getRequest()->getParam('type', Model_Logic_Offer_Flat_Sell::ID);
        
        $oForm = new Admin_Form_Wanted();

        $oWantedAdapter = $this->initAdapterById($iOfferType);
        $oForm->setWantedType($oWantedAdapter, $iOfferType);

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $this->saveValidatedWanted($oForm->getValues(), $oWantedAdapter) ?
                            $oCtrl->successMessage('Oferta dodana poprawnie.') :
                            $oCtrl->errorMessage('Nieudane dodanie oferty.');

            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'search')));
        }
        
        $oCtrl->view->headLink()->appendStylesheet('/scripts/select2/select2.css');
        
        $oCtrl->view->headScript()
                ->appendFile('/scripts/jquery.price_format.1.7.min.js')
                ->appendFile('/scripts/select2/select2.min.js')
                ->appendFile('/scripts/jquery.maskedinput.min.js')
                ->appendFile('/scripts/manager/admin/wanted/details.js');


        return $this->set('form', $oForm)->getView();
    }

    public function manageEditWanted(App_Controller_Admin_Abstract $oCtrl) {

        if (($iOfferId = $oCtrl->getRequest()->getParam('offer')) === null)
            throw new Model_Logic_Exception('Offer ID failed', 500);

        $aData = $this->getDetailedOffer($iOfferId);
        $oWantedAdapter = $this->initAdapterById($aData['id_type']);

        $oForm = new Admin_Form_Wanted();
        $oForm->setWantedType($oWantedAdapter, $aData['id_type'])->editMode();

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            if ($this->editValidatedWanted($iOfferId, $oForm->getValues(), $oWantedAdapter)) {
                $oCtrl->successMessage('Udana edycja poszukiwania.');
            } else {
                $oCtrl->errorMessage('Nieudana edycja poszukiwania.');
            }

            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'search'))));
        }

        $oForm->populate($this->transformDataToForm($aData, $iOfferId));

      $oCtrl->view->headLink()->appendStylesheet('/scripts/select2/select2.css');
        
        $oCtrl->view->headScript()
                ->appendFile('/scripts/jquery.price_format.1.7.min.js')
                ->appendFile('/scripts/select2/select2.min.js')
                ->appendFile('/scripts/jquery.maskedinput.min.js')
                ->appendFile('/scripts/manager/admin/wanted/details.js');

        return $this->set('form', $oForm)->set('id', $iOfferId)->getView();
    }

    public function manageList(App_Controller_Admin_Abstract $oCtrl) {

        $iClient = $oCtrl->getRequest()->getParam('client');

        $oGrid = new Admin_Grid_Wanted($iClient);
        return $this->set('list', $oGrid->deploy())->set('client', $iClient)->getView();
    }

    public function manageRemove(App_Controller_Admin_Abstract $oCtrl) {
        if (($iWantedId = $oCtrl->getRequest()->getParam('offer', null)) === null)
            throw new Model_Logic_Exception('Brak identyfikatora');

        $aWanted = Model_DbTable_Wanted::getInstance()->getWantedBasicInformationById($iWantedId);
        $oWantedAdapter = $this->initAdapterById($aWanted['id_type']);

        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction();
            Model_DbTable_Wanted::getInstance()->remove($iWantedId);
            $oWantedAdapter->remove($iWantedId);
            $oCtrl->successMessage('Poszukiwanie usunięte.');
            Zend_Db_Table::getDefaultAdapter()->commit();
        } catch (Exception $e) {
            Zend_Db_Table::getDefaultAdapter()->rollback();
            $this->getLog()->log($e, Zend_Log::CRIT);
            $oCtrl->errorMessage('Nieudane usunięcie poszukiwania.');
        }

        $oCtrl->getHelper('Redirector')->goToUrl($_SERVER['HTTP_REFERER']);
    }

    private function saveValidatedWanted($aWantedData, $oWantedAdapter) {

        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction();

            $iWantedBaseId = Model_DbTable_Wanted::getInstance()->addWanted($aWantedData);
            $oWantedAdapter->add(array_merge(array('id_offer' => $iWantedBaseId), $aWantedData));
            Model_Logic_Client::getInstance()->reloadClientType($aWantedData['id_client'], Model_DbTable_Client::ACCOUNT_TYPE_LOOKER); 
            Zend_Db_Table::getDefaultAdapter()->commit();
            return true;
        } catch (Exception $e) {
            Zend_Db_Table::getDefaultAdapter()->rollback();
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function editValidatedWanted($iWantedId, $aWantedData, $oWantedAdapter) {

        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction();
            Model_DbTable_Wanted::getInstance()->edit($aWantedData, $iWantedId);
            $oWantedAdapter->edit($aWantedData, $iWantedId);
            Zend_Db_Table::getDefaultAdapter()->commit();
            return true;
        } catch (Exception $e) {
            Zend_Db_Table::getDefaultAdapter()->rollback();
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    public function manageChangeWantedOfferType(App_Controller_Admin_Abstract $oCtrl) {
        $iOfferType = $oCtrl->getRequest()->getParam('type', null);
        $oForm = new Admin_Form_Wanted();
        try {
            if ($iOfferType !== null) {
                $oOfferAdapter = $this->initAdapterById($iOfferType);
                $oForm->setWantedType($oOfferAdapter, $iOfferType);
                $aResponse = array('status' => true,
                    'content' => $oForm->populate($oCtrl->getRequest()->getPost())->render());
            }
        } catch (Exception $e) {
            $aResponse = array('status' => false, 'message' => $e->getMessage());
        } //'Wystąpił błąd podczas budowania formularza


        return $aResponse;
    }

    public function manageChangeWantedFilter(App_Controller_Admin_Abstract $oCtrl) {
        $iFilterType = $oCtrl->getRequest()->getParam('type', self::FILTER_WANTED);
        try {
            if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null) {
                throw new Exception('Brak identyfikatora', 500);
            }
        } catch (Exception $e) {
            $aResponse = array('status' => false, 'message' => $e->getMessage());
        }

        return $aResponse;
    }

    public function manageSearch(App_Controller_Admin_Abstract $oCtrl) {


        $oWantedGrid = new Admin_Grid_WantedOffers();
        $oOffersGrid = new Admin_Grid_Offers(Admin_Grid_Offers::WANTED_LIST);

        $oCtrl->view->headScript()->appendFile('/scripts/manager/admin/wanted/search.js');

        return $this->set('wanted', $oWantedGrid->deploy())
                        ->set('offers', $oOffersGrid->deploy())
                        ->getView();
    }

    public function manageGetOffer(App_Controller_Admin_Abstract $oCtrl) {

        if (($iOfferId = $oCtrl->getRequest()->getParam('id')) === null)
            return array('status' => false, 'content' => 'Nie wysłano identyfikatora');


        if (($aOfferData = Model_DbTable_Offer::getInstance()->getOfferBasicInformationById($iOfferId)) == null)
            return array('status' => false, 'content' => 'Nie oferty o podanym identyfikatorze');


        return array('status' => true, 'content' => $aOfferData);
    }

    public function manageChangeSearchWanted(App_Controller_Admin_Abstract $oCtrl) {
        
        $iOfferId = $oCtrl->getRequest()->getParam('offer');

        $aOfferMerged = Model_Logic_Offer::getInstance()->getDetailedOffer($iOfferId); 
        
        $oWantedAdapter = $this->initAdapterById($aOfferMerged['id_type']);
        $oAdapter = $oWantedAdapter->getWantedForOffers($aOfferMerged);
        
        $oOffersGrid = new Admin_Grid_WantedOffers($oAdapter);
        
        return array('status' => true, 'content' => $oOffersGrid->deploy());
        
    }

    public function manageChangeSearchOffers(App_Controller_Admin_Abstract $oCtrl) {

        $iWantedId = $oCtrl->getRequest()->getParam('wanted');

        $aFilterMerged = $this->getDetailedOffer($iWantedId);
        $oOfferAdapter = Model_Logic_Offer::getInstance()->initAdapterById($aFilterMerged['id_type']);
        $oAdapter = $oOfferAdapter->getOffersForWanted($aFilterMerged);
        $oOffersGrid = new Admin_Grid_Offers(Admin_Grid_Offers::WANTED_LIST, null, $oAdapter);

        return array('status' => true, 'content' => $oOffersGrid->deploy());
    }

    public function transformDataToForm($aData) {

        $aSets = array(
            'province',
            'section',
            'building_type',
            'quality',
            'installation_quality',
            'windows',
            'kitchen',
            'noise',
            'heat',
            'additional',
            'attic',
            'basement',
            'building_material',
            'roof',
            'roof_type',
            'sewerage',
            'fence',
            'garage',
            'access',
            'land_type',
            'shape',
            'services',
            'purpose',
            'entry');


        foreach ($aSets as $sField) {
            if (isset($aData[$sField]))
                $aData[$sField] = explode(',', $aData[$sField]);
        }

        return $aData;
    }

}