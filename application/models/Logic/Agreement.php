<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Agreement extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Agreement
     */
    static private $_oInstance;
    

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Agreement
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    
    public function  manageSendFile(App_Controller_Admin_Abstract $oCtrl)
    {
          if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
                throw new Model_Logic_Exception('No offer id specified', 500);

        if (($sFile = $oCtrl->getRequest()->getParam('file', null)) === null)
                throw new Model_Logic_Exception('No offer id specified', 500);
        
        $sFile = urldecode($sFile); 
        
        
       $sFilePath = APPLICATION_PATH .
                    DIRECTORY_SEPARATOR .
                    'data' .
                    DIRECTORY_SEPARATOR .
                    'agreement' .
                    DIRECTORY_SEPARATOR .
                    $iId.
                    DIRECTORY_SEPARATOR . 
                    $sFile; 
        
        if(file_exists($sFilePath))
        {
            $oCtrl->getHelper('SendFile')->sendFile($sFilePath); 
        }
        else  throw new Model_Logic_Exception('Plik nie istnieje', 500);
        
    }
    
    public function manageUploadFile(App_Controller_Admin_Abstract $oCtrl) {

        try {
            if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
                throw new Model_Logic_Exception('No offer id specified', 500);

            if (!empty($_FILES['fd-file']) and is_uploaded_file($_FILES['fd-file']['tmp_name'])) {
                $name = $_FILES['fd-file']['name'];
                $data = file_get_contents($_FILES['fd-file']['tmp_name']);
            } else {
                $name = urldecode(@$_SERVER['HTTP_X_FILE_NAME']);
                $data = file_get_contents("php://input");
            }

            $sDir = APPLICATION_PATH .
                    DIRECTORY_SEPARATOR .
                    'data' .
                    DIRECTORY_SEPARATOR .
                    'agreement' .
                    DIRECTORY_SEPARATOR .
                    $iId;

            if (!is_dir($sDir))
                mkdir($sDir, 0775, true);

            if (file_exists($sDir . DIRECTORY_SEPARATOR . $name)) {
                throw new Exception($name . ' już jest dodany do tej umowy', 500);
            }

            file_put_contents($sDir . DIRECTORY_SEPARATOR . $name, $data);
            
        } catch (Exception $e) {
            return array('status' => false,
                'content' => array('message' => $e->getMessage()));
        }

        return array(
            'status' => true,
            'content' => array(
                'name' => $name
            )
        );
    }

    public function manageAgreement(App_Controller_Admin_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }

        $aOfferData = Model_DbTable_Offer::getInstance()->getOfferBasicInformationById($iId);
        $aAgreementData = Model_DbTable_Agreement::getInstance()->getAgreementForOffer($iId);
        
    
        
        
        $bAgreementIsSet = !empty($aAgreementData);

        $oForm = new Admin_Form_Agreement();
        $oForm->populateSelect(Model_DbTable_Client::getInstance()->getClients(), 'owner', array('id' => 'id', 'value' => 'client'), true);


        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {
            
            $aFormData = $oForm->getValues(); 
            $aFormData['commission_precentage'] = preg_replace('/,/' , '.' , $aFormData['commission_precentage']); 
            
            
            $bWorkStatus = $bAgreementIsSet ? $this->updateAgreement($iId, $aFormData) : $this->saveAgreement($iId, $aFormData);

            $bWorkStatus ? $oCtrl->successMessage('Umowa dodana') : $oCtrl->errorMessage('Wystąpił problem z dodaniem umowy');
            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'detail')));
        }

        if ($bAgreementIsSet) {
            $oForm->populate($aAgreementData);
        }
        
        $oCtrl->view->headLink()->appendStylesheet('/scripts/select2/select2.css');
        
        $oCtrl->view->headScript()->appendFile('/scripts/jquery.maskedinput.min.js')
                ->appendFile('/scripts/filedrop-min.js')
                 ->appendFile('/scripts/select2/select2.min.js')
                ->appendFile('/scripts/ckeditor/ckeditor.js')
                ->appendScript($this->__setJsInput(array('id' => $aAgreementData['id_offer'],
                            'price' => $aOfferData['price'],
                            'uploadUrl' => $oCtrl->view->url(
                                  array('module' => 'admin',
                                        'controller' => 'agreement',
                                        'action' => 'upload', 
                                        'offer' => $aAgreementData['id_offer'])))))
                ->appendFile('/scripts/manager/admin/offer/agreement.js');

        return $this->set('form', $oForm)
                    ->set('offer', $aOfferData)
                    ->getView();
    }
    
       public function manageDetail(App_Controller_Admin_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }

        $aOfferData = Model_DbTable_Offer::getInstance()->getOfferBasicInformationById($iId);
        $aAgreementData = Model_DbTable_Agreement::getInstance()->getAgreementForOffer($iId);

           $oForm = new Admin_Form_AgreementNotes();

           if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

               $aFormData = $oForm->getValues();
               Model_DbTable_Agreement::getInstance()->update($aFormData, array('id_offer = ?' => $iId));
               $oCtrl->successMessage('Notatki zapisane');
               $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'detail')));
           }
           elseif($aAgreementData)
           {
               $oForm->populate($aAgreementData);
           }


            $aOwners = array();
        if (!empty($aAgreementData['owners'])) 
        {
          $aOwners = Model_DbTable_Client::getInstance()->getClientsById(explode(',', $aAgreementData['owners'])); 
        }
        
     
        return $this->set('agreement', $aAgreementData)
                    ->set('form' , $oForm)
                     ->set('owners', $aOwners)
                    ->set('offer' , $aOfferData)->getView();
    }

    private function saveAgreement($iOfferId, $aAgreementData) {

        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction();

            Model_DbTable_Agreement::getInstance()->addAgreement(array_merge(array('id_offer' => $iOfferId), $aAgreementData));

          
            $aOwners = $this->translateAgreementOwnersToDb($aAgreementData, $iOfferId);
            $aFiles = $this->translateAgreementFilesToDb($aAgreementData, $iOfferId);
            
            foreach($aOwners as $aOwner)
            {
                Model_Logic_Client::getInstance()->reloadClientType($aOwner['id_owner'], Model_DbTable_Client::ACCOUNT_TYPE_OWNER); 
            }
            
            Model_DbTable_AgreementOwners::getInstance()->removeOwnersFromAgreement($iOfferId);
            Model_DbTable_AgreementOwners::getInstance()->addOwnersToAgrement($aOwners);
            Model_DbTable_AgreementFiles::getInstance()->removeFilesFromAgreement($iOfferId);
            Model_DbTable_AgreementFiles::getInstance()->addFilesToAgrement($aFiles);


            Zend_Db_Table::getDefaultAdapter()->commit();
            return true;
        } catch (Exception $e) {
            Zend_Db_Table::getDefaultAdapter()->rollback();
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function updateAgreement($iOfferId, $aAgreementData) {

        try {
            Zend_Db_Table::getDefaultAdapter()->beginTransaction();

            Model_DbTable_Agreement::getInstance()->editAgreement($aAgreementData, $iOfferId);

            $aOwners = $this->translateAgreementOwnersToDb($aAgreementData, $iOfferId);
            $aFiles = $this->translateAgreementFilesToDb($aAgreementData, $iOfferId);
            
            foreach($aOwners as $aOwner)
            {
                Model_Logic_Client::getInstance()->reloadClientType($aOwner['id_owner'], Model_DbTable_Client::ACCOUNT_TYPE_OWNER); 
            }
            
            Model_DbTable_AgreementOwners::getInstance()->removeOwnersFromAgreement($iOfferId);
            Model_DbTable_AgreementOwners::getInstance()->addOwnersToAgrement($aOwners);
            Model_DbTable_AgreementFiles::getInstance()->removeFilesFromAgreement($iOfferId);
            Model_DbTable_AgreementFiles::getInstance()->addFilesToAgrement($aFiles);
            
            
            Zend_Db_Table::getDefaultAdapter()->commit();
            return true;
        } catch (Exception $e) {
            Zend_Db_Table::getDefaultAdapter()->rollback();
            $this->getLog()->log($e, Zend_Log::CRIT);
            return false;
        }
    }

    private function __setJsInput($aData) {
        return sprintf($this->getJsDataTemplate(), Zend_Json::encode($aData));
    }
/**
    private function translateAgreementOwnersToForm($aDbData) {
        $aReturn = '';

        if (!empty($aDbData)) {
            foreach ($aDbData as $aRow) {
                $aReturn[] = $aRow['id_user'];
            }
        }

        return implode(',', $aReturn);
    }
*/
    private function translateAgreementOwnersToDb($aFormData, $iId) {
        $aReturn = array();

        if (isset($aFormData['owners']) && !empty($aFormData['owners'])) {
            $aOwnersIds = explode(',', $aFormData['owners']);
            foreach ($aOwnersIds as $iOwnerId) {
                $aReturn[] = array('id_offer' => $iId, 'id_owner' => $iOwnerId);
            }
        }
        return $aReturn;
    }
    
    
      private function translateAgreementFilesToDb($aFormData, $iId) {
        $aReturn = array();

        if (isset($aFormData['files']) && !empty($aFormData['files'])) {
            $aFileNames = explode(',', $aFormData['files']);
            foreach ($aFileNames as $sName) {
                $aReturn[] = array('id_offer' => $iId, 'filename' => $sName);
            }
        }
        return $aReturn;
    }

}