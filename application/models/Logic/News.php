<?php

/**
 * Model_Logic_Admin
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_News extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_News
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_News
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function manageAdd(App_Controller_Admin_Abstract $oCtrl) {

        $oForm = new Admin_Form_News();

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $aFormData = $oForm->getValues();

            try {
                Zend_Db_Table::getDefaultAdapter()->beginTransaction();
                $iNewsId = Model_DbTable_News::getInstance()->add($aFormData);
                Zend_Db_Table::getDefaultAdapter()->commit();
                $oCtrl->successMessage('News dodany');
            } catch (Exception $e) {
                Zend_Db_Table::getDefaultAdapter()-> rollBack();
                $this->getLog()->log($e, Zend_Log::CRIT);
                $oCtrl->errorMessage('Błąd podczas dodania newsa');
            }

            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
        }
        App_Addons::getInstance()->add('Timepicker', 'ckeditor');
        
        $oCtrl->view->form = $oForm;
    }

    public function manageEdit(App_Controller_Admin_Abstract $oCtrl) {

        if (($iNewsId = $oCtrl->getRequest()->getParam('id')) === null) {
            throw new Exception('Błąd identyfikatora', 500);
        }
        if (($aNews = Model_DbTable_News::getInstance()->getById($iNewsId)) == false) {
            throw new Exception('Brak newsa o podanym identyfikatorze', 404);
        }

        $oForm = new Admin_Form_News();

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $aFormData = $oForm->getValues();


            try {
                Zend_Db_Table::getDefaultAdapter()->beginTransaction();
                Model_DbTable_News::getInstance()->edit($aFormData, $iNewsId);
                Zend_Db_Table::getDefaultAdapter()->commit();

                $oCtrl->successMessage('News zmieniony');
            } catch (Exception $e) {
                $this->getLog()->log($e, Zend_Log::CRIT);
                Zend_Db_Table::getDefaultAdapter()->rollBack();
                $oCtrl->errorMessage('Błąd podczas edycji newsa');
            }

            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
        }
        else
            $oForm->populate($aNews);


        App_Addons::getInstance()->add('Timepicker' , 'ckeditor');
        
        $oCtrl->view->form = $oForm;
    }

    public function manageRemove(App_Controller_Admin_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id')) === null) {
            throw new Exception('Błąd identyfikatora');
        }

        Model_DbTable_News::getInstance()->remove($iId);
        $oCtrl->successMessage('News usunięty');
        $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
    }

    public function manageList(App_Controller_Admin_Abstract $oCtrl) {

        $oPaginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect(Model_DbTable_News::getInstance()->getListQuery()));
        $oPaginator->setItemCountPerPage(self::DEFAULT_ITEM_PER_PAGE)
                ->setCurrentPageNumber($oCtrl->getRequest()->getParam('page', 1));

        $oCtrl->view->paginator = $oPaginator;
    }
    
    
    public function managePageList(App_Controller_Page_Abstract $oCtrl) {

        $oPaginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect(Model_DbTable_News::getInstance()->getPageListQuery()));
        $oPaginator->setItemCountPerPage(4)
                ->setCurrentPageNumber($oCtrl->getRequest()->getParam('page', 1));

        $oCtrl->view->data = $oPaginator;
    }
    
    public function manageDisplay(App_Controller_Page_Abstract $oCtrl)
    {
        
        if (($iNewsId = $oCtrl->getRequest()->getParam('id')) === null) {
            throw new Exception('Błąd identyfikatora', 500);
        }
        if (($aNews = Model_DbTable_News::getInstance()->getById($iNewsId)) == false) {
            throw new Exception('Brak newsa o podanym identyfikatorze', 404);
        }
        
        $oCtrl->view->data = $aNews; 
        
    }

}