<?php

class Model_Logic_Abstract {

    protected $oView;

    const DEFAULT_ITEM_PER_PAGE = 15;

    
    
    public function __construct() {

        $this->oView = new stdClass();
    }

    public function set($sIndex = 'data', $mVal = null) {
        $this->oView->$sIndex = $mVal;
        return $this;
    }

    public function getView() {
        return $this->oView;
    }
    
    protected function getJsDataTemplate()
    {
        return 'var inputData = %s;'; 
    }
    

    /**
     * Alias
     * @return Model_DbTable_Adapter 
     */
    protected function getAdapter() {
        return Model_DbTable_Admin::getInstance()->getAdapter();
    }

    /**
     *
     * @return Zend_Log
     */
    protected function getLog() {
        return Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('log');
    }

    protected function getResourceView() {
        return Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
    }
    /**
     *
     * @return Zend_Cache_Core
     */
    protected function getCache()
    {
        
         return Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('cachemanager')->getCache('soap');
        
        
    }

}

