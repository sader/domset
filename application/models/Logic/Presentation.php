<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Presentation extends Model_Logic_Abstract {

    

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Presentation
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Presentation 
     *    */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

  
    public function manageAddPresentation(App_Controller_Admin_Abstract $oCtrl, $bAjaxed = true ) {

      if (($iOfferId = $oCtrl->getRequest()->getParam('offer')) === null)
            throw new Model_Logic_Exception('Offer ID failed', 500);

        $oForm = new Admin_Form_Presentation();
        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $aData = $oForm->getValues(); 
            $aData['id_offer'] = $iOfferId; 
            
          if( Model_DbTable_Presentation::getInstance()->add($aData))
          {
                          
              Model_Logic_Client::getInstance()->reloadClientType($aData['id_client'], Model_DbTable_Client::ACCOUNT_TYPE_VISITER); 
              $oCtrl->successMessage('Prezentacja dodana poprawnie.'); 
          }
          else
          {
                            $oCtrl->errorMessage('Nieudane dodanie prezentacji.');
          }
        $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
        }
        
        $oCtrl->view->headScript()
                ->appendFile('/scripts/jquery.price_format.1.7.min.js')
                ->appendFile('/scripts/select2/select2.min.js')
                ->appendFile('/scripts/jquery-ui-timepicker-addon.js')
                ->appendFile('/scripts/jquery.maskedinput.min.js')
                ->appendFile('/scripts/manager/admin/presentation/details.js');
        
        
        $oCtrl->view->headLink()->appendStylesheet('/scripts/select2/select2.css');
       

        return $this->set('form', $oForm)->set('offer' , Model_Logic_Offer::getInstance()->getDetailedOffer($iOfferId))->getView();
    }

    public function manageEditPresentation(App_Controller_Admin_Abstract $oCtrl) {

        if (($iPresentationId = $oCtrl->getRequest()->getParam('presentation')) === null)
            throw new Model_Logic_Exception('Offer ID failed', 500);

        $aJob = Model_DbTable_Presentation::getInstance()->getPresentation($iPresentationId); 
        
        $oForm = new Admin_Form_Presentation();
        $oForm->editMode(); 

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            if (Model_DbTable_Presentation::getInstance()->edit($oForm->getValues(), $iPresentationId)) {
                $oCtrl->successMessage('Udana edycja prezentacji.');
            } else {
                $oCtrl->errorMessage('Nieudana edycja prezentacji.');
            }

            $oCtrl->getHelper('Redirector')
                  ->goToUrl($oCtrl->view->url(array('module' => 'admin',
                                                    'controller' => 'presentation',
                                                    'action' => 'list', 
                                                    'offer' => $aJob['id_offer']) , null, true));
        }

        $oForm->populate($aJob);

        $oCtrl->view->headScript()
                ->appendFile('/scripts/jquery.price_format.1.7.min.js')
                ->appendFile('/scripts/select2/select2.min.js')
                ->appendFile('/scripts/jquery-ui-timepicker-addon.js')
                ->appendFile('/scripts/jquery.maskedinput.min.js')
                ->appendFile('/scripts/manager/admin/job/details.js');
        
        
        $oCtrl->view->headLink()->appendStylesheet('/scripts/select2/select2.css');
        
        
          return $this->set('form', $oForm)->set('offer' , Model_Logic_Offer::getInstance()->getDetailedOffer($aJob['id_offer']))->getView();
        
    }

    public function manageList(App_Controller_Admin_Abstract $oCtrl) {

       if (($iOfferId = $oCtrl->getRequest()->getParam('offer')) === null)
            throw new Model_Logic_Exception('Offer ID failed', 500);
        
       $oCtrl->view->headScript()->appendFile('/scripts/manager/admin/job/list.js');

        
        $oGrid = new Admin_Grid_Presentation($iOfferId, 'offer');
        
        
        return $this->set('list', $oGrid->deploy())->set('offer' , Model_Logic_Offer::getInstance()->getDetailedOffer($iOfferId))->getView();
   }
   
   public function manageRemove(App_Controller_Admin_Abstract $oCtrl) {

       if (($iPresentationId = $oCtrl->getRequest()->getParam('presentation')) === null)
            throw new Model_Logic_Exception('Offer ID failed', 500);
        
        
    Model_DbTable_Presentation::getInstance()->delete(array('id = ?' => $iPresentationId));
        
        
        $oCtrl->getHelper('Redirector')->goToUrl($_SERVER['HTTP_REFERER']); 
   }

}