<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Enum {

    private $aMarket = array(
        1 => 'Wtórny',
        2 => 'Pierwotny');
    private $aProvince = array(
        1 => 'warmińsko-mazurskie',
        2 => 'dolnośląskie',
        3 => 'kujawsko-pomorskie',
        4 => 'lubelskie',
        5 => 'lubuskie',
        6 => 'łódzkie',
        7 => 'małopolskie',
        8 => 'mazowieckie',
        9 => 'opolskie',
        10 => 'podkarpackie',
        11 => 'podlaskie',
        12 => 'pomorskie',
        13 => 'śląskie',
        14 => 'świętokrzyskie',
        15 => 'wielkopolskie',
        16 => 'zachodniopomorskie');
    private $aLevels = array(
        1 => 'Jednopoziomowe',
        2 => 'Dwupoziomowe');
    private $aBuildingType = array(
        1 => 'Blok',
        2 => 'Kamienica',
        3 => 'Wieżowiec',
        4 => 'Apartamentowiec',
        5 => 'Dom wielorodzinny',
        6 => 'Inny');
    private $aFlatMaterialType = array(
        1 => 'Wielka płyta',
        2 => 'Cegła',
        3 => 'Pustak',
        4 => 'Beton',
        5 => 'Drewno',
        6 => 'Ytong',
        7 => 'Inny');
    private $aHouseMaterialType = array(
        1 => 'Cegła',
        2 => 'Pustak',
        3 => 'Płyta',
        3 => 'Bloczki',
        4 => 'Drewno',
        5 => 'Silikat',
        6 => 'Mieszany',
        7 => 'Inny');
    private $aOwnership = array(
        1 => 'Pełna własność',
        2 => 'Społdzielcze własnościowe',
        3 => 'Spółdzielcze własnościowe z kw',
        4 => 'Udział',
        5 => 'Inne');
    private $aQuality = array(
        1 => 'Wysoki standard',
        2 => 'Bardzo dobry',
        3 => 'Dobry',
        4 => 'Do odświeżenia',
        5 => 'Do odnowienia',
        6 => 'Do remontu',
        7 => 'Do wykończenia');
    private $aInstallationState = array(
        1 => 'Nowa',
        2 => 'Po wymianie',
        3 => 'Po częściowej wymianie',
        4 => 'Do wymiany');
    private $aWindows = array(
        1 => 'Nowe plastikowe',
        2 => 'Nowe drewniane',
        3 => 'Plastikowe',
        4 => 'Drewniane',
        5 => 'Stare plastikowe',
        6 => 'Stare drewniane',
        7 => 'Aluminiowe');
    private $aKitchen = array(
        1 => 'Kuchnia',
        2 => 'Aneks kuchenny');
    private $aNoise = array(
        1 => 'Ciche',
        2 => 'Umiarkowanie ciche',
        3 => 'Umiarkowanie głośne',
        4 => 'Głośne');
    private $aFlatHeat = array(
        1 => 'Miejskie',
        2 => 'Elektryczne',
        3 => 'Gazowe',
        4 => 'Piec',
        5 => 'Kotłownia',
        6 => 'Kominkowe',
        7 => 'Inne');
    private $aWarehouseHeat = array(
        1 => 'Miejskie',
        2 => 'Elektryczne',
        3 => 'Gazowe',
        4 => 'Piec',
        5 => 'Kotłownia',
        6 => 'Kominkowe',
        7 => 'Inne',
        8 => 'Brak');
    private $aAvailable = array(
        1 => 'Od zaraz',
        2 => 'Za miesiąc',
        3 => 'Za 2 miesiące',
        4 => 'Za 3 miesiące',
        5 => 'Za pół roku');
    private $aFlatAdditionalSell = array(
        1 => 'Wspólnota mieszkaniowa',
        2 => 'Osiedle zamknięte',
        3 => 'Podziemny garaż',
        4 => 'Garaż',
        5 => 'Duży parking',
        6 => 'Piwnica',
        7 => 'Winda',
        8 => 'Mieszkanie umeblowane',
        9 => 'Meble w zabudowie',
        10 => 'Klimatyzacja',
        11 => 'Ogrzewanie podłogowe',
        12 => 'Gaz',
        13 => 'Telefon',
        14 => 'Internet',
        15 => 'Telewizja kablowa',
        16 => 'Kominek',
        17 => 'Ogródek',
        18 => 'Taras',
        19 => 'Strych');
    private $aFlatAdditionalRent = array(
        1 => 'Osiedle zamknięte',
        2 => 'Podziemny garaż',
        3 => 'Garaż',
        4 => 'Duży parking',
        5 => 'Piwnica',
        6 => 'Winda',
        7 => 'Mieszkanie umeblowane',
        8 => 'Klimatyzacja',
        9 => 'Ogrzewanie podłogowe',
        10 => 'Telefon',
        11 => 'Internet',
        12 => 'Telewizja kablowa',
        13 => 'Kominek',
        14 => 'Ogródek',
        15 => 'Taras',
        16 => 'Strych'
    );
    private $aHouseAdditionalSell = array(
        1 => 'Klimatyzacja',
        2 => 'Ogrzewanie podłogowe',
        3 => 'Telefon',
        4 => 'Internet',
        5 => 'Telewizja kablowa',
        6 => 'Kominek',
        7 => 'Ogródek',
        8 => 'Taras',
        9 => 'Strych',
        10 => 'Woda miejska',
        11 => 'Zagospodarowany ogród',
        12 => 'Oczko wodne',
        13 => 'W pobliżu las',
        14 => 'W pobliżu jezioro',
        15 => 'Dostęp do jeziora'
    );
    private $aHouseAdditionalRent = array(
        1 => 'Klimatyzacja',
        2 => 'Ogrzewanie podłogowe',
        3 => 'Telefon',
        4 => 'Internet',
        5 => 'Telewizja kablowa',
        6 => 'Kominek',
        7 => 'Ogródek',
        8 => 'Taras',
        9 => 'Strych',
        10 => 'Woda miejska',
        11 => 'Zagospodarowany ogród',
        12 => 'Oczko wodne',
        13 => 'W pobliżu las',
        14 => 'W pobliżu jeziora',
        15 => 'Dostęp do jeziora',
        16 => 'Dom umeblowany'
    );
    private $aOfferTypes = array(
        1 => 'Mieszkanie na sprzedaż',
        2 => 'Mieszkanie do wynajęcia',
        3 => 'Dom na sprzedaż',
        4 => 'Dom do wynajęcia',
        5 => 'Działka na sprzedaż',
        6 => 'Działka, dzierżawa',
        7 => 'Lokal na sprzedaż',
        8 => 'Lokal do wynajęcia',
        9 => 'Przemysłowe na sprzedaż',
        10 => 'Przemysłowe do wynajęcia'
    );
    private $aHouseType = array(
        1 => 'Wolnostojący',
        2 => 'Segment środkowy',
        3 => 'Segment skrajny',
        4 => 'Bliźniak',
        5 => 'Kamienica',
        6 => 'Dworek',
        7 => 'Letniskowy',
        8 => 'Gospodarstwo'
    );
    private $aRent = array(
        1 => 'Płatny dodatkowo',
        2 => 'W cenie najmu',
        3 => 'Tylko opłaty eksploatacyjne',
        4 => 'Tylko prąd'
    );
    private $aAttic = array(
        1 => 'Użytkowe',
        2 => 'Nieużytkowe',
        3 => 'Brak'
    );
    private $aBasement = array(
        1 => 'Całościowe',
        2 => 'Częściowe',
        3 => 'Brak'
    );
    private $aRoof = array(
        1 => 'Płaski',
        2 => 'Dwuspadowy',
        3 => 'Wielospadowy'
    );
    private $aRoofType = array(
        1 => 'Papa',
        2 => 'Dachówka',
        3 => 'Blachodachówka',
        4 => 'Blacha',
        5 => 'Inne'
    );
    private $aHouseHeat = array(
        1 => 'Gazowe',
        2 => 'Węglowe',
        3 => 'Biomasa',
        4 => 'Pompa ciepła',
        5 => 'Kolektor słoneczny',
        6 => 'Olejowe',
        7 => 'Geotermika',
        8 => 'Elektryczne',
        9 => 'Miejskie',
        10 => 'Kominkowe',
        11 => 'Piece kaflowe',
        12 => 'Inne'
    );
    private $aSewerage = array(
        1 => 'Miejska',
        2 => 'Szambo',
        3 => 'Przydomowa oczyszczalnia ścieków',
        4 => 'Brak'
    );
    private $aFence = array(
        1 => 'Brak',
        2 => 'Siatka',
        3 => 'Murowane',
        4 => 'Drewniane',
        5 => 'Betonowe',
        6 => 'Żywopłot',
        7 => 'Inne'
    );
    private $aGarage = array(
        1 => 'Brak',
        2 => 'W budynku',
        3 => 'Wolnostojący',
        4 => 'Wiata'
    );
    private $aAccess = array(
        1 => 'Utwardzany',
        2 => 'Nieutwardzany',
        3 => 'Asfaltowy',
        4 => 'Droga polna'
    );
    private $aLandType = array(
        1 => 'budowlana',
        2 => 'rolna',
        3 => 'usługowa',
        4 => 'przemysłowa',
        5 => 'inwestycyjna',
        6 => 'rekreacyjna',
        7 => 'leśna',
        8 => 'gospodarstwo',
        9 => 'siedlisko',
        10 => 'rolno-budowlana',
        11 => 'inna'
    );
    private $aShape = array(
        1 => 'Prostokąt',
        2 => 'Kwadrat',
        3 => 'Romb',
        4 => 'Trapez',
        5 => 'Trójkąt',
        6 => 'Inny'
    );
    private $aServices = array(
        1 => 'Kanalizacja',
        2 => 'Szabmo',
        3 => 'Oczyszczalnia',
        4 => 'Wodociąg',
        5 => 'Studnia',
        6 => 'Prąd',
        7 => 'Gaz'
    );
    private $aLocalPurpose = array(
        1 => 'Biuro',
        2 => 'Handel i usługi',
        3 => 'Gastronomia',
        4 => 'Przemysł',
        5 => 'Inne'
    );
    private $aWarehousePurpose = array(
        1 => 'Biurowe',
        2 => 'Magazynowe',
        3 => 'Produkcyjne',
        4 => 'Handlowe',
        5 => 'Inne'
    );
    private $aLocalType = array(
        1 => 'Centrum handlowe',
        2 => 'Biurowiec',
        3 => 'Blok',
        4 => 'Kamienica',
        5 => 'Pawilon',
        6 => 'Kiosk',
        7 => 'Sklep',
        8 => 'Hotel',
        9 => 'Kawiarnia',
        10 => 'Restauracja',
        11 => 'Gabinet',
        12 => 'Oddzielny obiekt',
        13 => 'Inny'
    );
    private $aEntry = array(
        1 => 'Od ulicy',
        2 => 'Z klatki schodowej',
        3 => 'Od podwórza'
    );
    private $aLocalAdditional = array(
        1 => 'Witryna',
        2 => 'Ochrona',
        3 => 'Monitoring',
        4 => 'System alarmowy',
        5 => 'Piwnica',
        6 => 'Zaplecze',
        7 => 'Winda',
        8 => 'Klimatyzacja',
        9 => 'Telefon',
        10 => 'Internet',
        11 => 'Telewizja kablowa',
        12 => 'Duży parking',
        13 => 'Rampa'
    );
    private $aWarehouseAdditional = array(
        1 => 'Ochrona',
        2 => 'Monitoring',
        3 => 'System alarmowy',
        4 => 'Zaplecze socjalne',
        5 => 'Pomieszczenia biurowe',
        6 => 'Domofon',
        7 => 'Telefon',
        8 => 'Internet',
        9 => 'Telewizja kablowa',
        10 => 'Duży parking',
        11 => 'Rampa',
        12 => 'Woda',
        13 => 'Siła',
        14 => 'Kanalizacja',
        15 => 'Klimatyzacja',
        16 => 'Teren ogrodzony'
    );
    private $aLandAdditional = array(
        1 => 'Działka z linią brzegową jeziora',
        2 => 'Działka z dostępem do jeziora',
        3 => 'Działka przy rzece',
        4 => 'Blisko jeziora',
        5 => 'Blisko lasu'
    );
    private $aJobResult = array(
        1 => 'Nowe',
        2 => 'Nieaktualne',
        3 => 'Nie chce',
        4 => 'Zły numer',
        5 => 'Umowa',
        6 => 'Transakcja',
        7 => 'Nie odbiera',
        8 => 'Na razie nie',
        9 => 'Może',
        10 => 'Prawie zdecydowany',
        11 => 'Spotkanie',
        12 => 'Nie dzwonić'
    );
    private $aPresentationStatus = array(
        1 => 'Otwarte',
        2 => 'Anulowane',
        3 => 'Zakończone'
    );
    
    private $aOfferStatuses = array(
        1 => 'Otwarte',
        2 => 'Transakcja',
        3 => 'Anulowane',
        4 => 'Usunięte'
    ); 

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Enum
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Enum
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function getProvinceList() {
        return $this->aProvince;
    }

    public function getProvinceById($iId = null) {
        return ( $iId !== null && isset($this->aProvince[$iId])) ? $this->aProvince[$iId] : null;
    }

    public function getMarketList() {
        return $this->aMarket;
    }

    public function getMarketById($iId = null) {
        return ( $iId !== null && isset($this->aMarket[$iId])) ? $this->aMarket[$iId] : null;
    }

    public function getLevelsList() {
        return $this->aLevels;
    }

    public function getLevelsById($iId = null) {
        return ( $iId !== null && isset($this->aLevels[$iId])) ? $this->aLevels[$iId] : null;
    }

    public function getBuildingTypeList() {
        return $this->aBuildingType;
    }

    public function getBuildingTypeByid($iId = null) {

        return ( $iId !== null && isset($this->aBuildingType[$iId])) ? $this->aBuildingType[$iId] : null;
    }

    public function getFlatMaterialTypeList() {
        return $this->aFlatMaterialType;
    }

    public function getFlatMaterialTypeById($iId = null) {
        return ( $iId !== null && isset($this->aFlatMaterialType[$iId])) ? $this->aFlatMaterialType[$iId] : null;
    }

    public function getHouseMaterialTypeList() {
        return $this->aHouseMaterialType;
    }

    public function getHouseMaterialTypeById($iId = null) {
        return ( $iId !== null && isset($this->aHouseMaterialType[$iId])) ? $this->aHouseMaterialType[$iId] : null;
    }

    public function getOwnershipList() {
        return $this->aOwnership;
    }

    public function getOwnershipById($iId = null) {
        return ( $iId !== null && isset($this->aOwnership[$iId])) ? $this->aOwnership[$iId] : null;
    }

    public function getQualityList() {
        return $this->aQuality;
    }

    public function getQualityById($iId = null) {
        return ( $iId !== null && isset($this->aQuality[$iId])) ? $this->aQuality[$iId] : null;
    }

    public function getInstallationStateList() {
        return $this->aInstallationState;
    }

    public function getInstallationStateById($iId = null) {
        return ( $iId !== null && isset($this->aInstallationState[$iId])) ? $this->aInstallationState[$iId] : null;
    }

    public function getWindowList() {
        return $this->aWindows;
    }

    public function getWindowById($iId = null) {
        return ( $iId !== null && isset($this->aWindows[$iId])) ? $this->aWindows[$iId] : null;
    }

    public function getKitchenTypeList() {
        return $this->aKitchen;
    }

    public function getKitchenById($iId = null) {
        return ( $iId !== null && isset($this->aKitchen[$iId])) ? $this->aKitchen[$iId] : null;
    }

    public function getNoiseList() {
        return $this->aNoise;
    }

    public function getNoiseById($iId = null) {
        return ( $iId !== null && isset($this->aNoise[$iId])) ? $this->aNoise[$iId] : null;
    }

    public function getFlatHeatList() {
        return $this->aFlatHeat;
    }

    public function getFlatHeatById($iId = null) {
        return ( $iId !== null && isset($this->aFlatHeat[$iId])) ? $this->aFlatHeat[$iId] : null;
    }

    public function getWarehouseHeatList() {
        return $this->aWarehouseHeat;
    }

    public function getWarehouseHeatById($iId = null) {
        return ( $iId !== null && isset($this->aWarehouseHeat[$iId])) ? $this->aWarehouseHeat[$iId] : null;
    }

    public function getHouseHeatList() {
        return $this->aHouseHeat;
    }

    public function getHouseHeatById($iId = null) {
        return ( $iId !== null && isset($this->aHouseHeat[$iId])) ? $this->aHouseHeat[$iId] : null;
    }

    public function getAvailableList() {
        return $this->aAvailable;
    }

    public function getAvailableById($iId) {
        return ( $iId !== null && isset($this->aAvailable[$iId])) ? $this->aAvailable[$iId] : null;
    }

    public function getFlatAdditionalSellList() {
        return $this->aFlatAdditionalSell;
    }

    public function getFlatAdditionalSellById($iId) {
        return ( $iId !== null && isset($this->aFlatAdditionalSell[$iId])) ? $this->aFlatAdditionalSell[$iId] : null;
    }

    public function getFlatAdditionalSellByGroupOfId($aIds = array()) {
        return array_values(array_intersect_key($this->aFlatAdditionalSell, array_flip($aIds)));
    }

    public function getFlatAdditionalRentList() {
        return $this->aFlatAdditionalRent;
    }

    public function getFlatAdditionalRentById($iId) {
        return ( $iId !== null && isset($this->aFlatAdditionalRent[$iId])) ? $this->aFlatAdditionalRent[$iId] : null;
    }

    public function getFlatAdditionalRentByGroupOfId($aIds = array()) {
        return array_values(array_intersect_key($this->aFlatAdditionalRent, array_flip($aIds)));
    }

    public function getLandAdditionalList() {
        return $this->aLandAdditional;
    }

    public function getLandAdditionalById($iId) {
        return ( $iId !== null && isset($this->aLandAdditional[$iId])) ? $this->aLandAdditional[$iId] : null;
    }

    public function getlandAdditionalByGroupOfId($aIds = array()) {
        return array_values(array_intersect_key($this->aLandAdditional, array_flip($aIds)));
    }

    public function getHouseAdditionalSellList() {
        return $this->aHouseAdditionalSell;
    }

    public function getHouseAdditionalSellById($iId) {
        return ( $iId !== null && isset($this->aHouseAdditionalSell[$iId])) ? $this->aHouseAdditionalSell[$iId] : null;
    }

    public function getHouseAdditionalSellByGroupOfId($aIds = array()) {
        return array_values(array_intersect_key($this->aHouseAdditionalSell, array_flip($aIds)));
    }

    public function getHouseAdditionalRentList() {
        return $this->aHouseAdditionalRent;
    }

    public function getHouseAdditionalRentById($iId) {
        return ( $iId !== null && isset($this->aHouseAdditionalRent[$iId])) ? $this->aHouseAdditionalRent[$iId] : null;
    }

    public function getHouseAdditionalRentByGroupOfId($aIds = array()) {
        return array_values(array_intersect_key($this->aHouseAdditionalRent, array_flip($aIds)));
    }

    public function getOfferTypesList() {
        return $this->aOfferTypes;
    }

    public function getOfferTypeById($iId) {
        return ( $iId !== null && isset($this->aOfferTypes[$iId])) ? $this->aOfferTypes[$iId] : null;
    }

    public function getHouseTypeList() {
        return $this->aHouseType;
    }

    public function getHouseTypeById($iId = null) {
        return ( $iId !== null && isset($this->aHouseType[$iId])) ? $this->aHouseType[$iId] : null;
    }

    public function getRentList() {
        return $this->aRent;
    }

    public function getRentById($iId = null) {
        return ( $iId !== null && isset($this->aRent[$iId])) ? $this->aRent[$iId] : null;
    }

    public function getAtticList() {
        return $this->aAttic;
    }

    public function getAtticById($iId = null) {
        return ( $iId !== null && isset($this->aAttic[$iId])) ? $this->aAttic[$iId] : null;
    }

    public function getBasementList() {
        return $this->aBasement;
    }

    public function getBasementById($iId = null) {
        return ( $iId !== null && isset($this->aBasement[$iId])) ? $this->aBasement[$iId] : null;
    }

    public function getRoofList() {
        return $this->aRoof;
    }

    public function getRoofById($iId = null) {
        return ( $iId !== null && isset($this->aRoof[$iId])) ? $this->aRoof[$iId] : null;
    }

    public function getRoofTypeList() {
        return $this->aRoofType;
    }

    public function getRoofTypeById($iId = null) {
        return ( $iId !== null && isset($this->aRoofType[$iId])) ? $this->aRoofType[$iId] : null;
    }

    public function getSewerageList() {
        return $this->aSewerage;
    }

    public function getSewerageById($iId = null) {
        return ( $iId !== null && isset($this->aSewerage[$iId])) ? $this->aSewerage[$iId] : null;
    }

    public function getFenceList() {
        return $this->aFence;
    }

    public function getFenceById($iId = null) {
        return ( $iId !== null && isset($this->aFence[$iId])) ? $this->aFence[$iId] : null;
    }

    public function getGarageList() {
        return $this->aGarage;
    }

    public function getGarageById($iId = null) {
        return ( $iId !== null && isset($this->aGarage[$iId])) ? $this->aGarage[$iId] : null;
    }

    public function getAccessList() {
        return $this->aAccess;
    }

    public function getAccessById($iId = null) {
        return ( $iId !== null && isset($this->aAccess[$iId])) ? $this->aAccess[$iId] : null;
    }

    public function getLandTypeList() {
        return $this->aLandType;
    }

    public function getLandTypeById($iId = null) {
        return ( $iId !== null && isset($this->aLandType[$iId])) ? $this->aLandType[$iId] : null;
    }

    public function getShapeList() {
        return $this->aShape;
    }

    public function getShapeById($iId = null) {
        return ( $iId !== null && isset($this->aShape[$iId])) ? $this->aShape[$iId] : null;
    }

    public function getServicesList() {
        return $this->aServices;
    }

    public function getServicesById($iId = null) {
        return ( $iId !== null && isset($this->aServices[$iId])) ? $this->aServices[$iId] : null;
    }
    
        public function getSerwicesByGroupOfId($aIds = array()) {
            
                       
        return array_values(array_intersect_key($this->aServices, array_flip($aIds)));
    }


    public function getLocalPurposeList() {
        return $this->aLocalPurpose;
    }

    public function getLocalPurposeById($iId = null) {
        return ( $iId !== null && isset($this->aLocalPurpose[$iId])) ? $this->aLocalPurpose[$iId] : null;
    }

    public function getWarehousePurposeList() {
        return $this->aWarehousePurpose;
    }

    public function getWarehousePurposeById($iId = null) {
        return ( $iId !== null && isset($this->aWarehousePurpose[$iId])) ? $this->aWarehousePurpose[$iId] : null;
    }

    public function getLocalTypeList() {
        return $this->aLocalType;
    }

    public function getLocalTypeById($iId = null) {
        return ( $iId !== null && isset($this->aLocalType[$iId])) ? $this->aLocalType[$iId] : null;
    }

    public function getEntryList() {
        return $this->aEntry;
    }

    public function getEntryById($iId = null) {
        return ( $iId !== null && isset($this->aEntry[$iId])) ? $this->aEntry[$iId] : null;
    }

    public function getLocalAdditionalList() {
        return $this->aLocalAdditional;
    }

    public function getLocalAdditionalById($iId = null) {
        return ( $iId !== null && isset($this->aLocalAdditional[$iId])) ? $this->aLocalAdditional[$iId] : null;
    }
    
    public function getLocalAdditionalByGroupOfId($aIds = array()) {
        return array_values(array_intersect_key($this->aLocalAdditional, array_flip($aIds)));
    }

    public function getWarehouseAdditionalList() {
        return $this->aWarehouseAdditional;
    }

    public function getWarehouseAdditionalById($iId = null) {
        return ( $iId !== null && isset($this->aWarehouseAdditional[$iId])) ? $this->aWarehouseAdditional[$iId] : null;
    }
    
     public function getWarehouseAdditionalByGroupOfId($aIds = array()) {
        return array_values(array_intersect_key($this->aWarehouseAdditional, array_flip($aIds)));
    }

    public function getJobResultList() {
        return $this->aJobResult;
    }

    public function getJobResultById($iId = null) {
        return ( $iId !== null && isset($this->aJobResult[$iId])) ? $this->aJobResult[$iId] : null;
    }

    public function getPresentationStatusList() {
        return $this->aPresentationStatus;
    }

    public function getPresentationStatustById($iId = null) {
        return ( $iId !== null && isset($this->aPresentationStatus[$iId])) ? $this->aPresentationStatus[$iId] : null;
    }
    
    
    
    public function getOfferStatusesList() {
        return $this->aOfferStatuses;
    }

    public function getOfferStatusestById($iId = null) {
        return ( $iId !== null && isset($this->aOfferStatuses[$iId])) ? $this->aOfferStatuses[$iId] : null;
    }

}