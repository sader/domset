<?php

/**
 * Model_Logic_Admin
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Admin extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Admin
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Admin
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    private $__salt = 'salty44salt';

    public function manageLogin(App_Controller_Admin_Abstract $oCtrl) {


        if (Zend_Auth::getInstance()->hasIdentity()) {


            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('module' => 'admin', 'controller' => 'index', 'action' => 'index')));
        }

        $oForm = new Admin_Form_Login();


        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $oAuth = new Zend_Auth_Adapter_DbTable();
            $oResult = $oAuth->setTableName('admin')
                    ->setIdentityColumn('login')
                    ->setCredentialColumn('password')
                    ->setIdentity($oForm->getValue('login'))
                    ->setCredential($this->__hashPassword($oForm->getValue('password')))
                    ->authenticate();

            if ($oResult->isValid()) {

                Zend_Auth::getInstance()->getStorage()->write($oAuth->getResultRowObject());
                $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array() , 'admin', true));
            }
            else
                $oForm->getElement('login')->addError('Błedne dane');
        }

        return $this->set('form', $oForm)->getView();
    }

    public function manageLogout(App_Controller_Admin_Abstract $oCtrl) {

        if (Zend_Auth::getInstance()->hasIdentity())
            Zend_Auth::getInstance()->clearIdentity();

        $oCtrl->successMessage('Wylogowano');
        $oCtrl->getHelper('Redirector')
                ->goToUrl($oCtrl->view->url(array('module' => 'admin', 'controller' => 'auth', 'action' => 'login')));
    }

    private function __hashPassword($sPass) {
        return md5($sPass . $this->__salt);
    }

    public function manageAdd(App_Controller_Admin_Abstract $oCtrl) {

        $oAccountForm = new Admin_Form_Account();
        $oAccountForm->populateSelect(array(Model_DbTable_Admin::ACCOUNT_TYPE_STANDARD => 'Moderator',
                                            Model_DbTable_Admin::ACCOUNT_TYPE_MASTER => 'Administrator'), 
                                       'type', false, false);
        if ($oCtrl->getRequest()->isPost() && $oAccountForm->isValid($oCtrl->getRequest()->getPost())) {

            try {
                $aValidatedFormData = $oAccountForm->getValues();
                $aValidatedFormData['crypted_password'] = $this->__hashPassword($aValidatedFormData['password']);
                Model_DbTable_Admin::getInstance()->addAccount($aValidatedFormData);
                $oCtrl->successMessage('Administrator dodany');
                $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
            } catch (Exception $e) {
                $this->getLog()->log($e, Zend_Log::CRIT);
                $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji');
            }
        }
        return $this->set('form', $oAccountForm)->getView();
    }

    public function manageEdit(App_Controller_Admin_Abstract $oCtrl) {
        if (($iAccountId = $oCtrl->getRequest()->getParam('id', null)) === null)
            throw new Model_Logic_Exception('Account id failed', 500);

        if (($aData = Model_DbTable_Admin::getInstance()->getAccountById($iAccountId)) == false)
            throw new Model_Logic_Exception('Not Found', 404);

        $oAccountForm = new Admin_Form_Account();
        $oAccountForm->populateSelect(array(Model_DbTable_Admin::ACCOUNT_TYPE_STANDARD => 'Moderator',
                                            Model_DbTable_Admin::ACCOUNT_TYPE_MASTER => 'Administrator'), 
                                       'type', false, false)
                       ->setEditMode($iAccountId, $aData);

        if ($oCtrl->getRequest()->isPost() && $oAccountForm->isValid($oCtrl->getRequest()->getPost())) {
            try {
                Model_DbTable_Admin::getInstance()->editAccount($iAccountId, $oAccountForm->getValues());
                $oCtrl->successMessage('Edycja wpisu zakończona');
            } catch (Exception $e) {
                $this->getLog()->log($e, Zend_Log::CRIT);
                $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji');
            }

            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
        }


        return $this->set('form', $oAccountForm)->getView();
    }

    public function manageList(App_Controller_Admin_Abstract $oCtrl) {

        $oGrid = new Admin_Grid_Admins();

        $oCtrl->view->headScript()->appendFile('/scripts/manager/admin/admin/list.js');

        return $this->set('list', $oGrid->deploy())->getView();
    }

    public function manageRemove(App_Controller_Admin_Abstract $oCtrl) {
        
       if (($iAccountId = $oCtrl->getRequest()->getParam('id', null)) === null)
            throw new Model_Logic_Exception('Account id failed', 500);

        if (($aData = Model_DbTable_Admin::getInstance()->getAccountById($iAccountId)) == false)
            throw new Model_Logic_Exception('Not Found', 404);
          try {
                Model_DbTable_Admin::getInstance()->removeAccount($iAccountId);
                $oCtrl->successMessage('Konto usunięte');
            } catch (Exception $e) {
                $this->getLog()->log($e, Zend_Log::CRIT);
                $oCtrl->errorMessage('Wystąpił błąd podczas próby usunięcia konta');
            }

            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
    
    }
    
    public function manageChangePassword(App_Controller_Admin_Abstract $oCtrl)
    {
        
         if (($iAccountId = $oCtrl->getRequest()->getParam('id', null)) === null)
            throw new Model_Logic_Exception('Account id failed', 500);

        if (($aData = Model_DbTable_Admin::getInstance()->getAccountById($iAccountId)) == false)
            throw new Model_Logic_Exception('Not Found', 404);

        $oAccountForm = new Admin_Form_Account();
        $oAccountForm->setChangePasswordMode(); 

        if ($oCtrl->getRequest()->isPost() && $oAccountForm->isValid($oCtrl->getRequest()->getPost())) {
            try {
                Model_DbTable_Admin::getInstance()->changePasswordToAccount($iAccountId, $this->__hashPassword($oAccountForm->getValue('password')));
                $oCtrl->successMessage('Edycja wpisu zakończona');
            } catch (Exception $e) {
                $this->getLog()->log($e, Zend_Log::CRIT);
                $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji hasła');
            }

            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
        }


        return $this->set('form', $oAccountForm)->getView();
        
        
        
    }

    public function getActiveAdminId() {

        return Zend_Auth::getInstance()->hasIdentity() ? Zend_Auth::getInstance()->getStorage()->read()->id : null;
    }

}