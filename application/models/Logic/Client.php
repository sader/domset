<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Client extends Model_Logic_Abstract {

    
    const CLIENT_PHONE_LENGHT_VALIDATE = 9; 
    
    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Client
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Client
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public static function getClientTypes() {
        return array(
            Model_DbTable_Client::ACCOUNT_TYPE_VISITER => 'Oglądający',
            Model_DbTable_Client::ACCOUNT_TYPE_LOOKER => 'Poszukujący',
            Model_DbTable_Client::ACCOUNT_TYPE_OWNER => 'Właściciel',
            Model_DbTable_Client::ACCOUNT_TYPE_CALLED => 'Wydzwoniony');
    }

    public function manageQuickList(App_Controller_Admin_Abstract $oCtrl) {

        $sData = $oCtrl->getRequest()->getParam('phone', '');
        $sType = $oCtrl->getRequest()->getParam('type', 'list');



        if (mb_strlen($sData) >= 3) {

            $aClients = Model_DbTable_Client::getInstance()->getClientByPhone($sData);

            if (count($aClients) == 1) {
                $oCtrl->getRequest()->setParam('id', $aClients['0']['id']);
                return $this->manageQuickDetail($oCtrl);
            } else {

                return array('status' => true,
                    'content' => $oCtrl->view->partial('_partial/quick/list.phtml',
                            array('clients' => $aClients, 'type' => $sType)));
            }
        }
    }

    public function getClientByPhone($sPhone, $iType) {
        $aClient = Model_DbTable_Client::getInstance()->getClientByPhone($sPhone);

        if (empty($aClient[0])) {
            return Model_DbTable_Client::getInstance()->addClient(array('phone' => $sPhone, 'type' => array($iType)));
        } else {
            Model_Logic_Client::getInstance()->reloadClientType($aClient[0]['id'], $iType);
            return $aClient[0]['id'];
        }
    }

    public function manageQuickDetail(App_Controller_Admin_Abstract $oCtrl) {
        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
                throw new Model_Logic_Exception('Account id failed', 500);



        $aClient['data'] = Model_DbTable_Client::getInstance()->getClientById($iId);


        $aTypes = explode(',', $aClient['data']['type']);

        if (in_array(Model_DbTable_Client::ACCOUNT_TYPE_VISITER, $aTypes)) {
            $aClient['visiter'] = Model_DbTable_Presentation::getInstance()
                    ->getPresentationForClient($iId)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC);
        }

        if (in_array(Model_DbTable_Client::ACCOUNT_TYPE_LOOKER, $aTypes)) {

            $aClient['wanted'] = Model_DbTable_Wanted::getInstance()->getListOfWantedForUser($iId)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC);
        }

        if (in_array(Model_DbTable_Client::ACCOUNT_TYPE_CALLED, $aTypes)) {
            $aClient['jobs'] = Model_DbTable_Job::getInstance()
                    ->getListOfJobsForClient($iId)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC);
        }


        if (in_array(Model_DbTable_Client::ACCOUNT_TYPE_OWNER, $aTypes)) {
            $aClient['owner'] = Model_DbTable_AgreementOwners::getInstance()
                    ->getOffersByAgreementClient($iId)
                    ->query()
                    ->fetchAll(Zend_Db::FETCH_ASSOC);
        }

        return array('status' => true,
            'content' => $oCtrl->view->partial('_partial/quick/detail.phtml',
                    array('client' => $aClient)));
    }

    public function manageAddClient(App_Controller_Admin_Abstract $oCtrl,
            $bAjax = false) {


        $iType = $oCtrl->getRequest()->getParam('prepare', null);
        $sResponse = $oCtrl->getRequest()->getParam('response', null);

        $oForm = new Admin_Form_Client();

        if ($oCtrl->getRequest()->isPost()) {
            if ($oForm->isValid($oCtrl->getRequest()->getPost())) {

                $aValidData = $oForm->getValues();

                $aResponse['id'] = Model_DbTable_Client::getInstance()->addClient(array_merge($aValidData));


                switch ($sResponse) {
                    case 'phone':
                        $aResponse['value'] = $aValidData['phone'];

                        break;

                    default:
                        $aResponse['value'] = sprintf('%s - %s %s',
                                $aValidData['phone'], $aValidData['surname'],
                                $aValidData['name']);
                        break;
                }

                return ($bAjax) ? array('status' => true, 'content' => $aResponse) : $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
            }
            else return array('status' => false, 'content' => $oForm->render());
        }
        else {
            if ($iType !== null)
                    $oForm->populate(array('type' => array($iType)));

            return array('status' => true, 'content' => $oForm->render());
        }
    }

    public function manageDetail(App_Controller_Admin_Abstract $oCtrl) {
        if (($iId = $oCtrl->getRequest()->getParam('client', null)) === null)
                throw new Model_Logic_Exception('Account id failed', 500);

        $aClient = Model_DbTable_Client::getInstance()->getClientById($iId);
        $aTypes = explode(',', $aClient['type']);

        if (in_array(Model_DbTable_Client::ACCOUNT_TYPE_VISITER, $aTypes)) {
            $oVisitGrid = new Admin_Grid_Presentation($iId, 'client');
            $oCtrl->view->visits = $oVisitGrid->deploy();
        }

        if (in_array(Model_DbTable_Client::ACCOUNT_TYPE_LOOKER, $aTypes)) {

            $oWantedGrid = new Admin_Grid_Wanted($iId);
            $oCtrl->view->wanted = $oWantedGrid->deploy();
        }

        if (in_array(Model_DbTable_Client::ACCOUNT_TYPE_CALLED, $aTypes)) {
            $oJobsGrid = new Admin_Grid_ClientJob($iId);
            $oCtrl->view->jobs = $oJobsGrid->deploy();
        }


        if (in_array(Model_DbTable_Client::ACCOUNT_TYPE_OWNER, $aTypes)) {
            $oOfferGrid = new Admin_Grid_Offers(Admin_Grid_Offers::OFFERS_FOR_AGREEMENT_CLIENT,
                    array('client' => $iId));
            $oCtrl->view->agreement = $oOfferGrid->deploy();
        }
    }

    public function manageEditClient(App_Controller_Admin_Abstract $oCtrl) {
        if (($iId = $oCtrl->getRequest()->getParam('client', null)) === null)
                throw new Model_Logic_Exception('Account id failed', 500);

        if (($aData = Model_DbTable_Client::getInstance()->getClientById($iId)) == false)
                throw new Model_Logic_Exception('Not Found', 404);

        $oForm = new Admin_Form_Client();
        $oForm->editMode();
        $aData['type'] = explode(',', $aData['type']);
        $oForm->populate($aData);

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            try {
                Model_DbTable_Client::getInstance()->editClient($oForm->getValues(),
                        $iId);
                $oCtrl->successMessage('Edycja klienta zakończona');
                $oCtrl->getHelper('Redirector')
                        ->goToUrl($oCtrl->view->url(array('module' => 'admin',
                                    'controller' => 'client',
                                    'action' => 'list'), null, true));
            } catch (Exception $e) {
                $this->getLog()->log($e, Zend_Log::CRIT);
                $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji');
            }
        }
        return $this->set('form', $oForm)->getView();
    }

    public function manageListClients(App_Controller_Admin_Abstract $oCtrl) {
        $oGrid = new Admin_Grid_Clients();
        return $this->set('list', $oGrid->deploy())->getView();
    }

    /**
     * Aktualizacja typów klienta
     * @param int $iClient
     * @param int $iNewType
     * @return boolean
     */
    public function reloadClientType($iClient, $iNewType) {
        $aClient = Model_DbTable_Client::getInstance()->getClientById($iClient);

        if (empty($aClient['type'])) {
            Model_DbTable_Client::getInstance()->editClient(array('type' => array($iNewType)),
                    $iClient);
            return true;
        }

        $aTypes = explode(',', $aClient['type']);
        if (in_array($iNewType, $aTypes)) return true;

        $aTypes[] = $iNewType;
        Model_DbTable_Client::getInstance()->editClient(array('type' => $aTypes), $iClient);
        return true;
    }

}
