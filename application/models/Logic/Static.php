<?php

/**
 * Model_Logic_Admin
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Static extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Static
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Static
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function manageAdd(App_Controller_Admin_Abstract $oCtrl) {

        $oForm = new Admin_Form_Static();

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $aFormData = $oForm->getValues();


            try {
                Zend_Db_Table::getDefaultAdapter()->beginTransaction();
                $iStaticId = Model_DbTable_Static::getInstance()->add($aFormData);
                Zend_Db_Table::getDefaultAdapter()->commit();
                $oCtrl->successMessage('Strona ' . $aFormData['name'] . ' dodana');
            } catch (Exception $e) {
                Zend_Db_Table::getDefaultAdapter()->rollBack();
                $this->getLog()->log($e, Zend_Log::CRIT);
                $oCtrl->errorMessage('Błąd podczas dodania strony');
            }

            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('module' => 'admin',
                        'controller' => 'static',
                        'action' => 'list'), null, true));
        }


        $oCtrl->view->headScript()
                ->appendFile('/scripts/ckeditor/ckeditor.js');

        $oCtrl->view->form = $oForm;
    }

    public function manageEdit(App_Controller_Admin_Abstract $oCtrl) {

        if (($iStaticId = $oCtrl->getRequest()->getParam('id')) === null) {
            throw new Exception('Błąd identyfikatora', 500);
        }
        if (($aStatic = Model_DbTable_Static::getInstance()->getById($iStaticId)) == false) {
            throw new Exception('Brak Staticu o podanym identyfikatorze', 404);
        }

        $oForm = new Admin_Form_Static();
        $oForm->editMode($iStaticId);
        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            $aFormData = $oForm->getValues();



            try {
                Zend_Db_Table::getDefaultAdapter()->beginTransaction();
                Model_DbTable_Static::getInstance()->edit($aFormData, $iStaticId);
                Zend_Db_Table::getDefaultAdapter()->commit();

                $oCtrl->successMessage('Strona ' . $aFormData['name'] . ' zmieniony');
            } catch (Exception $e) {
                $this->getLog()->log($e, Zend_Log::CRIT);
                Zend_Db_Table::getDefaultAdapter()->rollBack();
                $oCtrl->errorMessage('Błąd podczas edycji strony');
            }

            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('module' => 'admin',
                        'controller' => 'static',
                        'action' => 'list'), null, true));
        }
        else
            $oForm->populate($aStatic);


       $oCtrl->view->headScript()
                ->appendFile('/scripts/ckeditor/ckeditor.js');
        $oCtrl->view->form = $oForm;
    }

    public function manageRemove(App_Controller_Admin_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id')) === null) {
            throw new Exception('Błąd identyfikatora');
        }

        Model_DbTable_Static::getInstance()->remove($iId);
        $oCtrl->successMessage('Strona usunięta');
        $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('module' => 'admin',
                    'controller' => 'static',
                    'action' => 'list'), null, true));
    }

    public function manageList(App_Controller_Admin_Abstract $oCtrl) {


        $oPaginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect(Model_DbTable_Static::getInstance()->getListQuery()));
        $oPaginator->setItemCountPerPage(self::DEFAULT_ITEM_PER_PAGE)
                ->setCurrentPageNumber($oCtrl->getRequest()->getParam('page', 1));

        $oCtrl->view->paginator = $oPaginator;
    }

    public function manageDisplay(App_Controller_Page_Abstract $oCtrl) {
        if (($sUrl = $oCtrl->getRequest()->getParam('url')) === null) {
            throw new Exception('Błąd adresu');
        }

        if (($aStatic = Model_DbTable_Static::getInstance()->getByUrl($sUrl)) == false) {
            throw new Exception('Not Found', 404);
        }
        $oCtrl->view->content = $aStatic['content'];
        $oCtrl->view->name = $aStatic['head'];

        $oForm = new Page_Form_Contact();

        if ($oCtrl->getRequest()->isPost()) {
            if ($oForm->isValid($oCtrl->getRequest()->getPost())) {

                Model_Logic_Mail::getInstance()->sendContactEmail($oForm->getValues());
                $oCtrl->view->success = true;
                $oForm->reset();
            }
        }

        $oCtrl->view->form = $oForm; 
    }

}