<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Wanted_Warehouse extends Model_Logic_Wanted_Abstract implements Model_Logic_Wanted_Interface
{
    const ID = 9; 
    
       public function __construct() {
        parent::__construct();
        
        $this->oTableModel = $this->getTableModel(); 
    }
    
     public function getFormElements()
        {
            $oForm = new Admin_Form_Wanted_Warehouse(); 
            return $oForm->getElements(); 
        }
        
        public function getId() {
        return self::ID;
    }

    public function getTableModel() {
        return Model_DbTable_Wanted_Warehouse::getInstance();
    }
    
     public function getFilter()
    {
        return new Admin_Form_Offer_Warehouse_Filter(self::ID); 
    }
}
