<?php

class Model_Logic_Wanted_Abstract extends Model_Logic_Abstract {

    protected $oTableModel;
    protected $sDetailViewTemplate; 

    public function add($aValidatedData) {
        $this->oTableModel->add($aValidatedData);
    }
    
    public function edit($aValidatedData, $iId) {
        $this->oTableModel->edit($aValidatedData, $iId);
    }

    public function remove($iWantedId) {
        $this->oTableModel->remove($iWantedId);
    }
    
       public function get($iWantedId) {
        $this->oTableModel->get($iWantedId);
    }
    
    public function getWantedForOffers($aOfferData)
    {
        return $this->oTableModel->getWantedByOfferData($aOfferData); 
    }
  
}

