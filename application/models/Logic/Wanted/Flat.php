<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Wanted_Flat extends Model_Logic_Wanted_Abstract implements Model_Logic_Wanted_Interface {
    
    
    const ID = 1;
    
    public function getId()
    {
        return self::ID; 
    }
    
    public function __construct() {
        parent::__construct();
        
        $this->oTableModel = $this->getTableModel();
        $this->sDetailViewTemplate = 'offer_flat_sell.phtml'; 
        
    }

    public function getTableModel()
    {
        return Model_DbTable_Wanted_Flat::getInstance(); 
    }


    public function getFormElements() {
        $oForm = new Admin_Form_Wanted_Flat();
        return $oForm->getElements();
    }

    public function getFilterQuery($aFilters)
    {
        return $this->getTableModel()->getFilterQuery($aFilters); 
    }
   

}
