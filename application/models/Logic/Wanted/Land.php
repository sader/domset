<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Wanted_Land extends Model_Logic_Wanted_Abstract implements Model_Logic_Wanted_Interface
{
    const ID = 5; 
    
       public function __construct() {
        parent::__construct();
        
        $this->oTableModel = $this->getTableModel(); 
        $this->sDetailViewTemplate = 'offer_land_sell.phtml'; 
        
    }
    
      public function getFormElements()
        {
            $oForm = new Admin_Form_Wanted_Land(); 
            return $oForm->getElements(); 
        }
   
    public function getId() {
        return self::ID;
    }

    public function getTableModel() {
        return Model_DbTable_Wanted_Land::getInstance();
    }
    
     public function getFilter()
    {
        return new Admin_Form_Offer_Land_Filter(self::ID); 
    }
        
}
