<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Wanted_House extends Model_Logic_Wanted_Abstract implements Model_Logic_Wanted_Interface {
   
    const ID = 3;

       public function __construct() {
        parent::__construct();
        
        $this->oTableModel = $this->getTableModel(); 
        $this->sDetailViewTemplate = 'offer_house_sell.phtml'; 
        
    }
    
    
    public function getFormElements() {
        $oForm = new Admin_Form_Wanted_House();
        return $oForm->getElements();
    }
    
      public function getId()
    {
        return self::ID; 
    }
    
    public function getTableModel()
    {
         return Model_DbTable_Wanted_House::getInstance(); 
    }
    
     public function getFilter()
    {
        return new Admin_Form_Offer_House_Filter(self::ID); 
    }
}
