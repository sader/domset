<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Wanted_Local extends Model_Logic_Wanted_Abstract implements Model_Logic_Wanted_Interface  {
    
    const ID = 4;

       public function __construct() {
        parent::__construct();
        
        $this->oTableModel = $this->getTableModel(); 
        
    }
    
    public function getFormElements() {
        $oForm = new Admin_Form_Wanted_Local();
        return $oForm->getElements();
    }

    public function getId() {
        return self::ID;
    }

    public function getTableModel() {
        return Model_DbTable_Wanted_Local::getInstance();
    }
    
     

}

