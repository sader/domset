<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Job extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Job
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Job 
     *    */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function manageAddJob(App_Controller_Admin_Abstract $oCtrl,
            $bAjaxed = true) {



        $oForm = new Admin_Form_Job();


        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {


            $aData = $oForm->getValues();
            
            $aData['id_client'] = Model_Logic_Client::getInstance()->getClientByPhone($aData['client_phone'], 
                                                                                      Model_DbTable_Client::ACCOUNT_TYPE_CALLED); 
            unset($aData['client_phone']); 
            
            if (Model_DbTable_Job::getInstance()->add($aData)) {
                $oCtrl->successMessage('Oferta dodana poprawnie.');
            } else {

                $oCtrl->errorMessage('Nieudane dodanie oferty.');
            }


            $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
        }


        $oCtrl->view->headScript()
                ->appendFile('/scripts/jquery.price_format.1.7.min.js')
                ->appendFile('/scripts/select2/select2.min.js')
                ->appendFile('/scripts/jquery-ui-timepicker-addon.js')
                ->appendFile('/scripts/jquery.maskedinput.min.js')
                ->appendFile('/scripts/manager/admin/job/details.js');


        $oCtrl->view->headLink()->appendStylesheet('/scripts/select2/select2.css');


        return $this->set('form', $oForm)->getView();
    }

    public function manageEditJob(App_Controller_Admin_Abstract $oCtrl) {

        if (($iJobId = $oCtrl->getRequest()->getParam('id')) === null)
                throw new Model_Logic_Exception('Offer ID failed', 500);

        $aJob = Model_DbTable_Job::getInstance()->getJob($iJobId);

        $oCtrl->view->user = Model_DbTable_Client::getInstance()->getClientById($aJob['id_client']);
        $oCtrl->view->job = $aJob;
        

        $oForm = new Admin_Form_Job();
        $oForm->editMode();

        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            if (Model_DbTable_Job::getInstance()->edit($oForm->getValues(),
                            $iJobId)) {
                $oCtrl->successMessage('Udana edycja zadania.');
            } else {
                $oCtrl->errorMessage('Nieudana edycja zadania.');
            }

            $oCtrl->getHelper('Redirector')
                  ->goToUrl($oCtrl->view->url(array('module'  => 'admin',
                                                    'controller' => 'job', 
                                                    'action' => 'list'), null, true));
        }

        $oForm->populate($aJob);

        $oCtrl->view->headScript()
                ->appendFile('/scripts/jquery.price_format.1.7.min.js')
                ->appendFile('/scripts/select2/select2.min.js')
                ->appendFile('/scripts/jquery-ui-timepicker-addon.js')
                ->appendFile('/scripts/jquery.maskedinput.min.js')
                ->appendFile('/scripts/manager/admin/job/details.js');


        $oCtrl->view->headLink()->appendStylesheet('/scripts/select2/select2.css');

        return $this->set('form', $oForm)->getView();
    }

    public function manageList(App_Controller_Admin_Abstract $oCtrl) {

        $sData = $oCtrl->getRequest()->getParam('ndate', date('Y-m-d'));

        $oCtrl->view->headScript()->appendFile('/scripts/manager/admin/job/list.js');


        $oGridFull = new Admin_Grid_Job();
        $oGridDaily = new Admin_Grid_Job($sData);

        return $this->set('full', $oGridFull->deploy())
                    ->set('daily',$oGridDaily->deploy())
                    ->set('date' , $sData)
                    ->getView();
    }

}