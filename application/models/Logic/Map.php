<?php

/**
 * Model_Logic_Map
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Map extends Model_Logic_Abstract {

    private $sApiKey = 'AIzaSyDy0CKx15WJ7PlB9_7RNSZviGTFAlMNNdo';

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Map
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Map
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function manageMap(App_Controller_Admin_Abstract $oCtrl) {
        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }

        $oMapForm = new Admin_Form_Map();

        if ($oCtrl->getRequest()->isPost() && $oMapForm->isValid($oCtrl->getRequest()->getPost())) {
            Model_DbTable_Offer::getInstance()->setMapCoordinates($iId, $oMapForm->getValues());
        }

        $aOfferData = Model_DbTable_Offer::getInstance()->getOfferBasicInformationById($iId);

        if ($this->__locationWasDeclaredForOffer($aOfferData)) {
            $oMapForm->populate(array('lat' => $aOfferData['lat'], 'lng' => $aOfferData['lng']));
        }

        $oCtrl->view->headScript()->appendFile('https://maps.googleapis.com/maps/api/js?sensor=false&key=' . $this->sApiKey)
                ->appendScript($this->__setJsInput($aOfferData))
                ->appendFile('/scripts/manager/admin/offer/map.js');

        return $this->set('mapForm', $oMapForm)
                        ->set('offer', $aOfferData)
                        ->set('id', $iId)->getView();
    }

    private function __locationWasDeclaredForOffer($aData) {

        return (isset($aData['lat']) &&
                !empty($aData['lat']) &&
                isset($aData['lng']) &&
                !empty($aData['lng']));
    }

    private function __setJsInput($aData) {
        $aData = array(
            'defaultLocation' => 'Polska ' . Admin_Form_Offer::CITY_DEFAULT_VALUE,
            'address' => implode(' ', array('Polska', $aData['province'], $aData['community'], $aData['city'], $aData['street']))
        );

        return sprintf($this->getJsDataTemplate(), Zend_Json::encode($aData));
    }

}