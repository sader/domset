<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Category extends Model_Logic_Abstract {
    
    
    const NO_PROVISION = 'Bez prowizji';
    
    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Category
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Category
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public function manageAddCategory(App_Controller_Admin_Abstract $oCtrl) {
        $oForm = new Admin_Form_Category();


        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            try {
                Model_DbTable_Category::getInstance()->addCategory($oForm->getValues());
                $oCtrl->successMessage('Kategoria została dodana');
                $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
            } catch (Exception $e) {
                $this->getLog()->log($e, Zend_Log::CRIT);
                $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji');
            }
        }
        return $this->set('form', $oForm)->getView();
    }

    public function manageEditCategory(App_Controller_Admin_Abstract $oCtrl) {
        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
                throw new Model_Logic_Exception('Account id failed', 500);

        if (($aData = Model_DbTable_Category::getInstance()->getCategoryById($iId)) == false)
                throw new Model_Logic_Exception('Not Found', 404);

        $oForm = new Admin_Form_Category();
        $oForm->editMode();


        if ($oCtrl->getRequest()->isPost() && $oForm->isValid($oCtrl->getRequest()->getPost())) {

            try {
                Model_DbTable_Category::getInstance()->editCategory($oForm->getValues(),
                        $iId);
                $oCtrl->successMessage('Kategoria została zedytowana');
                $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('action' => 'list')));
            } catch (Exception $e) {
                $this->getLog()->log($e, Zend_Log::CRIT);
                $oCtrl->errorMessage('Wystąpił błąd podczas próby edycji');
            }
        }

        $oForm->populate($aData);

        return $this->set('form', $oForm)->getView();
    }

    public function manageListCategory(App_Controller_Admin_Abstract $oCtrl) {

        $oGrid = new Admin_Grid_Category();

        return $this->set('list', $oGrid->deploy())->getView();
    }

    public function manageRemoveCategory(App_Controller_Admin_Abstract $oCtrl) {

        if (($iId = $oCtrl->getRequest()->getParam('id', null)) === null)
                throw new Model_Logic_Exception('Account id failed', 500);

        if (($aData = Model_DbTable_Category::getInstance()->getCategoryById($iId)) == false)
                throw new Model_Logic_Exception('Not Found', 404);
        try {
            Model_DbTable_Category::getInstance()->removeCategory($iId);
            $oCtrl->successMessage('Kategoria usunięta');
        } catch (Exception $e) {
            $this->getLog()->log($e, Zend_Log::CRIT);
            $oCtrl->errorMessage('Wystąpił błąd podczas próby usunięcia kategorii');
        }

        $oCtrl->getHelper('Redirector')->goToUrl($oCtrl->view->url(array('module' => 'admin', 'controller' => 'category', 'action' => 'list'),
                        null, true));
    }

    public function translateCategoriesToDb($aData) {

        $aReturn = array();
        if (!empty($aData['categories'])) {
            foreach ($aData['categories'] as $iCategory) {

                $aReturn[] = $this->isCategoryDeclaredWithExtraData($aData,
                                $iCategory) ?
                        array('id_category' => $iCategory, 'data' => $aData[$this->getExtraDataField($iCategory)]) :
                        array('id_category' => $iCategory, 'data' => null);
            }
        }

        return $aReturn;
    }

    public function translateCategoriesToForm($aData) {

        $aReturn = array();
        foreach ($aData as $aRow) {

            $aReturn['categories'][] = $aRow['id_category'];

            if ($aRow['data'] !== null)
                    $aReturn[$this->getExtraDataField($aRow['id_category'])] = $aRow['data'];
        }


        return $aReturn;
    }

    private function isCategoryDeclaredWithExtraData($aData, $iCategory) {

        return (isset($aData[$this->getExtraDataField($iCategory)]) && !empty($aData[$this->getExtraDataField($iCategory)]));
    }

    private function getExtraDataField($iId) {

        return 'additional_' . $iId;
    }

}