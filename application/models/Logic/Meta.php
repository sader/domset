<?php

/**
 * Model_Logic_Admin
 *
 * @author Krzysztof Janicki
 */
class Model_Logic_Meta extends Model_Logic_Abstract {

    /**
     * Instancja klasy.
     * 
     * @var Model_Logic_Meta
     */
    static private $_oInstance;

    /**
     * Zwraca instancje klasy.
     * 
     * @return Model_Logic_Meta
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    public static function getListMeta($sMeta, $iType) {

        $aMeta = array(
        Model_Logic_Offer_Flat_Sell::ID => array(
        'title' => 'Mieszkania na sprzedaż w Ełku i okolicach. Nieruchomości mieszkania na Mazurach.',
        'keywords' => 'Mieszkania do wynajęcia w Ełku. Najlepsze ceny na rynku. Mieszkania na Mazurach, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.',
        'description' => 'Mieszkania do wynajęcia w Ełku. Najlepsze ceny na rynku. Mieszkania na Mazurach, w okolicach Ełku: Prostki, Grajewo,  Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.'
        ),
        Model_Logic_Offer_Flat_Rent::ID => array(
        'title' => 'Mieszkania do wynajęcia w Ełku. Wynajm mieszkań Ełk.',
        'keywords' => 'Mieszkania do wynajęcia w Ełku. Najlepsze ceny na rynku. Mieszkania na Mazurach, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.',
        'description' => 'Mieszkania do wynajęcia w Ełku. Najlepsze ceny na rynku. Mieszkania na Mazurach, w okolicach Ełku: Prostki, Grajewo,  Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.'
        ),
        Model_Logic_Offer_House_Sell::ID => array(
        'title' => 'Domy na sprzedaż w Ełku i okolicach. Sprzedam dom na mazurach.',
        'keywords' => 'Domy na sprzedaż w Ełku. Domy nowe oraz z rynku wtórnego. Najlepsze ceny na rynku. Domy na Mazurach nad jeziorem, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.',
        'description' => 'Domy na sprzedaż w Ełku. Domy nowe oraz z rynku wtórnego. Najlepsze ceny na rynku. Domy na Mazurach nad jeziorem, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.'
        ),
        Model_Logic_Offer_House_Rent::ID => array(
        'title' => 'Domy do wynajęcia w Ełku i okolicach. Dom do wynajęcia na Mazurach.',
        'keywords' => 'Dom do wynajęcia w Ełku. Domy na  na Mazurach nad jeziorem, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.',
        'description' => 'Dom do wynajęcia w Ełku. Domy na  na Mazurach nad jeziorem, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.'
        ),
        Model_Logic_Offer_Local_Sell::ID => array(
        'title' => 'Lokale na sprzedaż w Ełku oraz okolicach. Lokale użytkowe, handlowe i biurowe.',
        'keywords' => 'Lokale na sprzedaż w Ełku. Lokale handlowe, usługowe i biurowe na Mazurach w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.',
        'description' => 'Lokale na sprzedaż w Ełku. Lokale handlowe, usługowe i biurowe na Mazurach w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.'
        ),
        Model_Logic_Offer_Local_Rent::ID => array(
        'title' => 'Lokale na wynajem w Ełku oraz okolicach. Lokale użytkowe, handlowe i biurowe.',
        'keywords' => 'Lokale do wynajęcia w Ełku. Lokale handlowe, usługowe i biurowe na Mazurach, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.',
        'description' => 'Lokale do wynajęcia w Ełku. Lokale handlowe, usługowe i biurowe na Mazurach, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.'
        ),
        Model_Logic_Offer_Land_Sell::ID => array(
        'title' => 'Działki na Mazurach na sprzedaż. Działki w Ełku i okolicach. Działki na jeziorem.',
        'keywords' => 'Działki na sprzedaż w Ełku. Działki z dostępem do jeziora. Działki na Mazurach nad jeziorem, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.',
        'description' => 'Działki na sprzedaż w Ełku. Działki z dostępem do jeziora. Działki na Mazurach nad jeziorem, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.'
        ),
        Model_Logic_Offer_Land_Rent::ID => array(
        'title' => 'Najem, dzierżawa działek - Najmę działkę na Mazurach, Ełk oraz okolice.',
        'keywords' => 'Działki w Ełku. Działki z dostępem do jeziora. Działki na Mazurach nad jeziorem, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.',
        'description' => 'Działki w Ełku. Działki z dostępem do jeziora. Działki na Mazurach nad jeziorem, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.'
        ),
        Model_Logic_Offer_Warehouse_Sell::ID => array(
        'title' => 'Obiekty przemysłowe na sprzedaż w Ełku i okolicach. Obiekty na Mazurach.',
        'keywords' => 'Obiekty przemysłowe na sprzedaż w Ełku. Obiekty przemysłowe na Mazurach, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.',
        'description' => 'Obiekty przemysłowe na sprzedaż w Ełku. Obiekty przemysłowe na Mazurach, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.'
        ),
        Model_Logic_Offer_Warehouse_Rent::ID => array(
        'title' => 'Obiekty przemysłowe do wynajęcia w Ełku i okolicach na Mazurach.',
        'keywords' => 'Obiekty przemysłowe do wynajęcia w Ełku. Obiekty przemysłowe na Mazurach, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.',
        'description' => 'Obiekty przemysłowe do wynajęcia w Ełku. Obiekty przemysłowe na Mazurach, w okolicach Ełku: Prostki, Grajewo, Stare Juchy, Wydminy, Woszczele, Nowa Wieś Ełcka, Barany, Chruściele.'
        )

        );
        return (isset($aMeta[$iType][$sMeta])) ? $aMeta[$iType][$sMeta] : ''; 
    }
    
     public static function getDetailMeta($sMeta) {

        $aMeta = array(
        'title' => '%s - oferta nr %s. DOMSET.PL – nieruchomości na sprzedaż na Mazurach, Ełk i okolice',
        'keywords' => '%s - oferta nr %s. DOMSET.PL – nieruchomości na sprzedaż na Mazurach, Ełk i okolice Prostki, Grajewo, Stare Juchy, Wydminy, Chruściele, Mrozy Wielkie, Szeligi, Woszczele, Siedliska.',
        'description' => '%s- oferta nr %s. DOMSET.PL – nieruchomości na sprzedaż na Mazurach, Ełk i okolice Prostki, Grajewo, Stare Juchy, Wydminy, Chruściele, Mrozy Wielkie, Szeligi, Woszczele, Siedliska.'
        );
        
        return (isset($aMeta[$sMeta])) ? $aMeta[$sMeta] : ''; 
        
    }

}