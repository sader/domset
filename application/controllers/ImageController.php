<?php

class ImageController extends Zend_Controller_Action {

    public function imageAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $oFileAdapter = Model_Logic_Gallery::getInstance()->displayImage($this);

                $this->getResponse()->setBody($oFileAdapter->output(Model_Logic_Gallery::IMAGE_EXTENSTION));
    }

}

