<?php

/**
 * Klasa matka dla formularzy
 */
abstract class Grid_Abstract {

    protected $oGrid;
    protected $oSmallInputFilterWidth = '30px';

    public function __construct() {
        // Bvb_Grid_Deploy_JqGrid::$defaultJqGridLibPath = '/scripts/jqgrid';
       
        
        
         $this->oGrid = Bvb_Grid::factory('Table', array(), 'tablegrid');
        Bvb_Grid::useModRewrite(false);
        $this->oGrid->setImagesUrl('/images/');
        // $this->oGrid->setAjax('ajaxed');
        $this->oGrid->setDeployOption('enableUnmodifiedFieldPlaceholders', 1);
        $this->oGrid->setExport(array());
        
        $this->oGrid->setCharEncoding('utf8');
        
    }

    public function deploy() {
        return $this->oGrid->deploy();
    }

}

