<?php

class Page_Form_Contact extends Form_Abstract {

    public function __construct() {
        parent::__construct();

        $oElem = new Zend_Form_Element_Text('name');
        $oElem->setLabel('Imię i nazwisko')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setDecorators(array('Label', 'ViewHelper', 'Errors'));
        $this->addElement($oElem);

        
        $oElem = new Zend_Form_Element_Text('phone');
        $oElem->setLabel('Telefon *')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setRequired(true)
                ->addValidator('NotEmpty' , true, array('messages' => 'Podaj numer telefonu'))
                ->setDecorators(array('Label', 'ViewHelper', 'Errors'));
        $this->addElement($oElem);
        
        
        $oElem = new Zend_Form_Element_Text('email');
        $oElem->setLabel('Email *')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setRequired(true)
                ->addValidator('CorrectEmail' , true, array('messages' => 'Niepoprawny format emaila'))
                ->addValidator('NotEmpty' , true, array('messages' => 'Podaj email'))
                ->setDecorators(array('Label', 'ViewHelper', 'Errors'));
        $this->addElement($oElem);
        
        $oElem = new Zend_Form_Element_Textarea('body');
        $oElem->setLabel('Treść wiadomości')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setDecorators(array('Label', 'ViewHelper'));
        $this->addElement($oElem);


         $oElem = new Zend_Form_Element_Hidden('url');
         $oElem  ->addFilter('StringTrim')
                 ->setValue('http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'])
                ->addFilter('HtmlSpecialchars')
                ->setDecorators(array('ViewHelper'));

          $this->addElement($oElem);

         $oRecaptcha = new Zend_Captcha_ReCaptcha();
        $oRecaptcha->setPrivkey('6LeRVecSAAAAAOrj5cRaxumie-ENd2kbd4owux1g')
                   ->setPubkey('6LeRVecSAAAAAIF-JhjF6er2l1qx3hyt4vWji_Bg')
                    ->setOption('theme', 'clean')
                    ->setOption('lang', 'pl');

         $oElem  = new Zend_Form_Element_Captcha('challenge',
                        array('captcha' => $oRecaptcha));
         $oElem ->setLabel('Wpisz kod z obrazka *')
                                    ->setErrorMessages(array('errCaptcha' => 'Błędny kod'));

         $this->addElement($oElem);

         $this->addElement($oElem);
        $this->setMethod('POST'); 
        
    }

}

