<?php

class Page_Form_FilterHouse extends Form_Abstract {

    public function __construct($iType) {
        parent::__construct();

        $oElem = new Zend_Form_Element_Text('offer_id');
        $oElem->setLabel('Nr. oferty')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
        $this->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('district');
        $oElem->setLabel('Powiat')
                ->setAttrib('class', 'superselect')
                ->setAttrib('multiple', 'multiple');
        $this->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('community');
        $oElem->setLabel('Gmina')
                ->setAttrib('class', 'superselect')
                ->setAttrib('multiple', 'multiple');
        $this->addElement($oElem);


        $oElem = new Zend_Form_Element_Select('city');
        $oElem->setLabel('Miasto')
                ->setAttrib('class', 'superselect')
                ->setAttrib('multiple', 'multiple');
        $this->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('price_from');
        $oElem->setLabel('Cena')
                ->setAttrib('placeholder', 'od')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
        $this->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('price_to');
        $oElem->setAttrib('placeholder', 'do')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
        $this->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('price_surface_from');
        $oElem->setLabel('Cena za 1m2')
                ->setAttrib('placeholder', 'od')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
        $this->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('price_surface_to');
        $oElem->setAttrib('placeholder', 'do')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
        $this->addElement($oElem);



        $oElem = new Zend_Form_Element_Text('surface_from');
        $oElem->setLabel('Powierzchnia domu')
                ->setAttrib('placeholder', 'od')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
        $this->addElement($oElem);

        $oElem = new Zend_Form_Element_Text('surface_to');
        $oElem->setAttrib('placeholder', 'do')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
        $this->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('terrain_surface_from');
        $oElem->setLabel('Powierzchnia działki')
                ->setAttrib('placeholder', 'od')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
        $this->addElement($oElem);


        $oElem = new Zend_Form_Element_Text('terrain_surface_to');
        $oElem->setAttrib('placeholder', 'do')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
        $this->addElement($oElem);

        $oElem = new Zend_Form_Element_Text('rooms_from');
        $oElem->setLabel('Liczba pokoi')
                ->setAttrib('placeholder', 'od')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
        $this->addElement($oElem);

        $oElem = new Zend_Form_Element_Text('rooms_to');
        $oElem->setAttrib('placeholder', 'do')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
        $this->addElement($oElem);

        $oElem = new Zend_Form_Element_MultiCheckbox('categories');
        $this->addElement($oElem);

        $oElem = new Zend_Form_Element_Select('sort');
        $oElem->setLabel('Sortuj według')
                ->setDecorators(array('Label', 'ViewHelper'));
        $this->addElement($oElem);
        
           $this->populateSelect(Model_Logic_Offer::getInstance()->getSortingMethods(), 
                              'sort', 
                              array('id' => 'id', 'value' => 'label'),
                              false);
        
        $this->populateSelect(Model_DbTable_Category::getInstance()->getByType($iType), 'categories', array('id' => 'id', 'value' => 'name'), false);
        
         $this->populateSelect(Model_DbTable_Offer::getInstance()->getUniqueDistrictsByType($iType),
                             'district', 
                             array('id' => 'district', 'value' => 'district'),
                             false);
        
    }

}

