<?php

class Page_StaticController extends App_Controller_Page_Abstract {

    public function displayAction() {

        Model_Logic_Static::getInstance()->manageDisplay($this);
    }

    public function contactAction() {
        

        $oForm = new Page_Form_Contact();

        if ($this->getRequest()->isPost()) {
            if ($oForm->isValid($this->getRequest()->getPost())) {

           Model_Logic_Mail::getInstance()->sendContactEmail($oForm->getValues());
             $this->view->success = true; 
             $oForm->reset(); 
             
            }
            else $this->view->slide = 'form';
        }
        
        
        $this->view->form = $oForm;
        $this->view->headScript()
                ->appendFile('https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyDy0CKx15WJ7PlB9_7RNSZviGTFAlMNNdo')
                ->appendFile('/scripts/manager/page/contact.js');
    }
    
    public function aboutusAction()
    {
        
        
    }

}