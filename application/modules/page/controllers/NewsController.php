<?php

class Page_NewsController extends App_Controller_Page_Abstract {
    
   


    public function listAction() {
        
        Model_Logic_News::getInstance()->managePageList($this); 
    }
    
     public function displayAction() {
        
        Model_Logic_News::getInstance()->manageDisplay($this); 
    }
    

   
}