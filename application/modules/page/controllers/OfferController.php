<?php

class Page_OfferController extends App_Controller_Page_Abstract {
 
    
    
    public function listAction()
    {
        Model_Logic_Offer::getInstance()->managePageList($this); 
    }
    
        public function detailAction()
    {
        Model_Logic_Offer::getInstance()->managePageOfferDetail($this); 
    }
    
    
    public function cityAction()
    {
        $this->_helper->json(Model_Logic_Offer::getInstance()->manageCity($this)); 
    }
        
      public function communityAction()
    {
        $this->_helper->json(Model_Logic_Offer::getInstance()->manageCommunity($this)); 
    }
    
      public function districtAction()
    {
        $this->_helper->json(Model_Logic_Offer::getInstance()->manageDistrict($this)); 
    }
    
     public function printAction()
    {
        Model_Logic_Offer::getInstance()->managePrint($this); 
    }
    
    
}

