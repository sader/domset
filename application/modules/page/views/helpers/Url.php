<?php

/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @version    $Id: Url.php 23775 2011-03-01 17:25:24Z ralph $
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
/** Zend_View_Helper_Abstract.php */
require_once 'Zend/View/Helper/Abstract.php';

/**
 * Helper for making easy links and getting urls that depend on the routes and router
 *
 * @package    Zend_View
 * @subpackage Helper
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Zend_View_Helper_Url extends Zend_View_Helper_Abstract {

    public function url(array $urlOptions = array(), $name = null, $replace = '_', $reset = true, $encode = true) {
      
        $fixedUrlOptions = array();

        foreach ($urlOptions as $sKey => $sOption) {
            $fixedUrlOptions[$sKey] = is_string($sOption) ? self::seo($sOption, $replace) : $sOption;
        }


        $router = Zend_Controller_Front::getInstance()->getRouter();
        return $router->assemble($fixedUrlOptions, $name, $reset, $encode);
    }

    private static function seo($str, $replace) {

        $str = @iconv('UTF-8', 'ASCII//TRANSLIT', trim($str, ' -' . $replace));
        $str = preg_replace('/[^a-zA-Z0-9\s\.' . $replace . ']/', '', strtolower($str));
        $str = preg_replace('/' . $replace . '/', ' ', $str);
        $str = preg_replace('/ +/', ' ', $str);

        return str_replace(' ', $replace, $str);
    }

}
