<?php

class Admin_Grid_Wanted extends Grid_Abstract {

    public function __construct($iClientId) {
        parent::__construct();

        $oDataSource = new Bvb_Grid_Source_Zend_Select(Model_DbTable_Wanted::getInstance()->getListOfWantedForUser($iClientId));
        $this->oGrid->setSource($oDataSource);

         $this->oGrid->updateColumn('client_id',
                array('hidden' =>  true));
        
        $this->oGrid->updateColumn('client', array('hidden' => true));



           $this->oGrid->updateColumn('id_type', array('helper' =>
            array('name' => 'Key',
                'params' => array('{{id_type}}', Model_Logic_Offer::getInstance()->getOfferTypeNames()))));

     $this->oGrid->updateColumn('id_wanted', array('hidden' => true));
     $this->oGrid->addExtraColumn($this->operationColumn());
     
     
       $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id_type', array(
            'values' => Model_Logic_Offer::getInstance()->getOfferTypeNames()
        ));


        $this->oGrid->addFilters($oFilters); 
    
    }
    
    
      private function operationColumn() {
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' =>  'CrudDisplay' ,  
                                   'params' => array('{{id_wanted}}' , 
                                                      'wanted')));

        return $oDetailColumn;
    }

}

