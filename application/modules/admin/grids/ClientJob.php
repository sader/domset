<?php

class Admin_Grid_ClientJob extends Grid_Abstract {

    
    
    public function __construct($iId) {
        parent::__construct();

        
        
        $this->oGrid->setGridId('jobs'); 
        
        $oDataSource = new Bvb_Grid_Source_Zend_Select(Model_DbTable_Job::getInstance()->getListOfJobsForClient($iId));
        $this->oGrid->setSource($oDataSource);

        $this->oGrid->updateColumn('id', array('remove' => true));
        

        
        $this->oGrid->updateColumn('price', array('decorator' => '{{price}} PLN' , 'title' => 'Cena'));
        $this->oGrid->updateColumn('surface', array('decorator' => '{{surface}} m&sup2;'));
        $this->oGrid->updateColumn('phone', array('remove' => true));
        $this->oGrid->updateColumn('price_surface', array('decorator' => '{{price_surface}} PLN/m&sup2;' , 'title' => 'Cena za m&sup2;'));
        
        $this->oGrid->updateColumn('status', array(
            'position' => 'first',
            'helper' => array('name' => 'JobStatus',
                'params' => array('{{status}}'))));
        
                
        
        $this->oGrid->updateColumn('offer_type',
                array('helper' =>
            array('name' => 'Key',
                'params' => array('{{offer_type}}', Model_Logic_Offer::getInstance()->getOfferTypeNames()))));
        
        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('rooms', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('floor', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
         $oFilters->addFilter('zadanie', array('style' => 'width:50px;'));
        $oFilters->addFilter('dodane', array('style' => 'width:60px;'));
        $oFilters->addFilter('aktualizowane', array('style' => 'width:60px;'));
        $oFilters->addFilter('status', array('style' => 'max-width:30px;', 'values' => Model_Logic_Enum::getInstance()->getJobResultList()));
        
          $oFilters->addFilter('offer_type',
                array('style' => 'max-width:30px;' , 'values' => Model_Logic_Offer::getInstance()->getOfferTypeNames()));
        
        $oFilters->addFilter('surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('date', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price_surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));

      
        $this->oGrid->addFilters($oFilters);
        $this->oGrid->addExtraColumn($this->operationColumn());
    }
    
    
      private function operationColumn() {
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' =>  'CrudDisplay' ,  'params' => array('{{id}}', 'job')));

        return $oDetailColumn;
    }

}
