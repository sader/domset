<?php

class Admin_Grid_Clients extends Grid_Abstract {

    public function __construct() {
        parent::__construct();

        $oDataSource = new Bvb_Grid_Source_Zend_Select(Model_DbTable_Client::getInstance()->getListOfClients());
        $this->oGrid->setSource($oDataSource);

        $this->oGrid->updateColumn('id', array('remove' => true));
        $this->oGrid->updateColumn('type', array('searchType' => 'sqlExp',
            'searchSqlExp' => 'FIND_IN_SET({{value}} , type ) > 0',
            'helper' => array('name' => 'ClientType',
                'params' => array('{{type}}'))));
        $oFilters = new Bvb_Grid_Filters();

        $oFilters->addFilter('type', array('values' => Model_Logic_Client::getClientTypes()));

        $this->oGrid->addFilters($oFilters);
        $this->oGrid->addExtraColumn($this->operationColumn());
    }
    
    
      private function operationColumn() {
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' =>  'CrudDisplay' ,  'params' => array('{{id}}', 'client',array('type' => '{{=type}}'))));

        return $oDetailColumn;
    }

}
