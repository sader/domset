<?php

class Admin_Grid_Job extends Grid_Abstract {

    public function __construct($sDate = null) {
        parent::__construct();

        $sGridId = ($sDate === null) ? 'full' : 'daily';

        $this->oGrid->setGridId($sGridId);

        $oDataSource = new Bvb_Grid_Source_Zend_Select(Model_DbTable_Job::getInstance()->getListOfJobs($sDate));
        $this->oGrid->setSource($oDataSource);

        $this->oGrid->updateColumn('id', array('remove' => true));
        $this->oGrid->updateColumn('id_client', array('remove' => true));

        $this->oGrid->updateColumn('offer_type', array('class' => 'clickable',
            'title' => 'Typ oferty',
            'helper' =>
            array('name' => 'Key',
                'params' => array('{{offer_type}}', Model_Logic_Offer::getInstance()->getOfferTypeNames()))));

        $this->oGrid->updateColumn('client', array('decorator' => '<a href="#" data="{{id_client}}" class="quickdetail">{{client}}</a>', 'title' => 'Klient'));

        $this->oGrid->updateColumn('location', array('title' => 'Lokalizacja', 'class' => 'clickable'));
        $this->oGrid->updateColumn('price', array('class' => 'clickable', 'title' => 'Cena'));
        $this->oGrid->updateColumn('surface', array('class' => 'clickable', 'title' => 'Pow.'));
        $this->oGrid->updateColumn('rooms', array('class' => 'clickable', 'title' => 'Pokoje'));
        $this->oGrid->updateColumn('floor', array('class' => 'clickable', 'title' => 'Piętro'));
        $this->oGrid->updateColumn('zadanie', array('class' => 'clickable'));
        $this->oGrid->updateColumn('price_surface', array('class' => 'clickable', 'title' => 'PLN/m&sup2;'));
        $this->oGrid->updateColumn('status', array('position' => 'first', 'class' => 'clickable', 'helper' => array('name' => 'JobStatus', 'params' => array('{{status}}'))));



        $this->oGrid->updateColumn('aktualizowane', array('class' => 'clickable', 'title' => 'Aktualiz.'));
        $this->oGrid->updateColumn('dodane', array('class' => 'clickable'));

        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('phone', array('style' => 'width:80px;'));
        $oFilters->addFilter('offer_type', array('style' => 'max-width:30px;', 'values' => Model_Logic_Enum::getInstance()->getOfferTypesList()));
        $oFilters->addFilter('zadanie', array('style' => 'width:50px;'));

        $oFilters->addFilter('location', array('style' => 'width:80px;'));
        $oFilters->addFilter('client', array('style' => 'width:60px;'));
        $oFilters->addFilter('dodane', array('style' => 'width:60px;'));
        $oFilters->addFilter('aktualizowane', array('style' => 'width:60px;'));
        $oFilters->addFilter('status', array('style' => 'max-width:30px;', 'values' => Model_Logic_Enum::getInstance()->getJobResultList())
        );

        $oFilters->addFilter('floor', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('rooms', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('date', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price_surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));



        $this->oGrid->addFilters($oFilters);
        $this->oGrid->addExtraColumn($this->operationColumn());
    }

    private function operationColumn() {
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName(' ')
                ->setStyle('text-align : right; padding-right : 5px')
                ->setHelper(array('name' => 'CrudDisplay', 'params' => array('{{id}}', 'job')));

        return $oDetailColumn;
    }

}