<?php

class Admin_Grid_Presentation extends Grid_Abstract {

    
    
    public function __construct($iId, $sType = 'client') {
        parent::__construct();

        
        
        $this->oGrid->setGridId('presentation'); 
        
        switch ($sType) {
            case 'client':
                    $this->client($iId); 
                break;
            case 'offer' :
                $this->offer($iId);
                break; 
            default:
                $this->offer($iId); 
                break;
        }
        
   
        $this->oGrid->addExtraColumn($this->operationColumn());
    }
    
    
    private function client($iId)
    {
          $oDataSource = new Bvb_Grid_Source_Zend_Select(Model_DbTable_Presentation::getInstance()->getPresentationForClient($iId));
        $this->oGrid->setSource($oDataSource);
        
            $this->oGrid->updateColumn('photo', array('search' => false,
            'position' => 'first',
            'helper' => array('name' => 'image',
                'params' => array('{{offer_id}}',
                    '{{photo}}',
                    Model_Logic_Gallery::IMAGE_THUMB_WIDTH,
                    Model_Logic_Gallery::IMAGE_THUMB_HEIGHT))));
        
        $this->oGrid->updateColumn('id', array('remove' => true));
        $this->oGrid->updateColumn('id_offer', array('remove' => true));
        $this->oGrid->updateColumn('info', array('search' => false));
        
            
           $this->oGrid->updateColumn('status', array('helper' =>
            array('name' => 'Key',
                'params' => array('{{status}}', Model_Logic_Enum::getInstance()->getPresentationStatusList()))));
           
          $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('status', array(
            'values' => Model_Logic_Enum::getInstance()->getPresentationStatusList())
        );

        $this->oGrid->addFilters($oFilters);     
    }
    
    private function offer($iId)
    {
         $oDataSource = new Bvb_Grid_Source_Zend_Select(Model_DbTable_Presentation::getInstance()->getListOfPresentationsForOffer($iId));
        $this->oGrid->setSource($oDataSource);

        $this->oGrid->updateColumn('id', array('remove' => true));
        $this->oGrid->updateColumn('id_offer', array('remove' => true));
        $this->oGrid->updateColumn('id_client', array('remove' => true));
        $this->oGrid->updateColumn('info', array('search' => false));
        $this->oGrid->updateColumn('client', array('decorator' => '<a href="#" data="{{id_client}}" class="quickdetail">{{client}}</a>'));
           $this->oGrid->updateColumn('status', array('helper' =>
            array('name' => 'Key',
                'params' => array('{{status}}', Model_Logic_Enum::getInstance()->getPresentationStatusList()))));
           
          $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('status', array(
            'values' => Model_Logic_Enum::getInstance()->getPresentationStatusList())
        );

        $this->oGrid->addFilters($oFilters);
    }


    
    
      private function operationColumn() {
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' =>  'CrudDisplay' ,  'params' => array('{{id}}', 'presentation')));

        return $oDetailColumn;
    }

}
