<?php

class Admin_Grid_Admins extends Grid_Abstract {

    public function __construct() {
        parent::__construct();

        $oDataSource = new Bvb_Grid_Source_Zend_Select(Model_DbTable_Admin::getInstance()->getListOfAdminsQuery());
        $this->oGrid->setSource($oDataSource);


        $this->oGrid->updateColumn('id', array('hidden' => true));


        $this->oGrid->addExtraColumn($this->operationColumn());
        $this->typeColumn();
    }

    private function operationColumn() {
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' =>  'CrudDisplay' , 'params' => array('{{id}}')));

        return $oDetailColumn;
    }

    private function typeColumn() {
        $this->oGrid->updateColumn('type', array('helper' =>
            array('name' => 'AdminType',
                'params' => array('{{type}}'))));

        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('type', array(
            'values' => array(
            Model_DbTable_Admin::ACCOUNT_TYPE_STANDARD => 'Moderator',
            Model_DbTable_Admin::ACCOUNT_TYPE_MASTER => 'Administrator',
            )
        ));
        
        $this->oGrid->addFilters($oFilters); 
        
    }

}
