<?php

class Admin_Grid_Category extends Grid_Abstract {

    public function __construct() {
        parent::__construct();

        $oDataSource = new Bvb_Grid_Source_Zend_Select(Model_DbTable_Category::getInstance()->getListOfCategoriesQuery());
        $this->oGrid->setSource($oDataSource);


        $this->oGrid->updateColumn('id', array('remove' => true));
        $this->oGrid->updateColumn('type', array('remove' => true));
        $this->oGrid->updateColumn('info', array('search' => false, 'order' => false));
        $this->oGrid->updateColumn('additional_data', array('search' => false, 'title' => 'Czy kategoria wymaga parametru', 'order' => false, 'helper' => array('name' => 'AdditionalData' , 'params' => array('{{additional_data}}'))));

    $this->oGrid->addExtraColumn($this->operationColumn());
        
    }

    private function operationColumn() {
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' =>  'CrudDisplay' ,  'params' => array('{{id}}' , 'category')));

        return $oDetailColumn;
    }
}
