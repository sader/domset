<?php

class Admin_Grid_WantedOffers extends Grid_Abstract {

    public function __construct($oAdapter = null) {
        parent::__construct();

        if(!isset($oAdapter) || empty($oAdapter))
        {
            $oAdapter = Model_DbTable_Wanted::getInstance()->getListOfWantedOffersQuery(); 
        }
            
        $oDataSource = new Bvb_Grid_Source_Zend_Select($oAdapter);
        $this->oGrid->setSource($oDataSource);
        $this->oGrid->setGridId('wanted'); 
        
         $this->oGrid->updateColumn('client_id',
                array('hidden' =>  true));
        
        $this->oGrid->updateColumn('client',
                array('decorator' =>  '<a class="quickdetail" data="{{client_id}}" href="/admin/client/detail/client/{{client_id}}">{{client}}</a>'));
        
           $this->oGrid->updateColumn('id_type', array('helper' =>
            array(  'class' => 'clickable',
                    'name' => 'Key',
                    'params' => array('{{id_type}}', Model_Logic_Offer::getInstance()->getOfferTypeNames()))));
     
           $this->oGrid->updateColumn('price_from', array('title' => 'Cena od' ,'class' => 'clickable'));
           $this->oGrid->updateColumn('price_to', array('title' => 'Cena do' ,'class' => 'clickable'));
           $this->oGrid->updateColumn('surface_from', array('title' => 'Pow. od' ,'class' => 'clickable'));
           $this->oGrid->updateColumn('surface_to', array('title' => 'Pow. do' ,'class' => 'clickable'));

           
           
     $this->oGrid->updateColumn('id_wanted', array('hidden' => true));
     $this->oGrid->addExtraColumn($this->operationColumn());
     
     
       $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id_type', array('values' => Model_Logic_Offer::getInstance()->getOfferTypeNames()));
        $oFilters->addFilter('info', array('style' => 'width:400px;'));

        $this->oGrid->addFilters($oFilters); 
    }
    
    
      private function operationColumn() {
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' => 'CrudDisplay', 'params' => array('{{id_wanted}}', 'wanted_offers')));

        return $oDetailColumn;
    }


}
