<?php

class Admin_Grid_Preview extends Grid_Abstract {

    public function __construct($aExtraFilters = array()) {
        parent::__construct();

        $oAdapter = Model_DbTable_Offer_Flat_Sell::getInstance()->getPreviewQuery($aExtraFilters);
        $this->flatGrid($oAdapter);

        $this->formatter();
    }

    private function formatter() {
        $this->oGrid->updateColumn('cover', array('search' => false,
            'order' => false,
            'title' => '',
            'position' => 'first',
            'helper' => array('name' => 'image',
                'params' => array('{{id}}',
                    '{{cover}}',
                    Model_Logic_Gallery::IMAGE_THUMB_WIDTH,
                    Model_Logic_Gallery::IMAGE_THUMB_HEIGHT))));


        $this->oGrid->updateColumn('price', array('title' => 'Cena',
            'class' => 'clickable'));
        $this->oGrid->updateColumn('surface', array('title' => 'Pow', 'class' => 'clickable'));

        $this->oGrid->updateColumn('price_surface', array('title' => 'PLN/m&sup2;', 'class' => 'clickable'));




        $this->oGrid->updateColumn('created', array('title' => 'Dodana',
            'class' => 'clickable'));


        $this->oGrid->updateColumn('location', array('title' => 'Lokalizacja',
            'class' => 'clickable'));



        $this->oGrid->updateColumn('id', array('title' => 'ID',
            'class' => 'clickable'));

        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('floors', array('style' => 'width:40px;'));
        $oFilters->addFilter('price', array('style' => 'width:40px;'));
        $oFilters->addFilter('surface', array('style' => 'width:40px;'));
        $oFilters->addFilter('price_surface', array('style' => 'width:40px;'));
        $oFilters->addFilter('rooms', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('created', array('style' => 'width:100px;'));
        $this->oGrid->addFilters($oFilters);
    }

    private function flatGrid($oAdapter) {
        $oDataSource = new Bvb_Grid_Source_Zend_Select($oAdapter);
        $this->oGrid->setSource($oDataSource);

        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('floors', array('style' => 'width:40px;'));

        $oFilters->addFilter('rooms', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('created', array('style' => 'width:100px;'));


        $this->oGrid->addFilters($oFilters);
        $this->oGrid->addExtraColumn($this->operationColumn());
    }

    private function houseGrid($oAdapter) {
        $oDataSource = new Bvb_Grid_Source_Zend_Select($oAdapter);
        $this->oGrid->setSource($oDataSource);
        $this->oGrid->updateColumn('quality', array('class' => 'clickable', 'helper' =>
            array('name' => 'Key',
                'params' => array('{{quality}}', Model_Logic_Enum::getInstance()->getQualityList()))));

        $this->oGrid->updateColumn('terrain_surface', array('title' => 'Pow. działki', 'class' => 'clickable'));

        $this->oGrid->updateColumn('building_type', array('title' => 'Typ budynku',
            'class' => 'clickable',
            'helper' =>
            array('name' => 'Key',
                'params' => array('{{building_type}}', Model_Logic_Enum::getInstance()->getHouseTypeList()))));

        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('terrain_surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('created', array('style' => 'width:100px;'));
        $oFilters->addFilter('price_surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('building_type', array(
            'values' => Model_Logic_Enum::getInstance()->getHouseTypeList())
        );

        $oFilters->addFilter('quality', array('style' => 'max-width:40%',
            'values' => Model_Logic_Enum::getInstance()->getQualityList())
        );


        $this->oGrid->addFilters($oFilters);
        $this->oGrid->addExtraColumn($this->operationColumn());
    }

    private function landGrid($oAdapter) {

        $oDataSource = new Bvb_Grid_Source_Zend_Select($oAdapter);
        $this->oGrid->setSource($oDataSource);

        $this->oGrid->updateColumn('city', array('title' => 'Miasto', 'class' => 'clickable'));
        $this->oGrid->updateColumn('district', array('title' => 'Powiat', 'class' => 'clickable'));

        $this->oGrid->updateColumn('land_type', array('title' => 'typ', 'class' => 'clickable',
            'helper' =>
            array('name' => 'Key',
                'params' => array('{{land_type}}', Model_Logic_Enum::getInstance()->getLandTypeList()))));

        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('terrain_surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('created', array('style' => 'width:100px;'));
        $oFilters->addFilter('price_surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('land_type', array(
            'values' => Model_Logic_Enum::getInstance()->getLandTypeList())
        );

        $this->oGrid->addFilters($oFilters);
        $this->oGrid->addExtraColumn($this->operationColumn());
    }

    private function localGrid($oAdapter) {

        $oDataSource = new Bvb_Grid_Source_Zend_Select($oAdapter);
        $this->oGrid->setSource($oDataSource);

        $this->oGrid->updateColumn('section', array(
            'title' => 'Dzielnica',
            'class' => 'clickable'));

        $this->oGrid->updateColumn('purpose', array(
            'title' => 'Przeznaczenie',
            'class' => 'clickable',
            'helper' =>
            array('name' => 'Key',
                'params' => array('{{purpose}}', Model_Logic_Enum::getInstance()->getLocalPurposeList()))));

        $this->oGrid->updateColumn('building_type', array('title' => 'Typ',
            'class' => 'clickable',
            'helper' =>
            array('name' => 'Key',
                'params' => array('{{building_type}}', Model_Logic_Enum::getInstance()->getLocalTypeList()))));



        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('created', array('style' => 'width:100px;'));
        $oFilters->addFilter('price_surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('purpose', array(
            'values' => Model_Logic_Enum::getInstance()->getLocalPurposeList())
        );

        $oFilters->addFilter('building_type', array(
            'values' => Model_Logic_Enum::getInstance()->getLocalTypeList())
        );



        $this->oGrid->addFilters($oFilters);
        $this->oGrid->addExtraColumn($this->operationColumn());
    }

    private function warehouseGrid($oAdapter) {

        $oDataSource = new Bvb_Grid_Source_Zend_Select($oAdapter);
        $this->oGrid->setSource($oDataSource);



        $this->oGrid->updateColumn('purpose', array(
            'title' => 'Przeznaczenie',
            'class' => 'clickable',
            'helper' =>
            array('name' => 'Key',
                'params' => array('{{purpose}}', Model_Logic_Enum::getInstance()->getWarehousePurposeList()))));


        $this->oGrid->updateColumn('section', array(
            'title' => 'Dzielnica',
            'class' => 'clickable'));

        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('created', array('style' => 'width:100px;'));
        $oFilters->addFilter('price_surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('purpose', array(
            'values' => Model_Logic_Enum::getInstance()->getWarehousePurposeList())
        );
        $this->oGrid->addFilters($oFilters);
        $this->oGrid->addExtraColumn($this->operationColumn());
    }

    private function fullGrid($oAdapter) {
        $oDataSource = new Bvb_Grid_Source_Zend_Select($oAdapter);
        $this->oGrid->setGridId('offers');
        $this->oGrid->setSource($oDataSource);



        $this->oGrid->updateColumn('id_type', array('helper' =>
            array('title' => 'Typ',
                'name' => 'Key',
                'params' => array('{{id_type}}', Model_Logic_Offer::getInstance()->getOfferTypeNames()))));


        $this->oGrid->updateColumn('status', array('hidden' => true));
        $this->oGrid->updateColumn('id', array('hidden' => true));
        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('id_type', array('values' => Model_Logic_Offer::getInstance()->getOfferTypeNames()));
        $oFilters->addFilter('surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price_surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));


        $this->oGrid->addFilters($oFilters);
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' => 'CrudDisplay', 'params' => array('{{id}}', 'wanted_searches')));

        $this->oGrid->addExtraColumn($oDetailColumn);
    }

    private function offersByOwner($oAdapter) {
        $oDataSource = new Bvb_Grid_Source_Zend_Select($oAdapter);
        $this->oGrid->setGridId('offers');
        $this->oGrid->setSource($oDataSource);



        $this->oGrid->updateColumn('id_type', array('helper' =>
            array('name' => 'Key',
                'params' => array('{{id_type}}', Model_Logic_Offer::getInstance()->getOfferTypeNames()))));


        $this->oGrid->updateColumn('status', array('helper' =>
            array('name' => 'Key',
                'params' => array('{{status}}', Model_Logic_Enum::getInstance()->getOfferStatusesList()))));



        $this->oGrid->updateColumn('id', array('hidden' => true));
        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price_surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('status', array('values' => Model_Logic_Enum::getInstance()->getOfferStatusesList()));

        $this->oGrid->addFilters($oFilters);
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' => 'CrudDisplay', 'params' => array('{{id}}', 'offersOwner')));

        /**
          $oDetailColumn = new Bvb_Grid_Extra_Column();
          $oDetailColumn->setPosition('right')
          ->setName('Operacje')
          ->setHelper(array('name' => 'CrudDisplay', 'params' => array('{{id}}', 'archive')));
         */
        $this->oGrid->addExtraColumn($oDetailColumn);
    }

    private function archiveGrid($oAdapter) {
        $oDataSource = new Bvb_Grid_Source_Zend_Select($oAdapter);
        $this->oGrid->setGridId('offers');
        $this->oGrid->setSource($oDataSource);



        $this->oGrid->updateColumn('id_type', array('title' => 'Typ',
            'class' => 'clickable',
            'helper' =>
            array('name' => 'Key',
                'params' => array('{{id_type}}', Model_Logic_Offer::getInstance()->getOfferTypeNames()))));


        $this->oGrid->updateColumn('status', array(
            'class' => 'clickable',
            'helper' =>
            array('name' => 'Key',
                'params' => array('{{status}}', Model_Logic_Enum::getInstance()->getOfferStatusesList()))));



        $this->oGrid->updateColumn('id', array('hidden' => true));
        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price_surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('status', array('values' => Model_Logic_Enum::getInstance()->getOfferStatusesList()));


        $oFilters->addFilter('id_type', array('values' => Model_Logic_Offer::getInstance()->getOfferTypeNames()));



        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' => 'CrudDisplay', 'params' => array('{{id}}', 'archive')));
        $this->oGrid->addFilters($oFilters);
        $this->oGrid->addExtraColumn($oDetailColumn);
    }

    private function promotionGrid($oAdapter) {


        $oDataSource = new Bvb_Grid_Source_Zend_Select($oAdapter);
        $this->oGrid->setGridId('promoted');
        $this->oGrid->setAjax('ajaxed');
        $this->oGrid->setSource($oDataSource);


        $this->oGrid->updateColumn('promoted', array('remove' => true));

        $this->oGrid->updateColumn('id_type', array('helper' =>
            array('name' => 'Key',
                'params' => array('{{id_type}}', Model_Logic_Offer::getInstance()->getOfferTypeNames()))));


        $this->oGrid->updateColumn('status', array('remove' => true));



        $this->oGrid->updateColumn('id', array('hidden' => true));


        $oFilters = new Bvb_Grid_Filters();
        $oFilters->addFilter('id', array('style' => 'width:40px;'));
        $oFilters->addFilter('surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('price_surface', array('style' => 'width:' . $this->oSmallInputFilterWidth . ';'));
        $oFilters->addFilter('status', array('values' => Model_Logic_Enum::getInstance()->getOfferStatusesList()));

        $oFilters->addFilter('id_type', array('values' => Model_Logic_Enum::getInstance()->getOfferTypesList()));

        $this->oGrid->addFilters($oFilters);

        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' => 'CrudDisplay', 'params' => array('{{id}}', 'promotion', '{{promoted}}')));

        $this->oGrid->addExtraColumn($oDetailColumn);
    }

    private function operationColumn() {
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' => 'CrudDisplay', 'params' => array('{{id}}', 'offer')));

        return $oDetailColumn;
    }

}
