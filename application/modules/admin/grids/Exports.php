<?php

class Admin_Grid_Exports extends Grid_Abstract  {

  
    
    public function __construct($iOfferId) {
        parent::__construct(); 
        
     $oDataSource = new Bvb_Grid_Source_Zend_Select(Model_DbTable_Export::getInstance()->getExportsForOffer($iOfferId)); 
     $this->oGrid->setSource($oDataSource); 
     
     $this->oGrid->addExtraColumn($this->operationColumn());
     
     $this->oGrid->updateColumn('id' , array('remove' => true)); 
     $this->oGrid->updateColumn('data' , array('remove' => true));
     $this->oGrid->updateColumn('date' , array('search' => true)); 
     $this->oGrid->updateColumn('id_offer' , array('remove' => true));
     $this->oGrid->updateColumn('remote_id' , array('title' => 'Id na serwerze' , 'search' => false));
     $this->oGrid->updateColumn('type' , array('title' => 'Ostatnia operacja' , 
                                'helper' => array('name' => 'ExportWorkType', 'params' => array('{{type}}')),
                                'search' => false));
     $this->oGrid->updateColumn('id_export' , array('helper' => array('name' => 'ExportType'  , 
                                                                      'params' => array('{{id_export}}' , '{{id_offer}}')),
                                                    )
                                ); 
       
     $oFilters = new Bvb_Grid_Filters();

        $oFilters->addFilter('id_export', array('values' => Model_Logic_Export::getRegisteredAdatersLabels()));

        $this->oGrid->addFilters($oFilters);
    }

    
    private function operationColumn() {
        $oDetailColumn = new Bvb_Grid_Extra_Column();
        $oDetailColumn->setPosition('right')
                ->setName('Operacje')
                ->setHelper(array('name' =>  'CrudDisplay' ,  
                                  'params' => array('{{id}}' , 
                                                    'export' , 
                                                    array('type' => '{{type}}'))));

        return $oDetailColumn;
    }
}
