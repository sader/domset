<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Wanted_House extends Form_Abstract {

    public function __construct($iType = Model_Logic_Offer_House_Sell::ID) {
        parent::__construct(null);

        $this->_aFields['province'] = new Zend_Form_Element_Multiselect('province');
        $this->_aFields['province']->setLabel('Wojewodztwo')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['district'] = new Zend_Form_Element_Text('district');
        $this->_aFields['district']->setLabel("Powiat")
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['city'] = new Zend_Form_Element_Text('city');
        $this->_aFields['city']->setLabel('Miasto')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['street'] = new Zend_Form_Element_Text('street');
        $this->_aFields['street']->setLabel('Ulica')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['section'] = new Zend_Form_Element_Multiselect('section');
        $this->_aFields['section']->setLabel('Dzielnica')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['surface_from'] = new Zend_Form_Element_Text('surface_from');
        $this->_aFields['surface_from']->setLabel('Powierchnia od / do')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['surface_to'] = new Zend_Form_Element_Text('surface_to');
        $this->stickyElem($this->_aFields['surface_to'], 'surface_from');
        $this->_aFields['price_from'] = new Zend_Form_Element_Text('price_from');
        $this->_aFields['price_from']->setLabel('Cena od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['price_to'] = new Zend_Form_Element_Text('price_to');
        $this->stickyElem($this->_aFields['price_to'], 'price_from');
        $this->_aFields['price_surface_from'] = new Zend_Form_Element_Text('price_surface_from');
        $this->_aFields['price_surface_from']->setLabel('Cena za metr  od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['price_surface_to'] = new Zend_Form_Element_Text('price_surface_to');
        $this->stickyElem($this->_aFields['price_surface_to'], 'price_surface_from');

        $this->_aFields['house_surface_from'] = new Zend_Form_Element_Text('house_surface_from');
        $this->_aFields['house_surface_from']->setLabel('Powierzchnia domu  od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['house_surface_to'] = new Zend_Form_Element_Text('house_surface_to');
        $this->stickyElem($this->_aFields['house_surface_to'], 'house_surface_from');

        $this->_aFields['terrain_surface_from'] = new Zend_Form_Element_Text('terrain_surface_from');
        $this->_aFields['terrain_surface_from']->setLabel('Powierzchnia działki  od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['terrain_surface_to'] = new Zend_Form_Element_Text('terrain_surface_to');
        $this->stickyElem($this->_aFields['terrain_surface_to'], 'terrain_surface_from');

        $this->_aFields['building_type'] = new Zend_Form_Element_Multiselect('building_type');
        $this->_aFields['building_type']->setLabel('Typ budynku')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['rooms_from'] = new Zend_Form_Element_Text('rooms_from');
        $this->_aFields['rooms_from']->setLabel('Pokoje od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['rooms_to'] = new Zend_Form_Element_Text('rooms_to');
        $this->stickyElem($this->_aFields['rooms_to'], 'rooms_from');


        $this->_aFields['floors_from'] = new Zend_Form_Element_Text('floors_from');
        $this->_aFields['floors_from']->setLabel('Piętra od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['floors_to'] = new Zend_Form_Element_Text('floors_to');
        $this->stickyElem($this->_aFields['floors_to'], 'floors_from');

        $this->_aFields['attic'] = new Zend_Form_Element_Multiselect('attic');
        $this->_aFields['attic']->setLabel('Poddaszenie')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['basement'] = new Zend_Form_Element_Multiselect('basement');
        $this->_aFields['basement']->setLabel('Podpiwniczenie')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['year_from'] = new Zend_Form_Element_Text('year_from');
        $this->_aFields['year_from']->setLabel('Rok budowy od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['year_to'] = new Zend_Form_Element_Text('year_to');
        $this->stickyElem($this->_aFields['year_to'], 'year_from');

        $this->_aFields['levels_from'] = new Zend_Form_Element_Text('levels_from');
        $this->_aFields['levels_from']->setLabel('Poziomy od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['levels_to'] = new Zend_Form_Element_Text('levels_to');
        $this->stickyElem($this->_aFields['levels_to'], 'levels_from');

        $this->_aFields['building_material'] = new Zend_Form_Element_Multiselect('building_material');
        $this->_aFields['building_material']->setLabel('Materiał')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['quality'] = new Zend_Form_Element_Multiselect('quality');
        $this->_aFields['quality']->setLabel('Stan techniczny')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['windows'] = new Zend_Form_Element_Multiselect('windows');
        $this->_aFields['windows']->setLabel('Okna')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['roof'] = new Zend_Form_Element_Multiselect('roof');
        $this->_aFields['roof']->setLabel('Typ dachu')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['roof_type'] = new Zend_Form_Element_Multiselect('roof_type');
        $this->_aFields['roof_type']->setLabel('Typ pokrycia dachowego')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['sewerage'] = new Zend_Form_Element_Multiselect('sewerage');
        $this->_aFields['sewerage']->setLabel('Kanalizacja')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['heat'] = new Zend_Form_Element_Multiselect('heat');
        $this->_aFields['heat']->setLabel('Ogrzewanie')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['fence'] = new Zend_Form_Element_Multiselect('fence');
        $this->_aFields['fence']->setLabel('Ogrodzenie')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['garage'] = new Zend_Form_Element_Multiselect('garage');
        $this->_aFields['garage']->setLabel('Garaż')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['access'] = new Zend_Form_Element_Multiselect('access');
        $this->_aFields['access']->setLabel('Dojazd')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['additional'] = new Zend_Form_Element_MultiCheckbox('additional');
        $this->_aFields['additional']->setLabel('Opcje dodatkowe')
                ->setDecorators($this->_aFormElementTableDecorator);
        
        
        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Dodaj')
                                  ->setDecorators($this->_aFormSubmitTableDecorator);   

        $this->addElements($this->_aFields);
        
        $this->populateSelect(Model_DbTable_Section::getInstance()->getListOfSectionForDefaultCity(), 'section' , array('id' => 'name' , 'value' => 'name'), false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getProvinceList(), 'province', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getQualityList(), 'quality', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getHouseTypeList(), 'building_type', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getHouseMaterialTypeList(), 'building_material', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getFlatHeatList(), 'heat', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getBasementList(), 'basement', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getAtticList(), 'attic', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getAccessList(), 'access', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getGarageList(), 'garage', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getFenceList(), 'fence', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getSewerageList(), 'sewerage', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getRoofList(), 'roof', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getRoofTypeList(), 'roof_type', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getWindowList(), 'windows', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getHouseAdditionalSellList(), 'additional', false, false);
       
    }

}
