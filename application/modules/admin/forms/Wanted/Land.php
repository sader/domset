<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Wanted_Land extends Form_Abstract {

    public function __construct($iType = Model_Logic_Offer_Land_Sell::ID) {
        parent::__construct(null);

        $this->_aFields['province'] = new Zend_Form_Element_Multiselect('province');
        $this->_aFields['province']->setLabel('Wojewodztwo')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['district'] = new Zend_Form_Element_Text('district');
        $this->_aFields['district']->setLabel("Powiat")
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['city'] = new Zend_Form_Element_Text('city');
        $this->_aFields['city']->setLabel('Miasto')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['street'] = new Zend_Form_Element_Text('street');
        $this->_aFields['street']->setLabel('Ulica')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['section'] = new Zend_Form_Element_Multiselect('section');
        $this->_aFields['section']->setLabel('Dzielnica')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['surface_from'] = new Zend_Form_Element_Text('surface_from');
        $this->_aFields['surface_from']->setLabel('Powierchnia od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['surface_to'] = new Zend_Form_Element_Text('surface_to');
        $this->stickyElem($this->_aFields['surface_to'], 'surface_from');
        $this->_aFields['price_from'] = new Zend_Form_Element_Text('price_from');
        $this->_aFields['price_from']->setLabel('Cena od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['price_to'] = new Zend_Form_Element_Text('price_to');
        $this->stickyElem($this->_aFields['price_to'], 'price_from');
        $this->_aFields['price_surface_from'] = new Zend_Form_Element_Text('price_surface_from');
        $this->_aFields['price_surface_from']->setLabel('Cena za metr  od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['price_surface_to'] = new Zend_Form_Element_Text('price_surface_to');
        $this->stickyElem($this->_aFields['price_surface_to'], 'price_surface_from');
        $this->_aFields['land_type'] = new Zend_Form_Element_Multiselect('land_type');
        $this->_aFields['land_type']->setLabel('Typ działki')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['shape'] = new Zend_Form_Element_Multiselect('shape');
        $this->_aFields['shape']->setLabel('Kształt działki')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['width_from'] = new Zend_Form_Element_Text('width_from');
        $this->_aFields['width_from']->setLabel('Szerokość działki od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['width_to'] = new Zend_Form_Element_Text('width_to');
        $this->stickyElem($this->_aFields['width_to'], 'width_from');
        $this->_aFields['height_from'] = new Zend_Form_Element_Text('height_from');
        $this->_aFields['height_from']->setLabel('Długość działki od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['height_to'] = new Zend_Form_Element_Text('height_to');
        $this->stickyElem($this->_aFields['height_to'], 'height_from');
        $this->_aFields['services'] = new Zend_Form_Element_Multiselect('servies');
        $this->_aFields['services']->setLabel('Media')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['fence'] = new Zend_Form_Element_Multiselect('fence');
        $this->_aFields['fence']->setLabel('Ogrodzenie')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['access'] = new Zend_Form_Element_Multiselect('access');
        $this->_aFields['access']->setLabel('Dojazd')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['additional'] = new Zend_Form_Element_MultiCheckbox('additional');
        $this->_aFields['additional']->setLabel('Informacje dodatkowe')
                                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Dodaj')
                                  ->setDecorators($this->_aFormSubmitTableDecorator);

        $this->addElements($this->_aFields);
        
        $this->populateSelect(Model_DbTable_Section::getInstance()->getListOfSectionForDefaultCity(), 'section' , array('id' => 'name' , 'value' => 'name'), false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getProvinceList(), 'province', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getAccessList(), 'access', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getServicesList(), 'services', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getFenceList(), 'fence', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getShapeList(), 'shape', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getLandTypeList(), 'land_type', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getLandAdditionalList(), 'additional', false, false);
    }

}
