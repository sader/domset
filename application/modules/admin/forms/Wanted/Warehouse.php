<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Wanted_Warehouse extends Form_Abstract {

    public function __construct($iType = Model_Logic_Offer_Warehouse_Rent::ID) {
        parent::__construct(null);

        $this->_aFields['province'] = new Zend_Form_Element_Multiselect('province');
        $this->_aFields['province']->setLabel('Wojewodztwo')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['district'] = new Zend_Form_Element_Text('district');
        $this->_aFields['district']->setLabel("Powiat")
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['city'] = new Zend_Form_Element_Text('city');
        $this->_aFields['city']->setLabel('Miasto')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['street'] = new Zend_Form_Element_Text('street');
        $this->_aFields['street']->setLabel('Ulica')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['section'] = new Zend_Form_Element_Multiselect('section');
        $this->_aFields['section']->setLabel('Dzielnica')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['surface_from'] = new Zend_Form_Element_Text('surface_from');
        $this->_aFields['surface_from']->setLabel('Powierchnia od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['surface_to'] = new Zend_Form_Element_Text('surface_to');
        $this->stickyElem($this->_aFields['surface_to'], 'surface_from');
        $this->_aFields['price_from'] = new Zend_Form_Element_Text('price_from');
        $this->_aFields['price_from']->setLabel('Cena od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['price_to'] = new Zend_Form_Element_Text('price_to');
        $this->stickyElem($this->_aFields['price_to'], 'price_from');
        $this->_aFields['price_surface_from'] = new Zend_Form_Element_Text('price_surface_from');
        $this->_aFields['price_surface_from']->setLabel('Cena za metr  od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['price_surface_to'] = new Zend_Form_Element_Text('price_surface_to');
        $this->stickyElem($this->_aFields['price_surface_to'], 'price_surface_from');
        $this->_aFields['floors_from'] = new Zend_Form_Element_Text('floors_from');
        $this->_aFields['floors_from']->setLabel('Piętra od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['floors_to'] = new Zend_Form_Element_Text('floors_to');
        $this->stickyElem($this->_aFields['floors_to'], 'floors_from');

        $this->_aFields['levels_from'] = new Zend_Form_Element_Text('levels_from');
        $this->_aFields['levels_from']->setLabel('Piętra od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['levels_to'] = new Zend_Form_Element_Text('levels_to');
        $this->stickyElem($this->_aFields['levels_to'], 'levels_from');

        $this->_aFields['year_from'] = new Zend_Form_Element_Text('year_from');
        $this->_aFields['year_from']->setLabel('Rok budowy od / do')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['year_to'] = new Zend_Form_Element_Text('year_to');
        $this->stickyElem($this->_aFields['year_to'], 'year_from');

        $this->_aFields['height_from'] = new Zend_Form_Element_Text('height_from');
        $this->_aFields['height_from']->setLabel('Wysokość od / do')
                ->setDecorators($this->_aFormElementTableDecorator);
        $this->_aFields['height_to'] = new Zend_Form_Element_Text('height_to');
        $this->stickyElem($this->_aFields['height_to'], 'height_from');

        $this->_aFields['purpose'] = new Zend_Form_Element_Multiselect('purpose');
        $this->_aFields['purpose']->setLabel('Przeznaczenie')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['quality'] = new Zend_Form_Element_Multiselect('quality');
        $this->_aFields['quality']->setLabel('Stan')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['heat'] = new Zend_Form_Element_Multiselect('heat');
        $this->_aFields['heat']->setLabel('Ogrzewanie')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['additional'] = new Zend_Form_Element_Multiselect('additional');
        $this->_aFields['additional']->setLabel('Informache dodatkowe')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Dodaj')
                ->setDecorators($this->_aFormSubmitTableDecorator);
        $this->addElements($this->_aFields);

        $this->populateSelect(Model_DbTable_Section::getInstance()->getListOfSectionForDefaultCity(), 'section' , array('id' => 'name' , 'value' => 'name'), false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getProvinceList(), 'province', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getQualityList(), 'quality', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getWarehouseHeatList(), 'heat', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getWarehousePurposeList(), 'purpose', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getWarehouseAdditionalList(), 'additional', false, false);
    }

}