<?php


class Admin_Form_Gallery extends Form_Abstract
{
    
    public function __construct($sMode = null) {
        parent::__construct();
        
        $this->_aFields['visible'] = new Zend_Form_Element_Hidden('visible');
        $this->_aFields['visible']->setDecorators(array('ViewHelper')); 
                
        $this->_aFields['hidden'] = new Zend_Form_Element_Hidden('hidden');
        $this->_aFields['hidden']->setDecorators(array('ViewHelper')); 
        
        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Zapisz galerię')->setDecorators(array('ViewHelper')); 
        
        $this->addElements($this->_aFields)
              ->setDecorators(array('FormElements', 'Form'))
             ->setMethod(Zend_Form::METHOD_POST); 
    }

	
}
