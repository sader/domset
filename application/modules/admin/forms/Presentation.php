<?php

class Admin_Form_Presentation extends Form_Abstract {

    public function init() {

        parent::init();

        $this->_aFields['id_client'] = new Zend_Form_Element_Select('id_client');
        $this->_aFields['id_client']->setLabel('Klient')
                ->setRequired(true)
                ->setRegisterInArrayValidator(false)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj klienta'))
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['add_client'] = new Zend_Form_Element_Button('add_client');
        $this->_aFields['add_client']->setLabel('Dodaj klienta');
        $this->stickyElem($this->_aFields['add_client'], 'id_client');
        

        $this->_aFields['info'] = new Zend_Form_Element_Textarea('info');
        $this->_aFields['info']->setLabel('Informacje dodatkowe')
                               ->setAttrib('rows' , 5)
                ->setDecorators($this->_aFormElementTableDecorator);


        $this->_aFields['status'] = new Zend_Form_Element_Select('status');
        $this->_aFields['status']->setLabel('Status')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['date'] = new Zend_Form_Element_Text('date');
        $this->_aFields['date']->setLabel('Data zadania')
                ->setValue(date('Y-m-d'))
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['time'] = new Zend_Form_Element_Text('time');
        $this->stickyElem($this->_aFields['time'], 'date');


        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Dodaj')
                ->setDecorators($this->_aFormSubmitTableDecorator);

        $this->addElements($this->_aFields)
                ->setDecorators($this->_aFormTableDecorator)
                ->setMethod(Zend_Form::METHOD_POST);



        $this->populateSelect(Model_DbTable_Client::getInstance()->getClients(), 'id_client', array('id' => 'id', 'value' => 'client'), true);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getPresentationStatusList(), 'status', false, false);
    }
    
    public function editMode()
    {
        $this->getElement('submit')->setLabel('Zatwierdź'); 
    }

}

