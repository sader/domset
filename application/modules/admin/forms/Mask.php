<?php


class Admin_Form_Mask extends Form_Abstract
{
    
    public function __construct() {
        parent::__construct();
        
        $this->_aFields['fields'] = new Zend_Form_Element_Hidden('fields');
        $this->_aFields['fields']->setDecorators(array('ViewHelper')); 
                
        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Ustaw')->setDecorators(array('ViewHelper')); 
        
        $this->addElements($this->_aFields)
              ->setDecorators(array('FormElements', 'Form'))
             ->setMethod(Zend_Form::METHOD_POST); 
    }

	
}
