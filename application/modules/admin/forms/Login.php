<?php

class Admin_Form_Login extends Form_Abstract {

    public function init() {
        
        parent::init(); 



        $this->_aFields['login'] = new Zend_Form_Element_Text('login');
        $this->_aFields['login']->setLabel('login')
                ->setRequired(true)
                ->addValidator('NotEmpty', true)
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
				->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['password'] = new Zend_Form_Element_Password('password');
        $this->_aFields['password']->setLabel('password')
                ->setRequired(true)
                ->addValidator('NotEmpty', true)
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
				->setDecorators($this->_aFormElementTableDecorator);
                
                


        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Zaloguj')
							     ->setDecorators($this->_aFormSubmitTableDecorator);

        $this->addElements($this->_aFields)
			 ->setDecorators($this->_aFormTableDecorator)
			  ->setMethod(Zend_Form::METHOD_POST); 
			  
    }
	

}


