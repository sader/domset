<?php

class Admin_Form_News extends Form_Abstract {

    public function init() {

        parent::init();

        $this->_aFields['title'] = new Zend_Form_Element_Text('title');
        $this->_aFields['title']->setLabel('Tytuł')
                ->setRequired(true)
                ->setAttrib('class', 'input-xxlarge')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty', true, array('messages' => "Podaj tytuł newsa"))
                 ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['head'] = new Zend_Form_Element_Textarea('head');
        $this->_aFields['head']->setLabel('Nagłówek')
                ->setRequired(true)
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty', true, array('messages' => "Brak treści nagówka"))
                 ->setDecorators($this->_aFormElementTableDecorator);
        
        
        
        
        $this->_aFields['content'] = new Zend_Form_Element_Textarea('content');
        $this->_aFields['content']->setLabel('Treść')
                ->setRequired(true)
                ->setAttrib('class', 'ckeditor')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty', true, array('messages' => "Brak treści newsa"))
                 ->setDecorators($this->_aFormElementTableDecorator);


        $this->_aFields['display'] = new Zend_Form_Element_Text('display');
        $this->_aFields['display']->setLabel('Czas rozpoczęcia')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => "Podaj czas rozpoczęcia"))
                 ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Zapisz')
                    ->setDecorators($this->_aFormSubmitTableDecorator);

        $this->addElements($this->_aFields)
                ->setMethod(Zend_Form::METHOD_POST)
                        ->setDecorators($this->_aFormTableDecorator);
    }

}