<?php

class Admin_Form_Agreement extends Form_Abstract {

    public function __construct($sMode = null) {
        parent::__construct();

        $this->_aFields['owner'] = new Zend_Form_Element_Select('owner');
        $this->_aFields['owner']->setLabel('Właściciel')
                                         ->setDecorators($this->_aFormElementTableDecorator); 

        
        $this->_aFields['owners'] = new Zend_Form_Element_Hidden('owners');
        $this->_aFields['owners']->setDecorators(array('ViewHelper')); 
        
        
        $this->_aFields['pick_owner'] = new Zend_Form_Element_Button('pick_owner');
        $this->_aFields['pick_owner']->setLabel('Wybierz właściciela')
                                         ->setDecorators(array('ViewHelper'))
                                         ->setAttrib('class' , 'attachment')
                                         ->setAttrib('data', 'owner'); 
        
        $this->_aFields['add_owner'] = new Zend_Form_Element_Button('add_owner');
        $this->_aFields['add_owner']->setLabel('Dodaj nowego właściciela')
                                         ->setDecorators(array('ViewHelper'))
                                         ->setAttrib('class' , 'attachment')
                                         ->setAttrib('data', 'owner'); 
        
      
        
        $this->_aFields['postcode'] = new Zend_Form_Element_Text('postcode');
        $this->_aFields['postcode']->setLabel('Kod pocztowy')
                ->setDecorators($this->_aFormElementTableDecorator); 
        
        
        $this->_aFields['city'] = new Zend_Form_Element_Text('city');
        $this->_aFields['city']->setLabel('Miasto')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj kod pocztowy'))
                ->setDecorators($this->_aFormElementTableDecorator); 

        
        $this->_aFields['street'] = new Zend_Form_Element_Text('street');
        $this->_aFields['street']->setLabel('Ulica')
                                 ->setDecorators($this->_aFormElementTableDecorator); 
        

        $this->_aFields['house'] = new Zend_Form_Element_Text('house');
        $this->_aFields['house']->setLabel('Budynek')
                                ->setDecorators($this->_aFormElementTableDecorator); 
        

        $this->_aFields['flat'] = new Zend_Form_Element_Text('flat');
        $this->_aFields['flat']->setLabel('Numer mieszkania')
                                ->setDecorators($this->_aFormElementTableDecorator); 
        
        $this->_aFields['record_number'] = new Zend_Form_Element_Text('record_number');
        $this->_aFields['record_number']->setLabel('Numer działki')
                                        ->setDecorators($this->_aFormElementTableDecorator); 
        
        $this->_aFields['land_and_mortgage_register'] = new Zend_Form_Element_Text('land_and_mortgage_register');
        $this->_aFields['land_and_mortgage_register']->setLabel('Numer księgi wieczystej')
                ->setDecorators($this->_aFormElementTableDecorator); 
        

        $this->_aFields['agreement_type'] = new Zend_Form_Element_Text('agreement_type');
        $this->_aFields['agreement_type']->setLabel('Typ umowy')
                                         ->setDecorators($this->_aFormElementTableDecorator); 
        

                
        $this->_aFields['commission_precentage'] = new Zend_Form_Element_Text('commission_precentage');
        $this->_aFields['commission_precentage']->setLabel('Procent prowizji')
                                         ->setDecorators($this->_aFormElementTableDecorator); 
        

        
        $this->_aFields['commission_value'] = new Zend_Form_Element_Text('commission_value');
        $this->_aFields['commission_value']->setLabel('Wartość prowizji')
                                         ->setDecorators($this->_aFormElementTableDecorator); 
        

        
        $this->_aFields['agreement_date'] = new Zend_Form_Element_Text('agreement_date');
        $this->_aFields['agreement_date']->setLabel('Data zawarcia')
                                         ->setDecorators($this->_aFormElementTableDecorator); 
        

        
        $this->_aFields['agreement_end'] = new Zend_Form_Element_Text('agreement_end');
        $this->_aFields['agreement_end']->setLabel('Data zakończenia')
                                         ->setDecorators($this->_aFormElementTableDecorator); 
        

        
        $this->_aFields['information'] = new Zend_Form_Element_Textarea('information');
        $this->_aFields['information']->setLabel('Informacje dodatkowe')
                                         ->setDecorators($this->_aFormElementTableDecorator); 
        

        
        
        
        $this->_aFields['time'] = new Zend_Form_Element_Text('time');
        $this->_aFields['time']->setLabel('Godziny prezentacji')
                                         ->setDecorators($this->_aFormElementTableDecorator); 
        

        
         $this->_aFields['person'] = new Zend_Form_Element_Text('person');
         $this->_aFields['person']->setLabel('Prezenter')
                                         ->setDecorators($this->_aFormElementTableDecorator); 
        

        
         $this->_aFields['phone'] = new Zend_Form_Element_Text('phone');
         $this->_aFields['phone']->setLabel('Telefon prezentera')
                                         ->setDecorators($this->_aFormElementTableDecorator); 
        

        
         $this->_aFields['email'] = new Zend_Form_Element_Text('email');
         $this->_aFields['email']->setLabel('Email prezentera')
                                         ->setDecorators($this->_aFormElementTableDecorator); 
        

         $this->_aFields['files'] = new Zend_Form_Element_Hidden('files');
         $this->_aFields['files']->setDecorators(array('ViewHelper')); 
        
        
        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Zatwierdź')
                                         ->setDecorators($this->_aFormSubmitTableDecorator); 
                
                
        $this->addElements($this->_aFields)
             ->setDecorators($this->_aFormTableDecorator); 
    }
    
    public function isValid($aData)
    {
        $bStatus =  parent::isValid($aData); 
        
        return $bStatus ? $this->noOwnersErrorCheck($aData['owners'])  : $bStatus; 
        
        
    }
    
    private function noOwnersErrorCheck($sOwners = null)
    {
        if(empty($sOwners))
        {
            $this->getElement('owner')->addError('Musisz wybrać przynajmniej 1 właściciela'); 
            return false; 
        }
        else return true; 
    }

}
