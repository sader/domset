<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Offer extends Form_Abstract {
    
    const DISTRICT_DEFAULT_VALUE = 'ełcki';
    const CITY_DEFAULT_VALUE = 'Ełk';
    const COMMUNITY_DEFAULT_VALUE = 'Ełk';

    public function __construct($options = null) {
        parent::__construct($options);
        
        $this->setAttrib('id', 'offer')
                ->addElement($this->getTypeField())
                ->setDecorators($this->_aFormTableDecorator)
                ->populateSelect(Model_Logic_Enum::getInstance()->getOfferTypesList(), 'type', false, false); 
                
    }
    
    

    /**
     * Edycja pola według parametrów na wejściu
     * @param Zend_Form_Element $oElem
     * @param array $aParams
     * @return Zend_Form_Element 
     */
    public function modifyElemByInputParams(Zend_Form_Element $oElem, $aParams = array()) {
        if (isset($aParams['label']) && !empty($aParams['label'])) {
            $oElem->setLabel($aParams['label']);
        }
        
         if (isset($aParams['notrequired']) && !empty($aParams['notrequired'])) {
            $oElem->setRequired(false)
                  ->removeValidator('NotEmpty'); 
        }

        return $oElem;
    }

    public function setCategories($aCategories) {
               
        foreach ($aCategories as $iKey => $aCategory) {
            if ($aCategory['additional_data'] == Model_DbTable_Category::CATEGORY_HAS_ADDITIONAL_DATA) {
                
                $oAdditionalDataElement = new Zend_Form_Element_Hidden('additional_'.$aCategory['id']);
                $oAdditionalDataElement->setAttrib('class', 'add-extra-data')
                                       ->setAttrib('data', $aCategory['id'])
                                       ->setDecorators(array('ViewHelper')); 
              $this->addElement($oAdditionalDataElement);
            }
        }
        
        $this->populateSelect($aCategories, 'categories', array('id' => 'id', 'value' => 'name'), false); 
                

        return $this;
    }

    public function setOfferType($oOffer) {
        
        
        $this->addElements($oOffer->getFormElements());
        $this->getElement('type')->setValue($oOffer->getId()); 
        return $this;
    }

    public function setDefaultAutor($iId = null) {

        if ($iId !== null)
            $this->getElement('id_admin')->setValue($iId);
    }

    public function getTypeField($aParams = array()) {

        $oElem = new Zend_Form_Element_Select('type');
        $oElem->setLabel('Typ oferty')
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj typ'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->modifyElemByInputParams($oElem);
    }

    protected function getTitleField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('title');
        $oElem->setLabel('Tytuł')
                ->setRequired(true)
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj tytuł'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getAuthorField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('id_admin');
        $oElem->setLabel('Autor')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Wybierz autora'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getProvinceField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('province');
        $oElem->setRequired(true)
                ->setLabel('Województwo')
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj województwo'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getDistrictField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('district');
        $oElem->setRequired(true)
                ->setLabel('Powiat')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setValue(self::DISTRICT_DEFAULT_VALUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj powiat'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }
    
        protected function getCommunityField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('community');
        $oElem->setLabel('Gmina')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setValue(self::COMMUNITY_DEFAULT_VALUE)
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getCityField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('city');
        $oElem->setRequired(true)
                ->setLabel('Miasto')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setValue(self::CITY_DEFAULT_VALUE)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj miasto'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getSectionField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('section');
        $oElem->setRequired(true)
                ->setLabel('Dzielnica')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj dzielnicę'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getSectionForDefaultCity($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('section_selectable');
        $oElem->setDecorators(array('ViewHelper'))
                ->setAttrib('class', 'attachment')
                ->setAttrib('data', 'section'); 

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }
    
    protected function getNewSectionForDefaultCityField($aParams = array())
    {
        $oElem = new Zend_Form_Element_Button('add_section'); 
        $oElem->setLabel('Dodaj dzielnicę')
               ->setDecorators(array('ViewHelper'))
                ->setAttrib('class', 'attachment')
                ->setAttrib('data', 'section_selectable'); 
                
        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getStreetField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('street');
        $oElem->setLabel('Ulica')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getMarketField($aParams = array()) {

        $oElem = new Zend_Form_Element_Select('market');
        $oElem->setLabel('Rynek')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj rodzaj rynku'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getMarketVisibleField($aParams = array()) {
        $oElem = new Zend_Form_Element_Checkbox('market_visible');
        $oElem->setCheckedValue(1)
                ->setLabel('Rynek widoczny na stronie')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getSurfaceField($aParams = array()) {


        $oElem = new Zend_Form_Element_Text('surface');
        $oElem->setRequired(true)
                ->setLabel('Powierzchnia (m2)')
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj dzielnicę'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getPriceField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('price');
        $oElem->setRequired(true)
                ->setLabel('Cena')
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj cenę'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getPriceNegotiableField($aParams = array()) {

        $oElem = new Zend_Form_Element_Checkbox('price_negotiable');
        $oElem->setCheckedValue(1)
                ->setLabel('Cena do negocjacji')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getPricePerSurfaceField($aParams = array()) {

        $oElem = new Zend_Form_Element_Text('price_surface');
        $oElem->setLabel('Cena za 1m kw.')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getPriceRoundButton($aParams = array()) {
        $oElem = new Zend_Form_Element_Button('price_round');
        $oElem->setLabel('Zaokrąglenie')
                ->setattrib('class', 'attachment')
                ->setattrib('data', 'price_surface')
                ->setDecorators(array('ViewHelper'));

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getAdditionalField($aParams = array()) {
        $oElem = new Zend_Form_Element_MultiCheckbox('additional');
        $oElem->setLabel('Dodatkowe informacje')
                ->setDecorators($this->_aFormCheckboksTableDecorator);


        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getDescriptionField($aParams = array()) {
        $oElem = new Zend_Form_Element_Textarea('description');
        $oElem->setLabel('Opis')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getDescriptionPdfField($aParams = array()) {
        $oElem = new Zend_Form_Element_Textarea('description_pdf');
        $oElem->setLabel('Opis PDF')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getCategoriesField($aParams = array()) {

        $oElem = new Zend_Form_Element_MultiCheckbox('categories');
        $oElem->setLabel('Kategorie')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getRoomsField($aParams = array()) {

        $oElem = new Zend_Form_Element_Text('rooms');
        $oElem->setRequired(true)
                ->setLabel('Liczba pokoi')
                ->addFilters(array('StringTrim', 'HtmlSpecialchars'))
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj ilość pokoi'))
                ->addValidator('Digits', true, array('messages' => 'Niepoprawny format'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getFloorField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('floor');
        $oElem->setRequired(true)
                ->setLabel('Piętro')
                ->addFilters(array('StringTrim', 'HtmlSpecialchars'))
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj piętro'))
                ->addValidator('Digits', true, array('messages' => 'Niepoprawny format'))
                ->setDecorators($this->_aFormElementTableDecorator);
        
        

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

        protected function getFloorsField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('floors');
        $oElem->setRequired(true)
                ->setLabel('Ilość pięter')
                ->addFilters(array('StringTrim', 'HtmlSpecialchars'))
                ->addValidator('Digits', true, array('messages' => 'Niepoprawny format'))
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj ilość pięter'))
                ->setDecorators($this->_aFormElementTableDecorator);
    
        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }
    
    
    protected function getBuildingMaterialField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('building_material');
        $oElem->setLabel('Materiał')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getLevelsField($aParams = array()) {
        $oElem = new Zend_Form_element_Select('levels');
        $oElem->setLabel('Poziomy')
                ->addValidator('Digits', true, array('messages' => 'Niepoprawny format'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getFloorsInBuildingField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('floors_in_building');
        $oElem->setRequired(true)
                ->setLabel('Liczba pięter w budynku')
                ->addFilters(array('StringTrim', 'HtmlSpecialchars'))
                ->addValidator('Digits', true, array('messages' => 'Niepoprawny format'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getBuildingTypeField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('building_type');
        $oElem->setLabel('Typ budynku')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getYearField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('year');
        $oElem->setLabel('Rok budowy')
                ->addValidator('Date', true, array('messages' => 'Niepoprawny format', 'format' => 'yyyy'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getOwnershipField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('ownership');
        $oElem->setLabel('Forma własności')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getRentField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('rent');
        $oElem->setLabel('Czynsz')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getRentDesctriptionField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('rent_description');
        $oElem->setLabel('Informacje dodatkowe')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getQualityField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('quality');
        $oElem->setLabel('Stan')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getInstallationQualityField($aParams = array()) {

        $oElem = new Zend_Form_Element_Select('installation_quality');
        $oElem->setLabel('Stan instalacji')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getWindowsField($aParams = array()) {

        $oElem = new Zend_Form_Element_Select('windows');
        $oElem->setLabel('Okna')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getKitchenField($aParams = array()) {

        $oElem = new Zend_Form_Element_Select('kitchen');
        $oElem->setLabel('Kuchnia')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getNoiseField($aParams = array()) {

        $oElem = new Zend_Form_Element_Select('noise');
        $oElem->setLabel('Głośność')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getHeatField($aParams = array()) {

        $oElem = new Zend_Form_Element_Select('heat');
        $oElem->setLabel('Ogrzewanie')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getAvailableField($aParams = array()) {

        $oElem = new Zend_Form_Element_Text('available');
        $oElem->setLabel('Dostępne od')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getRentAdditionalField($aParams = array()) {

        $oElem = new Zend_Form_Element_Radio('rent_additional');
        $oElem->setLabel('Czynsz')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getCautionField($aParams = array()) {

        $oElem = new Zend_Form_Element_Text('caution');
        $oElem->setLabel('Kaucja zwrotna')
        ->setDecorators($this->_aFormElementTableDecorator);
        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getHouseSurfaceField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('house_surface');
        $oElem->setLabel('Powierzchnia zabudowy (m2)')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getTerrainSurfaceField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('terrain_surface');
        $oElem->setLabel('Powierzchnia działki (m2)')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj powierzchnię działki'))
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getAtticField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('attic');
        $oElem->setLabel('Poddasze')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getBasementField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('basement');
        $oElem->setLabel('Podpiwniczenie')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getRoofField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('roof');
        $oElem->setLabel('Dach')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getRoofTypeField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('roof_type');
        $oElem->setLabel('Pokrycie dachu')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getSewerageField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('sewerage');
        $oElem->setLabel('Kanalizacja')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getFenceField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('fence');
        $oElem->setLabel('Ogrodzenie')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getGarageField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('garage');
        $oElem->setLabel('Garaź')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getAccessField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('access');
        $oElem->setLabel('Dojazd')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getLandTypeField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('land_type');
        $oElem->setLabel('Typ działki')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getShapeField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('shape');
        $oElem->setLabel('Kształt')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getWidthField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('width');
        $oElem->setLabel('Szerokość')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getHeightField($aParams = array()) {
        $oElem = new Zend_Form_Element_Text('height');
        $oElem->setLabel('Wysokość lokalu')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getServicesField($aParams = array()) {
        $oElem = new Zend_Form_Element_MultiCheckbox('services');
        $oElem->setLabel('Media')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getPurposeField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('purpose');
        $oElem->setLabel('Przeznaczenie')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams)); 
    }

    protected function getEntryField($aParams = array()) {
        $oElem = new Zend_Form_Element_Select('entry');
        $oElem->setLabel('Wejście')
                ->setDecorators($this->_aFormElementTableDecorator);

        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }

    protected function getRentAdditionalPaymentField($aParams = array())
    {
        $oElem = new Zend_Form_Element_Text('additional_payment'); 
        $oElem->setDecorators(array('ViewHelper'))
              ->setAttrib('class', 'attachment')
              ->setAttrib('data', 'rent_additional-1'); 
              
        
        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }


    protected function getSubmitField($aParams = array())
    {
        $oElem = new Zend_Form_Element_Submit('submit'); 
        $oElem->setLabel('Dodaj')
               ->setDecorators($this->_aFormSubmitTableDecorator);
        
        return $this->addElement($this->modifyElemByInputParams($oElem, $aParams));
    }
    
    public function editMode()
    {
        $this->getElement('submit')->setLabel('Zatwierdź'); 
        return $this; 
    }

}