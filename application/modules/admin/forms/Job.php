<?php

class Admin_Form_Job extends Form_Abstract {

    public function init() {

        parent::init();

      
    
        
                $this->_aFields['status'] = new Zend_Form_Element_Select('status');
        $this->_aFields['status']->setLabel('Status')
                ->setDecorators($this->_aFormElementTableDecorator);
        
         

        $this->_aFields['date'] = new Zend_Form_Element_Text('date');
        $this->_aFields['date']->setLabel('Data zadania')
                ->setValue(date('Y-m-d'))
                ->setRequired(true)
                ->addValidator('NotEmpty' , true, array('messages' => 'Wymagana data zadania'))
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['time'] = new Zend_Form_Element_Text('time');
        $this->stickyElem($this->_aFields['time'], 'date');
        
    $this->_aFields['info'] = new Zend_Form_Element_Textarea('info');
        $this->_aFields['info']->setLabel('Informacje dodatkowe')
                               ->setAttrib('rows' , 6)
                               ->setAttrib('cols' , 48)
                ->setDecorators($this->_aFormElementTableDecorator);
        
          $this->_aFields['client_phone'] = new Zend_Form_Element_Text('client_phone');
          $this->_aFields['client_phone']->setLabel('Klient')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj klienta'))
                ->addValidator('StringLength', true, array('min' => Model_Logic_Client::CLIENT_PHONE_LENGHT_VALIDATE,
                                                           'max' => Model_Logic_Client::CLIENT_PHONE_LENGHT_VALIDATE, 
                                                           'messages' => 'Numer telefonu to '.Model_Logic_Client::CLIENT_PHONE_LENGHT_VALIDATE.' cyfr'))
                ->setDecorators($this->_aFormElementTableDecorator);
        
        $this->_aFields['offer_type'] = new Zend_Form_Element_Select('offer_type');
        $this->_aFields['offer_type']->setLabel('Typ oferty')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['page'] = new Zend_Form_Element_Text('page');
        $this->_aFields['page']->setLabel('Źródło')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['city'] = new Zend_Form_Element_Text('city');
        $this->_aFields['city']->setLabel('Miasto')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['section'] = new Zend_Form_Element_Text('section');
        $this->_aFields['section']->setLabel('Dzielnica')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['street'] = new Zend_Form_Element_Text('street');
        $this->_aFields['street']->setLabel('Ulica')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['surface'] = new Zend_Form_Element_Text('surface');
        $this->_aFields['surface']->setLabel('Powierzchnia')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['rooms'] = new Zend_Form_Element_Text('rooms');
        $this->_aFields['rooms']->setLabel('Ilość pokoi')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['floor'] = new Zend_Form_Element_Text('floor');
        $this->_aFields['floor']->setLabel('Piętro')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['price'] = new Zend_Form_Element_Text('price');
        $this->_aFields['price']->setLabel('Cena')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['price_surface'] = new Zend_Form_Element_Text('price_surface');
        $this->_aFields['price_surface']->setLabel('Cena za metr')
                ->setDecorators($this->_aFormElementTableDecorator);

      
        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Dodaj')
                ->setDecorators($this->_aFormSubmitTableDecorator);

        $this->addElements($this->_aFields)
                ->setDecorators($this->_aFormTableDecorator)
                ->setMethod(Zend_Form::METHOD_POST);


        $this->populateSelect(Model_Logic_Enum::getInstance()->getOfferTypesList(), 'offer_type', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getJobResultList(), 'status', false, false);
    }
    
    public function editMode()
    {
        $this->getElement('submit')->setLabel('Zapisz'); 
        $this->removeElement('client_phone'); 
        return true; 
    }

}

