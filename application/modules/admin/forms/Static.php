<?php

class Admin_Form_Static extends Form_Abstract {

    public function init() {

        parent::init();

        $this->_aFields['name'] = new Zend_Form_Element_Text('name');
        $this->_aFields['name']->setLabel('Adres')
                ->setRequired(true)
                ->setAttrib('class', 'input-xxlarge')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->addValidator('Regex' , true, array('pattern' => '/^[a-z0-9\-_]+$/' , 
                                                     'messages' => 'Nawa url strony powinna posiadać jedynie małe litery, cyfry oraz znaki - i _'))
                ->addValidator('NotEmpty', true, array('messages' => "Podaj adres strony"))
                ->addValidator('Db_NoRecordExists', true, array('table' => 'pages',
                    'field' => 'name',
                    'messages' => 'Strona o podaniej nazwie istnieje'))
                ->setDecorators($this->_aFormElementTableDecorator);

         $this->_aFields['head'] = new Zend_Form_Element_Text('head');
        $this->_aFields['head']->setLabel('Nagłówek')
                ->setRequired(true)
                ->setAttrib('class', 'input-xxlarge')
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setDecorators($this->_aFormElementTableDecorator);


        $this->_aFields['content'] = new Zend_Form_Element_Textarea('content');
        $this->_aFields['content']->setLabel('Treść')
                ->setRequired(true)
                ->setAttrib('class', 'ckeditor')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty', true, array('messages' => "Brak treści"))
                ->setDecorators($this->_aFormElementTableDecorator);



        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Zapisz')
                                 ->setDecorators($this->_aFormSubmitTableDecorator);

        $this->addElements($this->_aFields)
             ->setMethod(Zend_Form::METHOD_POST)
             ->setDecorators($this->_aFormTableDecorator);
    }

      public function editMode($iId) {
        $this->getElement('name')
                ->removeValidator('Db_NoRecordExists')
                ->addValidator('Db_NoRecordExists', true, array('table' => 'pages',
                    'field' => 'name',
                    'exclude' => array('field' => 'id' , 'value' => $iId),
                    'messages' => 'Strona o podaniej nazwie istnieje'));
    }

}