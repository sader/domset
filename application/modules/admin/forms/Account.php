<?php

class Admin_Form_Account extends Form_Abstract {

    public function __construct($sMode = null) {
        parent::__construct();


        $this->_aFields['login'] = new Zend_Form_Element_Text('login');
        $this->_aFields['login']->setLabel('Login')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj login'))
                ->setDecorators($this->_aFormElementTableDecorator)
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 50))
                ->addValidator('Db_NoRecordExists', true, array('table' => 'admin', 'field' => 'login', 'messages' => 'Taki login już istnieje'))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');

        $this->_aFields['password'] = new Zend_Form_Element_Password('password');
        $this->_aFields['password']->setLabel('Hasło')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj hasło'))
                ->setDecorators($this->_aFormElementTableDecorator)
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 20, 'min' => 8))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');

        $this->_aFields['repassword'] = new Zend_Form_Element_Password('repassword');
        $this->_aFields['repassword']->setLabel('Powtórz hasło')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj powtórzenie hasła'))
                ->setDecorators($this->_aFormElementTableDecorator)
                ->addValidator('Identical', true, array('token' => 'password', 'messages' => 'Powtórzenie nie zgadza się'))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');

        $this->_aFields['name'] = new Zend_Form_Element_Text('name');
        $this->_aFields['name']->setLabel('Imię')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj imię'))
                ->setDecorators($this->_aFormElementTableDecorator)
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 50))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');

        $this->_aFields['surname'] = new Zend_Form_Element_Text('surname');
        $this->_aFields['surname']->setLabel('Nazwisko')
                ->setRequired(true)
                ->setDecorators($this->_aFormElementTableDecorator)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj nazwisko'))
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 50))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');


        $this->_aFields['email'] = new Zend_Form_Element_Text('email');
        $this->_aFields['email']->setLabel('Email')
                ->setRequired(true)
                ->setDecorators($this->_aFormElementTableDecorator)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj email'))
                ->addValidator('CorrectEmail', true, array('messages' => 'Niepoprawny format adresu email'))
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 50))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');

        $this->_aFields['phone'] = new Zend_Form_Element_Text('phone');
        $this->_aFields['phone']->setLabel('Telefon')
                ->setRequired(true)
                ->setDecorators($this->_aFormElementTableDecorator)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj telefon'))
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 50))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');
        
        $this->_aFields['type'] = new Zend_Form_Element_Select('type'); 
        $this->_aFields['type']->setLabel('Typ użytkownika')
                               ->setDecorators($this->_aFormElementTableDecorator); 
        

        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Dodaj')
                ->setDecorators($this->_aFormSubmitTableDecorator);

        $this->addElements($this->_aFields)
                ->setDecorators($this->_aFormTableDecorator)
                ->setMethod(Zend_Form::METHOD_POST);
    }

    public function setEditMode($iEditedRecordId, $aData = array()) {
        $this->getElement('submit')->setLabel('Zatwierdź');
        $this->removeElement('password');
        $this->removeElement('repassword');
        $this->getElement('login')
                ->removeValidator('Db_NoRecordExists')
                ->addValidator('Db_NoRecordExists', true, array('table' => 'admin',
                    'field' => 'login',
                    'exclude' => 'id !=' . $iEditedRecordId,
                    'messages' => 'Taki login jest już zajęty'));
        
        $this->populate($aData); 
        
    }

    public function setChangePasswordMode() {
        $this->removeElement('login');
        $this->removeElement('name');
        $this->removeElement('surname');
        $this->removeElement('email');
        $this->removeElement('phone');
        $this->removeElement('type'); 
        $this->getElement('submit')->setLabel('Zmień hasło');
    }

}
