<?php

class Admin_Form_Section extends Form_Abstract {

    public function __construct($sMode = null) {
        parent::__construct();


        $this->_aFields['section'] = new Zend_Form_Element_Text('section');
        $this->_aFields['section']->setLabel('Nowa dzielnica')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj nazwę dzielnicy'))
                ->setDecorators($this->_aFormElementTableDecorator)
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 50))
                ->addValidator('Db_NoRecordExists', true, array('table' => 'section', 'field' => 'name', 'messages' => 'Taka dzielnica już istnieje'))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars');

        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Dodaj')
                ->setAttrib('id', 'addSectionSubmit')
                ->setDecorators($this->_aFormSubmitTableDecorator);

        $this->addElements($this->_aFields)
                ->setAttrib('id', 'sectionForm')
                ->setDecorators($this->_aFormTableDecorator)
                ->setMethod(Zend_Form::METHOD_POST);
    }

}
