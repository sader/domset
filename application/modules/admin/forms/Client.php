<?php

class Admin_Form_Client extends Form_Abstract {

    public function __construct($sMode = null) {
        parent::__construct();

        
        $this->_aFields['type'] = new Zend_Form_Element_MultiCheckbox('type');
        $this->_aFields['type']->setLabel('Typ klienta')
                ->setDecorators($this->_aFormElementTableDecorator);

         $this->_aFields['phone'] = new Zend_Form_Element_Text('phone');
        $this->_aFields['phone']->setLabel('Telefon')
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 9))
                ->addValidator('Db_NoRecordExists', true, array('table' => 'client' , 
                                                                'field' => 'phone' ,  
                                                                'messages' => 'Taki telefon istnieje'))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj telefon'))
                ->setDecorators($this->_aFormElementTableDecorator);

        
        
        
        $this->_aFields['name'] = new Zend_Form_Element_Text('name');
        $this->_aFields['name']->setLabel('Imię')
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 50))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['surname'] = new Zend_Form_Element_Text('surname');
        $this->_aFields['surname']->setLabel('Nazwisko')
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 50))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['parents'] = new Zend_Form_Element_Text('parents');
        $this->_aFields['parents']->setLabel('Imiona rodziców')
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 100))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setDecorators($this->_aFormElementTableDecorator);


        $this->_aFields['email'] = new Zend_Form_Element_Text('email');
        $this->_aFields['email']->setLabel('Email')
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 100))
                ->addValidator('CorrectEmail', true, array('messages' => 'Niepoprawny email'))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setDecorators($this->_aFormElementTableDecorator);

       

        $this->_aFields['phone_alt'] = new Zend_Form_Element_Text('phone_alt');
        $this->_aFields['phone_alt']->setLabel('Telefon dodatkowy')
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 9))
                ->addFilter('StringTrim')
                ->addValidator('Db_NoRecordExists', true, array('table' => 'client' , 
                                                                'field' => 'phone' ,  
                                                                'messages' => 'Taki telefon istnieje'))
                
                ->addFilter('HtmlSpecialchars')
                ->setDecorators($this->_aFormElementTableDecorator);


        $this->_aFields['postcode'] = new Zend_Form_Element_Text('postcode');
        $this->_aFields['postcode']->setLabel('Kod pocztowy')
                ->setDecorators($this->_aFormElementTableDecorator);


        $this->_aFields['city'] = new Zend_Form_Element_Text('city');
        $this->_aFields['city']->setLabel('Miasto')
                ->setDecorators($this->_aFormElementTableDecorator);


        $this->_aFields['street'] = new Zend_Form_Element_Text('street');
        $this->_aFields['street']->setLabel('Ulica')
                ->setDecorators($this->_aFormElementTableDecorator);


        $this->_aFields['house'] = new Zend_Form_Element_Text('house');
        $this->_aFields['house']->setLabel('Budynek')
                ->setDecorators($this->_aFormElementTableDecorator);


        $this->_aFields['flat'] = new Zend_Form_Element_Text('flat');
        $this->_aFields['flat']->setLabel('Numer mieszkania')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['docid'] = new Zend_Form_Element_Text('docid');
        $this->_aFields['docid']->setLabel('Dowód osobisty')
                ->setDecorators($this->_aFormElementTableDecorator);


        $this->_aFields['pesel'] = new Zend_Form_Element_Text('pesel');
        $this->_aFields['pesel']->setLabel('Pesel')
                ->setDecorators($this->_aFormElementTableDecorator);

        $this->_aFields['info'] = new Zend_Form_Element_Textarea('info');
        $this->_aFields['info']->setLabel('Informacje dodatkowe')
                ->setDecorators($this->_aFormElementTableDecorator);


        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Dodaj')
                ->setAttrib('id', 'clientSubmit')
                ->setDecorators($this->_aFormSubmitTableDecorator);

        $this->addElements($this->_aFields)
                ->setAttrib('id', 'clientForm')
                ->setDecorators($this->_aFormTableDecorator)
                ->setMethod(Zend_Form::METHOD_POST);

        $this->populateSelect(Model_Logic_Client::getClientTypes(), 'type', false, false);
    }

    public function editMode() {

        $this->getElement('submit')->setLabel('Zatwierdź');
        $this->getElement('phone')->removeValidator('Db_NoRecordExists'); 
        $this->getElement('phone_alt')->removeValidator('Db_NoRecordExists'); 
    }

}
