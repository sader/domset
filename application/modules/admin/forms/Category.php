<?php

class Admin_Form_Category extends Form_Abstract {

    public function __construct($sMode = null) {
        parent::__construct();



        $this->_aFields['name'] = new Zend_Form_Element_Text('name');
        $this->_aFields['name']->setLabel('Nazwa kategorii')
                ->setRequired(true)
                ->addValidator('StringLength', true, array('encoding' => 'utf-8', 'max' => 100))
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj imię'))
                ->addValidator('Db_NoRecordExists', true, array('table' => 'category' , 'field' => 'name' , 'messages' => 'Ta nazwa już istnieje'))
                ->addFilter('StringTrim')
                ->addFilter('HtmlSpecialchars')
                ->setDecorators($this->_aFormElementTableDecorator);

        
        $this->_aFields['additional_data'] = new Zend_Form_Element_Checkbox('additional_data');
        $this->_aFields['additional_data']->setLabel('Kategoria ma parametr ?')
                ->setDecorators($this->_aFormElementTableDecorator);


        $this->_aFields['info'] = new Zend_Form_Element_Textarea('info');
        $this->_aFields['info']->setLabel('Informacje dodatkowe')
                                 ->setAttribs(array('rows' => 10, 'cols' => 60))
                                 ->setDecorators($this->_aFormElementTableDecorator);
       
        
        $this->_aFields['type'] = new Zend_Form_Element_MultiCheckbox('type');
        $this->_aFields['type']->setDecorators($this->_aFormCheckboksTableDecorator)
                               ->setRequired(true)
                               ->addValidator('NotEmpty' , true, array('messages' => 'Zaznacz przynajmniej 1 opcję'));
       

        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Dodaj')
                                 ->setAttrib('id', 'clientSubmit')
                                 ->setDecorators($this->_aFormSubmitTableDecorator);

        $this->addElements($this->_aFields)
                 ->setAttrib('id', 'clientForm')
                ->setDecorators($this->_aFormTableDecorator)
                ->setMethod(Zend_Form::METHOD_POST);
        
        $this->populateSelect(Model_Logic_Offer::getInstance()->getOfferTypeNames(), 'type', false, false); 
        
    }

    public function editMode() {
        
        $this->getElement('submit')->setLabel('Zatwierdź'); 
        $this->getElement('name')->removeValidator('Db_NoRecordExists'); 
    }

}
