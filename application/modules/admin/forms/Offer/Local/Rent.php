<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Offer_Local_Rent extends Admin_Form_Offer {

    public function __construct($options = null) {
        parent::__construct($options);
        $this->buildForm();
    }

    public function buildForm() {

        $this->getTitleField()
                ->getAuthorField()
                ->populateSelect(Model_DbTable_Admin::getInstance()->getListOfAdmins(), 'id_admin', array('id' => 'id', 'value' => 'login'), false)
                ->getProvinceField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getProvinceList(), 'province', false, false)
                ->getDistrictField()
                ->getCommunityField()
                ->getCityField()
                ->getSectionField()
                ->getSectionForDefaultCity()
                ->populateSelect(Model_DbTable_Section::getInstance()->getListOfSectionForDefaultCity(), 'section_selectable', array('id' => 'id', 'value' => 'name'), false)
                ->getNewSectionForDefaultCityField()
                ->getStreetField()
                ->getPurposeField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getLocalPurposeList(), 'purpose', false, true)
                ->getSurfaceField()
                ->getPriceField()
                ->getPriceNegotiableField()
                ->getPricePerSurfaceField()
                ->getPriceRoundButton()
                ->getRentAdditionalField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getRentList(), 'rent_additional', false, false)
                ->getRentAdditionalPaymentField()
                ->getBuildingTypeField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getLocalTypeList(), 'building_type', false, true)
                ->getEntryField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getEntryList(), 'entry', false, true)
                ->getHeightField()
                ->getRoomsField(array('label' => 'Liczba pomieszczeń' , 'notrequired' => true))
                ->getFloorField(array('notrequired' => true))
                ->getLevelsField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getLevelsList(), 'levels', false, true)
                ->getFloorsInBuildingField()
                ->getYearField()
                ->getQualityField(array('label' => "Stan lokalu"))
                ->populateSelect(Model_Logic_Enum::getInstance()->getQualityList(), 'quality', false, true)
                ->getWindowsField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getWindowList(), 'windows', false, true)
                ->getHeatField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getFlatHeatList(), 'heat', false, true)
                ->getAvailableField()
                ->getAdditionalField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getLocalAdditionalList(), 'additional', false, false)
                ->getDescriptionField()
                ->getDescriptionPdfField()
                ->getCategoriesField()
                ->setCategories(Model_DbTable_Category::getInstance()->getByType(Model_Logic_Offer_Local_Rent::ID))
                ->getSubmitField();
    }

}