<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Offer_Local_Filter extends Form_Abstract {

    public function __construct($iType = Model_Logic_Offer_Local_Sell::ID) {
        parent::__construct(null);


        $this->_aFields['city'] = new Zend_Form_Element_Text('city');
        $this->_aFields['city']->setLabel('Miasto')
                ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['street'] = new Zend_Form_Element_Text('street');
        $this->_aFields['street']->setLabel('Ulica')
                ->setDecorators($this->_aFormElementInlineDecorator);

        $this->_aFields['year_from'] = new Zend_Form_Element_Text('year_from');
        $this->_aFields['year_from']->setLabel('Rok budowy od / do')
                ->setDecorators($this->_aFormElementInlineDecorator)
                ->setAttrib('width', '50px !important');
        $this->_aFields['year_to'] = new Zend_Form_Element_Text('year_to');
        $this->_aFields['year_to']->setAttrib('width', '50px !important');

        $this->stickyElem($this->_aFields['year_to'], 'year_from');
        $this->_aFields['entry'] = new Zend_Form_Element_Multiselect('entry');
        $this->_aFields['entry']->setLabel('Dojazd')
                ->setDecorators($this->_aFormElementInlineDecorator);

        $this->_aFields['height_from'] = new Zend_Form_Element_Text('height_from');
        $this->_aFields['height_from']->setLabel('Rok budowy od / do')
                ->setDecorators($this->_aFormElementInlineDecorator)
                ->setAttrib('width', '50px !important');
        $this->_aFields['height_to'] = new Zend_Form_Element_Text('height_to');
        $this->_aFields['height_to']->setAttrib('width', '50px !important');

        $this->_aFields['categories'] = new Zend_Form_Element_MultiCheckbox('categories');
        $this->_aFields['categories']->setLabel('Kategorie')
                ->setDecorators($this->_aFormElementInlineBigDecorator);


        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Filtruj')
                ->setDecorators($this->_aFormSubmitInlineDecorator);
        $this->addElements($this->_aFields)
                ->setDecorators($this->_aFormInlineDecorator)
                ->setMethod(Zend_Form::METHOD_GET);

        $this->populateSelect(Model_Logic_Enum::getInstance()->getEntryList(), 'entry', false, false);
        $this->populateSelect(Model_DbTable_Category::getInstance()->getListOfCategories(), 'categories', array('id' => 'id', 'value' => 'name'), false);
    }

}