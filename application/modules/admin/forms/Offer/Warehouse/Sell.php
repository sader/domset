<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Offer_Warehouse_Sell extends Admin_Form_Offer {

    public function __construct($options = null) {
        parent::__construct($options);
        $this->buildForm();
    }

    public function buildForm() {

        $this->getTitleField()
                ->getAuthorField()
                ->populateSelect(Model_DbTable_Admin::getInstance()->getListOfAdmins(), 'id_admin', array('id' => 'id', 'value' => 'login'), false)
                ->getProvinceField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getProvinceList(), 'province', false, false)
                ->getDistrictField()
                ->getCommunityField()
                ->getCityField()
                ->getSectionField()
                ->getSectionForDefaultCity()
                ->populateSelect(Model_DbTable_Section::getInstance()->getListOfSectionForDefaultCity(), 'section_selectable', array('id' => 'id', 'value' => 'name'), false)
                ->getNewSectionForDefaultCityField()
                ->getStreetField()
                ->getMarketField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getMarketList(), 'market', false, false)
                ->getMarketVisibleField()
                ->getPurposeField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getWarehousePurposeList(), 'purpose', false, true)
                ->getSurfaceField()
                ->getPriceField()
                ->getPriceNegotiableField()
                ->getPricePerSurfaceField()
                ->getPriceRoundButton()
                ->getRoomsField(array('label' => 'Liczba pomieszczeń'))
                ->getHeightField()
                ->getFloorField()
                ->getLevelsField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getLevelsList(), 'levels' , false, true)
                ->getYearField()
                ->getQualityField(array('label' => 'Stan'))
                ->populateSelect(Model_Logic_Enum::getInstance()->getQualityList(), 'quality', false, true)
                ->getHeatField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getWarehouseHeatList(), 'heat', false, true)
                ->getAdditionalField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getWarehouseAdditionalList(), 'additional', false, false)
                ->getDescriptionField()
                ->getDescriptionPdfField()
                ->getCategoriesField()
                ->setCategories(Model_DbTable_Category::getInstance()->getByType(Model_Logic_Offer_Warehouse_Sell::ID))
                ->getSubmitField();
    }

}