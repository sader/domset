<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Offer_Land_Filter extends Form_Abstract {

    public function __construct($iType = Model_Logic_Offer_Land_Sell::ID) {
        parent::__construct(null);


        $this->_aFields['province'] = new Zend_Form_Element_Multiselect('province');
        $this->_aFields['province']->setLabel('Wojewodztwo')
                                ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['access'] = new Zend_Form_Element_Multiselect('access');
        $this->_aFields['access']->setLabel('Dojazd')
                                ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['services'] = new Zend_Form_Element_MultiCheckbox('services');
        $this->_aFields['services']->setLabel('Media')
                                    ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['categories'] = new Zend_Form_Element_MultiCheckbox('categories');
        $this->_aFields['categories']->setLabel('Kategorie')
                                     ->setDecorators($this->_aFormElementInlineBigDecorator);
        
        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Filtruj')
                                ->setDecorators($this->_aFormSubmitInlineDecorator);
                $this->addElements($this->_aFields)
                     ->setDecorators($this->_aFormInlineDecorator)
                      ->setMethod(Zend_Form::METHOD_GET);

        $this->populateSelect(Model_Logic_Enum::getInstance()->getProvinceList(), 'province', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getAccessList(), 'access', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getServicesList(), 'services', false, false);
        $this->populateSelect(Model_DbTable_Category::getInstance()->getListOfCategories(), 'categories', array('id' => 'id' , 'value' => 'name'), false);
    }

}
