<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Offer_Land_Sell extends Admin_Form_Offer {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->buildForm();
        
    }

    public function buildForm() {

        $this->getTitleField()
        ->getAuthorField()
        ->populateSelect(Model_DbTable_Admin::getInstance()->getListOfAdmins(), 'id_admin', array('id' => 'id', 'value' => 'login'), false) 
        ->getProvinceField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getProvinceList(), 'province', false, false) 
        ->getDistrictField()
        ->getCommunityField()
        ->getCityField()
        ->getSectionField()
        ->getSectionForDefaultCity()
        ->populateSelect(Model_DbTable_Section::getInstance()->getListOfSectionForDefaultCity(), 'section_selectable', array('id' => 'id', 'value' => 'name'), false)
        ->getNewSectionForDefaultCityField()
        ->getStreetField()
        ->getSurfaceField()
        ->getPriceField()
        ->getPriceNegotiableField()
        ->getPricePerSurfaceField()
        ->getPriceRoundButton()
        ->getLandTypeField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getLandTypeList(), 'land_type', false, true) 
        ->getShapeField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getShapeList(), 'shape', false, true) 
        ->getWidthField()
        ->getHeightField(array('label' => 'Długość'))
        ->getServicesField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getServicesList(), 'services', false, false) 
        ->getFenceField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getFenceList(), 'fence', false, true) 
        ->getAccessField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getAccessList(), 'access', false, true) 
        ->getAdditionalField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getLandAdditionalList(), 'additional', false, false) 
        ->getDescriptionField()
        ->getDescriptionPdfField()
        ->getCategoriesField()
        ->setCategories(Model_DbTable_Category::getInstance()->getByType(Model_Logic_Offer_Land_Sell::ID))
        ->getSubmitField(); 

    }

  
}