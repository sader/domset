<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Offer_Flat_Rent extends Admin_Form_Offer {

    public function __construct($options = null) {
        parent::__construct($options);
        $this->buildForm();
    }

    public function buildForm() {

        $this->getTitleField()
                ->getAuthorField()
                ->populateSelect(Model_DbTable_Admin::getInstance()->getListOfAdmins(), 'id_admin', array('id' => 'id', 'value' => 'login'), false)
                ->getProvinceField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getProvinceList(), 'province', false, false)
                ->getDistrictField()
                ->getCommunityField()
                ->getCityField()
                ->getSectionField()
                ->getSectionForDefaultCity()
                ->populateSelect(Model_DbTable_Section::getInstance()->getListOfSectionForDefaultCity(), 'section_selectable', array('id' => 'id', 'value' => 'name'), false)
                ->getNewSectionForDefaultCityField()
                ->getStreetField()
                ->getSurfaceField()
                ->getPriceField()
                ->getPriceNegotiableField()
                ->getPricePerSurfaceField()
                ->getPriceRoundButton()
                ->getRentAdditionalField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getRentList(), 'rent_additional', false, false)
                ->getRentAdditionalPaymentField()
                ->getCautionField()
                ->getRoomsField()
                ->getFloorField()
                ->getLevelsField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getLevelsList(), 'levels', false, true)
                ->getFloorsInBuildingField()
                ->getBuildingTypeField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getBuildingTypeList(), 'building_type', false, true)
                ->getYearField()
                ->getQualityField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getQualityList(), 'quality', false, true)
                ->getWindowsField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getWindowList(), 'windows', false, true)
                ->getKitchenField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getKitchenTypeList(), 'kitchen', false, true)
                ->getNoiseField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getNoiseList(), 'noise', false, true)
                ->getHeatField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getFlatHeatList(), 'heat', false, true)
                ->getAvailableField()
                ->getAdditionalField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getFlatAdditionalRentList(), 'additional', false, false)
                ->getDescriptionField()
                ->getDescriptionPdfField()
                ->getCategoriesField()
                ->setCategories(Model_DbTable_Category::getInstance()->getByType(Model_Logic_Offer_Flat_Rent::ID))
                ->getSubmitField();
    }

}
