<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Offer_Flat_Sell extends Admin_Form_Offer {

    public function __construct($options = null) {
        parent::__construct($options);
        $this->buildForm();
    }

    public function buildForm() {

        $this->getTitleField()
                ->getAuthorField()
                ->populateSelect(Model_DbTable_Admin::getInstance()->getListOfAdmins(), 'id_admin', array('id' => 'id', 'value' => 'login'), false)
                ->getProvinceField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getProvinceList(), 'province', false, false)
                ->getDistrictField()
                ->getCommunityField()
                ->getCityField()
                ->getSectionField()
                ->getSectionForDefaultCity()
                ->populateSelect(Model_DbTable_Section::getInstance()->getListOfSectionForDefaultCity(), 'section_selectable', array('id' => 'id', 'value' => 'name'), false)
                ->getNewSectionForDefaultCityField()
                ->getStreetField()
                ->getMarketField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getMarketList(), 'market', false, false)
                ->getMarketVisibleField()
                ->getSurfaceField()
                ->getPriceField()
                ->getPriceNegotiableField()
                ->getPricePerSurfaceField()
                ->getPriceRoundButton()
                ->getRoomsField()
                ->getFloorField()
                ->getLevelsField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getLevelsList(), 'levels', false, true)
                ->getFloorsInBuildingField()
                ->getBuildingTypeField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getBuildingTypeList(), 'building_type', false, true)
                ->getYearField()
                ->getBuildingMaterialField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getFlatMaterialTypeList(), 'building_material', false, true)
                ->getOwnershipField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getOwnershipList(), 'ownership', false, true)
                ->getRentField()
                ->getRentDesctriptionField()
                ->getQualityField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getQualityList(), 'quality', false, true)
                ->getInstallationQualityField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getInstallationStateList(), 'installation_quality', false, true)
                ->getWindowsField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getWindowList(), 'windows', false, true)
                ->getKitchenField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getKitchenTypeList(), 'kitchen', false, true)
                ->getNoiseField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getNoiseList(), 'noise', false, true)
                ->getHeatField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getFlatHeatList(), 'heat', false, true)
                ->getAvailableField()
                ->getAdditionalField()
                ->populateSelect(Model_Logic_Enum::getInstance()->getFlatAdditionalSellList(), 'additional', false, true)
                ->getDescriptionField()
                ->getDescriptionPdfField()
                ->getCategoriesField()
                ->setCategories(Model_DbTable_Category::getInstance()->getByType(Model_Logic_Offer_Flat_Sell::ID))
                ->getSubmitField();
    }

}
