<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Offer_Flat_Filter extends Form_Abstract {

    public function __construct($iType = Model_Logic_Offer_Flat_Sell::ID) {
        parent::__construct(null);


        $this->_aFields['province'] = new Zend_Form_Element_Multiselect('province');
        $this->_aFields['province']->setLabel('Wojewodztwo')
                ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['district'] = new Zend_Form_Element_Text('district');
        $this->_aFields['district']->setLabel("Powiat")
                ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['city'] = new Zend_Form_Element_Text('city');
        $this->_aFields['city']->setLabel('Miasto')
                ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['quality'] = new Zend_Form_Element_Multiselect('quality');
        $this->_aFields['quality']->setLabel('Stan techniczny')
                ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['year_from'] = new Zend_Form_Element_Text('year_from');
        $this->_aFields['year_from']->setLabel('Rok budowy od / do')
                ->setDecorators($this->_aFormElementInlineDecorator)
                ->setAttrib('width', '50px !important');
        $this->_aFields['year_to'] = new Zend_Form_Element_Text('year_to');
        $this->_aFields['year_to']->setAttrib('width', '50px !important');

        $this->stickyElem($this->_aFields['year_to'], 'year_from');

        $this->_aFields['building_type'] = new Zend_Form_Element_Multiselect('building_type');
        $this->_aFields['building_type']->setLabel('Typ budynku')
                ->setDecorators($this->_aFormElementInlineDecorator);

        if ($iType == Model_Logic_Offer_Flat_Sell::ID) {
            $this->_aFields['building_material'] = new Zend_Form_Element_Multiselect('building_material');
            $this->_aFields['building_material']->setLabel('Materiał')
                    ->setDecorators($this->_aFormElementInlineDecorator);
        }

        $this->_aFields['kitchen'] = new Zend_Form_Element_Multiselect('kitchen');
        $this->_aFields['kitchen']->setLabel('Kuchnia')
                ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['levels'] = new Zend_Form_Element_Text('levels');
        $this->_aFields['levels']->setLabel('Poziomy')
                ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['heat'] = new Zend_Form_Element_Multiselect('heat');
        $this->_aFields['heat']->setLabel('Ogrzewanie')
                ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['additional'] = new Zend_Form_Element_MultiCheckbox('additional');
        $this->_aFields['additional']->setLabel('Opcje dodatkowe')
                ->setDecorators($this->_aFormElementInlineBigDecorator);
        $this->_aFields['categories'] = new Zend_Form_Element_MultiCheckbox('categories');
        $this->_aFields['categories']->setLabel('Kategorie')
                ->setDecorators($this->_aFormElementInlineBigDecorator);

        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Filtruj')
                ->setDecorators($this->_aFormSubmitInlineDecorator);
        $this->addElements($this->_aFields)->setMethod(Zend_Form::METHOD_GET)->setDecorators($this->_aFormInlineDecorator);

        $this->populateSelect(Model_Logic_Enum::getInstance()->getProvinceList(), 'province', false, true);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getQualityList(), 'quality', false, true);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getBuildingTypeList(), 'building_type', false, true);


        if ($iType == Model_Logic_Offer_Flat_Sell::ID) {
            $this->populateSelect(Model_Logic_Enum::getInstance()->getFlatMaterialTypeList(), 'building_material', false, true);
            $this->populateSelect(Model_Logic_Enum::getInstance()->getFlatAdditionalSellList(), 'additional', false, true);
        } else {
            $this->populateSelect(Model_Logic_Enum::getInstance()->getFlatAdditionalRentList(), 'additional', false, true);
        }
        $this->populateSelect(Model_Logic_Enum::getInstance()->getKitchenTypeList(), 'kitchen', false, true);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getFlatHeatList(), 'heat', false, true);

        $this->populateSelect(Model_DbTable_Category::getInstance()->getListOfCategories(), 'categories', array('id' => 'id', 'value' => 'name'), true);
    }

}
