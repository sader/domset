<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Offer_House_Filter extends Form_Abstract {

    public function __construct($iType = Model_Logic_Offer_House_Sell::ID) {
        parent::__construct(null);


        $this->_aFields['province'] = new Zend_Form_Element_Multiselect('province');
        $this->_aFields['province']->setLabel('Wojewodztwo')
                                    ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['district'] = new Zend_Form_Element_Text('district');
        $this->_aFields['district']->setLabel("Powiat")
                                    ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['section'] = new Zend_Form_Element_Text('section');
        $this->_aFields['section']->setLabel('Dzielnica')
                                    ->setDecorators($this->_aFormElementInlineDecorator);
        
        $this->_aFields['rooms_from'] = new Zend_Form_Element_Text('rooms_from');
        $this->_aFields['rooms_from']->setLabel('Pokoje od / do')
                ->setDecorators($this->_aFormElementInlineDecorator)
                ->setAttrib('width' , '50px !important'); 
        
        $this->_aFields['rooms_to'] = new Zend_Form_Element_Text('rooms_to');
        $this->_aFields['rooms_to']->setAttrib('width' , '50px !important'); 
        
                $this->stickyElem($this->_aFields['rooms_to'], 'rooms_from'); 
        
        $this->_aFields['quality'] = new Zend_Form_Element_Multiselect('quality');
        $this->_aFields['quality']->setLabel('Stan domu')
                                    ->setDecorators($this->_aFormElementInlineDecorator);
        
         $this->_aFields['year_from'] = new Zend_Form_Element_Text('year_from');
        $this->_aFields['year_from']->setLabel('Rok budowy od / do')
                ->setDecorators($this->_aFormElementInlineDecorator)
                ->setAttrib('width' , '50px !important'); 
        $this->_aFields['year_to'] = new Zend_Form_Element_Text('year_to');
        $this->_aFields['year_to']->setAttrib('width' , '50px !important'); 
        
        $this->stickyElem($this->_aFields['year_to'], 'year_from'); 
        
        
        $this->_aFields['sewerage'] = new Zend_Form_Element_Multiselect('sewerage');
        $this->_aFields['sewerage']->setLabel('Kanalizacja')
                                    ->setDecorators($this->_aFormElementInlineBigDecorator);
        $this->_aFields['building_material'] = new Zend_Form_Element_Multiselect('building_material');
        $this->_aFields['building_material']->setLabel('Materiał')
                                             ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['basement'] = new Zend_Form_Element_Multiselect('basement');
        $this->_aFields['basement']->setLabel('Podpiwniczenie')
                                    ->setDecorators($this->_aFormElementInlineDecorator);
        $this->_aFields['heat'] = new Zend_Form_Element_Multiselect('heat');
        $this->_aFields['heat']->setLabel('Ogrzewanie')
                               ->setDecorators($this->_aFormElementInlineDecorator);
        
          $this->_aFields['categories'] = new Zend_Form_Element_MultiCheckbox('categories');
        $this->_aFields['categories']->setLabel('Kategorie')
                ->setDecorators($this->_aFormElementInlineBigDecorator);
        
        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Filtruj')
                                  ->setDecorators($this->_aFormSubmitInlineDecorator);
                
                
                $this->addElements($this->_aFields)
                        ->setDecorators($this->_aFormInlineDecorator)
                        ->setMethod(Zend_Form::METHOD_GET);

        $this->populateSelect(Model_Logic_Enum::getInstance()->getProvinceList(), 'province', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getQualityList(), 'quality', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getHouseMaterialTypeList(), 'building_material', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getFlatHeatList(), 'heat', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getBasementList(), 'basement', false, false);
        $this->populateSelect(Model_Logic_Enum::getInstance()->getSewerageList(), 'sewerage', false, false);
        $this->populateSelect(Model_DbTable_Category::getInstance()->getListOfCategories(), 'categories', array('id' => 'id' , 'value' => 'name'), false);
    }

}
