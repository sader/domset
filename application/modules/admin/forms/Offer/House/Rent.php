<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Offer_House_Rent extends Admin_Form_Offer {

    public function __construct($options = null) {
        parent::__construct($options);

        $this->buildForm();
        
    }

    public function buildForm() {

        $this->getTitleField()
        ->getAuthorField()
        ->populateSelect(Model_DbTable_Admin::getInstance()->getListOfAdmins(), 'id_admin', array('id' => 'id', 'value' => 'login'), false)
        ->getProvinceField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getProvinceList(), 'province', false, false)
        ->getDistrictField()
        ->getCommunityField()
        ->getCityField()
        ->getSectionField()
        ->getSectionForDefaultCity()
        ->populateSelect(Model_DbTable_Section::getInstance()->getListOfSectionForDefaultCity(), 'section_selectable', array('id' => 'id', 'value' => 'name'), false)
        ->getNewSectionForDefaultCityField()
        ->getStreetField()
        ->getSurfaceField()
        ->getPriceField()
        ->getPriceNegotiableField()
        ->getPricePerSurfaceField()
        ->getPriceRoundButton()
        ->getAvailableField() 
        ->getTerrainSurfaceField()
        ->getBuildingTypeField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getHouseTypeList(), 'building_type', false, true)
        ->getRoomsField()
        ->getFloorsField()
        ->getAtticField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getAtticList(), 'attic', false, true) 
        ->getBasementField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getBasementList(), 'basement', false, true)    
        ->getYearField()
        ->getBuildingMaterialField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getHouseMaterialTypeList(), 'building_material', false, true)
        ->getQualityField(array('label' => 'Stan domu'))
        ->populateSelect(Model_Logic_Enum::getInstance()->getQualityList(), 'quality', false, true)
        ->getWindowsField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getWindowList(), 'windows', false, true)
        ->getRoofField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getRoofList(), 'roof', false, true)
        ->getRoofTypeField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getRoofTypeList(), 'roof_type', false, true)
        ->getHeatField()
         ->populateSelect(Model_Logic_Enum::getInstance()->getHouseHeatList(), 'heat', false, true)
        ->getSewerageField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getSewerageList(), 'sewerage', false, true)
        ->getFenceField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getFenceList(), 'fence', false, true)
        ->getGarageField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getGarageList(), 'garage', false, true)
        ->getAccessField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getAccessList(), 'access', false, true)
        ->getAdditionalField()
        ->populateSelect(Model_Logic_Enum::getInstance()->getHouseAdditionalRentList(), 'additional', false, false)
        ->getDescriptionField()
        ->getDescriptionPdfField()
        ->getCategoriesField()
        ->setCategories(Model_DbTable_Category::getInstance()->getByType(Model_Logic_Offer_House_Rent::ID))
        ->getSubmitField();

        
    }

}
