<?php

class Admin_Form_AgreementNotes extends Form_Abstract {

    public function __construct($sMode = null) {
        parent::__construct();

         $this->_aFields['noteone'] = new Zend_Form_Element_Textarea('noteone');
         $this->_aFields['notetwo'] = new Zend_Form_Element_Textarea('notetwo');
        $this->_aFields['noteone']->setLabel('Notka 1')
        ->setAttrib('cols', '80')
            ->setAttrib('rows', '10');
        $this->_aFields['notetwo']->setLabel('Notka 2')
            ->setAttrib('cols', '80')
            ->setAttrib('rows', '10');


        
        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Zatwierdź')
                                         ->setDecorators($this->_aFormSubmitTableDecorator); 
                
                
        $this->addElements($this->_aFields);

    }
    


}
