<?php


class Admin_Form_Map extends Form_Abstract
{
    
    public function __construct($sMode = null) {
        parent::__construct();
        
        $this->_aFields['lat'] = new Zend_Form_Element_Hidden('lat');
        $this->_aFields['lat']->setDecorators(array('ViewHelper')); 
                
        $this->_aFields['lng'] = new Zend_Form_Element_Hidden('lng');
        $this->_aFields['lng']->setDecorators(array('ViewHelper')); 
        
        $this->_aFields['submit'] = new Zend_Form_Element_Submit('submit');
        $this->_aFields['submit']->setLabel('Dodaj')->setDecorators(array('ViewHelper')); 
        
        $this->addElements($this->_aFields)
              ->setDecorators(array('FormElements', 'Form'))
             ->setMethod(Zend_Form::METHOD_POST); 
    }

	
}
