<?php

/**
 * Offer
 *
 * @author Krzysztof Janicki
 */
class Admin_Form_Wanted extends Form_Abstract {

    public function __construct($options = null) {
        parent::__construct($options);
        
        
        
               $oElem = new Zend_Form_Element_Select('id_client');
                $oElem->setLabel('Klient')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj klienta'))
                ->setRegisterInArrayValidator(false)
                ->setDecorators($this->_aFormElementTableDecorator);

                $this->addElement($oElem); 

        $oElem = new Zend_Form_Element_Button('add_client');
        $oElem->setLabel('Dodaj klienta');
        $this->stickyElem($oElem, 'id_client');

        $this->addElement($oElem);

         $oElem = new Zend_Form_Element_Textarea('info');
        $oElem->setLabel('Informacje szczegółowe')
                 ->setAttribs(array("cols" => 60, 'rows' => 4))
                ->setDecorators($this->_aFormElementTableDecorator);
        
        $this->addElement($oElem);

        
        $this->populateSelect(Model_DbTable_Client::getInstance()->getClients(), 'id_client', array('id' => 'id', 'value' => 'client'), true);
        
        $this->setAttrib('id', 'offer')
                ->setMethod(Zend_Form::METHOD_POST)
                ->addElement($this->getTypeField())
                ->setDecorators($this->_aFormTableDecorator)
                ->populateSelect(Model_Logic_Enum::getInstance()->getOfferTypesList(), 'type', false, false);
    }

    public function setWantedType($oWantedAdpated, $iType) {
        $this->addElements($oWantedAdpated->getFormElements());
        $this->getElement('type')->setValue($iType);
        return $this;
    }

    public function getTypeField($aParams = array()) {

        $oElem = new Zend_Form_Element_Select('type');
        $oElem->setLabel('Typ oferty')
                ->addValidator('NotEmpty', true, array('messages' => 'Podaj typ'))
                ->setDecorators($this->_aFormElementTableDecorator);
        
        return $oElem;
    }

    public function editMode() {
        $this->getElement('submit')->setLabel('Zatwierdź');
        $sVal = $this->getElement('type')->getValue();

        $this->removeElement('type');

        $oNewElem = new Zend_Form_Element_Hidden('type');
        $oNewElem->setDecorators(array('ViewHelper'))
                ->setValue($sVal);

        $this->addElement($oNewElem);

        return $this;
    }

}