<?php

class Admin_AuthController extends App_Controller_Admin_Abstract {
 
    
    
    public function loginAction()
    {
        $this->view->data = Model_Logic_Admin::getInstance()->manageLogin($this); 
    }
    
    
    
    public function logoutAction()
    {
        Model_Logic_Admin::getInstance()->manageLogout($this); 
    }
    
    
    
}

