<?php

class Admin_WantedController extends App_Controller_Admin_Abstract {

    public function addAction() {
        $this->view->data = Model_Logic_Wanted::getInstance()->manageAddWanted($this);
    }

    public function editAction() {
        $this->view->data = Model_Logic_Wanted::getInstance()->manageEditWanted($this);
    }

    public function changeAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->json(Model_Logic_Wanted::getInstance()->manageChangeWantedOfferType($this));
    }

    public function listAction() {
        $this->view->data = Model_Logic_Wanted::getInstance()->manageList($this);
    }

    public function removeAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        Model_Logic_Wanted::getInstance()->manageRemove($this);
    }
    
    public function searchAction()
    {
      $this->view->data =  Model_Logic_Wanted::getInstance()->manageSearch($this);
    }
    
    
      public function changeoffersAction() {

        $this->_helper->json(Model_Logic_Wanted::getInstance()->manageChangeSearchOffers($this));
    }
    
      public function changewantedAction() {

        $this->_helper->json(Model_Logic_Wanted::getInstance()->manageChangeSearchWanted($this));
    }

}

