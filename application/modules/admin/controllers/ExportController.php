<?php

class Admin_ExportController extends App_Controller_Admin_Abstract {
 
    
    public function listAction()
    {
    
        $this->view->data = Model_Logic_Export::getInstance()->managelist($this); 
        
    }
    
    public function addAction()
    {
        
        Model_Logic_Export::getInstance()->manageAdd($this); 
    }
    
        public function editAction()
    {
        
        Model_Logic_Export::getInstance()->manageEdit($this); 
    }
    
        public function removeAction()
    {
        
        Model_Logic_Export::getInstance()->manageRemove($this); 
    }

        public function reloadAction()
    {
        
        Model_Logic_Export::getInstance()->manageReload($this); 
    }

    
    
    
}

