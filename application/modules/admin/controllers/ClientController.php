<?php

class Admin_ClientController extends App_Controller_Admin_Abstract {

    public function addAction() {

       

        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->json(Model_Logic_Client::getInstance()->manageAddClient($this, true));
        }
        else $this->view->data = Model_Logic_Client::getInstance()->manageAddClient($this, false); 
    }
    
    
    public function editAction()
    {
        $this->view->data = Model_Logic_Client::getInstance()->manageEditClient($this); 
        
    }
    
    
    public function listAction()
    {
        $this->view->data = Model_Logic_Client::getInstance()->manageListClients($this); 
        
    }
    
    public function detailAction()
    {
        $this->view->data = Model_Logic_Client::getInstance()->manageDetail($this); 
    }
    
    
    public function quicklistAction()
    {
        $this->_helper->json(Model_Logic_Client::getInstance()->manageQuickList($this)); 
    }
    
    
    public function quickdetailAction()
    {
        $this->_helper->json(Model_Logic_Client::getInstance()->manageQuickDetail($this)); 
    }
}
