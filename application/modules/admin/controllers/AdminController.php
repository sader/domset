<?php

class Admin_AdminController extends App_Controller_Admin_Abstract {
 
    
    public function addAction()
    {
       $this->view->data =  Model_Logic_Admin::getInstance()->manageAdd($this); 
    }
    
    public function editAction()
    {
       $this->view->data =  Model_Logic_Admin::getInstance()->manageEdit($this); 
    }
    
    public function removeAction()
    {   
        
         $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);
        Model_Logic_Admin::getInstance()->manageRemove($this); 
    }
    
    public function listAction()
    {
        $this->view->data = Model_Logic_Admin::getInstance()->manageList($this); 
    }
    
        public function passwordAction()
    {
        $this->view->data = Model_Logic_Admin::getInstance()->manageChangePassword($this); 
    }

    
    
}

