<?php

class Admin_AgreementController extends App_Controller_Admin_Abstract {

    
    
       public function agreementAction()
    {
        $this->view->data = Model_Logic_Agreement::getInstance()->manageAgreement($this); 
    }
    
      public function detailAction()
    {
        $this->view->data = Model_Logic_Agreement::getInstance()->manageDetail($this); 
    }
    
       public function uploadAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->json(Model_Logic_Agreement::getInstance()->manageUploadFile($this));  
    }
    
       public function fileAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        Model_Logic_Agreement::getInstance()->manageSendFile($this); 
    }
    

}

