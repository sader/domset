<?php

class Admin_PresentationController extends App_Controller_Admin_Abstract {

    public function addAction() {
        $this->view->data = Model_Logic_Presentation::getInstance()->manageAddPresentation($this);
    }

    public function editAction() {
        $this->view->data = Model_Logic_Presentation::getInstance()->manageEditPresentation($this);
    }

  
    public function listAction() {
        $this->view->data = Model_Logic_Presentation::getInstance()->manageList($this);
    }

    public function removeAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        Model_Logic_Presentation::getInstance()->manageRemove($this);
    }
    
  
    

}

