<?php

class Admin_GalleryController extends App_Controller_Admin_Abstract{
 
    
     public function uploadAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->json(Model_Logic_Gallery::getInstance()->manageUploadImageFile($this));  
    }
    
    
    
   
  
}

