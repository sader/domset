<?php

class Admin_NewsController extends App_Controller_Admin_Abstract {
    
    public function init()
    {
        parent::init(); 
        $this->view->active = 'news'; 
    }


    public function indexAction() {
        
        $this->forward('list'); 
    }

    
     public function removeAction() {
        Model_Logic_News::getInstance()->manageRemove($this);
    }

    
     public function addAction() {
        Model_Logic_News::getInstance()->manageAdd($this);
    }

    
    public function editAction() {
        Model_Logic_News::getInstance()->manageEdit($this);
    }


    public function listAction() {
        Model_Logic_News::getInstance()->manageList($this);
    }
   
}