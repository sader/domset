<?php

class Admin_JobController extends App_Controller_Admin_Abstract {

    public function addAction() {
        $this->view->data = Model_Logic_Job::getInstance()->manageAddJob($this);
    }

    public function editAction() {
        $this->view->data = Model_Logic_Job::getInstance()->manageEditJob($this);
    }

  
    public function listAction() {
        $this->view->data = Model_Logic_Job::getInstance()->manageList($this);
    }

    public function removeAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        Model_Logic_Job::getInstance()->manageRemove($this);
    }
    
  
    

}

