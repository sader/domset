<?php

class Admin_CategoryController extends App_Controller_Admin_Abstract {
 
    
    public function addAction()
    {
       $this->view->data =  Model_Logic_Category::getInstance()->manageAddCategory($this); 
    }
    
    public function editAction()
    {
       $this->view->data =  Model_Logic_Category::getInstance()->manageEditCategory($this); 
    }
    
    public function removeAction()
    {   
        
        $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);
        Model_Logic_Category::getInstance()->manageRemoveCategory($this); 
    }
    
    public function listAction()
    {
        $this->view->data = Model_Logic_Category::getInstance()->manageListCategory($this); 
    }
    
      
    
    
}

