<?php

class Admin_OfferController extends App_Controller_Admin_Abstract {

   public function indexAction()
   {
       $this->view->data = Model_Logic_Offer::getInstance()->manageIndex($this); 
   }


   public function addAction() {
       $this->view->data = Model_Logic_Offer::getInstance()->manageAddOffer($this);
    }

    public function editAction() {
        $this->view->data = Model_Logic_Offer::getInstance()->manageEditOffer($this);
    }

    public function changeAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->json(Model_Logic_Offer::getInstance()->manageChangeOfferType($this));
    }

    public function mapAction() {
        $this->view->data = Model_Logic_Map::getInstance()->manageMap($this);
    }
    public function addsectionAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->json(Model_Logic_Offer::getInstance()->manageAddSection($this));
    }
    
    public function listAction()
    {
        $this->view->data = Model_Logic_Offer::getInstance()->manageList($this); 
    }
    
    
      public function archiveAction()
    {
        $this->view->data = Model_Logic_Offer::getInstance()->manageArchive($this); 
    }
    
    public function detailAction()
    {
        $this->view->data = Model_Logic_Offer::getInstance()->manageOfferDetails($this); 
    }
    
     public function galleryAction()
    {
      $this->view->data =   Model_Logic_Gallery::getInstance()->manageOfferGalleryForAdmin($this); 
    }

    public function galleryshowAction()
    {
        if (($iId = $this->getRequest()->getParam('id', null)) === null) {
            throw new Model_Logic_Exception('Id failed', 500);
        }

        $aGalleryImages = Model_DbTable_Gallery::getInstance()->getGalleryForOffer($iId);
        $aReturn = array();

        foreach($aGalleryImages as $i => $image)
        {
            $aReturn[] = array('href' => '/images/offer/'.$iId.'/' . $image['name'].'.jpg');
        }

        $this->_helper->json($aReturn);
    }

       public function agreementAction()
    {
        $this->view->data = Model_Logic_Agreement::getInstance()->manageAgreement($this); 
    }
    
       public function agreementfileAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->json(Model_Logic_Agreement::getInstance()->manageUploadFile($this));  
    }
    
    public function exportAction()
    {
        $this->view->data = Model_Logic_Offer::getInstance()->manageExport($this); 
    }
    
    public function promotedAction()
    {
         $this->view->data = Model_Logic_Offer::getInstance()->managePromoted($this); 
    }
    
       public function promotionsAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->json(Model_Logic_Offer::getInstance()->manageReloadPromotions($this));
    }
    
    
       public function removepromotionAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->json(Model_Logic_Offer::getInstance()->manageRemovePromotion($this));
    }
    
     public function getofferAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->json(Model_Logic_Offer::getInstance()->manageGetOffer($this));
    }
    
    public function statusAction()
    {
        Model_Logic_Offer::getInstance()->manageStatus($this); 
    }
    
    public function  printAction()
    {
        Model_Logic_Offer::getInstance()->managePrint($this); 
    }

}

