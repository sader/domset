<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_CrudDisplay extends Zend_View_Helper_Abstract {

    public function crudDisplay($iId, $sController = 'admin', $aParams = array()) {
        
        switch ($sController) {
            case 'admin':
                return $this->adminList($iId);
                break;
            case 'client':
                return $this->clientList($iId, $aParams['type']);
                break;
            case 'category':
                return $this->categoryList($iId);
                break;
            case 'export':
                return $this->exportList($iId, $aParams['type']);
                break;
            case 'offer':
                return $this->offerList($iId);
                break;
            case 'wanted':
                return $this->wantedList($iId);
                break;
            case 'wanted_offers':
                return $this->wantedOffers($iId);
                break;
            case 'wanted_searches':
                return $this->wantedSearches($iId);
                break;
            case 'job':
                return $this->jobList($iId);
                break;
            case 'presentation':
                return $this->presentationList($iId);
                break;
             case 'promotion':
                return $this->promotionList($iId, $aParams['promotion']);
                break;
             case 'archive':
                return $this->archiveList($iId);
                break;
            case 'offersOwner' :
                return $this->offersOwnerList($iId);
        }
    }

    private function adminList($iId) {
        return '<a class="goto" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'admin', 'action' => 'edit', 'id' => $iId), null, true) . '">Edytuj</a> ' .
                '<a class="remove" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'admin', 'action' => 'remove', 'id' => $iId), null, true) . '">Usuń</a> ' .
                '<a class="password" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'admin', 'action' => 'password', 'id' => $iId), null, true) . '">Zmień hasło</a>';
    }

    private function categoryList($iId) {
        return '<a class="goto" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'category', 'action' => 'edit', 'id' => $iId), null, true) . '">Edytuj</a> ' .
                '<a class="remove" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'category', 'action' => 'remove', 'id' => $iId), null, true) . '">Usuń</a> ';
    }

    private function exportList($iId, $iType) {

        switch ($iType) {
            case 'Usunięty':
                return '';
                break;

            default:
                return '<a class="edit" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'export', 'action' => 'edit', 'id' => $iId), null, true) . '">Edytuj</a> ' .
                        '<a class="remove confirm" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'export', 'action' => 'remove', 'id' => $iId), null, true) . '">Usuń</a> ' .
                        '<a class="reload" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'export', 'action' => 'reload', 'id' => $iId), null, true) . '">Przeładuj</a>';

                break;
        }
    }

    private function offerList($iId) {
        return '<a class="goto" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'offer', 'action' => 'edit', 'id' => $iId), null, true) . '">Zarządzaj</a><br />' .
                '<a show-gallery="'.$iId.'" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'offer', 'action' => 'galleryshow', 'id' => $iId), null, true) . '">Galeria</a><br />'.
                '<a class="remove confirm" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'offer', 'action' => 'status', 'status' => Model_DbTable_Offer::STATUS_DELETED, 'id' => $iId), null, true) . '">Usuń</a><br />'.
                '<a class="transaction confirm" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'offer', 'action' => 'status', 'status' => Model_DbTable_Offer::STATUS_FINISHED, 'id' => $iId), null, true) . '">Transakcja</a><br />'.
                '<a class="cancelled confirm" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'offer', 'action' => 'status', 'status' => Model_DbTable_Offer::STATUS_CANCELLED  , 'id' => $iId), null, true) . '">Anuluj</a> ';
    }
    
      private function archiveList($iId) {
          return 
                '<a class="activate confirm" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'offer', 'action' => 'status', 'status' => Model_DbTable_Offer::STATUS_OPEN, 'id' => $iId), null, true) . '">Aktywuj</a><br />'.
                '<a class="remove confirm" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'offer', 'action' => 'status', 'status' => Model_DbTable_Offer::STATUS_DELETED, 'id' => $iId), null, true) . '">Usuń</a><br />'.
                '<a class="transaction confirm" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'offer', 'action' => 'status', 'status' => Model_DbTable_Offer::STATUS_FINISHED, 'id' => $iId), null, true) . '">Transakcja</a><br />'.
                '<a class="cancelled confirm" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'offer', 'action' => 'status', 'status' => Model_DbTable_Offer::STATUS_CANCELLED  , 'id' => $iId), null, true) . '">Anuluj</a><br />'.
                '<a class="goto" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'offer', 'action' => 'edit', 'id' => $iId), null, true) . '">Zarządzaj</a>';
    }

    private function clientList($iId, $sTypes) {
        return '<a class="goto" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'client', 'action' => 'detail', 'client' => $iId), null, true) . '">Detale</a> ';
    }

    private function wantedList($iId) {
        return '<a class="edit" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'wanted', 'action' => 'edit', 'id' => $iId), null, true) . '">Edytuj</a> ' .
                '<a class="remove" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'wanted', 'action' => 'remove', 'id' => $iId), null, true) . '">Usuń</a> ';
    }

    private function wantedOffers($iId) {
        return '<a style="display:none" class="wanted_filter" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'wanted', 'action' => 'changeoffers', 'wanted' => $iId), null, true) . '">Wybierz</a> '.
                  '<a class="offers_edit" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'wanted', 'action' => 'edit', 'offer' => $iId), null, true) . '">Edytuj</a> ' .
                    '<a class="offers_delete" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'wanted', 'action' => 'remove', 'offer' => $iId), null, true) . '">Usuń</a>';
    }

    private function wantedSearches($iId) {
        return '<a class="offers_filter" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'wanted', 'action' => 'changewanted', 'offer' => $iId), null, true) . '">Wybierz</a>
                <a class="offers_filter" target="_blank" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'offer', 'action' => 'edit', 'id' => $iId), null, true) . '">Oferta</a>';
    }
    
    private function jobList($iId) {
        return '<a class="goto" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'job', 'action' => 'edit', 'id' => $iId), null, true) . '">Edytuj</a> ';
    }
    
    private function presentationList($iId) {
        return '<a class="goto" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'presentation', 'action' => 'edit', 'presentation' => $iId), null, true) . '">Edytuj</a> '.
                '<a class="confirm" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'presentation', 'action' => 'remove', 'presentation' => $iId), null, true) . '">Usuń</a> ';
    }
    
     private function promotionList($iId, $iPromoted) {
        if(!$iPromoted)
        {
         return '<a class="promote" data="'.$iId.'" href="#">Dodaj do promowanych</a> ';
        }
    }
    
    private function offersOwnerList($iId){
         return '<a class="goto" href="' . $this->view->url(array('module' => 'admin', 'controller' => 'offer', 'action' => 'edit', 'id' => $iId), null, true) . '">Wybierz</a>';
    }


}