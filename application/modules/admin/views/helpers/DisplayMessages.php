<?php
/**
 * Zend_View_Helper_DisplayMessages
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_DisplayMessages extends Zend_View_Helper_Abstract {

    
     
    /**
     * Metoda zwraca sformatowane wiadomości
     * @param array $aData
     * @return string 
     */
    
    public function displayMessages() {
    
        
        $oFlashMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('flashMessenger');
        $aData = array_merge($oFlashMessenger->getCurrentMessages() , $oFlashMessenger->getMessages()); 
        
        if(empty($aData) || !is_array($aData)) return ''; 
        
        foreach($aData as  $aMessage)
        {
            switch ($aMessage['type']) {
                case 'error':
                   return '<p class="msg error">'.$aMessage['message'].'</p>';
                    break;
                case 'success':
                   return '<p class="msg done">'.$aMessage['message'].'</p>';
                    break;
                default:
                    return '<p class="msg info">'.$aMessage['message'].'</p>';
                    break;
            }
            
          
            
        }
       
    }

}
