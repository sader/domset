<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_AdditionalData extends Zend_View_Helper_Abstract {

    public function additionalData($sType) {
        
        
        switch ($sType) {
            case Model_DbTable_Category::CATEGORY_HAS_ADDITIONAL_DATA:
                return 'Tak'; 
                break;
            case Model_DbTable_Category::CATEGORY_HAS_NOT_ADDITIONAL_DATA:
                return 'Nie'; 
                break;
            default:
                return null; 
                break;
        }
    }
}
