<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_AdminType extends Zend_View_Helper_Abstract {

    public function adminType($sType) {
        
        
        switch ($sType) {
            case Model_DbTable_Admin::ACCOUNT_TYPE_STANDARD:
                return 'Moderator'; 
                break;
            case Model_DbTable_Admin::ACCOUNT_TYPE_MASTER : 
                return 'Administrator'; 
                break;
            default:
                return null; 
                break;
        }
    }
}
