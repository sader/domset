<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_ExportType extends Zend_View_Helper_Abstract {

    public function exportType($iExport, $iId) {


        $aAdapters = Model_Logic_Export::getRegisteredAdapters(); 
        
     return sprintf('<div><strong>%s</strong></div>', $aAdapters[$iExport]['label']);
    }

}
