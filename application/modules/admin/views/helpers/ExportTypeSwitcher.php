<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_ExportTypeSwitcher extends Zend_View_Helper_Abstract {
    const TYPE_ADDED = 1;
    const TYPE_EDITED = 2;
    const TYPE_RELOADED = 3;
    const TYPE_DELETED = 4;

    private function links($iOfferId, $iExportId) {
        return array(
            'export' => sprintf('<p><a href="%s">Eksport</a></p>', $this->view->url(array('controller' => 'export', 'action' => 'add', 'id' => $iOfferId, 'adapter' => $iExportId))),
            'edit' => sprintf('<p><a href="%s">Edytuj</a></p>', $this->view->url(array('controller' => 'export', 'action' => 'edit', 'id' => $iOfferId, 'adapter' => $iExportId))),
            'delete' => sprintf('<p><a href="%s">Usuń</a></p>', $this->view->url(array('controller' => 'export', 'action' => 'delete', 'id' => $iOfferId, 'adapter' => $iExportId))),
            'reload' => sprintf('<p><a href="%s">Przeładuj</a></p>', $this->view->url(array('controller' => 'export', 'action' => 'reload', 'id' => $iOfferId, 'adapter' => $iExportId)))
        );
    }

    public function exportTypeSwitcher($iOfferId, $iExportId, $iType = null) {

        $aLinks = $this->links($iOfferId, $iExportId);

        switch ($iType) {
            case Model_DbTable_Export::TYPE_ADDED:
                return array('type' => 'Dodany' , 'links' => implode(array($aLinks['edit'], $aLinks['delete'], $aLinks['reload'])));
                break;
            case Model_DbTable_Export::TYPE_EDITED:
                return array('type' => 'Edytowany' , 'links' => implode(array($aLinks['edit'], $aLinks['delete'], $aLinks['reload'])));
                break;
            case Model_DbTable_Export::TYPE_DELETED:
                return array('type' => 'Usunięty' , 'links' => implode(array($aLinks['export'])));
                break;
            case Model_DbTable_Export::TYPE_RELOADED:
                return array('type' => 'Przeładowany' , 'links' => implode(array($aLinks['edit'], $aLinks['delete'], $aLinks['reload'])));
                break;
            default:
                 return array('type' => 'Brak' , 'links' => implode(array($aLinks['export'])));
                break;
        }
    }

}
