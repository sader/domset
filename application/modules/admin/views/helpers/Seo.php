<?php

class Zend_View_Helper_Seo {

    public function Seo($str, $replace = '-') {
        $str = iconv('UTF-8', 'ASCII//TRANSLIT', trim($str, ' -' . $replace));
        $str = preg_replace('/[^a-zA-Z0-9\s\.' . $replace . ']/', '', strtolower($str));
        $str = preg_replace('/' . $replace . '/', ' ', $str);
        $str = preg_replace('/ +/', ' ', $str);

        return str_replace(' ', $replace, $str);
    }

}
