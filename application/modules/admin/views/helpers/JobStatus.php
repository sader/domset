<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_JobStatus extends Zend_View_Helper_Abstract {

    private $aResultColors = array(
        1 => '009900',
        2 => 'CCCCCC',
        3 => 'CCCCCC',
        4 => 'CCCCCC',
        5 => '0000FF',
        6 => '00CCFF',
        7 => '000000',
        8 => '000000',
        9 => 'FF34FF',
        10 => 'FF0000',
        11 => 'FF0000',
        12 => 'CCCCCC'
    );

    public function jobStatus($iStatus = 1) {
        return '<p style="color:#' . $this->aResultColors[$iStatus] . '">' . Model_Logic_Enum::getInstance()->getJobResultById($iStatus) . '</p>';
    }

}
