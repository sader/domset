<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_OfferLink extends Zend_View_Helper_Abstract {

    public function offerLink($sName, $iId) {
        
        return sprintf('<a href="%s">%s</a>' , 
                        $this->view->url(array('module' => 'admin' , 'controller' => 'offer' , 'action' => 'detail' , 'id' => $iId)), 
                        $sName); 
        
    }
}
