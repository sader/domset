<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_ExportWorkType extends Zend_View_Helper_Abstract {

    public function exportWorkType($iType) {


        switch ($iType) {
            case Model_DbTable_Export::TYPE_ADDED:
               return 'Dodany'; 
                break;
            case Model_DbTable_Export::TYPE_EDITED:
                return 'Edytowany';
                break;
            case Model_DbTable_Export::TYPE_RELOADED:
                return 'Przeładowany'; 
                break;
            case Model_DbTable_Export::TYPE_DELETED:
                return 'Usunięty'; 
                break;
            default:
                break;
        }
    }

}
