<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_Image extends Zend_View_Helper_Abstract {

    private $width = null;
    private $height = null;
    private $defaultWidth = 640;
    private $defaultHeight = 480;

    public function image($iId, $sName = null, $iWidth = null, $iHeight = null, $sClass = null) {

        if (empty($sName)) {
            $sName = 'empty';
        }

        $this->width = empty($iWidth) ? $this->defaultWidth : $iWidth;
        $this->height = empty($iHeight) ? $this->defaultHeight : $iHeight;


        return sprintf('<img %s src="%s" width="%s" , height="%s" />',
                        //  Model_Logic_Gallery::getInstance()->getImageSrc($sName, $iId),
                        !empty ($sClass) ? 'class="'.$sClass.'"' : '',
                        $this->view->url(array('id' => $iId, 
                                               'name' => $sName, 
                                               'width' => $this->width, 
                                               'height' => $this->height), 'image'), 
                                        $this->width, 
                                        $this->height);
    }

}