<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_Key extends Zend_View_Helper_Abstract {

    public function key($iKey, $aStack) {

        return isset($aStack[$iKey]) ? $aStack[$iKey] : null;
    }

}
