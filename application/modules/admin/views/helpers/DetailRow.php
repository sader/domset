<?php

/**
 * Zend_View_Helper_DisplayMessages
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_DetailRow extends Zend_View_Helper_Abstract {

    /**
     * Metoda zwraca sformatowane wiadomości
     * @param array $aData
     * @return string 
     */
    public function DetailRow($sLabel, $sKey, $sValue, $bMasked) {

        $sButon = $bMasked  
                ? '<a href="#" class="hidden" data="' . $sKey . '"><img src="/images/visible-off.png"></a>' 
                : '<a href="#" class="visible" data="' . $sKey . '"><img src="/images/visible-on.png"></a>';
        $sClass  = $bMasked ? 'hidden' : 'visible';
        return sprintf('<p class="%s">
        <span class="label">%s : </span>
        <span class="value">%s</span>
        <span class="visibility">%s</span>
         </p>' , $sClass, $sLabel, $sValue, $sButon);
    }

}
