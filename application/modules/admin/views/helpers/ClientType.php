<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_ClientType extends Zend_View_Helper_Abstract {

    public function clientType($sType, $sDelimiter = '<br />') {
        
        
        if(empty($sType)) return ''; 
        
        
        $aTypes = explode(',', $sType); 
        $aClientsTypes = Model_Logic_Client::getClientTypes();
        
        
        
        foreach($aTypes as $iType)
        {
            if(isset($aClientsTypes[$iType]))
            {
                $aReturn[] = $aClientsTypes[$iType]; 
            }
        }
        
        
        return implode($sDelimiter, $aReturn); 
    }
}
