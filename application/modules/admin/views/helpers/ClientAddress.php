<?php

/**
 * formatLangs
 *
 * @author Krzysztof Janicki
 */
class Zend_View_Helper_ClientAddress extends Zend_View_Helper_Abstract {

    public function clientAddress($aData = array()) {
        
        
        
        
        $aReturn['postcode'] = (!isset($aData['postcode']) || empty($aData['postcode'])) ? '' : $aData['postcode']; 
        $aReturn['city'] = (!isset($aData['city']) || empty($aData['city'])) ? '' : ' '.$aData['city']; 
        $aReturn['street'] = (!isset($aData['street']) || empty($aData['street'])) ? '' : ' ul. '.$aData['street'];
        $aReturn['house'] = (!isset($aData['house']) || empty($aData['house'])) ? '' : ' '.$aData['house']; 
        $aReturn['flat'] = (!isset($aData['flat']) || empty($aData['flat'])) ? '' : ' m. '.$aData['flat'];
        
        
       return implode($aReturn); 
        
        
       
    }
}
