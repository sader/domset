<?php

class Cron_ExportController extends App_Controller_Api_Abstract {

   
    
    public function domimportaAction() {
	    
        Model_Logic_Export_Domimporta_Api::getInstance()->sendOffers();
            
    }


    public function otodomAction() {

        Model_Logic_Export_Otodom_Api::getInstance()->sendOffers();

    }


    public function olxAction() {

        Model_Logic_Export_Olx_Api::getInstance()->sendOffers();

    }
    
   

}

