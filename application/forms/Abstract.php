<?php

/**
 * Klasa matka dla formularzy
 */
class Form_Abstract extends Zend_Form {

    public $_aFields = array();
    //  public $_aBasicDecorator = array(array('ViewHelper'), array('Label'), array('Errors'));
    public $_aFormTableDecorator = array('FormElements',
        array('HtmlTag', array('tag' => 'table', 'class' => 'adder', 'cellpadding' => 0, 'cellspacing' => 0, 'border' => 0)),
        'Form');
	  public $_aSubFormTableDecorator = array('FormElements',
        array('HtmlTag', array('tag' => 'tr')),
        'Form');
    public $_aFormElementTableDecorator = array(
        array('ViewHelper'),
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('label', array('tag' => 'th', 'valding' => 'top', 'placement' => 'prepend')),
        array('ErrorsHtmlTag', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')));
    
    public $_aFormCheckboksTableDecorator = array(
        array('ViewHelper'),
        array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'checkboxes_wrap')),
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'checkboxes')),
        array('label', array('tag' => 'th', 'valding' => 'top', 'placement' => 'preppend')),
        array('ErrorsHtmlTag', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')));
    
    
    public $_aFormElementTableDecoratorMarked = array(
        array('ViewHelper'),
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('label', array('tag' => 'th', 'valding' => 'top', 'placement' => 'prepend')),
        'Errors',
        array(array('row' => 'HtmlTag'), array('tag' => 'tr', 'class' => 'param-elem')));
    public $_aFormSubmitTableDecorator = array(
        array('ViewHelper'),
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'submitbox', 'colspan' => 3)),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')));
    public $_aFormElementInlineDecorator = array(
        array('ViewHelper'),
        array(array('wrap' => 'HtmlTag'), array('tag' => 'p')),
        array('label', array('placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'filter')));
      public $_aFormElementInlineBigDecorator = array(
        array('ViewHelper'),
        array(array('wrap' => 'HtmlTag'), array('tag' => 'p')),
        array('label', array('placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'filter2')));
    
    public $_aFormSubmitInlineDecorator = array(
        array('ViewHelper'),
        array(array('rowa' => 'HtmlTag'), array('tag' => 'p', 'class' => 'submit_placement', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'filter submit')));
    public $_aFormInlineDecorator = array('FormElements',
        array('HtmlTag', array('tag' => 'div', 'id' => 'limiter')),
        'Form');
    public $_aFormTableInlineElement = array(
        array('ViewHelper'),
        array('label', array('tag' => 'p')),
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
    );
    public $_aFormTableInlineButton = array(
        array('ViewHelper'),
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'button_row')),
    );
    public $_aFormTableInlineDecorator = array('FormElements',
        array('HtmlTag', array('tag' => 'tr')),
        array('HtmlTag', array('tag' => 'table', 'cellpadding' => 0, 'cellspacing' => 0, 'border' => 0)),
        'Form');

    public function __construct($options = null) {

        parent::__construct($options);


        $this->addElementPrefixPath('App_Validate', 'App/Validate', 'validate');
        $this->addElementPrefixPath('App_Filter', 'App/Filter', 'filter');
        $this->addElementPrefixPath('App_Form_Decorator', 'App/Form/Decorator', 'decorator');
    }
    
    public function stickyElem($oElem, $sElemToStick)
    {
         return $oElem->setDecorators(array('ViewHelper'))
                ->setAttrib('class', 'attachment')
                ->setAttrib('data', $sElemToStick); 
                
    }

    /**
     * Populacja obiektu Zend_Form_Element_Select danymi
     * @param array $aData - dane do populacji
     * @param string $sElementName - nazwa elementu
     * @param array $aMapper - tablica mapująca
     * @param boolean $bBeginWithEmptyOption - czy select powinien zaczynać się pustym polem
     * @return App_Form 
     */
    public function populateSelect($aData, $sElementName, $aMapper = array('id' => 'id', 'value' => 'value'), $bBeginWithEmptyOption = true) {
        $oElement = $this->getElement($sElementName);

        if ($bBeginWithEmptyOption && ($oElement instanceof Zend_Form_Element_Select))
            $oElement->addMultiOption('', '');


        if (!empty($aData)) {
            if ($aMapper === false) {
                $oElement->addMultiOptions($aData);
            } else {
                foreach ($aData as $aRow) {
                    $oElement->addMultiOption($aRow[$aMapper['id']], $aRow[$aMapper['value']]);
                }
            }
        }
        return $this;
    }

    /**
     * Buduje ciąg GET na podstawie elementów formularza i danych na wejściu
     * @param array $aData
     * @return string 
     */
    public function buildQuery($aData) {

        $aElementNames = array();
        $aOutput = array();

        $aElements = $this->getElements();

        foreach ($aElements as $oElement) {

            if ((!($oElement instanceof Zend_Form_Element_Submit))) {
                $sName = $oElement->getName();

                if (isset($aData[$sName]) && !empty($aData[$sName])) {
                    $aOutput[] = $sName . '=' . $aData[$sName];
                }
            }
        }

        return (empty($aOutput)) ? null : '?'.implode('&' , $aOutput);
    }

    /**
     * Metoda sprawdza czy podana tablica parametrów pasuje do pól w formularzu (inicjacja filtra)
     * @param Zend_Form $oForm
     * @param array $aPost
     * @return boolean 
     */
    public function filterInit(Zend_Form $oForm, $aData) {
        foreach ($oForm->getElements() as $oElement) {
            if (isset($aData[$oElement->getName()]) && $aData[$oElement->getName()] != "") {
                return true;
            }
        }

        return false;
    }
	
	/**
	 * Podformularz z guzikami dla filtrów
	 * @return \Zend_Form_SubForm 
	 */
	public function getFilterSubForm()
	{
		$iBreakLineElements = 4; 
		
		$oSubFormButtons = new Zend_Form_SubForm('submit'); 
		
        $oSubmit = new Zend_Form_Element_Submit('submit');
        $oSubmit->setLabel('Filtruj')
                ->setAttrib('id', 'filter')
                ->setDecorators($this->_aFormTableInlineButton);

        $oReset = new Zend_Form_Element_Button('reset');
		$oReset->setLabel('Resetuj')
                ->setDecorators($this->_aFormTableInlineButton);
		$oSubFormButtons->addElements(array($oSubmit , $oReset)); 
				
		($iBreakLineElements < $this->count()) ? $oSubFormButtons->setDecorators ($this->_aSubFormTableDecorator) 
											     : $oSubFormButtons->setDecorators(array('FormElements' , 'Form')); 
		
		return $oSubFormButtons; 
	}

}

