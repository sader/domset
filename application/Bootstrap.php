<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

 protected function _initAutoloader() {
        $oModuleLoader = new Zend_Application_Module_Autoloader(
                        array('namespace' => '',
                              'basePath' => APPLICATION_PATH)
        );
        
        $oModuleLoader->addResourceType('grid', 'grids' , 'Grid');
    }
    
    protected function _initRouter(){
        
        
            $sRouteFile = APPLICATION_PATH . 
                          DIRECTORY_SEPARATOR. 
                          'configs'.
                          DIRECTORY_SEPARATOR.
                          'routes.ini'; 
        
                $oRoute = new Zend_Config_Ini($sRouteFile);
                Zend_Controller_Front::getInstance()
                                       ->getRouter()
                                       ->addConfig($oRoute, 'routes');
        
    }
    
     protected function _initModuleAutoloaders() 
    { 
        $this->bootstrap('FrontController'); 
        $front = $this->getResource('FrontController'); 

        foreach ($front->getControllerDirectory() as $module => $directory) { 
            $module = ucfirst($module); 
            $loader = new Zend_Application_Module_Autoloader(array( 
                'namespace' => $module, 
                'basePath'  => dirname($directory), 
            )); 
                  
             $loader->addResourceType('grid', 'grids' , 'Grid');
        } 
    }
    
    protected function _initViewHelpers()
{
	$this->bootstrap('view');

$view = $this->getResource('view'); //get the view object

 

//add the jquery view helper path into your project

$view->addHelperPath("ZendX/JQuery/View/Helper", "ZendX_JQuery_View_Helper");

}
    
    
}

