<?php
/**
 * Nakładka na walidator EmailAdress zmieniająca wszystkie typy błędów w jeden 
 */
class App_Validate_CorrectEmail extends Zend_Validate_EmailAddress
{
    public function getMessages()
    {
	$this->_messages = array();

	$this->_error(self::INVALID, $this->_messageTemplates[self::INVALID]);

	return $this->_messages;
    }
}