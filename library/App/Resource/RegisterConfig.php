<?php

/**
 * Plugin rejestruj�cy odpowiedni config dla danej domeny
 * Options :
 * - config : plik ini z konfiguracją dla domeny
 * - regkey : klucz pod jakim przechowywana będzie konfiguracja (default : domainCfg)
 * - versioning : plik konfiguracji jest uzależniony od APPLICATION_ENV
 * - mode : array/object : w rejestrze umieszczamy obiekt lub tablicę (default : array)
 * @author Krzysztof Janicki
 */
class App_Resource_RegisterConfig extends Zend_Application_Resource_ResourceAbstract {
    
  const DEFAULT_REGISTRY_KEY = 'cfg';
  const DEFAULT_VERSIONING_STATE = true; 
  const DEFAULT_CONFIG = 'array'; 

  /**
     * Inicjalizacja zasobu
     */
    public function init() {

        $this->__registerCfg(); 
    }

    /**
     * Metoda dodaje do obektu Zend_Registy wskazany plik konfiguracyjny
     *
     * @param string $sHost
     */
    private function __registerCfg()
    {

        $aOptions = $this->getOptions(); 

        if(!isset($aOptions['config']))
            throw new Exception('No config file specified');
         else
             $sConfigFile = $aOptions['config']; 

            $sRegistryKey = isset($aOptions['regkey']) ?
                                   $aOptions['regkey'] :
                                   self::DEFAULT_REGISTRY_KEY;
             $bVersioning =  isset($aOptions['versioning']) ?
                                   $aOptions['versioning'] :
                                   self::DEFAULT_VERSIONING_STATE;
             $sConfigMode =  isset($aOptions['mode']) ?
                                   $aOptions['mode'] :
                                   self::DEFAULT_CONFIG;


        if($bVersioning)
        $oCfg = new Zend_Config_Ini($sConfigFile, APPLICATION_ENV);
     else
        $oCfg = new Zend_Config_Ini($sConfigFile);


     $mFinalCofig = ($sConfigMode == self::DEFAULT_CONFIG) ? $oCfg->toArray() : $oCfg; 


            Zend_Registry::set($sRegistryKey, $mFinalCofig);
    }

   
}