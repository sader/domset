<?php

class App_Resource_Locate extends Zend_Application_Resource_ResourceAbstract {

    /**
     * Inicjalizacja zasobu
     */
    private $__routePath;
    private $__translatePath;
    private $__language;
    private $__currency_symbol;
    private $__currency_value;
    private $__router;
    private $__shop; 

    public function init() {


        $this->__routePath = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configs' .
                DIRECTORY_SEPARATOR . 'routes' . DIRECTORY_SEPARATOR;
        $this->__translatePath = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'data' .
                DIRECTORY_SEPARATOR . 'translate' . DIRECTORY_SEPARATOR;

        $this->__identify();
        $this->__setTranslate();
        $this->__setRouter();
    }

    /**
     * Identyfikacja sklepu po domenie  
     */
    private function __identify() {
        define('DOMAIN', preg_replace('/www\./', '', $_SERVER['HTTP_HOST']));

        $aSite = Model_DbTable_Site::getInstance()->getByDomain(DOMAIN);
            empty($aSite) ? $this->__setDefaultSite() : $this->__setSite($aSite);
        
        define('PRICE_MODIFIER' , isset($aSite['price_modifier']) ? $aSite['price_modifier'] : 1); 
        
        define('SHOP_ID' , $this->__shop); 
        define('LANGUAGE', $this->__language);
        define('CURRENCY_SYMBOL', $this->__currency_symbol);
	define('CURRENCY_VALUE', $this->__currency_value);
    }

    /**
     * Ustawienie domyślnych ustawnień dla nieistniejącego sklepu 
     */
    private function __setDefaultSite() {
        $this->__language = Model_Logic_Lang::DEFAULT_LANGUAGE;
        $this->__currency_symbol = Model_Logic_Currency::DEFAULT_CURRENCY;
        $this->__currency_value = 1; 
        $this->__shop = 0; 
	$this->__router = Model_Logic_Lang::DEFAULT_LANGUAGE;
    }

    /**
     * Ustawienia paremetrów dla instniejącego sklepu
     * @param array $aSite 
     */
    private function __setSite($aSite) {

        $this->__language = $aSite['language'];
        $this->__currency_symbol = $aSite['symbol'];
        $this->__currency_value = $aSite['value'];
	$this->__router = $aSite['language'];
        $this->__shop = $aSite['id']; 
        
        Zend_Registry::set('site', $aSite);
    }

    /**
     * Metoda dodaje do obektu Zend_Registy konfigurację przypadającą do danej domeny
     *
     * @param string $sHost
     */
    private function __setRouter() {


        $sRouteCfg = file_exists($this->__routePath . $this->__router . '.ini') ?
                $this->__routePath . $this->__router . '.ini' :
                $this->__routePath . Model_Logic_Lang::DEFAULT_LANGUAGE . '.ini';


        $oRoute = new Zend_Config_Ini($sRouteCfg);
        Zend_Controller_Front::getInstance()
                ->getRouter()
                ->addConfig($oRoute, 'routes');
    }

    /**
     * Ustawienie translatora 
     */
    private function __setTranslate() {


        if (file_exists($this->__translatePath . $this->__language . '.php')) {
            $sTranslateFile = $this->__translatePath . $this->__language . '.php';
            $sLang = $this->__language;
        } else {
            $sTranslateFile = $this->__translatePath . Model_Logic_Lang::DEFAULT_LANGUAGE . '.php';
            $sLang = Model_Logic_Lang::DEFAULT_LANGUAGE;
        }


        $oTranslate = new Zend_Translate(array(
                    'adapter' => 'array',
                    'content' => $sTranslateFile,
                    'language' => $sLang));

        Zend_Registry::set('Zend_Translate', $oTranslate);
    }
	
	
	private function __setCurrency()
	{
		
		
	}

}