<?php

/**
 * Table
 *
 * @author Krzysztof Janicki
 */
class App_Db_Table extends Zend_Db_Table_Abstract
{
	
	/**
	 * Wykorzystanie cache'a dla wszystkich metod : tak/nie
	 * @var boolean 
	 */
	protected $_cache = false; 
	
	/**
	 * Obiekt cache przekazywane do Db_Select
	 * @var type 
	 */
	private $_oCache = null; 
	
	/**
	 * Ustawienie cache dla całego modelu Db_Table
	 * @param Zend_Cache $oCache 
	 */
	protected function setCache(Zend_Cache $oCache)
	{
		$this->_oCache = $oCache;
		
	}
	

	public function getDbName()
	{
		return $this->_name; 
	}
	
	public function setCacheEnabled($bStatus = true)
	{
		
		$this->_cache = (bool) $bStatus; 
		
		if($bStatus)
				$this->select()->setCacheEnable(true, $this->_oCache); 
		
		return $this; 
	}

			/**
     * Zwraca instację App_Db_Table_Select
     * @param bool $withFromPart Whether or not to include the from part of the select based on the table
     * @return App_Db_Table_Select
     */
    public function select($withFromPart = self::SELECT_WITHOUT_FROM_PART)
    {
        require_once 'App/Db/Table/Select.php';
        $select = new App_Db_Table_Select($this);
        
		if($this->_cache)
		{
			$select->setCacheEnable(true, $this->_oCache); 
		}
		
		
		if ($withFromPart == self::SELECT_WITH_FROM_PART) {
            $select->from($this->info(self::NAME), Zend_Db_Table_Select::SQL_WILDCARD, $this->info(self::SCHEMA));
        }
        return $select;
    }
	
	
	public function update(array $data = array(), $where = null)
	{
		if($this->_cache)
			$this->__cleanCache(); 
		
		return parent::update($data, $where); 
	}
	
	public function insert(array $data)
	{
		if($this->_cache)
			$this->__cleanCache(); 
		return parent::insert($data);
	}
	
	
	public function delete($where = null)
	{
		if($this->_cache)
			$this->__cleanCache(); 
		return parent::delete($where);
	}
	
	
	private function __cleanCache()
	{
		$this->select()
			     ->setCacheEnable(true, $this->_oCache)
				 ->getCache()
			     ->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array($this->_name)); 
	}
	
	
	
	
}
