<?php

class App_Static {

    const PRIORITY_HIGH = 1;
    const PRIORITY_MEDIUM = 2;
    const PRIORITY_LOW = 3;
    const JQUERY = 'jquery';
    const TWITTER_BOOTSTRAP = 'twitterbootstrap';
    const DISPLAY_TYPE_BOTH = 'both';
    const DISPLAY_TYPE_CSS = 'css';
    const DISPLAY_TYPE_JS = 'js';

    private $pluginBasePath = '/jsPlugins/';
    private $aJs = array();
    private $aCss = array();

    /**
     * Instancja klasy.
     * 
     * @var App_JsPlugins
     */
    static private $_oInstance;

    // ------------------------------------------------------------------------    

    /**
     * Zwraca instancje klasy.
     * 
     * @return App_JsPlugins
     */
    static public function getInstance() {
        if (self::$_oInstance === null) {
            self::$_oInstance = new self();
        }
        return self::$_oInstance;
    }

    private function getRegisteredPlugins() {
        return array(
            self::JQUERY => array('js' => array('/jquery/jquery-1.8.3.min.js'),
                'css' => array(),
                'priority' => self::PRIORITY_HIGH),
            self::TWITTER_BOOTSTRAP => array('js' => array(),
                'css' => array()),
            'jquery' => array('js' => array(),
                'css' => array()),
            'jquery' => array('js' => array(),
                'css' => array()),
            'jquery' => array('js' => array(),
                'css' => array()),
            'jquery' => array('js' => array(),
                'css' => array()),
            'jquery' => array('js' => array(),
                'css' => array()),
            'jquery' => array('js' => array(),
                'css' => array()),
            'jquery' => array('js' => array(),
                'css' => array()),
            'jquery' => array('js' => array(),
                'css' => array())
        );
    }

    public function addJs($sFilePath, $sPrioriy = self::PRIORITY_LOW) {
        $this->aJs[$sPrioriy][] = $sFilePath;

        return $this;
    }

    public function addCss($sFilePath, $sPrioriy = self::PRIORITY_LOW) {
        $this->aCss[$sPrioriy][] = $sFilePath;

        return $this;
    }

    public function addPlugin() {
        $aPlugins = func_get_args();
        if (empty($aPlugins)) return null;
        $aRegisterPlugins = $this->getRegisteredPlugins();
        foreach ($aPlugins as $sPluginName) {
            if (isset($aRegisterPlugins[$sPluginName]['js']) && !empty($aRegisterPlugins[$sPluginName]['js'])) {
                foreach ($aRegisterPlugins[$sPluginName]['js'] as $sJs) {
                    if (isset($aRegisterPlugins[$sPluginName]['priority']) && !empty($aRegisterPlugins[$sPluginName]['priority'])) {
                        $sPriority = $aRegisterPlugins[$sPluginName]['priority'];
                    } else {
                        $sPriority = self::PRIORITY_LOW;
                    }
                    $this->addJs($sJs);
                }
            }

            if (isset($aRegisterPlugins[$sPluginName]['css']) && !empty($aRegisterPlugins[$sPluginName]['css'])) {
                foreach ($aRegisterPlugins[$sPluginName]['css'] as $sCss) {
                    if (isset($aRegisterPlugins[$sPluginName]['priority']) && !empty($aRegisterPlugins[$sPluginName]['priority'])) {
                        $sPriority = $aRegisterPlugins[$sPluginName]['priority'];
                    } else {
                        $sPriority = self::PRIORITY_LOW;
                    }
                    $this->addCss($sCss);
                }
            }
        }
        return $this;
    }

    public function display($sType = 'both') {

        switch ($sType) {
            case self::DISPLAY_TYPE_CSS:
                $this->displayCss();
                break;
            case self::DISPLAY_TYPE_JS:
                $this->displayJs();
                break;
            case self::DISPLAY_TYPE_BOTH:
                $this->displayCss();
                $this->displayJs();
                break;
            default:
                $this->displayCss();
                $this->displayJs();
                break;
        }
    }

    private function displayJs() {



        $sJsScript = '<script src="%s" type="text/javascript"></script>';
    }

    private function displayCss() {


        $sCssTpl = '<link rel="stylesheet" type="text/css" href="%s" />';
    }

}

