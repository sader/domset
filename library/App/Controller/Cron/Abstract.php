<?php

class App_Controller_Cron_Abstract extends App_Controller_Abstract {

    public function init() {
        parent::init();

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

}

