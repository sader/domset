<?php

class App_Controller_Admin_Abstract extends App_Controller_Abstract
{
    
	private $sLoginEntry = 'admin:auth:login';
	
	public function init()
    {
		
        parent::init(); 
        
		
        $this->_helper->_layout->setLayout('admin');  
        
       $this->view->headScript()->appendFile('/scripts/manager/admin/main.js')
                                ->appendFile('/scripts/jscrollpane/jquery.jscrollpane.min.js'); 
                                //->appendFile('/scripts/jqgrid/js/jquery.jqGrid.min.js')
                                //->appendFile('/scripts/jqgrid/js/i18n/grid.locale-pl.js');

       
       
       
        $this->view->headLink()->appendStylesheet('/styles/admin/reset.css')
                               ->appendStylesheet('/styles/admin/main.css')
                               ->appendStylesheet('/scripts/jscrollpane/jquery.jscrollpane.css') 
                               ->appendStylesheet('/styles/admin/2col.css') 
                              //  ->appendStylesheet('/scripts/jqgrid/css/ui.jqgrid.css') 
                               ->appendStylesheet('/styles/admin/style.css');
    
    }
	
	public function preDispatch()
	{
	
            
		if(Zend_Auth::getInstance()->hasIdentity())
		return; 
		
		
		
		$sRoute  = implode(":" , array($this->getRequest()->getModuleName(), 
									 $this->getRequest()->getControllerName(), 
									 $this->getRequest()->getActionName())); 
		
		
		if($sRoute == $this->sLoginEntry) return; 
		
		$this->_redirect($this->view->url(array('module' => 'admin' , 'controller' => 'auth' , 'action' => 'login'))); 
		
		
	}
	

}

