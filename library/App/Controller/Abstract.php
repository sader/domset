<?php

class App_Controller_Abstract extends Zend_Controller_Action
{




    public function forward($action, $controller = null, $module = null, array $params = null)
    {
        $this->_forward($action, $controller, $module, $params); 
    }


    
	 /**
     * Dodaj wiadomość typu "Error"
     * @param string $sMessage 
     */
    public function errorMessage($sMessage)
    {
        $this->__addMessage($sMessage, 'error');
    }
    
    /**
     * Dodaj wiadomość typu "Sukces"
     * @param string $sMessage 
     */
    public function successMessage($sMessage)
    {
        $this->__addMessage($sMessage, 'success');
    }
    
    /**
     * Dodaj wiadomość typu "Info"
     * @param string $sMessage 
     */
    public function infoMessage($sMessage)
    {
        $this->__addMessage($sMessage, 'info');
    }
    
    /**
     * Dodaj wiadomość do FlashMessengera
     * @param string $sMessage
     * @param string $sType 
     */
    private function __addMessage($sMessage, $sType = 'info')
    {
        $oMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('flashMessenger');
        $oMessenger->addMessage(array('type' => $sType , 'message' => $sMessage));
      
    }
   


}

