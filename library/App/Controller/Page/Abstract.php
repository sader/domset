<?php

class App_Controller_Page_Abstract extends App_Controller_Abstract {

    public function init() {
        parent::init();

        $this->view->headScript()
                ->appendFile('/scripts/jquery-1.8.3.min.js')
                ->appendFile('/scripts/jquery-ui-1.9.1.min.js')
                ->appendFile('/scripts/scrollto.js')
                ->appendFile('/page/js/jquery.fancybox.js?v=2.1.4')
                ->appendFile('/page/js/jquery.dimensions.js')
                ->appendFile('/page/js/bootstrap.js')
                ->appendFile('/page/js/jquery.tooltip.js')
                ->appendFile('/page/js/bootstrap-multiselect.js')
                ->appendFile('/page/js/prettify.js')
                ->appendFile('/page/js/jquery.easing.js')
                ->appendFile('/page/js/script.js')
                ->appendFile('/page/js/jquery.bxslider.min.js');



        $this->view->headLink()
                ->appendStylesheet('/page/style.css')
                ->appendStylesheet('/page/js/jquery.fancybox.css?v=2.1.4')
                ->appendStylesheet('/page/css/style1.css')
                ->appendStylesheet('/page/css/prettify.css')
                // ->appendStylesheet('/styles/jquery-ui-1.9.1.custom.min.css')
                ->appendStylesheet('/page/css/bootstrap-responsive.min.css')
                ->appendStylesheet('/page/css/bootstrap.min.css')
                ->appendStylesheet('/page/js/jquery.fancybox-buttons.css?v=1.0.5');
    }

}

