-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas wygenerowania: 30 Maj 2013, 10:31
-- Wersja serwera: 5.1.36
-- Wersja PHP: 5.2.9-2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Baza danych: `domset`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` char(50) NOT NULL,
  `password` char(32) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `name` char(50) NOT NULL,
  `surname` char(50) NOT NULL,
  `phone` char(15) NOT NULL,
  `email` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='administracja' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `agreement`
--

CREATE TABLE IF NOT EXISTS `agreement` (
  `id_offer` int(11) NOT NULL,
  `postcode` char(6) COLLATE utf8_bin NOT NULL,
  `city` char(50) COLLATE utf8_bin NOT NULL,
  `street` char(100) COLLATE utf8_bin NOT NULL,
  `house` char(10) COLLATE utf8_bin NOT NULL,
  `flat` char(10) COLLATE utf8_bin NOT NULL,
  `record_number` char(50) COLLATE utf8_bin NOT NULL,
  `land_and_mortgage_register` char(20) COLLATE utf8_bin NOT NULL,
  `agreement_type` char(20) COLLATE utf8_bin NOT NULL,
  `commission_precentage` decimal(5,2) NOT NULL,
  `commission_value` decimal(8,2) NOT NULL,
  `agreement_date` date NOT NULL,
  `agreement_end` date NOT NULL,
  `information` text COLLATE utf8_bin NOT NULL,
  `time` char(100) COLLATE utf8_bin NOT NULL,
  `person` char(100) COLLATE utf8_bin NOT NULL,
  `phone` char(100) COLLATE utf8_bin NOT NULL,
  `email` char(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `agreement_client`
--

CREATE TABLE IF NOT EXISTS `agreement_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_offer` int(11) NOT NULL,
  `id_owner` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `agreement_files`
--

CREATE TABLE IF NOT EXISTS `agreement_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_offer` int(11) NOT NULL,
  `filename` char(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(255) NOT NULL,
  `additional_data` tinyint(1) NOT NULL,
  `info` tinytext NOT NULL,
  `type` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `category_offer`
--

CREATE TABLE IF NOT EXISTS `category_offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_offer` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `data` char(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='wiele-do-wielu kategorie i oferty ' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL,
  `surname` char(50) NOT NULL,
  `parents` char(100) NOT NULL,
  `email` char(100) NOT NULL,
  `phone` int(9) NOT NULL,
  `phone_alt` int(9) NOT NULL,
  `postcode` char(6) NOT NULL,
  `city` char(50) NOT NULL,
  `street` char(50) NOT NULL,
  `house` char(5) NOT NULL,
  `flat` char(5) NOT NULL,
  `docid` char(10) NOT NULL,
  `pesel` char(10) NOT NULL,
  `info` text NOT NULL,
  `type` set('1','2','3','4') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `export`
--

CREATE TABLE IF NOT EXISTS `export` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_offer` int(10) unsigned NOT NULL,
  `id_export` tinyint(3) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `data` text,
  `remote_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_offer` int(11) NOT NULL,
  `name` char(32) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `ord` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`,`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) NOT NULL,
  `offer_type` tinyint(4) NOT NULL,
  `page` char(100) NOT NULL,
  `city` char(100) NOT NULL,
  `section` char(100) NOT NULL,
  `street` char(100) NOT NULL,
  `surface` decimal(8,2) NOT NULL,
  `rooms` int(11) NOT NULL,
  `floor` int(11) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `price_surface` decimal(6,2) NOT NULL,
  `info` text NOT NULL,
  `status` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` char(5) NOT NULL,
  `updated` datetime NOT NULL,
  `added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(255) COLLATE utf8_bin NOT NULL,
  `head` mediumtext COLLATE utf8_bin NOT NULL,
  `content` mediumtext COLLATE utf8_bin NOT NULL,
  `display` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `added` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `show` (`display`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `offer`
--

CREATE TABLE IF NOT EXISTS `offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_type` int(11) NOT NULL,
  `title` char(255) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `province` int(100) NOT NULL,
  `community` char(100) NOT NULL,
  `district` char(100) NOT NULL,
  `city` char(100) NOT NULL,
  `section` char(100) NOT NULL,
  `street` char(100) NOT NULL,
  `surface` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `price_negotiable` tinyint(1) NOT NULL,
  `price_surface` decimal(10,2) NOT NULL,
  `lat` char(32) NOT NULL,
  `lng` char(32) NOT NULL,
  `description` text NOT NULL,
  `description_pdf` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `offer_flat_rent`
--

CREATE TABLE IF NOT EXISTS `offer_flat_rent` (
  `id_offer` int(11) unsigned NOT NULL,
  `rent_additional` tinyint(1) DEFAULT NULL,
  `additional_payment` decimal(8,2) NOT NULL,
  `caution` decimal(8,2) NOT NULL,
  `rooms` tinyint(2) unsigned NOT NULL,
  `floor` char(10) NOT NULL,
  `levels` tinyint(1) NOT NULL,
  `floors_in_building` tinyint(1) NOT NULL,
  `building_type` tinyint(1) NOT NULL,
  `year` int(4) unsigned NOT NULL,
  `quality` tinyint(1) unsigned NOT NULL,
  `installation_quality` tinyint(1) NOT NULL,
  `windows` tinyint(1) NOT NULL,
  `kitchen` tinyint(2) NOT NULL,
  `noise` tinyint(2) NOT NULL,
  `heat` tinyint(2) NOT NULL,
  `available` char(100) NOT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`),
  KEY `market` (`rooms`,`floor`,`levels`,`floors_in_building`,`building_type`,`quality`,`installation_quality`,`windows`,`kitchen`,`noise`,`heat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `offer_flat_sell`
--

CREATE TABLE IF NOT EXISTS `offer_flat_sell` (
  `id_offer` int(11) unsigned NOT NULL,
  `market` tinyint(1) unsigned NOT NULL,
  `market_visible` tinyint(1) unsigned NOT NULL,
  `rooms` tinyint(2) NOT NULL,
  `floor` char(10) NOT NULL,
  `levels` tinyint(1) NOT NULL,
  `floors_in_building` tinyint(1) NOT NULL,
  `building_type` tinyint(1) NOT NULL,
  `year` int(4) unsigned NOT NULL,
  `building_material` tinyint(1) unsigned NOT NULL,
  `ownership` tinyint(1) unsigned NOT NULL,
  `rent` decimal(8,2) unsigned NOT NULL,
  `quality` tinyint(1) unsigned NOT NULL,
  `installation_quality` tinyint(1) NOT NULL,
  `windows` tinyint(1) NOT NULL,
  `kitchen` tinyint(2) NOT NULL,
  `noise` tinyint(2) NOT NULL,
  `heat` tinyint(2) NOT NULL,
  `available` char(100) NOT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`),
  KEY `market` (`market`,`market_visible`,`rooms`,`floor`,`levels`,`floors_in_building`,`building_type`,`building_material`,`ownership`,`rent`,`quality`,`installation_quality`,`windows`,`kitchen`,`noise`,`heat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `offer_house_rent`
--

CREATE TABLE IF NOT EXISTS `offer_house_rent` (
  `id_offer` int(11) NOT NULL,
  `available` char(100) NOT NULL,
  `house_surface` decimal(6,2) NOT NULL,
  `terrain_surface` decimal(6,2) NOT NULL,
  `building_type` tinyint(1) NOT NULL,
  `rooms` tinyint(2) NOT NULL,
  `floors` tinyint(1) NOT NULL,
  `attic` tinyint(1) NOT NULL,
  `basement` tinyint(1) NOT NULL,
  `year` year(4) NOT NULL,
  `building_material` tinyint(1) NOT NULL,
  `quality` tinyint(1) NOT NULL,
  `windows` tinyint(1) NOT NULL,
  `roof` tinyint(1) NOT NULL,
  `roof_type` tinyint(1) NOT NULL,
  `heat` tinyint(1) NOT NULL,
  `sewerage` tinyint(1) NOT NULL,
  `fence` tinyint(1) NOT NULL,
  `garage` tinyint(1) NOT NULL,
  `access` tinyint(1) NOT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `offer_house_sell`
--

CREATE TABLE IF NOT EXISTS `offer_house_sell` (
  `id_offer` int(11) NOT NULL,
  `market` int(11) NOT NULL,
  `market_visible` tinyint(1) NOT NULL,
  `house_surface` decimal(6,2) NOT NULL,
  `terrain_surface` decimal(6,2) NOT NULL,
  `building_type` tinyint(1) NOT NULL,
  `rooms` tinyint(2) NOT NULL,
  `floors` tinyint(1) NOT NULL,
  `attic` tinyint(1) NOT NULL,
  `basement` tinyint(1) NOT NULL,
  `year` year(4) NOT NULL,
  `building_material` tinyint(1) NOT NULL,
  `quality` tinyint(1) NOT NULL,
  `windows` tinyint(1) NOT NULL,
  `roof` tinyint(1) NOT NULL,
  `roof_type` tinyint(1) NOT NULL,
  `heat` tinyint(1) NOT NULL,
  `sewerage` tinyint(1) NOT NULL,
  `fence` tinyint(1) NOT NULL,
  `garage` tinyint(1) NOT NULL,
  `access` tinyint(1) NOT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `offer_land_rent`
--

CREATE TABLE IF NOT EXISTS `offer_land_rent` (
  `id_offer` int(11) NOT NULL,
  `land_type` tinyint(4) NOT NULL,
  `shape` tinyint(1) NOT NULL,
  `width` char(50) NOT NULL,
  `height` char(50) NOT NULL,
  `services` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  `fence` tinyint(1) NOT NULL,
  `access` tinyint(1) NOT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `offer_land_sell`
--

CREATE TABLE IF NOT EXISTS `offer_land_sell` (
  `id_offer` int(11) NOT NULL,
  `land_type` tinyint(4) NOT NULL,
  `shape` tinyint(1) NOT NULL,
  `width` char(50) NOT NULL,
  `height` char(50) NOT NULL,
  `services` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  `fence` tinyint(1) NOT NULL,
  `access` tinyint(1) NOT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `offer_local_rent`
--

CREATE TABLE IF NOT EXISTS `offer_local_rent` (
  `id_offer` int(11) NOT NULL,
  `purpose` tinyint(1) NOT NULL,
  `rent_additional` tinyint(1) NOT NULL,
  `additional_payment` decimal(8,2) NOT NULL,
  `building_type` tinyint(1) NOT NULL,
  `entry` tinyint(1) NOT NULL,
  `height` char(50) NOT NULL,
  `rooms` tinyint(4) NOT NULL,
  `floor` tinyint(4) NOT NULL,
  `levels` tinyint(4) NOT NULL,
  `floors_in_building` tinyint(4) NOT NULL,
  `year` year(4) NOT NULL,
  `quality` tinyint(1) NOT NULL,
  `windows` tinyint(1) NOT NULL,
  `heat` tinyint(1) NOT NULL,
  `available` char(100) NOT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `offer_local_sell`
--

CREATE TABLE IF NOT EXISTS `offer_local_sell` (
  `id_offer` int(11) NOT NULL,
  `market` tinyint(1) NOT NULL,
  `market_visible` tinyint(1) NOT NULL,
  `purpose` tinyint(1) NOT NULL,
  `building_type` tinyint(1) NOT NULL,
  `entry` tinyint(1) NOT NULL,
  `height` char(50) NOT NULL,
  `rooms` tinyint(4) NOT NULL,
  `floor` tinyint(4) NOT NULL,
  `levels` tinyint(4) NOT NULL,
  `floors_in_building` tinyint(4) NOT NULL,
  `year` year(4) NOT NULL,
  `ownership` tinyint(4) NOT NULL,
  `rent` decimal(6,2) NOT NULL,
  `rent_description` text NOT NULL,
  `quality` tinyint(1) NOT NULL,
  `windows` tinyint(1) NOT NULL,
  `heat` tinyint(1) NOT NULL,
  `available` char(100) DEFAULT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `offer_warehouse_rent`
--

CREATE TABLE IF NOT EXISTS `offer_warehouse_rent` (
  `id_offer` int(11) NOT NULL,
  `purpose` tinyint(1) NOT NULL,
  `rooms` tinyint(1) NOT NULL,
  `height` char(50) NOT NULL,
  `floor` tinyint(1) NOT NULL,
  `levels` tinyint(1) NOT NULL,
  `year` year(4) NOT NULL,
  `quality` tinyint(1) NOT NULL,
  `heat` tinyint(1) NOT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `offer_warehouse_sell`
--

CREATE TABLE IF NOT EXISTS `offer_warehouse_sell` (
  `id_offer` int(11) NOT NULL,
  `market` tinyint(1) NOT NULL,
  `market_visible` tinyint(1) NOT NULL,
  `purpose` tinyint(1) NOT NULL,
  `rooms` tinyint(1) NOT NULL,
  `height` char(50) NOT NULL,
  `floor` tinyint(1) NOT NULL,
  `levels` tinyint(1) NOT NULL,
  `year` year(4) NOT NULL,
  `quality` tinyint(1) NOT NULL,
  `heat` tinyint(1) NOT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(255) COLLATE utf8_bin NOT NULL,
  `head` mediumtext COLLATE utf8_bin NOT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `presentation`
--

CREATE TABLE IF NOT EXISTS `presentation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) NOT NULL,
  `id_offer` int(11) NOT NULL,
  `info` text NOT NULL,
  `status` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` char(5) NOT NULL,
  `added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `promoted`
--

CREATE TABLE IF NOT EXISTS `promoted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_offer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='ustawienie ofert promowanych' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='lista dzielnic dla miasta domyślnego' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `visibility`
--

CREATE TABLE IF NOT EXISTS `visibility` (
  `id_offer` int(11) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `wanted`
--

CREATE TABLE IF NOT EXISTS `wanted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_type` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `province` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16') DEFAULT NULL,
  `district` char(100) DEFAULT NULL,
  `city` char(100) DEFAULT NULL,
  `section` char(100) DEFAULT NULL,
  `street` char(100) DEFAULT NULL,
  `surface_from` decimal(10,2) DEFAULT NULL,
  `surface_to` decimal(10,2) DEFAULT NULL,
  `price_from` decimal(10,2) DEFAULT NULL,
  `price_to` decimal(10,2) DEFAULT NULL,
  `price_surface_from` decimal(10,2) DEFAULT NULL,
  `price_surface_to` decimal(10,2) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `wanted_flat`
--

CREATE TABLE IF NOT EXISTS `wanted_flat` (
  `id_offer` int(11) unsigned NOT NULL,
  `rooms_from` tinyint(2) unsigned DEFAULT NULL,
  `rooms_to` tinyint(4) DEFAULT NULL,
  `floor_from` tinyint(1) DEFAULT NULL,
  `floor_to` tinyint(4) DEFAULT NULL,
  `levels_from` tinyint(1) DEFAULT NULL,
  `levels_to` tinyint(4) DEFAULT NULL,
  `floors_in_building_from` tinyint(1) NOT NULL,
  `floors_in_building_to` int(11) DEFAULT NULL,
  `building_type` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `year_from` year(4) DEFAULT NULL,
  `year_to` year(4) DEFAULT NULL,
  `quality` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `installation_quality` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `windows` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `kitchen` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `noise` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `heat` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`),
  KEY `market` (`rooms_from`,`floor_from`,`levels_from`,`floors_in_building_from`,`building_type`,`quality`,`installation_quality`,`windows`,`kitchen`,`noise`,`heat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `wanted_house`
--

CREATE TABLE IF NOT EXISTS `wanted_house` (
  `id_offer` int(11) NOT NULL,
  `house_surface_from` decimal(6,2) DEFAULT NULL,
  `house_surface_to` decimal(6,2) DEFAULT NULL,
  `terrain_surface_from` decimal(6,2) DEFAULT NULL,
  `terrain_surface_to` decimal(6,2) DEFAULT NULL,
  `building_type` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `rooms_from` tinyint(2) DEFAULT NULL,
  `rooms_to` tinyint(2) DEFAULT NULL,
  `floors_from` tinyint(1) DEFAULT NULL,
  `floors_to` tinyint(1) DEFAULT NULL,
  `attic` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `basement` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `year_from` year(4) DEFAULT NULL,
  `year_to` year(4) DEFAULT NULL,
  `building_material` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `quality` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `windows` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `roof` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `roof_type` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `heat` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `sewerage` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `fence` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `garage` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `access` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `wanted_land`
--

CREATE TABLE IF NOT EXISTS `wanted_land` (
  `id_offer` int(11) NOT NULL,
  `land_type` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `shape` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `width_from` int(10) DEFAULT NULL,
  `width_to` int(11) DEFAULT NULL,
  `height_from` int(10) DEFAULT NULL,
  `height_to` int(10) DEFAULT NULL,
  `services` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  `fence` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `access` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `wanted_local`
--

CREATE TABLE IF NOT EXISTS `wanted_local` (
  `id_offer` int(11) NOT NULL,
  `purpose` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `building_type` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `entry` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `height_from` int(10) DEFAULT NULL,
  `height_to` int(10) DEFAULT NULL,
  `rooms_from` tinyint(4) DEFAULT NULL,
  `rooms_to` int(11) DEFAULT NULL,
  `floor_from` tinyint(4) DEFAULT NULL,
  `floor_to` int(11) DEFAULT NULL,
  `levels_from` tinyint(4) DEFAULT NULL,
  `levels_to` int(11) DEFAULT NULL,
  `floors_in_building_from` tinyint(4) DEFAULT NULL,
  `floors_in_building_to` int(11) DEFAULT NULL,
  `year_from` year(4) DEFAULT NULL,
  `year_to` year(4) DEFAULT NULL,
  `quality` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `windows` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `heat` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `wanted_warehouse`
--

CREATE TABLE IF NOT EXISTS `wanted_warehouse` (
  `id_offer` int(11) NOT NULL,
  `purpose` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16') DEFAULT NULL,
  `rooms_from` tinyint(1) DEFAULT NULL,
  `rooms_to` tinyint(1) DEFAULT NULL,
  `height_from` int(10) DEFAULT NULL,
  `height_to` int(10) DEFAULT NULL,
  `floor_from` tinyint(1) DEFAULT NULL,
  `floor_to` tinyint(1) DEFAULT NULL,
  `levels_from` tinyint(1) DEFAULT NULL,
  `levels_to` tinyint(1) DEFAULT NULL,
  `year_from` year(4) DEFAULT NULL,
  `year_to` year(4) DEFAULT NULL,
  `quality` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `heat` set('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `additional` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20') DEFAULT NULL,
  PRIMARY KEY (`id_offer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
