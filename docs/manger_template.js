var manager = {
    init : function(){
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();  
        if (manager.events.init !== 'undefined')
            manager.events.init();  
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();  
    }, 
    widgets : {
        init : function(){}
    }, 
    events : {
        init : function(){}   
    }, 
    handlers : {
        init : function(){}  
    }, 
    helpers : {
}
};

$(document).ready(function(){
    
    manager.init(); 
    
}); 
