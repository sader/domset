var manager = {
    map : null,
    marker : null,
    defaultAddress : 'Polska Ełk',
    
    init : function(){
        manager.widgets.init(); 
        // manager.events.init(); 
     //   manager.handlers.init(); 
    },
    widgets : {
       
        init : function(){},
       
        map : function(location) {
            var mapOptions = {
                center: location,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            manager.map = new google.maps.Map(document.getElementById("map"), mapOptions);
            manager.widgets.marker(location); 
            
            
        } , 
        marker : function (location){
            manager.marker = new google.maps.Marker({
                position: location,
                map: manager.map,
                draggable : false
            }); 
       
        }  
    },
    events : {
          },
    handlers : {
            
        init : function(){
            manager.handlers.location()
            },
            
        location : function(){
        
            if(manager.handlers.setDeclaredLocation()) {
                return; 
            }
        
            manager.handlers.setLocationByAddress(manager.defaultAddress); 
        },
        setDeclaredLocation : function() {
        
            var lat = locator.lat; 
            var lng = locator.lng; 
            
            console.log(locator.lat);
            
            if (lat != "" && lng != "")
            {
                console.log(lat);
                console.log(lng);
                
                manager.widgets.map(new google.maps.LatLng(lat, lng)); 
                return true; 
            }
            else return false; 
        },
        setLocationByAddress : function(address) {
        
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( {
                'address': address
            }, function(results, status) {
                
                if (status == google.maps.GeocoderStatus.OK) {
                    manager.widgets.map(results[0].geometry.location); 
                    return true; 
                } else {
                    return false; 
                }
            });
        },
        helpers : {}
    }
};

$(document).ready(function(){
    
    manager.init(); 
    
}); 