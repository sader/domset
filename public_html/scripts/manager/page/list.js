$(document).ready(function() {
    window.prettyPrint() && prettyPrint();
    $('.superwybor').multiselect({});
    $('.superselect').multiselect({
        buttonClass: 'btn',
        buttonWidth: '120px',
        buttonContainer: '<div class="btn-group" />',
        maxHeight: false,
        buttonText: function(options) {
            
            
            if (options.length == 0) {
                return 'Wybierz <b class="caret"></b>';
            }
            else if (options.length == 1) {
                return $(options).text() + ' <b class="caret"></b>';
            }
            else if (options.length > 0) {
                return options.length + ' wybrane <b class="caret"></b>';
            }
            else {
                var selected = '';
                options.each(function() {
                    selected += $(this).text() + ', ';
                });
                return selected.substr(0, selected.length -2) + ' <b class="caret"></b>';
            }
        },
        onChange:function(element, checked){
          
          
            switch($(element).parent().attr('id'))
            {
                case 'city':
                    changeSection(type); 
                    break;
                case 'district':
                    changeCommunity(type); 
                    break;
                case 'community':
                    changeCity(type); 
                    break;
            }
        }
    });
   
  
    changeSection(type); 
    changeCommunity(type); 
    changeCity(type); 
  

    
});
    
    
function changeSection(type)
{
    var cities = [];
    $.each($('#city option:selected'),  function() {
        cities[cities.length] = $(this).val();  
    });
            
    $.ajax({
        type: "POST",
        url: "/page/offer/city/type/" + type,
        data: 'cities=' + cities.join('|'),
        dataType : 'json'
    }).done(function(data) {
		
        $('#section option').remove(); 
        $.each($(data),  function(index, val) {
            if(jQuery.inArray(val.section, get.section) > -1)
            {
                $('#section').append('<option selected="selected" value="' + val.section + '">' + val.section + '</option>');  
            }
            else
            {
                $('#section').append('<option value="' + val.section + '">' + val.section + '</option>');  
            }
        });
        $('#section').multiselect('rebuild');    
                    
    });  
}
    
    
function changeCommunity(type)
{
    if($('#district').length > 0)
    {
       
        var districts = [];
        $.each($('#district option:selected'),  function() {
            districts[districts.length] = $(this).val();  
        });
            
        $.ajax({
            type: "POST",
            url: "/page/offer/district/type/" + type,
            data: 'districts=' + districts.join('|'),
            dataType : 'json'
        }).done(function(data) {
		
            $('#community option').remove(); 
            $.each($(data),  function(index, val) {
                if(jQuery.inArray(val.community, get.community) > -1)
                {
                    $('#community').append('<option selected="selected" value="' + val.community + '">' + val.community + '</option>');  
                }
                else
                {
                    $('#community').append('<option value="' + val.community + '">' + val.community + '</option>');  
                }
            });
            $('#community').multiselect('rebuild');    
                    
        });  
    }
}
    
function changeCity(type)
{   
     
    if($('#community').length > 0)
    {
       
        var communities = [];
        $.each($('#community option:selected'),  function() {
            communities[communities.length] = $(this).val();  
        });
            
        $.ajax({
            type: "POST",
            url: "/page/offer/community/type/" + type,
            data: 'communities=' + communities.join('|'),
            dataType : 'json'
        }).done(function(data) {
	
            $('#city option').remove(); 
                  $.each($(data),  function(index, val) {
                if(jQuery.inArray(val.city, get.city) > -1)
                {
                    $('#city').append('<option selected="selected" value="' + val.city + '">' + val.city + '</option>');  
                }
                else
                {
                    $('#city').append('<option value="' + val.city + '">' + val.city + '</option>');  
                }
            });
            $('#city').multiselect('rebuild');    
                    
        });  
    }
}
    
$(document).ready( function(){					 
    var buttons = {
        previous:$('#galeria .button-next') , 
        next:$('#galeria .button-previous')
    };			
    $('#galeria').lofJSidernews( {
        interval : 4000,
        direction		: 'opacitys',	
        easing			: 'easeInOutExpo',
        duration		: 1200,
        maxItemDisplay  : 4,
        navPosition     : 'horizontal',
        navigatorHeight : 86,
        navigatorWidth  : 160,
        mainWidth		: 724,
        buttons			: buttons
    } );	
});


   
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}