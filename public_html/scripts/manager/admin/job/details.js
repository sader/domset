var manager = {
    init: function() {
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();
        if (manager.events.init !== 'undefined')
            manager.events.init();
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();
    },
    widgets: {
        init: function() {
            $('#date').datetimepicker({altField: "#time", dateFormat: 'yy-mm-dd'});
        }
    },
    events: {
        init: function() {
            $('#client_phone').keyup(function() {
                manager.handlers.clientList(this);
                return false;
            });

            manager.helpers.getMainContainer().on('keyup', '#price', function() {
                manager.handlers.pricePerSurface();
            });

            manager.helpers.getMainContainer().on('keyup', '#surface', function() {
                manager.handlers.pricePerSurface();
            });

            $('#status').change(function() {
                manager.handlers.statusChange($(this).val());
            });

            $('body').on('click', '.addquick', function() {

                $('#client_phone').val($(this).attr('data-phone'));
                
                manager.handlers.clientDetail(this);
                $('#client_phone').focus();

            });

            $('#submit').click(function() {
                if ($('#date').hasClass('hidden'))
                {
                    $('#date').val('0000-00-00');

                }
            });
        }
    },
    handlers: {
        init: function() {
            this.attachmentElements();
            this.statusChange($('#status').val());
        },
        statusChange: function(status) {

            if (status == 2 || status == 4 || status == 5 || status == 6)
            {
                $('#date').addClass('hidden');
                $('#date').parent().parent().hide();
            }
            else
            {
                $('#date').removeClass('hiddden');
                $('#date').parent().parent().show();
            }
        },
        attachmentElements: function() {
            $('.attachment').each(function() {
                $(this).appendTo($('#' + $(this).attr('data')).parent());
            });
        },
        clientDetail: function(input) {


            $.ajax({
                url: '/admin/client/quickdetail',
                type: 'POST',
                data: 'id=' + $(input).attr('data'),
                dataType: 'json',
                success: function(response) {
                    if (response.status)
                    {
                        $('#dialog').html(response.content).dialog({
                            heigth: 1200,
                            width: 700,
                            position: {my: "left top", at: "right bottom", of: $(input)},
                        });
                        $('#main_client').focus();
                    }
                    else
                        alert(response.message);
                }
            });

        },
        pricePerSurface: function() {

            var price = ($('#price').val()).replace(/\s+/g, '');
            var surface = ($('#surface').val()).replace(/\s+/g, '');

            if (price == '' || price == '0.00' || surface == '' || surface == '0.00')
                return false;

            var new_price = manager.helpers.round(parseFloat(price / surface), 2);

            manager.helpers.getPricePerSurfaceField().val(new_price);
            return true;
        },
        clientList: function(input) {

            var inputed = $(input).val();

            if (inputed.length <= 3)
            {
                if ($('.ui-dialog').length > 0)
                {
                    $('#dialog').dialog('close');
                }
                return false;
            }

            $.ajax({
                url: '/admin/client/quicklist/type/add',
                type: 'POST',
                data: 'phone=' + inputed,
                dataType: 'json',
                success: function(response) {
                    if (response.status)
                    {
                        $('#dialog').html(response.content).dialog({
                            heigth: 1200,
                            width: 700,
                            position: {my: "left top", at: "right bottom", of: $(input)},
                            title: null
                        });

                        $(input).focus();
                    }
                    else
                        alert(response.message);
                }
            });


        },
    },
    helpers: {
        getMainContainer: function() {
            return $('#main');
        },
        getOfferType: function() {
            return $('#type');
        },
        getCity: function() {
            return $('#city');
        },
        getForm: function() {
            return $('#offer');
        },
        getBody: function() {
            return $('body');
        },
        getPricePerSurfaceField: function() {
            return $('#price_surface');
        },
        round: function(number, x) {
            var x = (!x ? 2 : x);
            return Math.round(number * Math.pow(10, x)) / Math.pow(10, x);
        }
    }
};

$(document).ready(function() {
    manager.init();
});
