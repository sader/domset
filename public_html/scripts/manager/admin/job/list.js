var manager = {
    init: function() {
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();
        if (manager.events.init !== 'undefined')
            manager.events.init();
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();
    },
    widgets: {
        init: function() {
             $('#ndate').datepicker({ dateFormat : 'yy-mm-dd' });  
        }
    },
    events: {
        init: function() {    }
    },
    handlers: {
       
       
        init: function() {
            this.attachmentElements();
        },
        attachmentElements: function() {
            $('.attachment').each(function() {
                $(this).appendTo($('#' + $(this).attr('data')).parent());
            });
        },
    },
    helpers: {
        getMainContainer : function() {
            return $('#main');
        },
        getOfferType : function() {
            return $('#type');
        },
        getCity : function() {
            return $('#city');   
        },
        getForm :  function() {
            return $('#offer');
        }
    }
};

$(document).ready(function() {
    manager.init();
});
