var manager = {
    init: function() {
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();
        if (manager.events.init !== 'undefined')
            manager.events.init();
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();
    },
    widgets: {
        init: function() {
            $('#id_client').select2();
        }
    },
    events: {
        init: function() {
            manager.helpers.getMainContainer().one('change', '#type', function() {
                manager.handlers.offerType();
            }); 
             
            $('body').on('click', '#clientSubmit', function() {
                manager.handlers.addNewOwner();
                return false;
            });

            $('#add_client').click(function() {
                manager.handlers.displayAddOwnerForm();
                return false;
            });
        }
    },
    handlers: {
        init: function() {
            this.attachmentElements();
        },
        attachmentElements: function() {
            $('.attachment').each(function() {
                $(this).appendTo($('#' + $(this).attr('data')).parent());
            });
        },
        offerType: function() {
            $.ajax({
                url: '/admin/wanted/change',
                type: 'POST',
                data: manager.helpers.getForm().serialize(),
                dataType: 'json',
                success: function(response) {
                    if (response.status)
                    {
                        manager.helpers.getForm().replaceWith(response.content);
                        manager.init();
                    }
                    else {
                        $('#dialog').html(response.message).dialog();
                        return false;
                    }
                }
            });
        },
        displayAddOwnerForm: function() {

            $.ajax({
                url: '/admin/client/add/prepare/2',
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    if (response.status)
                    {
                        $('#dialog').attr('title', 'Dodaj nowego klienta').html(response.content).dialog({
                            heigth: 1200,
                            width: 700,
                            position: 'top'
                        });
                    }
                    else
                        alert(response.message);
                }
            });
        },
        addNewOwner: function() {

            $.ajax({
                url: '/admin/client/add',
                type: 'POST',
                dataType: 'json',
                data: $('#clientForm').serialize(),
                success: function(response) {
                    if (response.status)
                    {
                        $('#dialog').dialog('close');
                        $('#id_client').prepend('<option value="' + response.content.id + '">' + response.content.value + '</option>');
                        $('#id_client').val(response.content.id);
                        $('.select2-choice span').text(response.content.value);
                    }
                    else
                        $('#dialog').html(response.content);
                }
            });
        }
    },
    helpers: {
        getMainContainer: function() {
            return $('#main');
        },
        getOfferType: function() {
            return $('#type');
        },
        getCity: function() {
            return $('#city');
        },
        getForm: function() {
            return $('#offer');
        }
    }
};

$(document).ready(function() {
    manager.init();
});
