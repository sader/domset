var manager = {
    init: function() {
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();
        if (manager.events.init !== 'undefined')
            manager.events.init();
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();
    },
    widgets: {
        init: function() {
        }
    },
    events: {
        init: function() {


            manager.helpers.getMainContainer().on('click', '#list_wanted table tr td.clickable', function() {

                var link_go_to = $(this).parent().find('a.wanted_filter');

                if (link_go_to.length > 0)
                {
                    manager.handlers.populateOffers(link_go_to.attr('href'));
                }
                return false;
            });


            manager.helpers.getMainContainer().on('click', '#list_offers table tr', function(e) {


                if (!$(e.target).hasClass("offers_filter")) {

                    var link_go_to = $(this).find('a.offers_filter');

                    if (link_go_to.length > 0)
                    {
                        manager.handlers.populateWanted(link_go_to.attr('href'));
                    }
                    return false;

                }

            });


            //        manager.helpers.getMainContainer().one('click', '.wanted_filter', function() {
            //        manager.handlers.populateOffers($(this).attr('href'));
            //        return false;
            //    });


            //        manager.helpers.getMainContainer().one('click', '.offers_filter', function() {
            //        manager.handlers.populateWanted($(this).attr('href'));
            //        return false;
            //    });
        }
    },
    handlers: {
        init: function() {

        },
        populateOffers: function(href) {

            $.ajax({
                url: href,
                type: 'GET',
                success: function(response) {
                    $('#list_offers').fadeOut().html(response).fadeIn();
                }
            });
        },
        populateWanted: function(href) {

            $.ajax({
                url: href,
                type: 'GET',
                success: function(response) {
                    $('#list_wanted').fadeOut().html(response).fadeIn();
                }
            });
        }
    },
    helpers: {
        getMainContainer: function() {
            return $('#main');
        },
        getOfferType: function() {
            return $('#type');
        },
        getCity: function() {
            return $('#city');
        },
        getForm: function() {
            return $('#offer');
        }
    }
};

$(document).ready(function() {
    manager.init();
});
