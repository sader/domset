var manager = {
    init: function() {
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();
        if (manager.events.init !== 'undefined')
            manager.events.init();
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();
    },
    widgets: {
        init: function() {
            $('#date').datetimepicker({altField: "#time", dateFormat: 'yy-mm-dd'});
            $('#id_client').select2();
            $('#price, #surface, #price_surface').priceFormat({prefix: '', thousandsSeparator: ''});
        }
    },
    events: {
        init: function() {
            
             $('body').on('click' , '#clientSubmit' , function(){
               manager.handlers.addNewOwner();
                return false; 
            });
                        
            
            $('#add_client').click(function() {
                manager.handlers.displayAddOwnerForm();
                return false; 
            });
        }

    },
    handlers: {
        init: function() {
            this.attachmentElements();
        },
        attachmentElements: function() {
            $('.attachment').each(function() {
                $(this).appendTo($('#' + $(this).attr('data')).parent());
            });
        },
     
        
        displayAddOwnerForm: function() {

            $.ajax({
                url: '/admin/client/add/prepare/1',
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    if (response.status)
                    {
                        $('#dialog').attr('title', 'Dodaj nowego klienta').html(response.content).dialog({
                            heigth: 1200,
                            width: 700,
                            position: 'top'
                        });
                    }
                    else
                        alert(response.message);
                }
            });
        },
        
        addNewOwner : function() {
          
          $.ajax( {
                url : '/admin/client/add/response/usr',
                type : 'POST',
                dataType : 'json',
                data : $('#clientForm').serialize(),
                success : function( response ) {
                    if(response.status)
                    {
                        $('#dialog').dialog('close');
                        $('#id_client').prepend('<option value="' + response.content.id + '">' + response.content.value + '</option>'); 
                        $('#id_client').val(response.content.id); 
                        $('.select2-choice span').text(response.content.value); 
                    }
                    else  $('#dialog').html(response.content); 
                }
            }); 
        },
    },
    helpers: {
        getMainContainer: function() {
            return $('#main');
        },
        getOfferType: function() {
            return $('#type');
        },
        getCity: function() {
            return $('#city');
        },
        getForm: function() {
            return $('#offer');
        }
    }
};

$(document).ready(function() {
    manager.init();
});
