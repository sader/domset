var manager = {
    init: function() {
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();
        if (manager.events.init !== 'undefined')
            manager.events.init();
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();
    },
    widgets: {
        init: function() {

            this.droppableAreas();

        },
        droppableAreas: function() {

            $('#promo-trash').droppable({
                greedy: true,
                drop: function(event, ui) {
                    manager.handlers.dropPictureOnTrash(event, ui)
                }
            });

        }
    },
    events: {
        init: function() {

            $('.promote').on('click', function() {
                manager.handlers.addPromotion($(this).attr('data'));
                $(this).remove(); 
                return false;
            });


            $('#promotedSubmit').on('click', function() {

                var offers = [];
                $.each($('li.promoted'), function() {
                    offers[offers.length] = $(this).attr('data');
                });
                $('input#offers').val(offers.join('|'));

            });


        }
    },
    handlers: {
        init: function() {

            this.populatePromotion();



        },
         addPromotion : function(id){
       
             if(id === '' || $('li[data='+ id +']').length > 0) {
                 alert('Ta oferta jest już promowana'); 
                 return false;
             }
     
             var appened = (manager.helpers.getPromoted() === '') ? id : manager.helpers.getPromoted() + '|' + id; 
             manager.handlers.reloadPromotions(appened, 'new');
         },
                
        populatePromotion: function()
        {
                    manager.handlers.reloadPromotions($('input#offers').val(), 'existed');

        },
        reloadPromotions: function(promotions, type) {

            $.ajax({
                url: '/admin/offer/promotions',
                type: 'POST',
                data: 'type=' + type + '&promotions=' + promotions,
                dataType: 'json',
                success: function(response) {
                    if (response.status)
                    {
                        $('li.promoted').remove(); 
                        
                        $.each(response.content, function(){
                            
                              $('<li />').addClass('promoted')
                                .attr('data', this.id)
                                .html(manager.helpers.getPromotedBoxTpl(this))
                                .appendTo('#prom-list');
                            
                        }); 
                        
                      
                        manager.handlers.reloadSort();
                    }
                    else {
                        $('#dialog').html(response.content).dialog();
                        return false;
                    }
                }
            });

            return false;
        },
        reloadSort: function(target) {
            $('#prom-list').sortable({
                connectWith: '.promoted', update : function() { manager.handlers.reloadPromotions(manager.helpers.getPromoted(), 'new'); }
            }).disableSelection();
        },
        dropPictureOnTrash: function(event, ui) {

          ui.draggable.fadeOut('slow', function() {
                            $(this).remove();
                            manager.handlers.reloadPromotions(manager.helpers.getPromoted(), 'new');
                        });
          
           
        }

    },
    helpers: {
        getPromotedBoxTpl: function(data) {

            var image = (data.photo !== null) ? '<img src="/image/' + data.id + '/' + data.photo + '/128/96" />'
                    : '<img src="/image/' + data.id + '/empty/128/96" />';

            return '<p class="title"><b>' + data.title + '</b></p>' +
                   '<p class="image">' + image + '</p>' +
                   '<p class="adress">' + data.city + ' ' + data.street + '</p>' +
                   '<p class="price">' + data.price + ' PLN</p>';


        },
       getPromoted : function()
       {
           var offers = [];
                $.each($('li.promoted'), function() {
                    offers[offers.length] = $(this).attr('data');
                });
                
                return offers.join('|');

       }




    }
};

$(document).ready(function() {

    manager.init();

});
