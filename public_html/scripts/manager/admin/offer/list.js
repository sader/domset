var manager = {
    init: function() {
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();
        if (manager.events.init !== 'undefined')
            manager.events.init();
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();
    },
    widgets: {
        init: function() {
            $(function() {
                $('#limiter').masonry({
                    // options

                    columnWidth: 250
                });
            });
        }
    },
    events: {
        init: function() {

            $('#limiter').hide();

            $('#hide-filters').click(function() {

                $('#limiter').hide();
                $(this).hide();
                $('#show-filters').show();

            });
            
            $('#main').on('click' , '#tablegrid table tr' , function(){
                
                var link_go_to = $(this).find('.goto'); 
                
                if(link_go_to.length > 0)
                    {
                        location.href = link_go_to.attr('href'); 
                    }
                
                
            }); 
            
            $('#show-filters').click(function() {
                $('#limiter').show();
                $(this).hide();
                $('#hide-filters').show();

                $('#limiter').masonry({
                    columnWidth: 250});
            });
            
            
            
        }
    },
    handlers: {
        init: function() {
            this.attachmentElements();
        },
        attachmentElements: function() {
            $('.attachment').each(function() {
                $(this).appendTo($('#' + $(this).attr('data')).parent());
            });
        }
    },
    helpers: {
    }
};

$(document).ready(function() {
    manager.init();
});
