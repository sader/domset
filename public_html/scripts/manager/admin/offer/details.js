var manager = {
    defaultCity : 'Ełk',
    
    init : function(){
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();  
        if (manager.events.init !== 'undefined')
            manager.events.init();  
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();  
    }, 
    widgets : {
        init : function(){
            this.inputMasks(); 
            this.textareas(); 
        },
        textareas : function(){
            
           
            for(var instanceName in CKEDITOR.instances)
            {
                CKEDITOR.remove(CKEDITOR.instances[instanceName]);    
            }
            CKEDITOR.replace( 'description' );
            CKEDITOR.replace( 'description_pdf' );
             
        },
        inputMasks : function() {
           // $('#rent').priceFormat({
           //     prefix: '', 
           //     thousandsSeparator: ''
           // });
           // $('#price').priceFormat({
           //     prefix: '', 
           //     thousandsSeparator: ''
           // });
          //  $('#price_surface').priceFormat({
          //      prefix: '', 
          //      thousandsSeparator: '' 
          //  });
         //   $('#surface').priceFormat({
         //       prefix: '', 
         //       thousandsSeparator: ''
        //    });
            
         //   if(($('#house_surface')).length > 0)
         //   {
         //       $('#house_surface').priceFormat({
         //           prefix: '', 
         //           thousandsSeparator: ''
         //       });
         //   }
                
         //   if(($('#terrain_surface')).length > 0)
         //   {
         //       $('#terrain_surface').priceFormat({
         //           prefix: '', 
         //           thousandsSeparator: ''
         //       });
         //   }
         //   
         //      if(($('#caution')).length > 0)
         //   {
        //        $('#caution').priceFormat({
        //            prefix: '', 
        //            thousandsSeparator: ''
        //        });
        //    }
            
            
        //       if(($('#additional_payment')).length > 0)
        //    {
        //        $('#additional_payment').priceFormat({
        //            prefix: '', 
        //            thousandsSeparator: ' '
        //        });
        //    }
            
            
            $('#year').mask('9999'); 
        }
    },
    events : {
        init : function(){
            manager.helpers.getMainContainer().on('click' , '#round' ,function(){
                manager.handlers.RoundPrice(); 
            }),
            manager.helpers.getMainContainer().one( 'change' , '#type', function() {
                manager.handlers.offerType(); 
            }),
        
            manager.helpers.getMainContainer().on('keyup' , '#price' ,function(){
                manager.handlers.pricePerSurface();
            });
          
            manager.helpers.getMainContainer().on('keyup' , '#surface' ,function(){
                manager. handlers.pricePerSurface();
            });
            
            manager.helpers.getMainContainer().on('keyup' , '#title' ,function(){
                manager.handlers.titleLength();
                
            });
            
            manager.helpers.getMainContainer().on('keyup' , '#city' , function(){
                manager.handlers.sectionForDefaultCity(); 
            }); 
            
            manager.helpers.getMainContainer().on('click' , '#price_round' , function(){
                manager.handlers.roundPrice(); 
            }); 
             
            manager.helpers.getMainContainer().on('click' , '#add_section' , function(){
                manager.handlers.addSectionToDefaultCity(); 
                return false; 
            });
            manager.helpers.getBody().on('click' , '#addSectionSubmit' , function(){
                manager.handlers.addNewSection();  
                return false; 
            });
            
            manager.helpers.getBody().on('click' , '.ui-dialog-titlebar-close' , function(){
                manager.handlers.hiddenFieldText(); 
            }); 
            
            
            manager.helpers.getMainContainer().on('click' , '.checkboxes input[name^="categories"]' , function(){
                manager.handlers.inputExtraDataForCategory($(this)); 
            }); 
            
            
            manager.helpers.getBody().on('click' , '#add-extra-data' , function(){
                manager.handlers.populateExtraDataForCategory($(this)); 
            });
            
            
            manager.helpers.getBody().on('click' , '#submit' , function(){
                manager.handlers.sendForm(); 
            });
            
        }
    }, 
    handlers : {
        init : function(){
            this.pricePerSurface(); 
            this.attachmentElements(); 
            this.sectionForDefaultCity();
            this.hiddenFieldText();
            this.populateSector();
            
        } ,
        populateSector : function(){
            
            var section_name = $('#section').val() 
            
            if($('#section_selectable').is(":visible") && section_name != '')
            {
                var selected_value = $('#section_selectable option:contains("' + section_name + '")').val(); 
                  
                $('#section_selectable').val(selected_value); 
            }
        },
        sendForm : function(){
            
            if(manager.helpers.getCity().val() == manager.defaultCity)
            {
                $('#section').val($('#section_selectable option:selected').text()); 
            }
          
            
        },
        
        inputExtraDataForCategory : function(box){
         
            var additional = $('#additional_' + box.val()); 
        
            if(additional.length > 0)
            {
                if(box.is(':checked'))
                {
                    $('#dialog').html('<input type="text" id="data" data="' + box.val() +'"/><br />' +
                        '<input value="Podaj wartość" type="submit" id="add-extra-data">').dialog(); 
                }
                else
                {
                    additional.val(''); 
                    this.hiddenFieldText(); 
                }
            }
        },
        populateExtraDataForCategory : function(){
               
            var inputData = $('#data').val(); 
            var dataElement = $('#additional_' + $('#data').attr('data')); 
            var checkElement = $('#categories-' + $('#data').attr('data'));
                
            if(inputData.length > 0)
            {
                dataElement.val(inputData); 
            }
            else checkElement.attr('checked' , false); 
                
            $('#dialog').dialog('close');
            manager.handlers.hiddenFieldText(); 
        }, 
        
        offerType : function() {
            $.ajax( {
                url : '/admin/offer/change',
                type : 'POST',
                data : manager.helpers.getForm().serialize(),				
                dataType : 'json',
                success : function( response ) {
                    if(response.status)
                    {
                        manager.helpers.getForm().replaceWith(response.content);
                        manager.init(); 
                    }
                    else {
                        $('#dialog').html(response.message).dialog(); 
                        return false; 
                    }
                }
            }); 
        },
        attachmentElements : function(){
            $('.attachment').each(function(){
                $(this).appendTo($('#' + $(this).attr('data')).parent()); 
            }); 
        },
    
        pricePerSurface: function(){
          
            var price = ($('#price').val()).replace(/\s+/g, '');
            price = parseFloat(price.replace(',' , '.'));
            var surface = ($('#surface').val()).replace(/\s+/g, '');
            surface = parseFloat(surface.replace(',' , '.'));
            if(price == '' || price == '0.00' || surface == '' || surface == '0.00')
                return false; 
            
           
          var price_per_surface  = manager.helpers.round(parseFloat(price / surface), 2); 
            
            if(!isNaN(price_per_surface))
            {
            $('#price_surface').val(price_per_surface);
            }
            else  $('#price_surface').val('');
            
        
            return true; 
        }, 
        sectionForDefaultCity : function(){
         
            if(manager.helpers.getCity().val() == manager.defaultCity)
            {
                $('#section').hide();  
                $('#section_selectable').show(); 
                $('#add_section').show(); 
            }
            else 
            {
                $('#section').show(); 
                $('#section_selectable').hide();      
                $('#add_section').hide(); 
            }
        },
        addNewSection : function()
        {
            $.ajax( {
                url : '/admin/offer/addsection',
                type : 'POST',
                dataType : 'json',
                data : $('#sectionForm').serialize(),
                success : function( response ) {
                    if(response.status)
                    {
                        $('#dialog').dialog('close');
                        $('#section_selectable').prepend('<option value="' + response.content.id + '">' + response.content.value + '</option>'); 
                        $('#section_selectable').val(response.content.id); 
                    }
                    else  $('#dialog').html(response.content); 
                }
            }); 
        },
        addSectionToDefaultCity : function()
        {
            $.ajax( {
                url : '/admin/offer/addsection',
                type : 'GET',
                dataType : 'json',
                success : function( response ) {
                    if(response.status)
                    {
                        $('#dialog').html(response.content).dialog({
                            autoOpen: true,
                            height: 300,
                            width: 700,
                            modal: true
                        });
                        
                    }
                    else alert(response.message); 
                }
            }); 
        },
        titleLength: function(){
        
            if(($('#titlel').length) == 0)
            {
                $('#title').parent().append('<span style="margin-left:10px" id="titlel"></span>');  
            }
            var input_chars = ($('#title').val()).length; 
        
            $('#titlel').html(input_chars);
        }, 
        roundPrice : function(){
        
            if(price == '' || price == '0.00')
                return false;
            var roundedPrice = Math.ceil($('#price_surface').val()); 
            ; 
            $('#price_surface').val(roundedPrice + '.00'); 
            return true; 
        }, 
        hiddenFieldText : function() {
            
            
            $('.add-extra-data').each(function(){
                var comment = 'category-comment-' + $(this).attr('data');
                if(($(this).val()).length > 0)
                {
                         
                    if( $('#' + comment).length > 0)
                    {
                        $('#' + comment).val($(this).val()); 
                    }
                    else ($('#categories-' + $(this).attr('data')).parent().append('<span id="' + comment + '" class="comment">' + $(this).val() + '</span>'));
                }
                else
                {
                    $('#' + comment).remove(); 
                    $('#categories-' + $(this).attr('data')).attr('checked', false); 
                }
            }); 
        }
    }, 
    helpers : {
        getMainContainer : function() {
            return $('#main');
        },
        getOfferType : function() {
            return $('#type');
        },
        getCity : function() {
            return $('#city');   
        },
        getForm :  function() {
            return $('#offer');
        },
        getBody : function(){
            return $('body'); 
        },
        getPricePerSurfaceField : function() {
            return $('#price_surface'); 
        },
        round : function(number,x) {
            var x = (!x ? 2 : x);
            return Math.round(number*Math.pow(10,x)) / Math.pow(10,x);
        }
    }
};

$(document).ready(function(){
    
    manager.init(); 
    
}); 