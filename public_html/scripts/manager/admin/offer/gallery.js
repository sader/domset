var manager = {
    
    zone : null,
    imgClass : 'con',
    
    init : function() {
        
        this.widgets.init(); 
        this.events.init();
        this.handlers.init(); 
    }, 
     
    widgets : {
        init : function() {
            manager.widgets.droppableAreas(); 
            manager.widgets.dropZone();
        },
        droppableAreas : function () {
            $('#gallery').droppable({
                greedy : true,
                drop: function(event, ui) {
                    manager.handlers.dropPictureOnGallery(event, ui)
                }
            });
         
            $('#trash').droppable({
                greedy : true,
                drop:function(event, ui) {
                    manager.handlers.dropPictureOnTrash(event, ui)
                }
            });
                
                
            $('#hidden').droppable({
                greedy : true,
                drop: function(event, ui) {
                    manager.handlers.dropPictureOnHidden(event, ui)
                }
            });
        }, 
        dropZone : function(){
        
            manager.zone =  new FileDrop('galleryDropZone' , {
                iframe : {
                    url : inputData.image.uploadUrl
                }
            }); 
        }    
    },
    events : {
        init : function() {
            manager.events.disableDefaultDragDropForBrownser(); 
            manager.events.registerDropZoneEvents(); 
            manager.events.submitChanges(); 
            
        },
        disableDefaultDragDropForBrownser : function() {
            jQuery('div#main').bind('dragover drop', function(event){
                event.preventDefault();
                return false;
            })
        },
        registerDropZoneEvents : function(){
            manager.zone.on.send.push(manager.handlers.zoneOnSend); 
             
        },
        submitChanges: function() {
            $('#main').on('click' , '#submit' , function(){
                manager.handlers.saveGallery()
            }); 
        }
        
    },
    handlers : {
        init : function(){
            
            this.prepareGallery(); 
            
        },
        prepareGallery : function()
        {
            var galleryInput = $('input#visible').val(); 
            var hiddenInput = $('input#hidden').val(); 
          
            if ( galleryInput != '')
            {
                $.each(galleryInput.split('|'),  function() {
                    var src = inputData.image.path + "/" + this + '.' + inputData.image.extension; 
                    var pic = manager.helpers.createPicture(this , src); 
                    manager.handlers.addPicture(pic, '#gallery'); 
                });
                
                manager.handlers.reloadSort('#gallery'); 
            }
            
            if ( hiddenInput != '')
            {
                $.each(hiddenInput.split('|'),  function() {
                    var src = inputData.image.path + "/" + this + '.' + inputData.image.extension; 
                    var pic = manager.helpers.createPicture(this , src); 
                    manager.handlers.addPicture(pic, '#hidden'); 
                });
                
                manager.handlers.reloadSort('#hidden'); 
            } 
            
            
        },
        saveGallery : function()
        {
            var gallery = [];
            var hidden = []; 
            $.each($('#gallery li'),  function() {
                gallery[gallery.length] = $(this).attr('id');  
            });

            $('input#visible').val(gallery.join('|')); 
            
            $.each($('#hidden li') , function() {
                hidden[hidden.length] = $(this).attr('id'); 
            });
            $('input#hidden').val(hidden.join('|'));
        },

        progressDone : function() {
            $('#progress').fadeOut('slow').html('').fadeIn(); 
        },
            
        zoneOnSend : function (files) {
              
            for (var i = 0; i < files.length; i++) {
                var file = files[i]; 
                if (!manager.helpers.isFileAcceptable(file)) {
                    return; 
                }
                file.on.progress.push(manager.handlers.zoneOnProgress); 
                file.on.done.push(manager.handlers.zoneOnDone);
                file.SendTo(inputData.image.uploadUrl);
            }
        }, 
        zoneOnDone : function (xhr) {
                
            manager.handlers.progressDone(); 
				
            var response = JSON.parse(xhr.response); 
            if(response.status)
            {
                var picture = manager.helpers.createPicture(response.content.hash, response.content.src); 
                manager.handlers.addPicture(picture, '#gallery'); 
                manager.handlers.reloadSort('#gallery'); 
            }
            else manager.helpers.error(response.content.message); 
            
        }, 
        zoneOnProgress : function(current, total, xhr, e) {
            
            console.log(manager.helpers); 
            var precentage = manager.helpers.getPrecentage(current, total); 
            $('#progress').html('Czekaj : ' + precentage + '%');
            
        },
        addPicture : function(picture, target) {
            picture.hide().addClass(target.substr(1, target.length)).appendTo(target).fadeIn('slow');
          
        },
        reloadSort : function(target) {
            $(target).sortable({
                connectWith: '.' + manager.imgClass
            }).disableSelection()
        },
        dropPictureOnGallery : function( event, ui ) {
        
            if(!ui.draggable.hasClass('gallery'))
            {
                ui.draggable.fadeOut(function() {
                    ui.draggable
                    .removeClass('hidden')
                    .removeClass('trash')
                    .addClass('gallery')
                    .appendTo( $('#gallery') )
                    .fadeIn();
                });
            
                manager.handlers.reloadSort('#gallery'); 
         
            }
        },
        dropPictureOnTrash : function( event, ui ) {
           
            if(!ui.draggable.hasClass('trash'))
            {
                ui.draggable.fadeOut(function() {
                    ui.draggable
                    .removeClass('hidden')
                    .removeClass('gallery')
                    .addClass('trash')
                    .appendTo( $('#trash') )
                    .fadeIn();
                                
                    manager.handlers.reloadSort('#trash');   
                });
            }
        },
        dropPictureOnHidden : function( event, ui ) {
        
            if(!ui.draggable.hasClass('hidden'))
            {
                ui.draggable.fadeOut(function() {
                    ui.draggable
                    .removeClass('gallery')
                    .removeClass('trash')
                    .addClass('hidden')
                    .appendTo( $('#hidden') )
                    .fadeIn();
                                
                    manager.handlers.reloadSort('#hidden'); 
                });
            }
        }
    },
    helpers : {
        isFileAcceptable : function(file) {
            return file.nativeFile.type.match(/^image\//);
        }, 
        getPrecentage : function(current, total) {
            return Math.round(current / total) * 100;
        },
        createPicture  : function(id , src) {
            return $('<li class="'+ manager.imgClass  + '" id="' + id + '">' + 
                '<img width="' + inputData.thumb.width  + '" height="' + inputData.thumb.height + '" src="' + src + '" />' +
                '</li>'); 
        },
        error : function(message) {
            
            $('#dialog').html(message).dialog(); 
            
        }
    }
};
 
         
$(document).ready(function(){
    manager.init(); 
});  