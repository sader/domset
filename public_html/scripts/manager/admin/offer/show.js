var manager = {
    init: function() {
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();
        if (manager.events.init !== 'undefined')
            manager.events.init();
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();
    },
    widgets: {
        init: function() {
            $('#gallery').galleryView();
        }
    },
    events: {
        init: function() {

            $('#main').on('click', 'a.visible', function() {
                manager.handlers.turnHidden(this);
                return false;
            });


            $('#main').on('click', 'a.hidden', function() {
                manager.handlers.turnVisible(this);
                return false;
            });

            $('#submit').click(function() {
                var fields = [];
                $.each($('p.hidden'), function() {
                    fields[fields.length] = $(this).find('a').attr('data');
                });

                $('input#fields').val(fields.join(','));

            });




        }
    },
    handlers: {
        init: function() {
        },
        turnHidden: function(elem) {

            $(elem).find('img').attr('src', '/images/visible-off.png');
            $(elem).removeClass('visible').addClass('hidden');
            $(elem).parent().parent().removeClass('visible').addClass('hidden');
        },
        turnVisible: function(elem) {

            $(elem).find('img').attr('src', '/images/visible-on.png');
            $(elem).removeClass('hidden').addClass('visible');
            $(elem).parent().parent().removeClass('hidden').addClass('visible');
        }




    },
    helpers: {
    }
};

$(document).ready(function() {

    manager.init();

});
