
var manager = {
    dropzone: null,
    init: function() {
        this.widgets.init();
        this.events.init();
        this.handlers.init();

    },
    widgets: {
        init: function() {
            this.inputMasks();
            this.calendars();
            this.editors();
            this.dropZone();
            $('#owner').select2(); 
        },
        inputMasks: function() {
            $.mask.definitions['h'] = "[A-Za-z0-9]";
            $.mask.definitions['l'] = "[A-Z]";

            $('#land_and_mortgage_register').mask("hhhh/99999999/9");
            $('#postcode').mask('99-999');
        },
        calendars: function() {
            $('#agreement_date').datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $('#agreement_end').datepicker({
                dateFormat: 'yy-mm-dd'
            });
        },
        clientMasks: function() {

            $('#pesel').mask('99999999999');
            $('#docid').mask('lll999999');
            $('#postcode').mask('99-999');
        },
        editors: function() {
            CKEDITOR.replace('information');
        },
        dropZone: function() {

            manager.zone = new FileDrop('galleryDropZone', {
                iframe: {
                    url: inputData.uploadUrl
                }
            });
        }

    },
    events: {
        init: function() {

            manager.events.disableDefaultDragDropForBrownser();
            manager.events.registerDropZoneEvents();

            $('#main').on('click', '.delete', function() {
                $(this).parent().remove();
                return false;
            });

            $('#add_owner').click(function() {
                manager.handlers.displayAddOwnerForm();
            });
            $('#main').on('click', '#add_owner_submit', function() {
                manager.handlers.addNewOwner();
            });
            $('#main').on('click', '.remove_owner', function() {
                manager.handlers.removeOwner(this);
            });
            $('#commission_precentage').keyup(function() {
                manager.handlers.commisionPrecentageChange();
            });
            $('#submit').click(function() {
                manager.handlers.submitForm();
            });
            $('#pick_owner').click(function() {
                manager.handlers.ownerPicked($('#owner option:selected').val(), $('#owner option:selected').text());
            });

            $('body').on('click', '#clientSubmit', function() {
                manager.handlers.addNewOwner();
                return false;
            });

            $('body').on('click', '.removeOwner', function() {
                manager.handlers.removeOwner(this);
            });

        },
        disableDefaultDragDropForBrownser: function() {
            jQuery('div#main').bind('dragover drop', function(event) {
                event.preventDefault();
                return false;
            });
        },
        registerDropZoneEvents: function() {
            manager.zone.on.send.push(manager.handlers.zoneOnSend);

        }
    },
    handlers: {
        init: function() {
            this.attachmentElements();
            this.populateOwners();
            this.populateFiles();
        },
        saveAgreement: function()
        {
            var agreementfiles = [];

            $.each($('.afilename'), function() {
                agreementfiles[agreementfiles.length] = $(this).text();
            });
            $('input#files').val(agreementfiles.join(','));
        },
        removeOwner: function(elem) {
            $(elem).parent().remove();
        },
        submitForm: function() {

            var owners = [];
            $.each($('#cowner li'), function() {
                owners[owners.length] = $(this).attr('data');
            });
            $('input#owners').val(owners.join(','));
            
            this.saveAgreement();

        },
        populateOwners: function() {

            var owners = $('#owners').val();
            if (owners != '')
            {
                var elem = owners.split(',');
                $(elem).each(function() {
                    var select_elem = $('#owner option[value=' + this + ']');
                    manager.handlers.ownerPicked(select_elem.val(), select_elem.text());
                });
            }
        },
        populateFiles: function() {

            var files = $('#files').val();

            if (files != '')
            {
                var elem = files.split(',');
                $(elem).each(function() {
                    manager.helpers.addFile(this);
                });

            }

        },
        ownerPicked: function(id, name) {

            if ($('.powner[data=' + id + ']').length > 0)
                return false;
            if ($('#cowner').length == 0)
            {
                $('#owner').parent().append('<ul id="cowner" />');
            }
            $('#cowner').append('<li class="powner" data="' + id + '">'
                    + '<a href="#" class="removeOwner">'
                    + '<img src="/images/remove.png" />'
                    + '</a><span class="owner-text">'
                    + name
                    +
                    '</span></li>');
        },
     
        attachmentElements: function() {
            $('.attachment').each(function() {
                $(this).appendTo($('#' + $(this).attr('data')).parent());
            });
        },
        addNewOwner: function() {

            $.ajax({
                url: '/admin/client/add',
                type: 'POST',
                dataType: 'json',
                data: $('#clientForm').serialize(),
                success: function(response) {
                    if (response.status)
                    {
                        $('#dialog').dialog('close');
                        $('#owner').prepend('<option value="' + response.content.id + '">' + response.content.value + '</option>');
                        $('#owner').val(response.content.id);
                        $('.select2-choice span').text(response.content.value);
                    }
                    else
                        $('#dialog').html(response.content);
                }
            });
        },
        displayAddOwnerForm: function() {

            $.ajax({
                url: '/admin/client/add/prepare/3',
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    if (response.status)
                    {
                        $('#dialog').attr('title', 'Dodaj nowego klienta').html(response.content).dialog({
                            heigth: 1200,
                            width: 700,
                            position: 'top'
                        });
                    }
                    else
                        alert(response.message);
                }
            });

        },
        commisionPrecentageChange: function() {
            var percentage = parseFloat(($('#commission_precentage').val()).replace(',' , '.'));
            var commision = (percentage / 100) * inputData.price;
            commision = manager.helpers.round(commision,2)
            
            if(!isNaN(commision))
            {
            $('#commission_value').val(commision);
            }
            else  $('#commission_value').val('');
        },
        progressDone: function() {
            $('#progress').fadeOut('slow').html('').fadeIn();
        },
        zoneOnSend: function(files) {

            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                file.on.progress.push(manager.handlers.zoneOnProgress);
                file.on.done.push(manager.handlers.zoneOnDone);
                file.SendTo(inputData.uploadUrl);
            }
        },
        zoneOnDone: function(xhr) {

            manager.handlers.progressDone();

            var response = JSON.parse(xhr.response);
            if (response.status)
            {
                manager.helpers.addFile(response.content.name);
            }
            else
                manager.helpers.error(response.content.message);

        },
        zoneOnProgress: function(current, total, xhr, e) {

            var precentage = manager.helpers.getPrecentage(current, total);
            $('#progress').html('Czekaj : ' + precentage + '%');

        }
    },
    helpers: {
        isFileAcceptable: function(file) {
            return file.nativeFile.type.match(/^image\//);
        },
        getPrecentage: function(current, total) {
            return Math.round(current / total) * 100;
        },
        createPicture: function(id, src) {
            return $('<li class="' + manager.imgClass + '" id="' + id + '">' +
                    '<img width="' + inputData.thumb.width + '" height="' + inputData.thumb.height + '" src="' + src + '" />' +
                    '</li>');
        },
        error: function(message) {

            $('#dialog').html(message).dialog();

        },
        addFile: function(name)
        {
            $('#filebox').append('<div class="file"><span class="afilename">' + name + '</span>' +
                    '<a href="#" class="delete">Usuń</a></div>');
        },
            round : function(number,x) {
            var x = (!x ? 2 : x);
            return Math.round(number*Math.pow(10,x)) / Math.pow(10,x);
        }
    }
};

$(document).ready(function() {
    manager.init();
}); 