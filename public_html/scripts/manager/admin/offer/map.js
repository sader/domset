var manager = {
    map : null,
    marker : null,
    defaultAddress : 'Polska Ełk',
    
    init : function(){
        manager.widgets.init(); 
        manager.events.init(); 
        manager.handlers.init(); 
    },
    widgets : {
       
        init : function(){},
       
        map : function(location) {
            var mapOptions = {
                center: location,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            manager.map = new google.maps.Map(document.getElementById("map"), mapOptions);
            manager.handlers.saveLocation(location.lat(), location.lng()); 
            manager.widgets.marker(location); 
        } , 
        marker : function (location){
            manager.marker = new google.maps.Marker({
                position: location,
                map: manager.map,
                draggable : true
            }); 
            manager.events.markerDrop(); 
        }  
    },
    events : {
        init : function() {},
        markerDrop : function(){
            google.maps.event.addListener(manager.marker, 'dragend', function (evt) {
                manager.handlers.markerDropped(evt); 
            }); 
        }
    },
    handlers : {
            
        init : function(){
            manager.handlers.location()
            },
            
        markerDropped : function(evt) {
            manager.marker.setAnimation(google.maps.Animation.BOUNCE);
            manager.handlers.saveLocation(evt.latLng.lat() , evt.latLng.lng()); 
        },
        saveLocation : function(lat , lng) {
            $('#lat').val(lat); 
            $('#lng').val(lng); 
        },
        location : function(){
        
            if(manager.handlers.setDeclaredLocation()) {
                return; 
            }
        
            if (manager.handlers.setLocationByAddress(inputData.address)){
                return; 
            }
            manager.handlers.setLocationByAddress(manager.defaultAddress); 
        },
        setDeclaredLocation : function() {
        
            var lat = $('#lat').val(); 
            var lng = $('#lng').val(); 
      
            if (lat != "" && lng != "")
            {
                manager.widgets.map(new google.maps.LatLng(lat, lng)); 
                return true; 
            }
            else return false; 
        },
        setLocationByAddress : function(address) {
        
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( {
                'address': address
            }, function(results, status) {
                
                if (status == google.maps.GeocoderStatus.OK) {
                    manager.widgets.map(results[0].geometry.location); 
                    return true; 
                } else {
                    return false; 
                }
            });
        },
        helpers : {}
    }
};

$(document).ready(function(){
    
    manager.init(); 
    
}); 