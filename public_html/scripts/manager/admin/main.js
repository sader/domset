var manage = {
    init: function() {
        if (manage.widgets.init !== 'undefined')
            manage.widgets.init();
        if (manage.events.init !== 'undefined')
            manage.events.init();
        if (manage.handlers.init !== 'undefined')
            manage.handlers.init();
    },
    widgets: {
        init: function() {
            
        //   $('#tablegrid, #daily, #full, #offers, #promotion').jScrollPane();
        }
    },
    events: {
        init: function() {
            
            $('.confirm').click(function() {
                return confirm('Czy potwierdzasz to zadanie ?'); 
            });  
        
            $('#detailbox').click(function(){
                
               document.location.href = $(this).attr('data-href');
            }); 
        
            $('body').on('click' , '.quickdetail', function(){                           
                 
                 
                manage.handlers.clientDetail(this); 
               
                return false; 
                 
                
            }); 
           
         
            $('body').on('click', '.clickable', function(event) {
              
                event.preventDefault();   
                manage.handlers.redirector(this);
                
            });
           
            
          
           
            $('#main_client').keyup(function() {
                manage.handlers.clientList(this);
                return false; 
            });
        }
    },
handlers: {
    init: function() {
    },
    redirector: function(link) {



        var link_go_to_close = $(link).parent().find('.goto');
        
          if (link_go_to_close.length > 0)
        {
            location.href = link_go_to_close.attr('href');
            return; 
        }
        

        var link_go_to = $(link).parent().parent().find('.goto');
        if (link_go_to.length > 0)
        {
            location.href = link_go_to.attr('href');
            return; 
        }
            

    },
    clientList: function(input) {

        var inputed = $(input).val();

        if (inputed.length <= 3)
        {
            if ($('.ui-dialog').length > 0)
            {
                $('#dialog').dialog('close');
            }
            return false;
        }

        $.ajax({
            url: '/admin/client/quicklist/type/list',
            type: 'POST',
            data: 'phone=' + inputed,
            dataType: 'json',
            success: function(response) {
                if (response.status)
                {
                    $('#dialog').html(response.content).dialog({
                        heigth: 1200,
                        width: 700,
                        position: {
                            my: "left top", 
                            at: "right bottom", 
                            of: $(input)
                            },
                        title: null
                    });
                        
                    $(input).focus();
                }
                else
                    alert(response.message);
            }
        });

           
    },
    clientDetail: function(input) {

   
        $.ajax({
            url: '/admin/client/quickdetail',
            type: 'POST',
            data: 'id=' + $(input).attr('data'),
            dataType: 'json',
            success: function(response) {
                if (response.status)
                {
                    $('#dialog').html(response.content).dialog({
                        heigth: 1200,
                        width: 700,
                        position: {
                            my: "left top", 
                            at: "right bottom", 
                            of: $(input), 
                            collision : "flipfit"
                        }
                    });
                //                        $('#main_client').focus();
                }
                else
                    alert(response.message);
            }
        });

            
    }
        
        
},
helpers: {
}
};

$(document).ready(function() {

    manage.init();

});