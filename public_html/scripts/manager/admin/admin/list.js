var manager = {
    init : function(){
        if (manager.widgets.init !== 'undefined')
            manager.widgets.init();  
        if (manager.events.init !== 'undefined')
            manager.events.init();  
        if (manager.handlers.init !== 'undefined')
            manager.handlers.init();  
    }, 
    widgets : {
        init : function(){}
    }, 
    events : {
        init : function(){
            $('#main').on('click' , '.remove' , function(){manager.handlers.removePrompt()}); 
            
            
        }   
    }, 
    handlers : {
        init : function(){},
        removePrompt : function() {return confirm('Czy chcesz usunąć tego administratora ?')}
        
        
    }, 
    helpers : {
}
};


$(document).ready(function(){
    
    manager.init(); 
    
}); 
